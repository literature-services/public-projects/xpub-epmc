/* eslint-disable */
const _react = require('react')

const _propTypes = require('prop-types')

const _dompurify = require('dompurify')

function $cbdec73d1b09b7fec0179e56b2763bde$var$_typeof(obj) {
  '@babel/helpers - typeof'

  if (typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol') {
    $cbdec73d1b09b7fec0179e56b2763bde$var$_typeof = function _typeof(obj) {
      return typeof obj
    }
  } else {
    $cbdec73d1b09b7fec0179e56b2763bde$var$_typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === 'function' &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? 'symbol'
        : typeof obj
    }
  }

  return $cbdec73d1b09b7fec0179e56b2763bde$var$_typeof(obj)
}

Object.defineProperty(exports, '__esModule', {
  value: true,
})
let $cbdec73d1b09b7fec0179e56b2763bde$export$default = void 0
exports.default = $cbdec73d1b09b7fec0179e56b2763bde$export$default
const $cbdec73d1b09b7fec0179e56b2763bde$var$_react = $cbdec73d1b09b7fec0179e56b2763bde$var$_interopRequireWildcard(
  _react,
)
const $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes = $cbdec73d1b09b7fec0179e56b2763bde$var$_interopRequireDefault(
  _propTypes,
)
const $cbdec73d1b09b7fec0179e56b2763bde$var$_dompurify = $cbdec73d1b09b7fec0179e56b2763bde$var$_interopRequireDefault(
  _dompurify,
)

function $cbdec73d1b09b7fec0179e56b2763bde$var$_interopRequireDefault(obj) {
  return obj && obj.__esModule
    ? obj
    : {
        default: obj,
      }
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_getRequireWildcardCache() {
  if (typeof WeakMap !== 'function') return null
  const cache = new WeakMap()

  $cbdec73d1b09b7fec0179e56b2763bde$var$_getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache
  }

  return cache
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj
  }

  if (
    obj === null ||
    ($cbdec73d1b09b7fec0179e56b2763bde$var$_typeof(obj) !== 'object' &&
      typeof obj !== 'function')
  ) {
    return {
      default: obj,
    }
  }

  const cache = $cbdec73d1b09b7fec0179e56b2763bde$var$_getRequireWildcardCache()

  if (cache && cache.has(obj)) {
    return cache.get(obj)
  }

  const newObj = {}
  const hasPropertyDescriptor =
    Object.defineProperty && Object.getOwnPropertyDescriptor

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const desc = hasPropertyDescriptor
        ? Object.getOwnPropertyDescriptor(obj, key)
        : null

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc)
      } else {
        newObj[key] = obj[key]
      }
    }
  }

  newObj.default = obj

  if (cache) {
    cache.set(obj, newObj)
  }

  return newObj
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$ownKeys(object, enumerableOnly) {
  const keys = Object.keys(object)

  if (Object.getOwnPropertySymbols) {
    let symbols = Object.getOwnPropertySymbols(object)
    if (enumerableOnly)
      symbols = symbols.filter(
        sym => Object.getOwnPropertyDescriptor(object, sym).enumerable,
      )
    keys.push(...symbols)
  }

  return keys
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_objectSpread(target) {
  for (let i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {}

    if (i % 2) {
      $cbdec73d1b09b7fec0179e56b2763bde$var$ownKeys(
        Object(source),
        true,
      ).forEach(key => {
        $cbdec73d1b09b7fec0179e56b2763bde$var$_defineProperty(
          target,
          key,
          source[key],
        )
      })
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source))
    } else {
      $cbdec73d1b09b7fec0179e56b2763bde$var$ownKeys(Object(source)).forEach(
        key => {
          Object.defineProperty(
            target,
            key,
            Object.getOwnPropertyDescriptor(source, key),
          )
        },
      )
    }
  }

  return target
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_defineProperty(
  obj,
  key,
  value,
) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value,
      enumerable: true,
      configurable: true,
      writable: true,
    })
  } else {
    obj[key] = value
  }

  return obj
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_slicedToArray(arr, i) {
  return (
    $cbdec73d1b09b7fec0179e56b2763bde$var$_arrayWithHoles(arr) ||
    $cbdec73d1b09b7fec0179e56b2763bde$var$_iterableToArrayLimit(arr, i) ||
    $cbdec73d1b09b7fec0179e56b2763bde$var$_nonIterableRest()
  )
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_nonIterableRest() {
  throw new TypeError('Invalid attempt to destructure non-iterable instance')
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_iterableToArrayLimit(arr, i) {
  if (
    !(
      Symbol.iterator in Object(arr) ||
      Object.prototype.toString.call(arr) === '[object Arguments]'
    )
  ) {
    return
  }

  const _arr = []
  let _n = true
  let _d = false
  let _e

  try {
    for (
      var _i = arr[Symbol.iterator](), _s;
      !(_n = (_s = _i.next()).done);
      _n = true
    ) {
      _arr.push(_s.value)

      if (i && _arr.length === i) break
    }
  } catch (err) {
    _d = true
    _e = err
  } finally {
    try {
      if (!_n && _i.return != null) _i.return()
    } finally {
      if (_d) throw _e
    }
  }

  return _arr
}

function $cbdec73d1b09b7fec0179e56b2763bde$var$_arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr
}

const $cbdec73d1b09b7fec0179e56b2763bde$var$baseConfig = {
  showMathMenu: true,
  tex2jax: {
    inlineMath: [
      ['$', '$'],
      ['\\(', '\\)'],
    ],
  },
  skipStartupTypeset: true,
}
const $cbdec73d1b09b7fec0179e56b2763bde$var$MathJaxPreview = $cbdec73d1b09b7fec0179e56b2763bde$var$_react.default.forwardRef(
  (_ref, ref) => {
    let script = _ref.script,
      config = _ref.config,
      className = _ref.className,
      math = _ref.math,
      id = _ref.id,
      style = _ref.style,
      msDelayDisplay = _ref.msDelayDisplay,
      onDisplay = _ref.onDisplay,
      onLoad = _ref.onLoad,
      onError = _ref.onError
    const sanitizedMath = $cbdec73d1b09b7fec0179e56b2763bde$var$_dompurify.default.sanitize(
      math,
      {
        USE_PROFILES: { mathMl: true },
        ADD_ATTR: ['columnalign'],
      },
    )
    const previewRef = (0,
    $cbdec73d1b09b7fec0179e56b2763bde$var$_react.useRef)()

    let _useState = (0, $cbdec73d1b09b7fec0179e56b2763bde$var$_react.useState)(
        'none',
      ),
      _useState2 = $cbdec73d1b09b7fec0179e56b2763bde$var$_slicedToArray(
        _useState,
        2,
      ),
      display = _useState2[0],
      setDisplay = _useState2[1] // prevent display during processing

    let _useState3 = (0, $cbdec73d1b09b7fec0179e56b2763bde$var$_react.useState)(
        window.MathJax ? 'loaded' : 'loading',
      ),
      _useState4 = $cbdec73d1b09b7fec0179e56b2763bde$var$_slicedToArray(
        _useState3,
        2,
      ),
      loadingState = _useState4[0],
      setLoadingState = _useState4[1]

    ;(0, $cbdec73d1b09b7fec0179e56b2763bde$var$_react.useEffect)(() => {
      let mathjaxScriptTag = document.querySelector(
        'script[src="'.concat(script, '"]'),
      )

      if (!mathjaxScriptTag) {
        mathjaxScriptTag = document.createElement('script')
        mathjaxScriptTag.async = true
        mathjaxScriptTag.src = script

        for (
          let _i2 = 0, _Object$entries = Object.entries(config || {});
          _i2 < _Object$entries.length;
          _i2++
        ) {
          let _Object$entries$_i = $cbdec73d1b09b7fec0179e56b2763bde$var$_slicedToArray(
              _Object$entries[_i2],
              2,
            ),
            k = _Object$entries$_i[0],
            v = _Object$entries$_i[1]

          mathjaxScriptTag.setAttribute(k, v)
        }

        const node = document.head || document.getElementsByTagName('head')[0]
        node.appendChild(mathjaxScriptTag)
      }

      const onloadHandler = function onloadHandler() {
        setLoadingState('loaded')
        window.MathJax.Hub.Config(
          $cbdec73d1b09b7fec0179e56b2763bde$var$_objectSpread(
            $cbdec73d1b09b7fec0179e56b2763bde$var$_objectSpread(
              {},
              $cbdec73d1b09b7fec0179e56b2763bde$var$baseConfig,
            ),
            config,
          ),
        )
        onLoad && onLoad()
      }

      const onerrorHandler = function onerrorHandler() {
        setLoadingState('failed')
        onError && onError()
      }

      mathjaxScriptTag.addEventListener('load', onloadHandler)
      mathjaxScriptTag.addEventListener('error', onerrorHandler)
      return function() {
        mathjaxScriptTag.removeEventListener('load', onloadHandler)
        mathjaxScriptTag.removeEventListener('error', onloadHandler)
      }
    }, [
      setLoadingState,
      config,
      $cbdec73d1b09b7fec0179e56b2763bde$var$baseConfig,
    ])
    ;(0, $cbdec73d1b09b7fec0179e56b2763bde$var$_react.useEffect)(() => {
      if (loadingState !== 'loaded') {
        return
      }

      previewRef.current.innerHTML = sanitizedMath
      window.MathJax.Hub.Queue([
        'Typeset',
        window.MathJax.Hub,
        previewRef.current,
      ]) // delay display of div

      let msDelay

      if (
        // msDelayDisplay prop is a reasonable number of ms
        msDelayDisplay !== null &&
        !isNaN(+msDelayDisplay) &&
        +msDelayDisplay >= 0 &&
        +msDelayDisplay < 10000
      ) {
        msDelay = +msDelayDisplay
      } else {
        msDelay = 300 // default 300ms delay
      }

      const timeout = setTimeout(() => {
        setDisplay('inline') // display div after delay, hopefully typeset has finished

        onDisplay && onDisplay()
      }, msDelay)
      return function() {
        setDisplay('none')
        clearTimeout(timeout)
      }
    }, [sanitizedMath, loadingState, previewRef])
    return $cbdec73d1b09b7fec0179e56b2763bde$var$_react.default.createElement(
      'span',
      {
        className,
        id,
        style: $cbdec73d1b09b7fec0179e56b2763bde$var$_objectSpread(
          {
            display,
          },
          style,
        ),
        ref,
      },
      loadingState === 'failed' &&
        $cbdec73d1b09b7fec0179e56b2763bde$var$_react.default.createElement(
          'span',
          null,
          'fail loading mathjax lib',
        ),
      $cbdec73d1b09b7fec0179e56b2763bde$var$_react.default.createElement(
        'span',
        {
          className: 'react-mathjax-preview-result',
          ref: previewRef,
        },
      ),
    )
  },
)
$cbdec73d1b09b7fec0179e56b2763bde$var$MathJaxPreview.displayName =
  'MathJaxPreview'
$cbdec73d1b09b7fec0179e56b2763bde$var$MathJaxPreview.propTypes = {
  script: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.string,
  config: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.object,
  className: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.string,
  math: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.string,
  style: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.object,
  id: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.string,
  onLoad: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.func,
  onError: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.func,
  onDisplay: $cbdec73d1b09b7fec0179e56b2763bde$var$_propTypes.default.func,
}
$cbdec73d1b09b7fec0179e56b2763bde$var$MathJaxPreview.defaultProps = {
  script:
    'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.6/MathJax.js?config=TeX-MML-AM_HTMLorMML',
  id: 'react-mathjax-preview',
}
const $cbdec73d1b09b7fec0179e56b2763bde$var$_default = $cbdec73d1b09b7fec0179e56b2763bde$var$MathJaxPreview
$cbdec73d1b09b7fec0179e56b2763bde$export$default = $cbdec73d1b09b7fec0179e56b2763bde$var$_default
exports.default = $cbdec73d1b09b7fec0179e56b2763bde$export$default
