import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link, H3, Icon } from '@pubsweet/ui'
import { createBrowserHistory } from 'history'
import { Loading, LoadingIcon, Table, Notification } from '../ui'
import { COUNT_MANUSCRIPTS, ALERT_DAYS } from './operations'
import DashboardBase from './DashboardBase'

const ListTable = styled(Table)`
  width: 100%;
  margin-bottom: calc(${th('gridUnit')} * 3);
  tr > *:first-child {
    width: calc(${th('gridUnit')} * 10);
    text-align: right;
  }
`
const Alert = styled.small`
  display: inline-flex;
  align-items: center;
  color: ${th('colorError')};
  margin-left: calc(${th('gridUnit')} * 5);
`
const HelpdeskQueue = {
  'needs submission QA': ['submitted'],
  'has file errors': ['file-error'],
  'has XML QA errors': ['xml-triage'],
  'XML errors corrected': ['xml-corrected'],
  'has reviewer XML errors': ['xml-error'],
  'needs citation': ['xml-complete'],
  'published ahead of print': ['published-ahead-of-print'],
  'failed EBI/NCBI loading': ['repo-triage'],
  'problem being withdrawn': ['withdrawal-triage'],
}

const SubmitterQueue = {
  'not yet submitted': ['INITIAL', 'READY'],
  'needs reviewer approval': ['in-review'],
  'submission error': ['submission-error'],
  'needs final review': ['xml-review'],
}

const TaggerQueue = { tagging: ['tagging'] }

const ExternalQueue = { 'needs XML QA': ['xml-qa'] }

const RepoQueue = {
  'approved for Europe PMC': ['repo-ready'],
  'sent to Europe PMC': ['repo-processing'],
}

const Completed = {
  'available in Europe PMC': ['published'],
  'grant linking only': ['link-existing'],
  'being withdrawn': ['being-withdrawn'],
  deleted: ['deleted'],
}

const AlertQuery = ({ states, settings }) => {
  const { days, weekdays } = settings
  return (
    <Query
      fetchPolicy="network-only"
      query={ALERT_DAYS}
      variables={{ states, ...settings }}
    >
      {({ data, loading }) => {
        if (loading) {
          return <LoadingIcon size={1.5} />
        }
        if (data.checkAge && data.checkAge.alert) {
          return (
            <Alert>
              <Icon color="currentColor" size={2}>
                alert-octagon
              </Icon>{' '}
              Submissions waiting more than {days} {weekdays ? 'week' : ''}day
              {days === 1 ? '' : 's'}
            </Alert>
          )
        }
        return null
      }}
    </Query>
  )
}

const QueueTable = ({ title, queue, data, alerts }) => {
  let total = 0
  const tableData = Object.keys(queue).map(label => {
    const items = data.filter(s => queue[label].includes(s.type))
    let count = 0
    const states = items.map(i => {
      count += i.count
      total += i.count
      return i.type
    })
    return { label, status: queue[label], count, states }
  })
  return (
    <React.Fragment>
      <H3 style={{ marginTop: 0 }}>{title}</H3>
      <ListTable>
        <tbody>
          <tr>
            <th>{total}</th>
            <th>All</th>
          </tr>
          {tableData.map(row => (
            <tr key={row.label}>
              <td>{row.count}</td>
              <td>
                <span style={{ display: 'inline-flex', alignItems: 'center' }}>
                  {row.count ? (
                    <Link to={`/search?status=${row.status}`}>{row.label}</Link>
                  ) : (
                    row.label
                  )}
                  {alerts && (
                    <AlertQuery settings={alerts} states={row.states} />
                  )}
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </ListTable>
    </React.Fragment>
  )
}

const QueueCheck = ({ data }) => {
  const myQueue = [
    'submitted',
    'file-error',
    'xml-triage',
    'xml-corrected',
    'xml-error',
    'repo-triage',
    'withdrawal-triage',
  ]
  const needCite = data.find(r => r.type === 'xml-complete').count
  const filtered = data.reduce(
    (sum, row) => (myQueue.includes(row.type) ? sum + row.count : sum),
    0,
  )
  if (filtered === 0) {
    return (
      <React.Fragment>
        <Notification type="info">
          Your queue is empty.
          {needCite > 0 && (
            <React.Fragment>
              {' '}
              <Link to="/search?status=xml-complete">
                {needCite} submission{needCite !== 1 && 's'}
              </Link>{' '}
              need{needCite === 1 && 's'} a citation
            </React.Fragment>
          )}
        </Notification>
        <br />
      </React.Fragment>
    )
  }
  return null
}

const Dashboard = ({ currentUser }) => (
  <Query fetchPolicy="cache-and-network" query={COUNT_MANUSCRIPTS}>
    {({ data, loading }) => {
      if (loading || !data) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      if (!data) {
        createBrowserHistory({ forceRefresh: true }).go()
        return null
      }
      return (
        <React.Fragment>
          <QueueCheck data={data.countByStatus} />
          <QueueTable
            data={data.countByStatus}
            queue={HelpdeskQueue}
            title="Helpdesk queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={SubmitterQueue}
            title="Submitter/Reviewer queue"
          />
          <QueueTable
            alerts={{ days: 3, weekdays: true }}
            data={data.countByStatus}
            queue={TaggerQueue}
            title="Tagger queue"
          />
          <QueueTable
            data={data.countByStatus}
            queue={ExternalQueue}
            title="External QA queue"
          />
          <QueueTable
            alerts={{ days: 1, weekdays: false, preprint: true }}
            data={data.countByStatus}
            queue={RepoQueue}
            title="Repository processing"
          />
          <QueueTable
            data={data.countByStatus}
            queue={Completed}
            title="Completed"
          />
        </React.Fragment>
      )
    }}
  </Query>
)

const AdminDashboard = DashboardBase(Dashboard)

const AdminDashboardPage = ({ currentUser }) => {
  if (currentUser.admin) {
    return <AdminDashboard currentUser={currentUser} pageTitle="Dashboard" />
  }
  createBrowserHistory().push('/')
  return null
}

export default AdminDashboardPage
