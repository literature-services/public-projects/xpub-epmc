import React from 'react'
import { Query } from 'react-apollo'
import URLSearchParams from 'url-search-params'
import { pageSize } from 'config'
import {
  MANUSCRIPTS_BY_STATUS,
  MANUSCRIPTS_BY_EMAIL_OR_LASTNAME,
} from './operations'
import { Loading, LoadingIcon, Pagination, Notification } from '../ui'
import NeedsCitation from './NeedsCitation'
import SearchBoxes, { QueryLabel, PaginationPane } from './SearchBoxes'
import DashboardList from './DashboardList'
import DashboardBase from './DashboardBase'
import RepoProcessing from './RepoProcessing'

const Dashboard = ({ currentUser, history }) => {
  const params = new URLSearchParams(history.location.search)
  const key = params.has('status') ? 'status' : 'search'
  const query = key === 'status' ? params.get('status') : params.get('search')
  const currentPage = params.get('page') ? parseInt(params.get('page'), 10) : 1
  const page = params.has('page') ? currentPage - 1 : 0

  const onPageEntered = p => {
    history.push(`/search?${key}=${query}&page=${p}`)
  }

  if (query === 'xml-complete') {
    return <NeedsCitation currentUser={currentUser} history={history} />
  }

  if (query === 'repo-processing') {
    return <RepoProcessing currentUser={currentUser} history={history} />
  }

  return (
    <Query
      fetchPolicy="cache-and-network"
      query={
        key === 'status'
          ? MANUSCRIPTS_BY_STATUS
          : MANUSCRIPTS_BY_EMAIL_OR_LASTNAME
      }
      variables={{ query, page, pageSize }}
    >
      {({ data, loading }) => {
        if (loading) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }

        const src =
          key === 'status' ? data.findByStatus : data.searchManuscripts
        const { total = 0, manuscripts } = src
        const PageNavigation = () => (
          <PaginationPane>
            <Pagination
              currentPage={currentPage}
              onPageEntered={onPageEntered}
              pageSize={pageSize}
              totalSize={total}
            />
          </PaginationPane>
        )
        return (
          <React.Fragment>
            <QueryLabel>
              {query} ({total})
            </QueryLabel>
            <SearchBoxes />
            <div style={{ clear: 'both' }} />
            {total > 0 && <PageNavigation />}
            {!total && (
              <Notification type="info">No results found</Notification>
            )}
            <DashboardList currentUser={currentUser} listData={manuscripts} />
            {total > 0 && <PageNavigation />}
          </React.Fragment>
        )
      }}
    </Query>
  )
}

const SearchResults = DashboardBase(Dashboard)

const SearchResultsPage = ({ currentUser, history }) => (
  <SearchResults
    currentUser={currentUser}
    hideSearchBoxes="true"
    history={history}
    pageTitle="Search results"
  />
)

export default SearchResultsPage
