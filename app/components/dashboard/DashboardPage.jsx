import React from 'react'
import { compose, graphql } from 'react-apollo'
import URLSearchParams from 'url-search-params'
import { USER_MANUSCRIPTS, ADD_REVIEWER } from './operations'
import DashboardBase from './DashboardBase'
import MyQueue from './MyQueue'
import MyManuscripts from './MyManuscripts'

const QueuePage = DashboardBase(MyQueue)
const ManuscriptsPage = DashboardBase(MyManuscripts)

class DashboardPage extends React.Component {
  state = { errors: [] }
  componentDidMount() {
    const { search, pathname } = this.props.location
    if (search) {
      const params = new URLSearchParams(search)
      const noteId = params.get('accept')
      if (noteId) {
        params.delete('accept')
        this.setReviewer(noteId, `${pathname}?${params.toString()}`)
      }
    }
  }
  componentDidUpdate() {
    if (this.state.errors.length > 0) {
      setTimeout(() => {
        this.setState({ errors: [] })
      }, 5000)
    }
  }
  async setReviewer(noteId, newPath) {
    const set = await this.props.addReviewer({
      variables: { noteId },
      refetchQueries: [{ query: USER_MANUSCRIPTS }],
      awaitRefetchQueries: true,
    })
    this.props.history.replace(newPath)
    const { success, message } = await set.data.addReviewer
    if (success && message) {
      this.props.history.push(message)
    } else {
      this.setState({
        errors: [
          {
            message,
            type: 'warning',
          },
        ],
      })
    }
  }
  render() {
    const { currentUser, history } = this.props
    if (currentUser.admin || currentUser.external || currentUser.tagger) {
      return (
        <QueuePage
          currentUser={currentUser}
          errors={this.state.errors}
          pageTitle="My queue"
          setErrors={errors => this.setState({ errors })}
        />
      )
    }
    return (
      <ManuscriptsPage
        currentUser={currentUser}
        errors={this.state.errors}
        history={history}
        pageTitle="My manuscripts"
      />
    )
  }
}

export default compose(graphql(ADD_REVIEWER, { name: 'addReviewer' }))(
  DashboardPage,
)
