import moment from 'moment'

export const hasEmbargo = manuscript =>
  manuscript &&
  manuscript.meta &&
  manuscript.meta.releaseDelay &&
  manuscript.meta.releaseDelay !== '0'

/**
 * Check whether manuscript is currently under embargo
 * true: is under embargo.
 * false: no embargo or embargo period has lapsed.
 */
export const isUnderEmbargo = manuscript => {
  if (!hasEmbargo(manuscript)) {
    return false
  }
  const { meta } = manuscript
  const releaseDelay = parseInt(meta.releaseDelay, 10)
  const pubDates = manuscript.meta.publicationDates
  const epubDate =
    pubDates &&
    pubDates.find(date => date.type === 'epub') &&
    pubDates.find(date => date.type === 'epub').date
  const ppubDate =
    pubDates &&
    pubDates.find(date => date.type === 'ppub') &&
    pubDates.find(date => date.type === 'ppub').date
  const startDate = (epubDate && epubDate) || (ppubDate && ppubDate)
  const publishDate = moment(startDate)
    .add(releaseDelay, 'M')
    .format('YYYY-MM-DD')
  return moment().isBefore(publishDate, 'day')
}

export default { hasEmbargo, isUnderEmbargo }
