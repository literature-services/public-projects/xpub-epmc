import React from 'react'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import URLSearchParams from 'url-search-params'
import { th } from '@pubsweet/ui-toolkit'
import { H3 as HT } from '@pubsweet/ui'
import { pageSize } from 'config'
import { Loading, LoadingIcon, Notification, Pagination } from '../ui'
import { CITATION_MANUSCRIPTS } from './operations'
import SearchBoxes, { QueryLabel, PaginationPane } from './SearchBoxes'
import DashboardList from './DashboardList'

const H3 = styled(HT)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
`

class NeedsCitation extends React.Component {
  state = { currentPage: 1 }
  componentDidMount() {
    const { search } = this.props.location
    if (search) {
      const params = new URLSearchParams(search)
      const page = params.get('page') ? parseInt(params.get('page'), 10) : 1
      this.setState({ currentPage: page })
    }
  }
  render() {
    const { currentUser, history } = this.props
    const { currentPage } = this.state
    const page = currentPage ? currentPage - 1 : 0
    const onPageEntered = p => {
      this.setState({ currentPage: p })
      history.replace(`/search?status=xml-complete&page=${p}`)
    }
    const variables = { page, pageSize }
    return (
      <Query
        fetchPolicy="network-only"
        query={CITATION_MANUSCRIPTS}
        variables={variables}
      >
        {({ data, loading }) => {
          if (loading) {
            return (
              <Loading>
                <LoadingIcon />
              </Loading>
            )
          }
          if (!data) {
            history.go()
            return null
          }
          const unmatched = []
          const manual = []
          const hasDOI = []
          const matched = []
          const { manuscripts, total } = data.citationManuscripts
          const PageNavigation = () => (
            <PaginationPane>
              <Pagination
                currentPage={currentPage}
                onPageEntered={onPageEntered}
                pageSize={pageSize}
                totalSize={total}
              />
            </PaginationPane>
          )
          manuscripts.forEach(m => {
            if (
              m.meta.articleIds &&
              m.meta.articleIds.some(aid => aid.pubIdType === 'pmid')
            ) {
              matched.push(m)
            } else if (
              m.meta.articleIds &&
              m.meta.articleIds.some(aid => aid.pubIdType === 'doi')
            ) {
              hasDOI.push(m)
            } else if (m.journal && m.journal.meta.pubmedStatus) {
              unmatched.push(m)
            } else {
              manual.push(m)
            }
          })
          return (
            <React.Fragment>
              <QueryLabel>xml-complete ({total})</QueryLabel>
              <SearchBoxes />
              <div style={{ clear: 'both' }} />
              {total > 0 && <PageNavigation />}
              {!total && (
                <Notification type="info">No results found</Notification>
              )}
              {unmatched.length > 0 && (
                <React.Fragment>
                  <H3>PMID matching needed ({unmatched[0].count})</H3>
                  <DashboardList
                    currentUser={currentUser}
                    listData={unmatched}
                  />
                </React.Fragment>
              )}
              {manual.length > 0 && (
                <React.Fragment>
                  <H3>DOI needed ({manual[0].count})</H3>
                  <DashboardList currentUser={currentUser} listData={manual} />
                </React.Fragment>
              )}
              {hasDOI.length > 0 && (
                <React.Fragment>
                  <H3>Matched to DOIs ({hasDOI[0].count})</H3>
                  <DashboardList currentUser={currentUser} listData={hasDOI} />
                </React.Fragment>
              )}
              {matched.length > 0 && (
                <React.Fragment>
                  <H3>Matched to PMIDs ({matched[0].count})</H3>
                  <DashboardList currentUser={currentUser} listData={matched} />
                </React.Fragment>
              )}
              {total > 0 && <PageNavigation />}
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

export default withRouter(NeedsCitation)
