import React from 'react'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import URLSearchParams from 'url-search-params'
import { th } from '@pubsweet/ui-toolkit'
import { H3 as HT } from '@pubsweet/ui'
import { pageSize } from 'config'
import { Loading, LoadingIcon, Notification, Pagination } from '../ui'
import { REPO_PROCESSING_MANUSCRIPTS } from './operations'
import SearchBoxes, { QueryLabel, PaginationPane } from './SearchBoxes'
import DashboardList from './DashboardList'
import { hasEmbargo } from './embargo'

const H3 = styled(HT)`
  font-size: ${th('fontSizeHeading4')};
  line-height: ${th('lineHeightHeading4')};
`
const isPreprint = manuscript =>
  !!(
    manuscript &&
    manuscript.organization &&
    manuscript.organization.name === 'Europe PMC Preprints'
  )
class RepoProcessing extends React.Component {
  state = { currentPage: 1 }
  componentDidMount() {
    const { search } = this.props.location
    if (search) {
      const params = new URLSearchParams(search)
      const page = params.get('page') ? parseInt(params.get('page'), 10) : 1
      this.setState({ currentPage: page })
    }
  }
  render() {
    const { currentUser, history } = this.props
    const { currentPage } = this.state
    const page = currentPage ? currentPage - 1 : 0
    const onPageEntered = p => {
      this.setState({ currentPage: p })
      history.replace(`/search?status=repo-processing&page=${p}`)
    }
    const variables = { page, pageSize }
    return (
      <Query
        fetchPolicy="network-only"
        query={REPO_PROCESSING_MANUSCRIPTS}
        variables={variables}
      >
        {({ data, loading }) => {
          if (loading) {
            return (
              <Loading>
                <LoadingIcon />
              </Loading>
            )
          }
          if (!data) {
            history.go()
            return null
          }
          const nonEmbargoedManuscripts = []
          const embargoedManuscripts = []
          const preprints = []
          const { manuscripts, total } = data.repoProcessingManuscripts
          const PageNavigation = () => (
            <PaginationPane>
              <Pagination
                currentPage={currentPage}
                onPageEntered={onPageEntered}
                pageSize={pageSize}
                totalSize={total}
              />
            </PaginationPane>
          )
          manuscripts.forEach(m => {
            if (isPreprint(m)) {
              preprints.push(m)
            } else if (hasEmbargo(m)) {
              embargoedManuscripts.push(m)
            } else {
              nonEmbargoedManuscripts.push(m)
            }
          })
          return (
            <React.Fragment>
              <QueryLabel>repo-processing ({total})</QueryLabel>
              <SearchBoxes />
              <div style={{ clear: 'both' }} />
              {total > 0 && <PageNavigation />}
              {!total && (
                <Notification type="info">No results found</Notification>
              )}
              {embargoedManuscripts.length > 0 && (
                <React.Fragment>
                  <H3>
                    Embargoed manuscripts ({embargoedManuscripts[0].count})
                  </H3>
                  <DashboardList
                    currentUser={currentUser}
                    listData={embargoedManuscripts}
                  />
                </React.Fragment>
              )}
              {nonEmbargoedManuscripts.length > 0 && (
                <React.Fragment>
                  <H3>
                    Non-embargoed manuscripts (
                    {nonEmbargoedManuscripts[0].count})
                  </H3>
                  <DashboardList
                    currentUser={currentUser}
                    listData={nonEmbargoedManuscripts}
                  />
                </React.Fragment>
              )}
              {preprints.length > 0 && (
                <React.Fragment>
                  <H3>Preprints ({preprints[0].count})</H3>
                  <DashboardList
                    currentUser={currentUser}
                    listData={preprints}
                  />
                </React.Fragment>
              )}
              {total > 0 && <PageNavigation />}
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

export default withRouter(RepoProcessing)
