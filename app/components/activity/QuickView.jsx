import React from 'react'
import { Icon } from '@pubsweet/ui'
import { th, darken } from '@pubsweet/ui-toolkit'
import styled, { withTheme } from 'styled-components'
import { states } from 'config'
import MetaSec from './MetaSec'

const Checklist = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  @media screen and (max-width: 970px) {
    flex-wrap: wrap;
  }
`
const Col = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-right: 0;

  & > div {
    flex: 1 1 calc(${th('gridUnit')} * 6.25);
    text-align: center;
    padding: ${th('gridUnit')};
    &:first-child {
      flex: 1;
      font-weight: 600;
      white-space: nowrap;
      background-color: ${darken('colorBackgroundHue', 7)};
      border-bottom: ${th('borderWidth')} ${th('borderStyle')}
        ${th('colorBorder')};
    }
  }

  @media screen and (max-width: 970px) {
    div {
      font-size: ${th('fontSizeBaseSmall')};
      white-space: nowrap;
    }
  }
`

const StatusIcon = withTheme(({ theme }) => (
  <Icon color={theme.colorSuccess} size={3}>
    check
  </Icon>
))

const Status = ({ current, mark }) => {
  const curr = states.indexOf(current)
  const compare = states.indexOf(mark)
  return (
    <div>
      {curr === compare && '...'}
      {curr > compare && <StatusIcon />}
    </div>
  )
}

const CheckCitation = ({ manuscript }) => {
  const { meta } = manuscript
  const { articleIds, volume, location, publicationDates, issue } = meta
  const hasPmid = articleIds && articleIds.some(aid => aid.pubIdType === 'pmid')
  const hasPprid =
    articleIds && articleIds.some(aid => aid.pubIdType === 'pprid')
  const hasCitation =
    volume &&
    publicationDates &&
    publicationDates.length > 0 &&
    location &&
    (location.fpage || location.elocationId)
  return (
    <div>
      {hasPmid || hasPprid || hasCitation ? (
        <React.Fragment>
          {(hasPmid && (
            <React.Fragment>
              {volume || issue ? <StatusIcon /> : '...'}
            </React.Fragment>
          )) ||
            (hasPprid && (
              <React.Fragment>
                {publicationDates && publicationDates.length > 0 ? (
                  <StatusIcon />
                ) : (
                  '...'
                )}
              </React.Fragment>
            )) ||
            (hasCitation && <StatusIcon />)}
        </React.Fragment>
      ) : (
        '...'
      )}
    </div>
  )
}

const QuickView = ({ manuscript }) => {
  const preprint =
    manuscript.organization &&
    manuscript.organization.name === 'Europe PMC Preprints'
  const indexOnly =
    manuscript.ebiState && manuscript.ebiState.includes('index only')
  return (
    <React.Fragment>
      <MetaSec manuscript={manuscript} />
      <Checklist>
        <Col>
          <div>Submitted</div>
          <Status current={manuscript.status} mark="READY" />
        </Col>
        {manuscript.status !== 'link-existing' && (
          <React.Fragment>
            {!preprint && (
              <Col>
                <div>Author review</div>
                <Status current={manuscript.status} mark="in-review" />
              </Col>
            )}
            <Col>
              <div>Initial QA</div>
              <Status current={manuscript.status} mark="submitted" />
            </Col>
            <Col>
              <div>Tagging</div>
              <Status current={manuscript.status} mark="tagging" />
            </Col>
            <Col>
              <div>XML QA</div>
              <Status
                current={manuscript.status}
                mark={
                  (indexOnly && manuscript.status) ||
                  (manuscript.status === 'xml-triage' && 'xml-triage') ||
                  (manuscript.status === 'xml-corrected' && 'xml-corrected') ||
                  (manuscript.status === 'file-error' && 'file-error') ||
                  'xml-qa'
                }
              />
            </Col>
            <Col>
              <div>Final review</div>
              <Status
                current={manuscript.status}
                mark={
                  (indexOnly && 'repo-processing') ||
                  (manuscript.status === 'xml-error' && 'xml-error') ||
                  'xml-review'
                }
              />
            </Col>
          </React.Fragment>
        )}
        <Col>
          <div>Citation</div>
          <CheckCitation manuscript={manuscript} />
        </Col>
        {!preprint && (
          <Col>
            <div>Grants sent</div>
            <div>
              {(manuscript.fundingState === 'Grants linked' && (
                <StatusIcon />
              )) ||
                (manuscript.fundingState === 'Linking grants' && '...') ||
                ''}
            </div>
          </Col>
        )}
        {manuscript.status !== 'link-existing' && (
          <React.Fragment>
            <Col>
              <div>Sent</div>
              <Status
                current={manuscript.status}
                mark={
                  (indexOnly && 'repo-processing') ||
                  (manuscript.status === 'repo-triage' && 'repo-triage') ||
                  'repo-ready'
                }
              />
            </Col>
            <Col>
              <div>Published</div>
              <Status current={manuscript.status} mark="repo-processing" />
            </Col>
          </React.Fragment>
        )}
      </Checklist>
    </React.Fragment>
  )
}

export default QuickView
