import React from 'react'
import { Button, H3, TextField, ErrorText } from '@pubsweet/ui'
import { withFormik, Field } from 'formik'
import { graphql, compose } from 'react-apollo'
import * as yup from 'yup'
import { Buttons, Notification } from '../ui'
import { EDIT_IDENTIFIERS, QUERY_ACTIVITY_INFO } from './operations'

const IdField = props => (
  <TextField
    disabled={props.disabled}
    invalidTest={props.invalidTest}
    label={props.label}
    {...props.field}
  />
)

const IdentifierEdit = ({ values, errors, touched, ...props }) => (
  <form onSubmit={props.handleSubmit}>
    <H3>Edit IDs</H3>
    {props.decision === 'accepted' && (
      <Notification type="info">
        These identifiers have already been sent to the repository. If changed,
        the record will be removed from the repository and, depending on the
        current submission status, resent with new values.
      </Notification>
    )}
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      {['pmid', 'pmcid', 'pprid'].map(type => (
        <div key={type} style={{ width: '200px' }}>
          <Field
            component={IdField}
            disabled={props.preprint ? type === 'pmcid' : type === 'pprid'}
            invalidTest={errors[type] && touched[type]}
            label={type.toUpperCase()}
            name={type}
          />
          {errors[type] && touched[type] && (
            <ErrorText>{errors[type]}</ErrorText>
          )}
        </div>
      ))}
    </div>
    <Buttons right>
      <Button
        disabled={Object.keys(touched).length === 0}
        primary
        type="submit"
      >
        Submit
      </Button>
      <Button onClick={props.close}>Exit</Button>
    </Buttons>
  </form>
)

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  let submit = false
  const { articleIds, id, decision, editIdentifiers } = props
  const types = ['pmcid', 'pprid', 'pmid', 'pre-pmc']
  const notPmid = articleIds.find(aid =>
    ['pmcid', 'pprid'].includes(aid.pubIdType),
  )
  const repoId = (notPmid && notPmid.id) || ''
  const existing = articleIds.filter(aid => !types.includes(aid.pubIdType))
  const updated = types.reduce((acc, type) => {
    const curr = articleIds.find(aid => aid.pubIdType === type)
    if ((curr && curr.id !== values[type]) || (!curr && values[type])) {
      submit = true
    }
    if (values[type]) {
      acc.push({
        pubIdType: type,
        id: values[type],
      })
    }
    return acc
  }, [])
  const data = {
    id,
    meta: {
      articleIds: existing.concat(updated),
    },
  }
  if (submit) {
    await editIdentifiers({
      variables: { data, repoId, replace: decision === 'accepted' },
    })
  }
  resetForm()
  props.close()
}

const enhancedFormik = withFormik({
  initialValues: {
    pprid: '',
    pmcid: '',
    pmid: '',
  },
  mapPropsToValues: props => {
    const { articleIds } = props
    const types = ['pmcid', 'pprid', 'pmid']
    return types.reduce((obj, type) => {
      const el = articleIds && articleIds.find(aid => aid.pubIdType === type)
      obj[type] = el ? el.id : ''
      return obj
    }, {})
  },
  validationSchema: props =>
    yup.object().shape({
      pmid: yup.string().matches(/^[\d]+$/, 'Invalid PMID'),
      pmcid: yup.string().matches(/^PMC[\d]+$/, 'Invalid PMCID'),
      pprid: yup.string().matches(/^PPR[\d]+$/, 'Invalid PPRID'),
    }),
  displayName: 'identifier-edit',
  handleSubmit,
})(IdentifierEdit)

export default compose(
  graphql(EDIT_IDENTIFIERS, {
    name: 'editIdentifiers',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.id } },
      ],
    }),
  }),
)(enhancedFormik)
