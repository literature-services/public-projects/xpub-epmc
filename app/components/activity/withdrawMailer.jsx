import React from 'react'
import { createBrowserHistory } from 'history'
import { Button, H3 } from '@pubsweet/ui'
import { Notification, Buttons } from '../ui'

/**
 * Success message
 */
const WithdrawSuccess = ({ close }) => (
  <React.Fragment>
    <Notification type="success">Email message sent</Notification>
    <H3>Thank you for withdraw your manuscript.</H3>
    <p>Mail sent.</p>
    <Buttons>
      <Button
        onClick={() => createBrowserHistory({ forceRefresh: true }).push('/')}
        primary
      >
        Exit
      </Button>
    </Buttons>
  </React.Fragment>
)

export const withdrawalEmail = manuscript => {
  const recipients = ['publisher@ncbi.nlm.nih.gov']
  const meta = manuscript && manuscript.meta
  const articleTitle = meta && meta.title
  const pmcidObj =
    meta && meta.articleIds.find(articleId => articleId.pubIdType === 'pmcid')
  const pmcid = pmcidObj && pmcidObj.id

  const doiObj =
    meta && meta.articleIds.find(articleId => articleId.pubIdType === 'doi')
  const doi = doiObj && doiObj.id
  const journalTitle =
    manuscript && manuscript.journal && manuscript.journal.journalTitle
  const vol = meta && meta.volume
  const issue = meta && meta.issue
  const location = meta && meta.location
  const firstPage = location && location.fpage
  const lastPage = location && location.lpage

  const message = `Dear Team,\n
The article “${articleTitle}” has been withdrawn from Europe PMC. Please could you delete the respective PubMed citation. The PMCID and the other relevant details is provided for easy reference.\n
PMCID: “${pmcid}”\n
Other details:\n
DOI: ${doi}\n
Journal title: ${journalTitle}\n
Vol: ${vol}; Issue: ${issue}; First page: ${firstPage}; Last page: ${lastPage}\n
Regards,
EuropePMC Helpdesk`
  return {
    recipients,
    cc: 'helpdesk@europepmc.org',
    message,
    subject: 'Request to delete the PubMed citation',
    showSuccess: WithdrawSuccess,
  }
}
