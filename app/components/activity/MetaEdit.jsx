import React from 'react'
import { omit } from 'lodash'
import { withTheme } from 'styled-components'
import { Portal, CloseModal } from '../ui'
import { ManuscriptMutations, NoteMutations } from '../SubmissionMutations'
import ResolveDuplicates from '../ResolveDuplicates'
import ReviewerEdit from './ReviewerEdit'
import CitationEdit from './CitationEdit'
import GrantsEdit from './GrantsEdit'
import EmbargoEdit from './EmbargoEdit'
import StatusEdit from './StatusEdit'
import { QUERY_ACTIVITY_INFO } from './operations'

const ReviewerWithMutations = NoteMutations(ReviewerEdit)

const MetaEdit = withTheme(
  ({
    theme,
    close,
    currentUser,
    duplicates,
    lastStatus,
    linkExisting,
    manuscript,
    toEdit,
    ...props
  }) => {
    const { meta } = manuscript
    const { fundingGroup: grants, releaseDelay, notes } = meta
    const planS = notes && notes.find(n => n.notesType === 'planS')
    const fundingGroup = grants
      ? grants.map(g => {
          const n = omit(g, '__typename')
          n.pi = omit(g.pi, '__typename')
          return n
        })
      : []

    if (toEdit === 'dupes' && duplicates) {
      return (
        <ResolveDuplicates
          close={close}
          duplicates={duplicates}
          manuscript={manuscript}
          note={notes ? notes.find(n => n.notesType === 'notDuplicates') : null}
          refetch={[
            {
              query: QUERY_ACTIVITY_INFO,
              variables: { id: manuscript.id },
            },
          ]}
        />
      )
    }
    return (
      <Portal style={{ backgroundColor: theme.colorBackground }} transparent>
        <CloseModal onClick={close} />
        <React.Fragment>
          {(() => {
            switch (toEdit) {
              case 'grants':
                return (
                  <GrantsEdit
                    close={close}
                    fundingGroup={fundingGroup}
                    manuscript={manuscript}
                    refetch={[
                      {
                        query: QUERY_ACTIVITY_INFO,
                        variables: { id: manuscript.id },
                      },
                    ]}
                    updateEmbargo={props.updateEmbargo}
                    updateGrants={props.updateGrants}
                  />
                )
              case 'embargo':
                return (
                  <EmbargoEdit
                    change={v => props.updateEmbargo(v)}
                    close={close}
                    embargo={releaseDelay}
                    planS={!!planS}
                  />
                )
              case 'reviewer':
                return (
                  <ReviewerWithMutations
                    close={close}
                    currentUser={currentUser}
                    manuscript={manuscript}
                    refetch={[
                      {
                        query: QUERY_ACTIVITY_INFO,
                        variables: { id: manuscript.id },
                      },
                    ]}
                  />
                )
              case 'citation':
                return (
                  <CitationEdit
                    change={async (e, d) => {
                      await props.changeCitation(e)
                      if (d) {
                        await linkExisting(d)
                      } else if (manuscript.status === 'xml-complete') {
                        const {
                          articleIds,
                          location,
                          publicationDates,
                        } = e.meta
                        const hasPmid =
                          articleIds.some(aid => aid.pubIdType === 'pmid') &&
                          Object.values(location).some(v => !!v)
                        const hasPprid =
                          articleIds.some(aid => aid.pubIdType === 'pprid') &&
                          publicationDates.length > 0
                        const hasCitation = publicationDates.length > 0
                        if (hasPprid || hasPmid || hasCitation) {
                          props.setStatus('repo-ready')
                        }
                      }
                    }}
                    close={close}
                    manuscript={manuscript}
                  />
                )
              case 'status':
                return (
                  <StatusEdit
                    close={close}
                    lastStatus={lastStatus}
                    manuscript={manuscript}
                    setStatus={props.setStatus}
                    updateGrants={props.updateGrants}
                  />
                )
              default:
                return null
            }
          })()}
        </React.Fragment>
      </Portal>
    )
  },
)

export default ManuscriptMutations(MetaEdit)
