import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import moment from 'moment'
import { withdrawalOptions } from 'config'
import { Icon, Action, Button, Link } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { B, A, HTMLString } from '../ui'
import { AllTypes } from '../upload-files'
import { UserContext } from '../App'
import Mailer from '../mailer'
import { GET_USER, GET_JOURNAL } from './operations'

const NoteEvent = styled.div`
  display: flex;
  align-items: flex-end;
  flex-wrap: wrap;
  justify-content: space-between;
  div {
    max-width: calc(100% - (${th('gridUnit')} * 4));
    font-size: ${th('fontSizeBaseSmall')};
  }
  button {
    line-height: 0;
  }
  .hidden {
    display: none;
  }
`
const NoPadding = styled(Action)`
  span {
    padding-top: 0;
    padding-bottom: 0;
  }
`
const EmailBody = styled.div`
  white-space: pre-wrap;
  overflow-wrap: break-word;
  padding-top: calc(${th('gridUnit')} * 2);
  &,
  & div,
  & div * {
    font-size: ${th('fontSizeBaseSmall')};
  }
`
const ReplyButton = styled(Button)`
  min-width: 0;
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('gridUnit')};
  float: right;
`
const Username = ({ id }) => (
  <Query query={GET_USER} variables={{ id }}>
    {({ data, loading, error }) => {
      if (error) {
        return <em>[User has been deleted]</em>
      }
      if (loading || !data) {
        return `...`
      }
      if (!data.user) {
        return <em>[User not found]</em>
      }
      const { givenNames, surname } = data.user.identities[0].name
      return (
        <Link to={`/manage-account/${id}`}>
          {givenNames} {surname}
        </Link>
      )
    }}
  </Query>
)

const Journal = ({ id }) => (
  <Query query={GET_JOURNAL} variables={{ id }}>
    {({ data, loading }) => {
      if (loading || !data || !data.journal) {
        return `...`
      }
      return <em>{data.journal.journalTitle}</em>
    }}
  </Query>
)

const toggleHidden = button => {
  const array = Array.from(button.children)
  array.forEach(element => {
    element.classList.toggle('hidden')
  })
  button.parentNode.nextElementSibling.classList.toggle('hidden')
}

const cleanUp = column => column.replace('meta,', '').replace(/_/g, ' ')
const isJson = str => {
  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }
  return true
}

const ParseJson = ({ col, val }) => {
  const set = isJson(val) && JSON.parse(val)
  switch (col) {
    case 'journal id':
      return val ? <Journal id={val} /> : '(unmatched)'
    case 'title':
      return <em>{val}</em>
    case 'deleted':
      return 'recovered'
    case 'article ids':
      return (
        set &&
        set.map(
          (p, i) =>
            `${p.pubIdType.toUpperCase()} ${p.id}${
              i !== set.length - 1 ? ', ' : ''
            }`,
        )
      )
    case 'funding group':
      return set.map(
        (p, i) =>
          `${p.pi.surname}, ${p.fundingSource} ${p.awardId}${
            i !== set.length - 1 ? '; ' : ''
          }`,
      )
    case 'publication dates':
      return set.map((p, i) => {
        const type =
          (p.type === 'epub' && 'Electronic') || (p.type === 'ppub' && 'Print')
        const { year, month = '', day = '', season = '' } = p.jatsDate || ''
        const jatsDate = `${year && year} ${season && season}${month &&
          moment(month).format('MMM')} ${day && day}`
        const date = p.date ? moment(p.date).format('DD MMM YYYY') : jatsDate
        return `${date} (${type})${i !== set.length - 1 ? '; ' : ''}`
      })
    case 'location':
      if (set.fpage) {
        return `page ${set.fpage}${set.lpage && `-${set.lpage}`}`
      } else if (set.elocationId) {
        return `elocation ${set.elocationId}`
      }
      return ''
    case 'form_state':
      return (
        <React.Fragment>
          <NoteEvent>
            <div>Errors reported:</div>
            <NoPadding
              onClick={e => toggleHidden(e.currentTarget)}
              title="Message body"
            >
              <Icon color="currentColor" size={3}>
                chevron-right
              </Icon>
              <Icon className="hidden" color="currentColor" size={3}>
                chevron-down
              </Icon>
            </NoPadding>
          </NoteEvent>
          <EmailBody className="hidden">{val}</EmailBody>
        </React.Fragment>
      )
    default:
      return val
  }
}

const ParseFunding = ({ changes, original }) => {
  const changeSet = isJson(changes) && JSON.parse(changes)
  const origSet = isJson(original) && JSON.parse(original)
  const added = changeSet.filter(
    p =>
      !origSet.some(
        o =>
          o.awardId === p.awardId &&
          o.fundingSource === p.fundingSource &&
          o.pi.surname === p.pi.surname,
      ),
  )
  const removed = origSet.filter(
    p =>
      !changeSet.some(
        o =>
          o.awardId === p.awardId &&
          o.fundingSource === p.fundingSource &&
          o.pi.surname === p.pi.surname,
      ),
  )
  if (added.length > 0) {
    return `Added funding: ${added.map(
      (p, i) =>
        `${p.pi.surname}, ${p.fundingSource} ${p.awardId}${
          i !== added.length - 1 ? '; ' : ''
        }`,
    )}`
  }
  if (removed.length > 0) {
    return `Removed funding: ${removed.map(
      (p, i) =>
        `${p.pi.surname}, ${p.fundingSource} ${p.awardId}${
          i !== removed.length - 1 ? '; ' : ''
        }`,
    )}`
  }
  const bool = changeSet.find(f => typeof f.dateChecked === 'boolean')
  if (bool) {
    return `Edited funding: set plan S submission date to ${bool.dateChecked.toString()}`
  }
  return '(Edited funding - bad data)'
}

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${name.givenNames} ${name.surname}`

class EmailMessage extends React.Component {
  state = { open: false, mail: false }
  static contextType = UserContext
  render() {
    const currentUser = this.context
    const { email, manuscript, sender, time } = this.props
    const { open, mail } = this.state
    const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
    const buildMessage = `\nKind regards,\n
${formatName(currentUser.identities[0].name)}
Europe PMC Helpdesk\n\n
On ${moment(time).format('MM/DD/YYYY HH:mm')} ${formatName(sender)} wrote:
>${email.message.replace(/\n/g, '\n>').replace(/(.{78}[\s])/g, '$1\n>')}
`
    return (
      <React.Fragment>
        <NoteEvent>
          <div>
            <B>Sent email to: </B>
            {email.to.map((to, i) => (
              <React.Fragment key={to}>
                {(to === 'helpdesk' && 'Helpdesk') ||
                  (to === 'tagger' && 'Taggers') ||
                  (regex.test(to) && <Username id={to} />) ||
                  to}
                {i !== email.to.length - 1 && ', '}
              </React.Fragment>
            ))}
            <br />
            {email.cc && (
              <React.Fragment>
                <B>CC:</B>
                {email.cc}
                <br />
              </React.Fragment>
            )}
            <B>Subject: </B>
            {email.subject}
          </div>
          <Action
            onClick={() => this.setState({ open: !open })}
            title="Message body"
          >
            <Icon color="currentColor" size={3}>
              chevron-{open ? 'down' : 'right'}
            </Icon>
          </Action>
        </NoteEvent>
        {open && (
          <div>
            <ReplyButton onClick={() => this.setState({ mail: true })}>
              Reply
            </ReplyButton>
            <EmailBody>
              <HTMLString element="div" string={email.message} />
            </EmailBody>
          </div>
        )}
        {mail && (
          <Mailer
            close={() => this.setState({ mail: false })}
            currentUser={currentUser}
            manuscript={manuscript}
            message={buildMessage}
            recipients={[sender.id]}
            subject={`Re: ${email.subject}`}
          />
        )}
      </React.Fragment>
    )
  }
}

const getType = set =>
  AllTypes.find(t => t.value === set.type) &&
  AllTypes.find(t => t.value === set.type).label

const EventDescription = ({ audit, manuscript }) => {
  const { originalData, objectType, changes } = audit
  if (objectType === 'note') {
    const notes_type = changes.notes_type || originalData.notes_type
    if (notes_type === 'userMessage' && !changes.deleted) {
      const content = JSON.parse(changes.content)
      if (content && content.to) {
        return (
          <EmailMessage
            email={content}
            manuscript={manuscript}
            sender={audit.user}
            time={audit.created}
          />
        )
      }
      return (
        <React.Fragment>
          <NoteEvent>
            <div>
              <B>Attached a note:</B>
            </div>
            <NoPadding
              onClick={e => toggleHidden(e.currentTarget)}
              title="Message body"
            >
              <Icon color="currentColor" size={3}>
                chevron-right
              </Icon>
              <Icon className="hidden" color="currentColor" size={3}>
                chevron-down
              </Icon>
            </NoPadding>
          </NoteEvent>
          <EmailBody className="hidden">{content}</EmailBody>
        </React.Fragment>
      )
    } else if (notes_type === 'selectedReviewer') {
      if (changes.deleted) {
        return 'Reviewer set; removed reviewer invitation'
      }
      const revNote = changes.content && JSON.parse(changes.content)
      if (revNote) {
        return `Selected reviewer to invite: ${revNote.name.givenNames} ${
          revNote.name.surname
        }${revNote.email ? `, ${revNote.email}` : ''}`
      }
    } else if (notes_type === 'userCitation') {
      if (changes.deleted) {
        return 'Deleted user-provided citation information'
      }
      return `Provided citation info: ${changes.content}`
    } else if (notes_type === 'notDuplicates') {
      return `Marked as not duplicates: ${JSON.parse(changes.content).join(
        ', ',
      )}`
    } else if (notes_type === 'versionList') {
      const { emsid, pprid } = JSON.parse(changes.content)
      return `Noted IDs of other versions: PPRIDs ${pprid.join(
        ', ',
      )}; EMSIDs ${emsid.join(', ')}`
    } else if (notes_type === 'openAccess') {
      if (changes.deleted) {
        return 'Noted: open access license no longer in XML'
      }
      return `Noted: open access license in XML: ${changes.content ||
        originalData.content}`
    } else if (notes_type === 'planS') {
      if (changes.deleted) {
        return 'Noted: Plan S funding has been removed'
      } else if (changes.id) {
        return 'Noted: submission has Plan S funding'
      }
      if (changes.content) {
        return `Noted: Plan S license exception added: ${changes.content
          .toUpperCase()
          .replace(/\//g, ' ')}`
      } else if (originalData.content) {
        return `Noted: Plan S license exception removed`
      }
    } else if (notes_type === 'versionApproval') {
      return `Noted: user ${changes.content ||
        originalData.content} the pre-approval of future preprint versions`
    } else if (notes_type === 'addLicense') {
      return `Noted: user ${changes.content ||
        originalData.content} adding Europe PMC open access license`
    } else if (notes_type === 'withdrawReason') {
      const forNote = changes.content && JSON.parse(changes.content)
      if (forNote) {
        const option = withdrawalOptions.find(t => t.value === forNote.type)
        const reason = option && option.label
        const { duplicate } = forNote
        return `Withdrawing manuscript because: ${reason}${
          duplicate ? ` of ${duplicate}` : ''
        }`
      }
    } else if (notes_type === 'schematron') {
      const list = changes.content && JSON.parse(changes.content)
      if (list) {
        return (
          <React.Fragment>
            Created schematron note
            {list.length > 0 && (
              <React.Fragment>
                <NoteEvent>
                  <div>Content:</div>
                  <NoPadding
                    onClick={e => toggleHidden(e.currentTarget)}
                    title="Message body"
                  >
                    <Icon color="currentColor" size={3}>
                      chevron-right
                    </Icon>
                    <Icon className="hidden" color="currentColor" size={3}>
                      chevron-down
                    </Icon>
                  </NoPadding>
                </NoteEvent>
                <ul className="hidden">
                  {list.map(item => (
                    <li key={item}>{item}</li>
                  ))}
                </ul>
              </React.Fragment>
            )}
          </React.Fragment>
        )
      }
    }
  }
  if (objectType === 'team') {
    if (
      ['submitter', 'reviewer'].includes(changes.role_name) ||
      (originalData &&
        ['submitter', 'reviewer'].includes(originalData.role_name))
    ) {
      if (audit.user.id === changes.user_id) {
        return `Became ${changes.role_name ||
          (originalData && originalData.role_name)}`
      }
      return (
        <React.Fragment>
          {`Set ${changes.role_name ||
            (originalData && originalData.role_name)}: `}
          <Username id={changes.user_id} />
        </React.Fragment>
      )
    }
  }
  if (objectType === 'file') {
    if (changes.filename && changes.url) {
      return (
        <React.Fragment>
          Uploaded{' '}
          <em>{changes.type && `${getType(changes) || changes.type}`}</em> file{' '}
          <A download={changes.filename} href={changes.url}>
            <B>{changes.filename}</B>
          </A>
          {changes.label && `; set label to: ${changes.label}`}
        </React.Fragment>
      )
    }
    if (changes.url) {
      return (
        <React.Fragment>
          {`Regenerated ${originalData.type &&
            `${getType(originalData) || originalData.type} `}file `}
          <A download={originalData.filename} href={changes.url}>
            <B>{originalData.filename}</B>
          </A>
        </React.Fragment>
      )
    }
    if (changes.type) {
      return (
        <React.Fragment>
          Set <em>{originalData.filename}</em> type to:{' '}
          {getType(changes) || changes.type}
        </React.Fragment>
      )
    }
    if (changes.label) {
      return (
        <React.Fragment>
          Set <em>{originalData.filename}</em> label to: {changes.label}
        </React.Fragment>
      )
    }
    if (changes.deleted) {
      return (
        <React.Fragment>
          {`Deleted ${originalData.type &&
            `${getType(originalData) || originalData.type} `}file `}
          <A download={originalData.filename} href={originalData.url}>
            <B>{originalData.filename}</B>
          </A>
        </React.Fragment>
      )
    }
    if (changes.updated_by) {
      return 'Regenerated previews'
    }
  }
  if (objectType === 'manuscript') {
    if (changes.claimed_by) {
      return `Claimed manuscript`
    }
  }

  if (changes.deleted) {
    return `Deleted ${objectType}`
  }

  const keys = Object.keys(changes)
  let list = keys.filter(k => k !== 'updated_by')

  if (changes.id) {
    list = list.filter(
      k =>
        changes[k] &&
        !['user_id', 'manuscript_id', 'manuscript_version'].includes(k),
    )
  }

  return list.map(k => {
    switch (k) {
      case 'organization_id':
      case 'retry_attempt':
        if (originalData) {
          return <React.Fragment key={k}>Retry PDF deposit</React.Fragment>
        }
        return null
      case 'id':
        return (
          <React.Fragment key={k}>
            Created {objectType}
            <br />
          </React.Fragment>
        )
      case 'claimed_by':
        return (
          <React.Fragment key={k}>
            Removed admin claim
            <br />
          </React.Fragment>
        )
      case 'form_state':
        return changes[k] ? (
          <React.Fragment key={k}>
            {changes.status && changes.status === 'submission-error' ? (
              'Reported errors (see below)'
            ) : (
              <React.Fragment>
                <ParseJson col={k} val={changes[k]} />
              </React.Fragment>
            )}
          </React.Fragment>
        ) : (
          <React.Fragment key={k}>
            Removed error report
            <br />
          </React.Fragment>
        )
      case 'meta,funding_group':
        if (originalData && originalData[k]) {
          return (
            <ParseFunding
              changes={changes[k]}
              key={k}
              original={originalData[k]}
            />
          )
        }
      /* falls through */
      default:
        return (
          <React.Fragment key={k}>
            {`Set ${objectType} ${cleanUp(k)} to: `}
            <ParseJson col={cleanUp(k)} val={changes[k]} />
            <br />
          </React.Fragment>
        )
    }
  })
}

export default EventDescription
