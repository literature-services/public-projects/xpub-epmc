import React from 'react'
import { Button, Icon } from '@pubsweet/ui'
import { Buttons } from '../ui'

export const Exit = ({ close }) => (
  <Buttons right>
    <Button onClick={close}>Exit</Button>
  </Buttons>
)

export const EditIcon = () => (
  <Icon color="currentColor" size={1.9} style={{ verticalAlign: 'bottom' }}>
    edit-3
  </Icon>
)
