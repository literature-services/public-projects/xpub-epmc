import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link } from '@pubsweet/ui'
import { B, Toggle } from '../ui'
import { GET_VERSION } from './operations'

const Flex = styled.div`
  display: flex;
  strong {
    margin-right: ${th('gridUnit')};
  }
  div a + a {
    margin-left: ${th('gridUnit')};
  }
`

const VersionList = ({ versions, current }) => (
  <Flex>
    <B>Versions:</B>
    <Query query={GET_VERSION} variables={{ query: versions }}>
      {({ data, loading, error }) => {
        if (error) {
          return <em>Error, unable to load</em>
        }
        if (loading || !data) {
          return `...`
        }
        const { manuscripts } = data.getManuscriptVersions
        return (
          <Toggle>
            {manuscripts.map(man => (
              <Link
                className={man.id === current && 'current'}
                key={man.id}
                to={`/submission/${man.id}/activity`}
              >
                {man.version}
              </Link>
            ))}
          </Toggle>
        )
      }}
    </Query>
  </Flex>
)

export default VersionList
