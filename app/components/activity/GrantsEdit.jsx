import React from 'react'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { th, darken } from '@pubsweet/ui-toolkit'
import { Button, H3, Icon, RadioGroup, Action } from '@pubsweet/ui'
import { Buttons } from '../ui'
import GrantSearch from '../grant-search'
import { EXCEPTION_ALERT, QUERY_ACTIVITY_INFO } from './operations'
import { NoteMutations, WithFunders } from '../SubmissionMutations'

const licenses = [
  'CC by-sa',
  'CC by-nc',
  'CC by-nc-sa',
  'CC by-nd',
  'CC by-nc-nd',
  'CC by/3.0/igo',
  'Open Government License v3',
]

const PlanSActions = styled.p`
  display: flex;
  align-items: center;
  & > span,
  & > button {
    display: inline-flex;
    align-items: center;
    margin-right: calc(${th('gridUnit')} * 3);
  }
  & > span {
    font-size: ${th('fontSizeBaseSmall')};
    background-color: ${props =>
      props.planS ? th('colorWarning') : darken('colorBackgroundHue', 7)};
    padding: ${th('gridUnit')};
    padding-right: calc(${th('gridUnit')} * 1.75);
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  }
`

class GrantsEdit extends React.Component {
  state = {
    originalData: null,
    isPlanS: false,
    exemptForm: false,
    updateGrants: null,
    newNote: null,
    changeNote: null,
    deleteNote: null,
    selectedLicense: '',
  }
  componentDidMount() {
    const { notes } = this.props.manuscript.meta
    const planS = notes && notes.find(n => n.notesType === 'planS')
    this.setState({
      originalData: this.props.fundingGroup,
      isPlanS: !!planS,
      exemptForm: planS && !!planS.content,
      selectedLicense: planS && planS.content,
    })
  }
  render() {
    const { exemptForm, isPlanS, selectedLicense } = this.state
    const { close, fundingGroup, manuscript } = this.props
    const { notes } = manuscript.meta
    const planS = notes && notes.find(n => n.notesType === 'planS')
    return (
      <React.Fragment>
        <GrantSearch
          changedGrants={v => this.setState({ updateGrants: v })}
          deleteNote={v =>
            this.setState({ deleteNote: v, newNote: null, isPlanS: false })
          }
          disableNext={() => true}
          funders={this.props.funders}
          isPlanS={isPlanS}
          newNote={v =>
            this.setState({ newNote: v, deleteNote: null, isPlanS: true })
          }
          planS={planS}
          selectedGrants={fundingGroup}
        />
        <PlanSActions planS={isPlanS}>
          <span>
            <Icon color="currentColor" size={2}>
              unlock
            </Icon>
            {isPlanS ? 'Has Plan S funding' : 'Not Plan S'}
          </span>
          {isPlanS && (
            <React.Fragment>
              <Button
                disabled={selectedLicense && planS && planS.content}
                onClick={() => this.setState({ exemptForm: true })}
              >
                Add license exception
              </Button>
              {selectedLicense && planS && planS.content && (
                <Action
                  onClick={() =>
                    this.setState({ selectedLicense: '', exemptForm: false })
                  }
                >
                  <Icon color="currentColor">x</Icon>
                  Cancel (Remove exception)
                </Action>
              )}
            </React.Fragment>
          )}
        </PlanSActions>
        {exemptForm && (
          <React.Fragment>
            <H3>Select approved license:</H3>
            <RadioGroup
              name="licenseGroup"
              onChange={v => this.setState({ selectedLicense: v })}
              options={licenses.map(l => ({
                value: l,
                label: `${l.toUpperCase().replace(/\//g, ' ')}`,
              }))}
              value={selectedLicense}
            />
          </React.Fragment>
        )}

        <Mutation
          awaitrefetchQueries
          mutation={EXCEPTION_ALERT}
          refetchQueries={() => [
            {
              query: QUERY_ACTIVITY_INFO,
              variables: { id: manuscript.id },
            },
          ]}
        >
          {(epmc_emailException, { data }) => {
            const checkException = async () => {
              const checkArray = ['newNote', 'deleteNote', 'updateGrants']
              await checkArray.reduce(async (promise, func) => {
                await promise
                const funcVal = this.state[func]
                if (funcVal) {
                  return this.props[func](funcVal)
                }
                return Promise.resolve()
              }, Promise.resolve())

              const { isPlanS, selectedLicense } = this.state
              const { notes } = this.props.manuscript.meta
              const planS = notes && notes.find(n => n.notesType === 'planS')
              if (isPlanS && planS) {
                const changeLicense =
                  (selectedLicense && !planS.content) ||
                  (planS.content && planS.content !== selectedLicense)
                if (changeLicense) {
                  await this.props.changeNote({
                    id: planS.id,
                    notesType: 'planS',
                    content: selectedLicense,
                  })
                  if (selectedLicense) {
                    await epmc_emailException({
                      variables: {
                        manuscriptId: manuscript.id,
                        license: selectedLicense,
                      },
                    })
                  }
                }
              }
            }
            return (
              <Buttons right>
                <Button
                  onClick={async () => {
                    await checkException()
                    close()
                  }}
                  primary
                >
                  Save &amp; close
                </Button>
              </Buttons>
            )
          }}
        </Mutation>
      </React.Fragment>
    )
  }
}

export default NoteMutations(WithFunders(GrantsEdit))
