import React from 'react'
import { Query, compose, graphql } from 'react-apollo'
import { Icon, Action, Button, H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { withTheme } from 'styled-components'
import moment from 'moment'
import { Notification } from '../ui'
import {
  CHECK_DUPES,
  CLAIM_MANUSCRIPT,
  UNCLAIM_MANUSCRIPT,
  GET_PREPRINT_SELECTIONS,
} from '../operations'
import { UserContext } from '../App'
import MetaEdit from './MetaEdit'
import { EditIcon } from './Edit'
import { QUERY_ACTIVITY_INFO, LINK_EXISTING } from './operations'

const Heading = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  margin: calc(${th('gridUnit')} * 3) 0px calc(${th('gridUnit')} * 2);
  padding-bottom: ${th('gridUnit')};
  h2 {
    display: inline-block;
    margin: 0;
  }
  dl {
    display: inline-block;
    margin-left: calc(${th('gridUnit')} * 2);
  }
`
const ClaimAction = styled(Button)`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('fontSize')};
  padding: 0 calc(${th('gridUnit')} * 1.5);
  margin-left: calc(${th('gridUnit')} * 2);
  margin-bottom: 0;
  vertical-align: text-bottom;
  min-width: 0;
`
const Container = styled.div`
  margin: 0 0 calc(${th('gridUnit')} * 2);
  column-count: 3;
  @media screen and (max-width: 1000px) {
    column-count: 2;
  }
  @media screen and (max-width: 600px) {
    column-count: 1;
  }
`
const DL = styled.dl`
  margin: 0 0 ${th('gridUnit')};
  dt {
    font-weight: 600;
    margin-right: ${th('gridUnit')};
    display: inline;
  }
  dd {
    margin: 0;
    display: inline;
    button {
      margin-left: calc(${th('gridUnit')} / 2);
    }
  }
`
const DupeButton = styled(Button)`
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
  font-size: ${th('fontSizeBaseSmall')};
`

const WarningIcon = withTheme(({ theme, title }) => (
  <Icon
    color={theme.colorWarning}
    size={2.5}
    strokeWidth={3}
    style={{
      verticalAlign: `calc(${theme.gridUnit}/-2)`,
      margin: `0 calc(${theme.gridUnit}/-2) 0 calc(${theme.gridUnit}/2)`,
    }}
    title={title}
  >
    alert-triangle
  </Icon>
))

const EditButton = ({ onClick }) => (
  <Action aria-label="Edit" onClick={onClick} title="Edit">
    <EditIcon />
  </Action>
)

class MetaSec extends React.Component {
  state = { edit: null, notif: null }
  componentDidMount() {
    this._mounted = true
    this.notifTimer = null
  }
  async componentDidUpdate() {
    const { notif } = this.state
    if (this.notifTimer) {
      clearTimeout(this.notifTimer)
    }
    if (notif && notif.type !== 'error') {
      this.notifTimer = setTimeout(() => {
        if (this._mounted) {
          this.setState({ notif: null })
        }
      }, 3000)
    }
  }
  componentWillUnmount() {
    this._mounted = false
    clearTimeout(this.notifTimer)
  }
  static contextType = UserContext
  render() {
    const currentUser = this.context
    const {
      manuscript,
      claimManuscript,
      unclaimManuscript,
      linkExistingMan,
    } = this.props
    const {
      teams,
      audits,
      meta,
      journal,
      claiming,
      organization,
      version,
    } = manuscript
    const {
      fundingGroup,
      releaseDelay,
      articleIds,
      volume,
      issue,
      location,
      publicationDates,
      title,
      notes,
    } = meta
    let duplicates = []
    const preprint = organization.name === 'Europe PMC Preprints'
    const pmid = articleIds && articleIds.find(aid => aid.pubIdType === 'pmid')
    const pmcid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pmcid')
    const pprid =
      articleIds && articleIds.find(aid => aid.pubIdType === 'pprid')
    const planS = notes && notes.find(n => n.notesType === 'planS')
    const openAccess = notes && notes.find(n => n.notesType === 'openAccess')
    const openAccessRegex =
      openAccess &&
      openAccess.content &&
      openAccess.content.match(
        /\/(publicdomain|by|by-nc|by-nd|by-sa|by-nc-sa|by-nc-nd)\//i,
      )
    const openAccessValue =
      (openAccessRegex &&
        (openAccessRegex[0] === '/publicdomain/'
          ? '0'
          : openAccessRegex[0].replace(/\//g, ''))) ||
      null
    const submitter = teams.find(team => team.role === 'submitter')
    const { title: st, givenNames: sg, surname: ss } = submitter
      ? submitter.teamMembers[0].alias.name
      : ''
    const reviewer = teams.find(team => team.role === 'reviewer')
    const { title: rt, givenNames: rg, surname: rs } = reviewer
      ? reviewer.teamMembers[0].alias.name
      : ''
    const reviewerNote =
      notes &&
      notes.find(n => n.notesType === 'selectedReviewer') &&
      JSON.parse(notes.find(n => n.notesType === 'selectedReviewer').content)
    const rnName = reviewerNote && reviewerNote.name
    const auditInfo = audits.reduce((info, audit) => {
      if (!info.timeInProcess) {
        info.timeInProcess = moment(audit.created).fromNow(true)
      }
      info.lastActivity = moment(audit.created).fromNow(true)
      return info
    }, {})
    const lastStatusChange = audits.reduce((log, current) => {
      if (Object.keys(current.changes).includes('status')) {
        return current
      }
      return log
    }, {})
    const year =
      publicationDates &&
      publicationDates.reduce((retYear, date) => {
        const getYear = date.jatsDate
          ? date.jatsDate.year
          : moment(date.date).year()
        if (date.type === 'ppub' || !retYear) {
          return getYear
        }
        return retYear
      }, null)
    const { edit, notif } = this.state
    return (
      <React.Fragment>
        {notif && (
          <React.Fragment>
            <br />
            <Notification type={notif.type}>{notif.message}</Notification>
          </React.Fragment>
        )}
        <DL style={{ float: 'right', marginTop: '1rem' }}>
          <dt>Status:</dt>
          <dd>
            {manuscript.deleted ? (
              <del>{manuscript.status}</del>
            ) : (
              manuscript.status
            )}
            <Action
              aria-label="Change"
              onClick={() => this.setState({ edit: 'status' })}
              title="Change"
            >
              <Icon color="currentColor" size={1.75}>
                send
              </Icon>
            </Action>
          </dd>
        </DL>
        <Heading>
          <H2>Quick view</H2>
          {claiming ? (
            <DL>
              <dt>Claimed:</dt>
              <dd>
                {claiming.givenNames}
                {currentUser.id === claiming.id && (
                  <Action
                    onClick={async () =>
                      unclaimManuscript({
                        variables: { id: manuscript.id },
                      })
                    }
                  >
                    <Icon color="currentColor" size={2}>
                      x
                    </Icon>
                  </Action>
                )}
              </dd>
            </DL>
          ) : (
            <ClaimAction
              onClick={async () =>
                claimManuscript({
                  variables: { id: manuscript.id },
                })
              }
            >
              Claim
            </ClaimAction>
          )}
        </Heading>
        <Container>
          {reviewerNote ? (
            <React.Fragment>
              <DL>
                <dt>Submitter:</dt>
                <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
              </DL>
              {rnName && (
                <DL>
                  <dt>Invited reviewer:</dt>
                  <dd>
                    {`${rnName.givenNames ? `${rnName.givenNames} ` : ''}${
                      rnName.surname
                    }`}
                    <EditButton
                      onClick={() => this.setState({ edit: 'reviewer' })}
                    />
                  </dd>
                </DL>
              )}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {reviewer &&
              reviewer.teamMembers[0].user.id ===
                submitter.teamMembers[0].user.id ? (
                <DL>
                  <dt>Submitter/Reviewer:</dt>
                  <dd>
                    {`${st ? `${st} ` : ''}${sg} ${ss}`}
                    <EditButton
                      onClick={() => this.setState({ edit: 'reviewer' })}
                    />
                  </dd>
                </DL>
              ) : (
                <React.Fragment>
                  <DL>
                    <dt>Submitter:</dt>
                    <dd>{`${st ? `${st} ` : ''}${sg} ${ss}`}</dd>
                  </DL>
                  {reviewer && (
                    <DL>
                      <dt>Reviewer:</dt>
                      <dd>
                        {`${rt ? `${rt} ` : ''}${rg ? `${rg} ` : ''}${rs}`}
                        <EditButton
                          onClick={() => this.setState({ edit: 'reviewer' })}
                        />
                      </dd>
                    </DL>
                  )}
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          <DL>
            <dt>Time in process:</dt>
            <dd>{auditInfo.timeInProcess || 'N/A'}</dd>
          </DL>
          <DL>
            <dt>Last activity:</dt>
            <dd>{`${auditInfo.lastActivity} ago` || 'N/A'}</dd>
          </DL>
          {fundingGroup && (
            <DL>
              <dt>Grants:</dt>
              <dd>
                {fundingGroup.map((f, t) => (
                  <span key={f.awardId}>
                    {`${f.fundingSource} ${f.awardId}`}
                    {t !== fundingGroup.length - 1 && ', '}
                  </span>
                ))}
                <EditButton onClick={() => this.setState({ edit: 'grants' })} />
              </dd>
            </DL>
          )}
          {preprint && (
            <React.Fragment>
              <DL>
                <dt>License:</dt>
                <dd>
                  {openAccess && openAccessValue
                    ? `CC ${openAccessValue.toUpperCase()}`
                    : 'None'}
                </dd>
              </DL>
              <Query
                fetchPolicy="cache-and-network"
                query={GET_PREPRINT_SELECTIONS}
                variables={{ id: manuscript.id }}
              >
                {({ data, loading, error }) => {
                  if (loading || !data) {
                    return '...'
                  }
                  const {
                    versionApproval,
                    addLicense,
                  } = data.getPreprintSelections
                  return (
                    <React.Fragment>
                      {versionApproval && (
                        <DL>
                          <dt>Automatic versions:</dt>
                          <dd>{versionApproval}</dd>
                        </DL>
                      )}
                      {addLicense && (
                        <DL>
                          <dt>Add EPMC license:</dt>
                          <dd>{addLicense}</dd>
                        </DL>
                      )}
                    </React.Fragment>
                  )
                }}
              </Query>
            </React.Fragment>
          )}
          {releaseDelay && !preprint && (
            <DL>
              <dt>Embargo:</dt>
              <dd>
                {releaseDelay} month{releaseDelay !== '1' && 's'}
                {planS && releaseDelay !== '0' && (
                  <WarningIcon title="Has Plan S funding but embargo is not 0" />
                )}
                <EditButton
                  onClick={() => this.setState({ edit: 'embargo' })}
                />
              </dd>
            </DL>
          )}
          <DL>
            <dt>Citation:</dt>
            <dd>
              {(journal && journal.meta.nlmta) || 'Unmatched'}
              {year && `. ${year}`}
              {volume && `, ${volume}`}
              {issue && `(${issue})`}
              {location && location.fpage ? (
                <React.Fragment>
                  {`:${location.fpage}`}
                  {location.lpage && `-${location.lpage}`}.
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {location &&
                    location.elocationId &&
                    `:${location.elocationId}.`}
                </React.Fragment>
              )}
              {pmid && ` PMID: ${pmid.id}.`}
              {pmcid && ` PMCID: ${pmcid.id}.`}
              {pprid && ` PPRID: ${pprid.id}.`}
              {version > 0 && ` v${version}`}
              <EditButton onClick={() => this.setState({ edit: 'citation' })} />
            </dd>
          </DL>
          <Query
            fetchPolicy="cache-and-network"
            query={CHECK_DUPES}
            variables={{
              id: manuscript.id,
              articleIds: articleIds ? articleIds.map(aid => aid.id) : null,
              title,
            }}
          >
            {({ data, loading }) => {
              if (loading || !data || !data.checkDuplicates) {
                return '...'
              }
              const { manuscripts } = data.checkDuplicates
              const dupeNote = notes
                ? notes.find(n => n.notesType === 'notDuplicates')
                : null
              const notDupes = dupeNote ? JSON.parse(dupeNote.content) : []
              duplicates = manuscripts.filter(m => !notDupes.includes(m.id))
              return (
                <React.Fragment>
                  {duplicates && duplicates.length > 0 && (
                    <DupeButton
                      onClick={() => this.setState({ edit: 'dupes' })}
                      primary
                    >
                      Resolve duplicates
                    </DupeButton>
                  )}
                  {edit && (
                    <MetaEdit
                      close={() => this.setState({ edit: null })}
                      currentUser={currentUser}
                      duplicates={duplicates}
                      lastStatus={
                        lastStatusChange &&
                        lastStatusChange.originalData &&
                        lastStatusChange.originalData.status
                      }
                      linkExisting={async v => {
                        const { data } = await linkExistingMan({
                          variables: { id: manuscript.id },
                        })
                        const result = data.linkExisting
                        this.setState({
                          notif: {
                            type: result.success ? 'success' : 'error',
                            message: result.message,
                          },
                        })
                      }}
                      manuscript={manuscript}
                      refetch={[
                        {
                          query: QUERY_ACTIVITY_INFO,
                          variables: { id: manuscript.id },
                        },
                      ]}
                      toEdit={edit}
                    />
                  )}
                </React.Fragment>
              )
            }}
          </Query>
        </Container>
      </React.Fragment>
    )
  }
}

export default compose(
  graphql(LINK_EXISTING, {
    name: 'linkExistingMan',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
  graphql(CLAIM_MANUSCRIPT, {
    name: 'claimManuscript',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
  graphql(UNCLAIM_MANUSCRIPT, {
    name: 'unclaimManuscript',
    options: props => ({
      refetchQueries: [
        { query: QUERY_ACTIVITY_INFO, variables: { id: props.manuscript.id } },
      ],
    }),
  }),
)(MetaSec)
