import React from 'react'
import styled from 'styled-components'
import { withdrawalOptions } from 'config'
import { th } from '@pubsweet/ui-toolkit'
import { RadioGroup, H3, H4, TextField, Flexbox, Button } from '@pubsweet/ui'
import { Buttons } from '../ui'

const Flex = styled(Flexbox)`
  & > *:first-child {
    margin-right: ${th('gridUnit')};
  }
`
class WithdrawReason extends React.Component {
  state = {
    selected: (this.props.withdrawNote && this.props.withdrawNote.type) || null,
    dupe:
      (this.props.withdrawNote && this.props.withdrawNote.duplicate) ||
      undefined,
    grants: 'keep',
  }
  render() {
    const { selected, dupe, grants } = this.state
    const { withdrawNote, changeNote, newNote } = this.props
    const enabled =
      selected &&
      ((selected === 'MC_DUPLICATE_ARTICLE' && dupe && !Number.isNaN(dupe)) ||
        selected !== 'MC_DUPLICATE_ARTICLE')
    return (
      <React.Fragment>
        <div>
          <H3>Handle grant links</H3>
          <RadioGroup
            name="keepgrants"
            onChange={v => this.setState({ grants: v })}
            options={[
              { value: 'remove', label: 'Remove grant links' },
              {
                value: 'keep',
                label: 'Remove full text only, change to grant linking status',
              },
            ]}
            value={grants}
          />
        </div>
        <Flex>
          <div>
            <H3>Select a reason</H3>
            <RadioGroup
              name="whywithdraw"
              onChange={v => this.setState({ selected: v })}
              options={withdrawalOptions}
              value={selected}
            />
          </div>
          {selected === 'MC_DUPLICATE_ARTICLE' && (
            <div>
              <H4>Enter PMCID of article to keep</H4>
              <TextField
                label="Duplicate article PMCID for linking"
                name="dupe-pmcid"
                onChange={e =>
                  this.setState({
                    dupe: e.target.value !== '' ? e.target.value : undefined,
                  })
                }
                value={dupe}
              />
            </div>
          )}
        </Flex>
        <Buttons>
          <Button
            disabled={!enabled}
            onClick={async () => {
              if (withdrawNote) {
                await changeNote({
                  id: withdrawNote.id,
                  notesType: 'withdrawReason',
                  content: JSON.stringify({ type: selected, duplicate: dupe }),
                })
              } else {
                await newNote({
                  notesType: 'withdrawReason',
                  content: JSON.stringify({ type: selected, duplicate: dupe }),
                })
              }
              this.props.onConfirm(grants)
            }}
            primary
          >
            Confirm withdrawal
          </Button>
        </Buttons>
      </React.Fragment>
    )
  }
}

export default WithdrawReason
