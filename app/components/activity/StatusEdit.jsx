import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Action, H2, Button, Icon, RadioGroup } from '@pubsweet/ui'
import { states } from 'config'
import { Notification } from '../ui'
import { NoteMutations } from '../SubmissionMutations'
import SubmissionCancel from '../SubmissionCancel'
import { QUERY_ACTIVITY_INFO } from './operations'
import { EditIcon, Exit } from './Edit'
import { UserContext } from '../App'
import WithdrawReason from './WithdrawReason'
import Mailer from '../mailer'
import { withdrawalEmail } from './withdrawMailer'

const FlexP = styled.p`
  display: flex;
  align-items: center;
  & > button {
    flex: 0 0 auto;
  }
  & > span {
    flex: 1 1 50%;
    margin-left: ${th('gridUnit')};
  }
`
const RouteButton = styled(Button)`
  align-items: center;
  display: inline-flex;
`
const StatusDesc = [
  'not yet submitted',
  'not yet submitted',
  'submission error',
  'needs reviewer approval',
  'needs submission QA',
  'tagging',
  'has file loading errors',
  'needs XML QA',
  'has XML errors',
  'XML errors corrected',
  'needs final review',
  'has reviewer XML errors',
  'needs citation',
  'grant links sent, no files',
  'approved for Europe PMC',
  'failed EBI/NCBI loading',
  'sent to Europe PMC',
  'available in Europe PMC',
  'available in Europe PMC (ahead of print)',
  'being withdrawn',
  'withdrawal error',
]

const DeleteButton = ({ deleteMan }) => (
  <Action onClick={() => deleteMan()}>Confirm deletion</Action>
)

const RecoverButton = ({ callback, recoverMan }) => (
  <RouteButton onClick={() => recoverMan(callback())}>
    <Icon color="currentColor" size={2.5}>
      refresh-cw
    </Icon>
    Recover manuscript
  </RouteButton>
)

const WithdrawWithNotes = NoteMutations(WithdrawReason)

class StatusEdit extends React.Component {
  state = {
    edit: !this.props.lastStatus,
    selected: this.props.lastStatus,
    withdraw: false,
    displayMailer: false,
  }
  render() {
    const { manuscript, lastStatus, setStatus, close } = this.props
    const { organization, meta, decision } = manuscript
    const { notes } = meta
    const preprint = organization.name === 'Europe PMC Preprints'
    const isJournalIndexedInPubmed =
      manuscript.journal &&
      manuscript.journal.meta &&
      manuscript.journal.meta.pubmedStatus === true
    const { edit, selected, withdraw } = this.state
    const options = states.reduce((map, s, i) => {
      if (!['being-withdrawn', 'withdrawal-triage'].includes(s)) {
        map.push({
          value: s,
          label: `"${s}" – ${StatusDesc[i]}${
            s === lastStatus ? ' [most recent]' : ''
          }${s === manuscript.status ? ' [current]' : ''}`,
          disabled: s === manuscript.status,
        })
      }
      return map
    }, [])
    const foundNote = notes && notes.find(n => n.notesType === 'withdrawReason')
    const withdrawNote = foundNote && JSON.parse(foundNote.content)
    if (withdrawNote) {
      withdrawNote.id = foundNote.id
    }
    return (
      <UserContext.Consumer>
        {currentUser => (
          <React.Fragment>
            <H2>Send/Remove</H2>
            {manuscript.deleted ? (
              <React.Fragment>
                <Notification type="info">
                  {`Manuscript succesfully deleted. If not already sent, please send a message to the user(s) explaining the deletion.`}
                </Notification>
                <FlexP>
                  <SubmissionCancel
                    callback={close}
                    manuscriptId={manuscript.id}
                    refetch={[
                      {
                        query: QUERY_ACTIVITY_INFO,
                        variables: { id: manuscript.id },
                      },
                    ]}
                    showComponent={RecoverButton}
                  />
                </FlexP>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {manuscript.status === 'submission-error' && (
                  <FlexP>
                    <RouteButton
                      onClick={() => setStatus(lastStatus || 'submitted')}
                    >
                      <Icon color="currentColor" size={2.5}>
                        check-circle
                      </Icon>
                      Cancel submission error
                    </RouteButton>
                    <span>
                      Cancel submission error request and send back to
                      {(lastStatus &&
                        lastStatus === 'xml-triage' &&
                        ` XML errors state ('xml-triage')`) ||
                        (lastStatus &&
                          lastStatus === 'in-review' &&
                          ` Initial review state (in-review)`) ||
                        (lastStatus &&
                          lastStatus === 'file-error' &&
                          ` File loading error state (file-error)`) ||
                        (lastStatus &&
                          lastStatus === 'xml-error' &&
                          ` Reviewer reported error state (xml-error)`) ||
                        (lastStatus &&
                          lastStatus === 'xml-corrected' &&
                          ` XML errors corrected state (xml-corrected)`) ||
                        ` QA state ('submitted')`}
                    </span>
                  </FlexP>
                )}
                <React.Fragment>
                  <FlexP>
                    <RouteButton
                      onClick={() => {
                        setStatus(selected)
                        close()
                      }}
                    >
                      <Icon color="currentColor" size={2.5}>
                        send
                      </Icon>
                      Change status
                    </RouteButton>
                    <span>
                      send{selected === lastStatus && ' back'} to{' '}
                      <b>{selected}</b>
                      <Action onClick={() => this.setState({ edit: true })}>
                        <EditIcon />
                      </Action>
                      <em style={{ marginLeft: '8px' }}>
                        {selected === lastStatus && '(most recent status)'}
                      </em>
                      <br />
                      Please note that no emails will be automatically sent!
                    </span>
                  </FlexP>
                  {edit && (
                    <RadioGroup
                      name="Statuses"
                      onChange={v => this.setState({ selected: v })}
                      options={options}
                      value={selected}
                    />
                  )}
                  {decision === 'accepted' ? (
                    <React.Fragment>
                      <Notification type="info">
                        {` Manuscript has been sent to the repository and must be withdrawn to be removed. `}
                      </Notification>
                      <FlexP>
                        <RouteButton
                          disabled={manuscript.status === 'being-withdrawn'}
                          onClick={e => this.setState({ withdraw: true })}
                        >
                          <Icon color="currentColor" size={2}>
                            skip-back
                          </Icon>
                          Withdraw manuscript
                        </RouteButton>
                        {withdraw && preprint && (
                          <span>
                            {` Are you certain? `}
                            <Action
                              onClick={() => {
                                setStatus('being-withdrawn')
                                close()
                              }}
                            >
                              Confirm withdraw.
                            </Action>
                          </span>
                        )}
                      </FlexP>
                      {withdraw && !preprint && (
                        <WithdrawWithNotes
                          manuscript={manuscript}
                          onConfirm={async grants => {
                            if (grants === 'remove') {
                              await this.props.updateGrants([])
                            }
                            setStatus('being-withdrawn')
                            close()
                          }}
                          withdrawNote={withdrawNote}
                        />
                      )}
                      {!preprint &&
                        !isJournalIndexedInPubmed &&
                        [
                          'being-withdrawn',
                          'withdrawal-triage',
                          'published',
                        ].includes(manuscript.status) && (
                          <React.Fragment>
                            <Notification type="info">
                              {['published'].includes(manuscript.status)
                                ? `After withdrawing, notify PubMed that the corresponding PubMed citation needs to be deleted.`
                                : `Notify PubMed that the corresponding PubMed citation needs to be deleted.`}
                            </Notification>
                            <Button
                              disabled={['published'].includes(
                                manuscript.status,
                              )}
                              onClick={() =>
                                this.setState({
                                  displayMailer: withdrawalEmail(manuscript),
                                })
                              }
                            >
                              Notify PubMed
                            </Button>
                          </React.Fragment>
                        )}
                    </React.Fragment>
                  ) : (
                    <FlexP>
                      <RouteButton
                        onClick={e =>
                          e.currentTarget.nextElementSibling.classList.remove(
                            'hidden',
                          )
                        }
                      >
                        <Icon color="currentColor" size={2.5}>
                          trash-2
                        </Icon>
                        Delete manuscript
                      </RouteButton>
                      <span className="hidden">
                        {` Are you certain? `}
                        <SubmissionCancel
                          manuscriptId={manuscript.id}
                          refetch={[
                            {
                              query: QUERY_ACTIVITY_INFO,
                              variables: { id: manuscript.id },
                            },
                          ]}
                          showComponent={DeleteButton}
                        />
                      </span>
                    </FlexP>
                  )}
                </React.Fragment>
              </React.Fragment>
            )}
            <Exit close={close} />
            {this.state.displayMailer && (
              <Mailer
                cc={this.state.displayMailer.cc}
                close={() => this.setState({ displayMailer: false })}
                currentUser={currentUser}
                manuscript={this.props.manuscript}
                message={this.state.displayMailer.message}
                recipients={this.state.displayMailer.recipients}
                showSuccess={this.state.displayMailer.showSuccess}
                subject={this.state.displayMailer.subject}
              />
            )}
          </React.Fragment>
        )}
      </UserContext.Consumer>
    )
  }
}

export default StatusEdit
