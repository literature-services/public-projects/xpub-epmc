import React from 'react'
import { Action } from '@pubsweet/ui'
import { Toggle, Loading, LoadingIcon } from '../ui'

class SubmitHighlights extends React.Component {
  state = { references: '', matchList: [] }
  componentDidMount() {
    if (this.props.highlights === 'empty') {
      this.setState({ matchList: null })
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.highlights !== this.props.highlights) {
      this.makeList(this.props.highlights)
    } else if (
      this.props.highlights &&
      this.state.matchList &&
      this.state.matchList.length === 0
    ) {
      this.makeList(this.props.highlights)
    }
  }
  makeList = matchString => {
    const sups = matchString.match(
      /(supp(?!lemented|lementing|lementation|ort[\W]|ort(?!ing)|orting [^i]|ress|ly|li|er|ose)[\w.]*\s*[\w.]*|extended data|append[\w.]*)\s*(?!and)\w*(,?\s*(\d+|\w\d+|\w))*((?!(\s*and\s*|-|\u2013|\u2014|&)supp|extended|fig|table)(\s*and\s*|-|\u2013|\u2014|&)\w*)?/gi,
    )
    const figs = matchString.match(
      /(\W(supp[\w.]*|extended data)\s*|\W?)fig[\w.]*\s*(\d+|\w\d+)\w*(,?\s*(\d+|\w\d+))*((\s*and\s*|-|\u2013|\u2014|&)(?!supp|extended|fig|table)(\d+|\w\d+|\w+)\w*)*/gi,
    )
    const tables = matchString.match(
      /\W((supp[\w.]*|extended data)\s*)?table[\w]*\s*(\d+|\w\d+)\w*(,?\s*(\d+|\w\d+))*((\s*and\s*|-|\u2013|\u2014|&)(?!supp|extended|fig|table)(\d+|\w\d+|\w+)\w*)*/gi,
    )
    const matches = [
      {
        title: 'Supplements',
        array: [
          ...new Set(sups && sups.map(match => match.replace(/\s\s+/g, ' '))),
        ],
      },
      {
        title: 'Figures',
        array: [
          ...new Set(
            figs &&
              figs.map(match =>
                match.replace(/^\W/, '').replace(/\s\s+/g, ' '),
              ),
          ),
        ].filter(match => !/^(supp|extended data)/i.test(match)),
      },
      {
        title: 'Tables',
        array: [
          ...new Set(
            tables &&
              tables.map(match =>
                match.replace(/^\W/, '').replace(/\s\s+/g, ' '),
              ),
          ),
        ].filter(match => !/^(supp|extended data)/i.test(match)),
      },
    ]
    const matchList = matches.filter(m => m.array.length > 0)
    this.setState({
      matchList: (matchList.length > 0 && matchList) || null,
      references: matchList[0] && matchList[0].title,
    })
  }
  render() {
    const { references, matchList } = this.state
    if (matchList === null) {
      return <div>No results</div>
    }
    return (
      <div style={{ padding: 0 }}>
        <Toggle>
          {matchList.map(regex => (
            <Action
              className={references === regex.title && 'current'}
              key={regex.title}
              onClick={() => this.setState({ references: regex.title })}
            >
              {regex.title}
            </Action>
          ))}
        </Toggle>
        {matchList.length === 0 && (
          <Loading>
            <LoadingIcon />
          </Loading>
        )}
        {matchList.map(regex => {
          if (references === regex.title) {
            return (
              <ul key={regex.title}>
                {regex.array.sort().map(match => (
                  <li key={match}>{match}</li>
                ))}
              </ul>
            )
          }
          return null
        })}
      </div>
    )
  }
}

export default SubmitHighlights
