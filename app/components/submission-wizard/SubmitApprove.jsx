import React from 'react'
import { Mutation } from 'react-apollo'
import { withRouter } from 'react-router'
import { europepmcUrl } from 'config'
import { Button, CheckboxGroup, Icon } from '@pubsweet/ui'
import { A, Portal, Buttons, Fieldset } from '../ui'
import { SUBMIT_MANUSCRIPT } from './operations'

const SubmitApprove = ({
  showResponse,
  adminButton,
  disabled,
  xml,
  manuscriptId,
  preprint,
  status,
  checkedBoxes,
  close,
  journal,
  releaseDelay,
  setChecked,
  history,
}) => {
  const options = [
    {
      value: '1',
      label: `This manuscript has been accepted for publication in ${journal} and includes all modifications resulting from the peer review process.`,
    },
    {
      value: '2',
      label: `I request that this manuscript be publicly accessible through Europe PMC ${releaseDelay &&
        (releaseDelay === '0'
          ? 'immediately'
          : `${releaseDelay} month${
              releaseDelay === '1' ? '' : 's'
            }`)} after the publisher's official date of final publication, and I confirm that I have retained the right to deposit this version of the manuscript with Europe PMC; or, I confirm this complies with the publisher's policy.`,
    },
    {
      value: '3',
      label:
        'This manuscript is the result of research supported, in whole or in part, by a member of the Europe PMC Funders Group.',
    },
  ]
  return (
    <Mutation mutation={SUBMIT_MANUSCRIPT}>
      {(submitManuscript, { data }) => {
        const submit = async newStatus => {
          if (newStatus === 'tagging') {
            await showResponse('info', 'Creating tagger package', false)
          }
          const variables = { data: { id: manuscriptId, status: newStatus } }
          if (newStatus === 'submitted' && xml) {
            variables.data.status = 'xml-triage'
          }
          const submit = await submitManuscript({ variables })
          const { errors } = submit.data.submitManuscript
          if (errors.length > 0) {
            await showResponse('error', errors[0].message)
          } else {
            await showResponse()
          }
          if (close) {
            close()
          }
        }
        if (
          adminButton === 'tagging' ||
          (preprint && adminButton === 'review')
        ) {
          return (
            <Button
              disabled={disabled}
              onClick={() => submit('tagging')}
              primary
            >
              Approve for tagging
            </Button>
          )
        }
        if (adminButton === 'review') {
          return (
            <Button
              disabled={disabled}
              onClick={() => {
                submit('submitted')
              }}
              primary
            >
              Send for new review
            </Button>
          )
        }
        if (preprint) {
          return (
            <Button
              disabled={disabled}
              onClick={() => {
                submit('submitted')
              }}
              primary
            >
              Submit preprint
            </Button>
          )
        }
        return (
          <Portal transparent>
            <Fieldset>
              <legend>Please certify the following statements are true:</legend>
              <CheckboxGroup
                name="reviewer-checklist"
                onChange={c => setChecked(c)}
                options={options}
                value={checkedBoxes}
              />
            </Fieldset>
            <p
              style={{
                fontStyle: 'italic',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Icon size={2}>help-circle</Icon>
              <span>
                What is the{' '}
                <A href={`${europepmcUrl}/Funders/`} target="_blank">
                  Europe PMC Funder&apos;s Group?
                </A>
              </span>
            </p>
            <Buttons right>
              <Button
                disabled={
                  checkedBoxes.length <= 0 ||
                  checkedBoxes.reduce(
                    (acc, val) => parseInt(acc, 10) + parseInt(val, 10),
                  ) < 6
                }
                onClick={() => submit('submitted')}
                primary
              >
                Confirm &amp; submit
              </Button>
              <Button onClick={() => close()}>Cancel</Button>
            </Buttons>
          </Portal>
        )
      }}
    </Mutation>
  )
}

export default withRouter(SubmitApprove)
