import React from 'react'
import { europepmcUrl } from 'config'
import { H2, H3, Action } from '@pubsweet/ui'
import { ExpandingHead, A } from '../ui'

const H = ({ children, id }) => (
  <ExpandingHead className="h4Style" h={H3} iconSize={2.5}>
    {children}
  </ExpandingHead>
)

const GetCredit = ({ pmcid, canceled }) => (
  <React.Fragment>
    <H>How do I get credit for my work?</H>
    <div>
      {!pmcid && (
        <p>
          {`Once the article is released to Europe PMC, you can link the article to your ORCID iD from the article page.`}
          {!canceled &&
            ' An invitation to do so will be included in the email notifying you of the release.'}
        </p>
      )}
      <p>
        {`An ORCID iD makes it easier for authors to get recognition for their work by distinguishing themselves from other researchers. Claiming articles to an ORCID iD builds a body of articles associated with that iD. `}
        <A href="https://orcid.org/" target="_blank">
          Find out more about ORCID
        </A>
        .
      </p>
      {pmcid && (
        <p>
          {`If you are an author of this article, you can `}
          <A
            href={`${europepmcUrl}/article/PMC/${pmcid}?claimWithOrcid`}
            style={{ display: 'inline-flex', alignItems: 'center' }}
            target="_blank"
          >
            link it to your ORCID iD
          </A>
          {` from the article page in Europe PMC. If you don’t have an ORCID iD, you will be directed to register for one.`}
        </p>
      )}
    </div>
  </React.Fragment>
)

const Canceled = ({ pmcid }) => (
  <React.Fragment>
    <H2>Grant links help</H2>
    <H>Why don’t I need to submit files?</H>
    <p>{`Europe PMC ${
      pmcid ? 'has already received' : 'is scheduled to receive'
    } a copy of this article that meets all of the open access funding requirements.`}</p>
    <H>When will my funding be linked?</H>
    <p>
      The grants you have selected will be linked to the record on Europe PMC
      within 48 hours.
    </p>
  </React.Fragment>
)

const CreateInfo = ({
  currentStep,
  preprint,
  planS,
  openMailer,
  funderMailer,
  cancelCheck,
  inEPMC,
  status,
  reviewer,
  doi,
}) => {
  switch (currentStep) {
    case -1:
      return (
        <div className="createInfo">
          <H2>Getting started help</H2>
          <H>What do I need to complete the submission?</H>
          {preprint ? (
            <ul>
              <li>
                {`A preprint from one of the servers indexed by Europe PMC and discoverable as part of the Europe PMC COVID-19 set`}
              </li>
              <li>Preprint title or DOI, for searching</li>
              <li>
                {`All files submitted to preprint server, including supplemental data`}
              </li>
            </ul>
          ) : (
            <ul>
              <li>The grant numbers</li>
              <li>The final manuscript file as a PDF or Microsoft Word file</li>
              <li>
                {`The figures and supplementary files in an `}
                <A href="/user-guide/allowedfiles" target="_blank">
                  accepted type
                </A>
              </li>
              <li>
                {`If the citation isn’t found, you will need the name of the publishing journal`}
              </li>
              <li>The embargo period required by the publishing journal</li>
            </ul>
          )}
          <H>What if I only want to link grants?</H>
          <p>{`Please continue with the submission process. Once you have provided citation information and your grant numbers, the system will automatically recognise that manuscript files are not required.`}</p>
          <H>How does the process work?</H>
          <ol>
            <li>The manuscript is submitted to Europe PMC plus</li>
            <li>
              {`Europe PMC converts the manuscript into a web version and archival PDF`}
            </li>
            <li>The reviewing author will be sent an email to approve these</li>
            <li>
              If no corrections are submitted the files will be automatically
              released 14 days later
            </li>
            <li>
              The manuscript will be made freely available on Europe PMC and
              PubMed Central (PMC)
            </li>
          </ol>
          <H>How long will it take?</H>
          <p>
            {preprint
              ? `If approval and communication are prompt, the process typically takes 1-2 weeks`
              : `If all manuscript files are submitted in appropriate formats, and if approval and communication are prompt, the process can take as little as two weeks.`}
          </p>
        </div>
      )
    case 0:
      return (
        <div className="createInfo">
          {cancelCheck ? (
            <Canceled pmcid={inEPMC} />
          ) : (
            <React.Fragment>
              <H2>Citation information help</H2>
              <H>Why do I need to search PubMed?</H>
              <p>{`Europe PMC records will be matched with PubMed records. If your article already has a PubMed record, we will use this information so you do not have to input it manually.`}</p>
              <H>What if my citation is not found in PubMed?</H>
              <p>{`If you do not find your citation in PubMed, use the “Click to enter citation manually” link to continue. You should provide the title and journal name, and if possible, the DOI.`}</p>
              <H>What is a DOI?</H>
              <p>{`A Digital Object Identifier (DOI) is a persistent identifier that stays the same, even if the web address of your article changes over time. It is a more stable method of finding your article. We need the part of the DOI that starts with ‘10…’`}</p>
            </React.Fragment>
          )}
        </div>
      )
    case 1:
      return (
        <div className="createInfo">
          <H2>Funding help</H2>
          <H>Do I need to indicate all funding?</H>
          <div>
            <p>
              {`Please indicate all grants from `}
              <A href={`${europepmcUrl}/Funders/`} target="_blank">
                Europe PMC funders
              </A>
              {`. Because funders have different requirements, especially regarding open access, indicating all funders allows us to inform you of the requirements during submission.`}
            </p>
          </div>
          <H>What if a funder isn’t on the list?</H>
          <div>
            <p>
              {`Only grants from `}
              <A href={`${europepmcUrl}/Funders/`} target="_blank">
                Europe PMC funders
              </A>
              {` will appear in our search, and those are the only ones that need to be indicated.`}
            </p>
            <p>{`If a particular grant or project that you are expecting to see is not listed when you search please verify that the proper PI name was entered. You should use an initial rather than the full first name, e.g. "R" instead of "Robert".`}</p>
            <p>
              {`If you can’t locate your grant and/or project in the list, `}
              <Action onClick={() => openMailer(true)}>
                contact the Helpdesk
              </Action>
              {`, and provide the grant/contract number, project title, and contact PIs name.`}
            </p>
          </div>
          <div className="hidden" id="planSDateInfo">
            <H>
              {`Why do I need to select the date submitted to the publishing journal?`}
            </H>
            <p>
              {`One or more of the grants you’ve selected is from a `}
              <A href="/user-guide#Licensing_policies" target="_blank">
                Plan S funder
              </A>
              {`, and this date is needed to determine your funding requirements.`}
            </p>
          </div>
        </div>
      )
    case 2:
      return (
        <div className="createInfo">
          {cancelCheck ? (
            <Canceled pmcid={inEPMC} />
          ) : (
            <React.Fragment>
              <H2>Access help</H2>
              {planS ? (
                <React.Fragment>
                  <H>Why does my funder require a CC BY license?</H>
                  <div>
                    <p>
                      <A href="/user-guide#Licensing_policies" target="_blank">
                        Plan S
                      </A>
                      {` is an initiative for Open Access publishing supported by Coalition S, an international consortium of research funding organisations. `}
                      <A
                        href="https://www.coalition-s.org/guidance-on-the-implementation-of-plan-s/"
                        target="_blank"
                      >
                        Plan S requires
                      </A>
                      {` that publications resulting from funded research be made openly available immediately, with a `}
                      <A
                        href="https://creativecommons.org/licenses/by/4.0/"
                        target="_blank"
                      >
                        Creative Commons Attribution (CC BY) license
                      </A>
                      {`, unless an exception has been granted by the funder.`}
                    </p>
                    <p>
                      {`The European Research Council `}
                      <A
                        href="https://ec.europa.eu/info/horizon-europe_en"
                        target="_blank"
                      >
                        Horizon Europe
                      </A>
                      {` programme also requires outputs to be made openly available immediately, with a CC BY license, or a license with equivalent rights.`}
                    </p>
                  </div>
                  <H>What if I have a CC BY license exception?</H>
                  <p>
                    {`If you have been granted a license exception and have a license other than CC BY, please `}
                    <Action onClick={() => openMailer(funderMailer)}>
                      contact your funder for confirmation
                    </Action>
                    .
                  </p>
                  <H>What if my journal requires a different embargo?</H>
                  <p>{`If the journal that has accepted your article requires an embargo, please contact your funder(s) for advice.`}</p>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <H>How do I select an embargo period?</H>
                  <div>
                    <p>
                      {`The embargo periods listed are those allowed by your grant funders. They indicate the delay between publication date and the date that the manuscript becomes freely available in Europe PMC and PubMed Central. Europe PMC strongly encourages authors to make their manuscripts available as soon as possible.  `}
                    </p>
                    <p>
                      {`If you’re not sure which embargo period to choose, check your funder's requirements with the `}
                      <A href="http://sherpa.ac.uk/fact/" target="_blank">
                        SHERPA/FACT tool
                      </A>
                      {`, or if there is a conflict between the period required by your journal and those allowed by your funder, please `}
                      <Action onClick={() => openMailer(true)}>
                        contact the Helpdesk
                      </Action>
                    </p>
                  </div>
                </React.Fragment>
              )}
            </React.Fragment>
          )}
        </div>
      )
    case 3:
      return (
        <div className="createInfo">
          <H2>Files help</H2>
          {preprint ? (
            <React.Fragment>
              <H>Which files should I upload?</H>
              <p>{`Please upload the same files loaded to the preprint server, including both manuscript and supplementary material.`}</p>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <H>Which versions of files should I upload?</H>
              <p>
                {`The accepted, peer-reviewed version of the manuscript should be submitted. The author may submit the version of record for Creative-Commons-licensed articles; otherwise, if the author wishes to submit a PDF created by the publisher, the publisher must have granted permission for this use. Check your journal’s version policy at `}
                <A
                  href="http://www.sherpa.ac.uk/romeo/index.php"
                  target="_blank"
                >
                  SHERPA/RoMEO
                </A>
                {`. Supplementary material that has been submitted to the journal in support of the manuscript should also be included.`}
              </p>
            </React.Fragment>
          )}
          <H>What is the best way to label files?</H>
          <p>
            {`The labels are used to help put your manuscript together as intended. Provide a label that matches the corresponding file as mentioned in the text.`}
          </p>
          <H>What file formats can I use?</H>
          <p>
            {`For the manuscript text, only Microsoft Word or PDF files can be used. For figures, tables, and supplementary files, check `}
            <A href="/user-guide/allowedfiles" target="_blank">
              our list of accepted file types
            </A>
            .
          </p>
        </div>
      )
    case 4:
      return (
        <div className="createInfo">
          <H2>Reviewer help</H2>
          <H>Who can be a reviewer?</H>
          <p>
            Only one of the authors of a manuscript may be designated as a
            reviewer.
          </p>
          <H>What is the reviewing author responsible for?</H>
          <p>{`The reviewer is responsible for checking that the manuscript matches what was accepted for publication by the journal, and approving the final web version of the manuscript which will be released to Europe PMC and PubMed Central.`}</p>
        </div>
      )
    case 5:
      return (
        <div className="createInfo">
          <H2>Next steps</H2>
          {['submitted', 'in-review'].includes(status) && (
            <React.Fragment>
              {reviewer ? (
                <React.Fragment>
                  <H>What is the next step?</H>
                  <p>{`If the submission materials are found to be complete, a web version will be created, and you will receive an email requesting that you check for errors before the files are released to Europe PMC automatically after 14 days.`}</p>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <H>What happens next?</H>
                  <p>{`An email will be sent to the reviewer, requesting that they log into the Europe PMC plus submission system and review the submission for completeness.`}</p>
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          {status !== 'link-existing' && (
            <React.Fragment>
              <H>{`When will the ${
                preprint ? 'preprint' : 'submission'
              } be in Europe PMC?`}</H>
              <p>{`If all manuscript files are submitted in appropriate formats, and if approval and communication are prompt, the process can take as little as two weeks. You will receive an email notification when the article is released to Europe PMC.`}</p>
            </React.Fragment>
          )}
          {(status === 'link-existing' || !reviewer || !doi) && (
            <GetCredit pmcid={inEPMC} />
          )}
        </div>
      )
    default:
      return null
  }
}

export default CreateInfo
