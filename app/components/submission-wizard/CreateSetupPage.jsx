import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { graphql, compose } from 'react-apollo'
import { europepmcUrl } from 'config'
import { H1, H2, Icon, Button } from '@pubsweet/ui'
import {
  SplitPage,
  InfoPanel,
  StepPanel,
  Notification,
  Buttons,
  BoldCheck,
  A,
} from '../ui'
import PubMedSearch, { PreprintSearch } from '../citation-search'
import CreateHeader from './CreateHeader'
import CreateInfo from './CreateInfo'
import { USER_MANUSCRIPTS } from '../dashboard/operations'
import { CREATE_NOTE } from '../operations'
import { CREATE_MANUSCRIPT } from './operations'

const SplitPageShort = styled(SplitPage)`
  min-height: calc(100vh - (${th('gridUnit')} * 20));
`

const freshState = {
  error: null,
  id: null,
  showInfo: false,
  checked: false,
}

class CreateSetup extends React.Component {
  state = freshState
  manuscriptSetup = async (citationData, component) => {
    if (citationData) {
      const { createNote, createManuscript, history } = component.props
      const { note, planSReq, ...citation } = citationData
      const created = await createManuscript({
        variables: { data: { ...citation } },
      })
      const { manuscript, errors } = created.data.createManuscript
      const { id, version } = manuscript || {}
      const route = id ? `/submission/${id}/create` : '/create'
      if (errors && errors.length > 0) {
        const error = errors[0]
        window.scrollY = 0
        window.pageYOffset = 0
        document.scrollingElement.scrollTop = 0
        component.setState({ error, id })
        setTimeout(() => {
          history.push(route)
          if (!id) component.setState(freshState)
        }, (id && 3500) || 4000)
      } else {
        if (note) {
          await createNote({
            variables: {
              data: {
                manuscriptId: id,
                manuscriptVersion: version,
                ...note,
              },
            },
          })
        }
        history.push(route)
      }
    }
  }
  render() {
    const { showInfo, id, error, checked } = this.state
    const { location, history } = this.props
    const enter = location.search.substr(1)
    const preprint = enter === 'preprint'
    return (
      <SplitPageShort>
        <StepPanel>
          {!enter ? (
            <div>
              <H1 style={{ textAlign: 'center' }}>Start a new submission</H1>
              <H2>Criteria for submission</H2>
              <p>
                {`Manuscripts submitted to Europe PMC plus must be peer-reviewed, accepted for publication, and derive from research supported, in whole or in part, by one or more `}
                <A href={`${europepmcUrl}/Funders/`}>Europe PMC funders</A>.
              </p>
              <BoldCheck
                checked={checked}
                label="I accept these requirements."
                onChange={e => this.setState({ checked: e.target.checked })}
              />
              <Buttons right style={{ marginTop: '100px' }}>
                <Button
                  disabled={!checked}
                  onClick={() => history.push({ search: '?manuscript' })}
                  primary
                >
                  Next
                </Button>
              </Buttons>
            </div>
          ) : (
            <div>
              <CreateHeader currentStep={0} preprint={preprint} />
              {error && (
                <Notification type={error.type}>
                  {error.message}
                  {id && ` Redirecting to ${id}.`}
                </Notification>
              )}
              <H2>Citation information</H2>
              {preprint ? (
                <PreprintSearch
                  changeNote
                  citationData={d => {
                    d.preprint = true
                    this.manuscriptSetup(d, this)
                  }}
                />
              ) : (
                <PubMedSearch
                  changeInfo={v => this.setState(v)}
                  changeNote
                  citationData={d => this.manuscriptSetup(d, this)}
                />
              )}
            </div>
          )}
        </StepPanel>
        <InfoPanel>
          <div
            className={showInfo ? 'showInfo' : 'hidden'}
            id="more-info"
            onClick={() => this.setState({ showInfo: !showInfo })}
          >
            More Info
            {!showInfo && <Icon>chevron-right</Icon>}
            {showInfo && <Icon>chevron-down</Icon>}
          </div>
          <CreateInfo currentStep={enter ? 0 : -1} preprint={preprint} />
        </InfoPanel>
      </SplitPageShort>
    )
  }
}

export default compose(
  graphql(CREATE_MANUSCRIPT, {
    name: 'createManuscript',
    options: { refetchQueries: [{ query: USER_MANUSCRIPTS }] },
  }),
  graphql(CREATE_NOTE, { name: 'createNote' }),
)(CreateSetup)
