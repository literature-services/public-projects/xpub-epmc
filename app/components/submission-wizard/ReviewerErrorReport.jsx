import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import { Button, H2, H3 } from '@pubsweet/ui'
import { B, Buttons, TextArea } from '../ui'
import { REJECT_MANUSCRIPT } from '../operations'

const ReviewerErrorReport = ({
  close,
  history,
  onChange,
  manuscript,
  message,
  submitter,
}) => (
  <Mutation mutation={REJECT_MANUSCRIPT}>
    {(rejectManuscript, { data }) => {
      const reject = async () => {
        await rejectManuscript({
          variables: {
            data: {
              manuscriptId: manuscript.id,
              manuscriptVersion: manuscript.version,
              notesType: 'userMessage',
              content: JSON.stringify(message),
            },
          },
        })
        history.push('/')
      }
      return (
        <React.Fragment>
          <H2>Reject submission</H2>
          <p>
            {`Please describe any errors in the submission that the submitter needs to fix. This message will be sent to the submitter.`}
          </p>
          <p>These errors may include:</p>
          <ul>
            <li>Missing figures, tables, or supplementary data</li>
            <li>Corrupted text or equations</li>
            <li>Poor quality images</li>
          </ul>
          <p>
            Please <B>do not</B>
            {` reject the manuscript because of typos, missing copy edits, or visible review artifacts. The version supplied to Europe PMC is usually the `}
            <em>final accepted</em> rather than <em>published</em>
            {` version of your manuscript. This version may contain typos and other errors fixed later in the publication process, which you will be able to address in the final version review. We will not retain any visible review artifacts.`}
          </p>
          <H3>Message for submitter, {submitter}</H3>
          <TextArea
            label="Enter your comments"
            onChange={e => onChange(e.target.value)}
            rows={10}
            value={message}
          />
          <Buttons right>
            <Button disabled={!message} onClick={() => reject()} primary>
              Reject and send message
            </Button>
            <Button onClick={close}>Cancel</Button>
          </Buttons>
        </React.Fragment>
      )
    }}
  </Mutation>
)

export default withRouter(ReviewerErrorReport)
