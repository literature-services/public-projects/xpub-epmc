import React from 'react'
import { Button } from '@pubsweet/ui'
import { Buttons } from '../ui'
import SelectReviewer from '../SelectReviewer'
import { NoteMutations } from '../SubmissionMutations'

class EditReviewer extends React.Component {
  state = { newReviewer: null }
  render() {
    const {
      manuscript,
      changeNote,
      deleteNote,
      newNote,
      close,
      ...props
    } = this.props
    const { meta, teams } = manuscript
    const { notes, fundingGroup } = meta
    const reviewerNote = notes
      ? notes.find(n => n.notesType === 'selectedReviewer')
      : null
    let reviewer = null
    if (teams && teams.find(team => team.role === 'reviewer')) {
      const [rev] = teams.find(team => team.role === 'reviewer').teamMembers
      reviewer = {
        id: rev.user.id,
        name: rev.alias.name,
      }
    }
    const { teamMembers } =
      (teams && teams.find(team => team.role === 'submitter')) || {}
    const [submitter = null] = teamMembers || []
    const { newReviewer } = this.state
    return (
      <React.Fragment>
        <SelectReviewer
          {...props}
          funding={fundingGroup || []}
          reviewer={reviewer}
          reviewerNote={reviewerNote}
          setReviewerNote={v => this.setState({ newReviewer: v })}
          submitter={submitter}
        />
        <Buttons right>
          <Button
            disabled={!newReviewer}
            onClick={async () => {
              let result = false
              if (reviewerNote && newReviewer) {
                if (newReviewer === 'delete') {
                  result = await deleteNote(reviewerNote.id)
                } else {
                  result = await changeNote({
                    id: reviewerNote.id,
                    notesType: 'selectedReviewer',
                    content: JSON.stringify(newReviewer),
                  })
                }
              } else {
                result = await newNote({
                  notesType: 'selectedReviewer',
                  content: JSON.stringify(newReviewer),
                })
              }
              if (result) {
                close()
              }
            }}
            primary
          >
            Save
          </Button>
          <Button onClick={() => close()}>Cancel</Button>
        </Buttons>
      </React.Fragment>
    )
  }
}

export default NoteMutations(EditReviewer)
