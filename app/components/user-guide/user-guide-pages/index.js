export { AllowedFiles } from './AllowedFiles'
export { VideoTutorials } from './VideoTutorials'
export { PreprintFaqs } from './PreprintFaqs'
export { HowTo } from './HowTo'
