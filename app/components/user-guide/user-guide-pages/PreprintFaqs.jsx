import React from 'react'
import { H2, H3 } from '@pubsweet/ui'
import { europepmcUrl } from 'config'
import { A } from '../../ui'
import { Head, GuidePage, Menu, Content } from './UserGuideElements'

const H = ({ id, children }) => (
  <Head className="h4Style" h={H3} iconSize={2.5} id={id}>
    {children}
  </Head>
)

export const PreprintFaqs = () => (
  <GuidePage>
    <Menu />
    <Content>
      <H2>Preprint FAQs</H2>
      <p>
        {`In July 2020, Europe PMC began indexing the full text of COVID-19 related preprints to make them searchable, alongside peer reviewed articles. In April 2022, Europe PMC also began indexing the full text of preprints supported by Europe PMC funders. These preprints are processed through the Europe PMC plus submission system. Learn more about the `}
        <A href={`${europepmcUrl}/Preprints`}>
          COVID-19 full text preprints initiative
        </A>
        , or find answers to specific questions below.
      </p>
      <H id="whatiseuropepmc">What is Europe PMC?</H>
      <div>
        <p>
          <A href={`${europepmcUrl}/About`} target="_blank">
            Europe PMC
          </A>
          {` is a free repository of life sciences articles, funded by 36 biomedical funders. Europe PMC contains over 40 million abstracts and 7.7 million full text articles (including content from PubMed and PubMed Central). Since 2018 Europe PMC has also `}
          <A
            href="https://blog.europepmc.org/2018/07/preprints.html"
            target="_blank"
          >
            indexed preprints
          </A>
          {` from `}
          <A
            href={`${europepmcUrl}/Preprints#preprint-servers`}
            target="_blank"
          >
            preprint servers
          </A>
          {` including medRxiv, bioRxiv, Preprints.org, F1000R, arXiv, ChemRxix, Research Square, SSRN and SciELO. Europe PMC links articles to data behind the paper, citations, author ORCID identifiers, reviews and protocols.`}
        </p>
      </div>
      <H id="funding">
        {`Who has funded the initiative to include the full text of COVID-19 related preprints in Europe PMC?`}
      </H>
      <div>
        <p>
          {`This initiative was financially supported by Wellcome in partnership with the UK Medical Research Council (MRC) and the Swiss National Science Foundation (SNSF) from June 2020 to July 2022, and will continue to be supported by Wellcome until July 2023.`}
        </p>
      </div>
      <H id="whydisplay">
        Where did you get my preprint and why are you displaying it?
      </H>
      <div>
        <p>
          {'Europe PMC indexes preprints from a number of '}
          <A
            href={`${europepmcUrl}/Preprints#preprint-servers`}
            target="_blank"
          >
            preprint servers.
          </A>
          {` The preprints on these servers are publicly available, and as a repository of open access content, Europe PMC has permission to download the PDF and supplementary files, and to index the full text. This indexing makes the preprint discoverable, alongside journal-published articles, from a single search. Displaying the preprint in Europe PMC makes it available for Europe PMC users to read.`}
        </p>
        <p>
          {`For preprints supported by Europe PMC funders, and all COVID-19 preprints, an email notification is sent to the first corresponding author before the preprint is made available, to give them an opportunity to review the formatting if desired. For preprints released under a Creative Commons license, Europe PMC will automatically display the full text once a period of one week has elapsed. For preprints released without a license allowing reuse, Europe PMC can only display the full text with author approval.`}
        </p>
      </div>
      <H id="email">How did you get my email address?</H>
      <div>
        <p>
          {`As you are the corresponding author of the article, your email address was publicly available on the preprint server website and within the preprint article PDF file.`}
        </p>
      </div>
      <H id="license">What is the Europe PMC license?</H>
      <div>
        <p>
          {`The Europe PMC license can be applied to full text preprints to ensure they are available for text and data mining. If your preprint has an all rights reserved license, or no specified license, we will ask your permission to add a Europe PMC license so that it can be available via the Europe PMC open access subset.`}
        </p>
        <p>
          <A href={`${europepmcUrl}/downloads/openaccess`} target="_blank">
            The Europe PMC open access subset
          </A>
          {` is a collection of articles protected by copyright, but made available under a Creative Commons license, or similar, that generally allows more liberal redistribution and reuse than a traditional copyrighted work. The Europe PMC open access subset is available for text and data mining via the Europe PMC open APIs and bulk downloads.`}
        </p>
        <p>
          {`If your preprint has a restrictive Creative Commons license (for example CC BY-ND or CC BY-NC-ND) it will already be included in the open access subset, but we will ask your permission to add a Europe PMC license in order to allow the display within the article of highlighted annotations resulting from text and data mining.`}
        </p>
        <p>
          {`If your preprint is supported by Europe PMC funders but is not COVID-19 related, the full text will only appear in Europe PMC if it has a Creative Commons license.`}
        </p>
      </div>
      <H id="impact">
        {`Will there be any impact on my article getting published in a journal if it appears in Europe PMC?`}
      </H>
      <div>
        <p>
          {`Europe PMC is not a publisher. Europe PMC is a discovery tool – by indexing preprints it allows access to the latest research alongside peer-reviewed studies, in one place. Roughly one-third of preprints in Europe PMC have been `}
          <A
            href={`${europepmcUrl}/search?query=HAS_PUBLISHED_VERSION%3AY`}
            target="_blank"
          >
            matched to journal-published versions
          </A>
          {`.`}
        </p>
      </div>
      <H id="server">
        {`My preprint is already on a preprint server. What is the benefit of it being on Europe PMC as well?`}
      </H>
      <div>
        <p>
          {`The main benefit of your preprint appearing on Europe PMC is increased visibility and discovery by researchers, scientific curators, clinicians and policy makers.`}
        </p>
        <ul>
          <li>
            {`Researchers can search for preprints in one place, rather than having to know about, and visit, many different preprint servers to carry out the same search.`}
          </li>
          <li>
            {`Preprints in Europe PMC are indexed alongside peer-reviewed articles, providing access to a wide range of related research on a topic.`}
          </li>
          <li>
            {`A search on Europe PMC finds relevant words and phrases matching the user’s search query in the title, abstract and full text of your preprint, unlike a search the preprint server which may only search through the abstract.`}
          </li>
          <li>
            {`Europe PMC has several mechanisms to include links to related material - whether it is to `}
            <A
              href={`${europepmcUrl}/article/PPR/PPR158775#data`}
              target="_blank"
            >
              data behind the paper
            </A>{' '}
            to substantiate conclusions,{' '}
            <A href={`${europepmcUrl}/article/PPR/PPR161858`} target="_blank">
              claiming of preprints to author ORCID records
            </A>
            , inclusion of preprints in{' '}
            <A
              href={`${europepmcUrl}/article/PPR/PPR30268#impact`}
              target="_blank"
            >
              citation networks
            </A>
            , or to{' '}
            <A
              href={`${europepmcUrl}/article/PPR/PPR118574#reviews`}
              target="_blank"
            >
              comments on peer review platforms
            </A>
            ,{' '}
            <A
              href={`${europepmcUrl}/article/PPR/PPR63523#impact`}
              target="_blank"
            >
              metrics
            </A>
            , or{' '}
            <A
              href={`${europepmcUrl}/article/MED/3798106#protocols-materials`}
              target="_blank"
            >
              reagents
            </A>
            .
          </li>
          <li>
            {`Many other organisations and apps access data from Europe PMC programmatically via its open APIs and bulk downloads. This means that your preprint could be available to a wider audience and its content could be used for deeper scientific analysis.`}
          </li>
        </ul>
      </div>
      <H id="versions">
        {`I have revised my preprint. Why is Europe PMC asking me to approve an older version?`}
      </H>
      <div>
        <p>
          {`A goal of the COVID-19 preprint initiative at Europe PMC is to create as complete a scientific record as possible, including the indexing of multiple versions of preprints. See `}
          <A href={`${europepmcUrl}/Preprints#preprint-display`}>
            more information on how preprint versions are displayed in Europe
            PMC.
          </A>
        </p>
        <p>
          {`You may be asked to approve a previous version of a preprint, which you have revised and replaced. Please be assured that after you approve this version, any later versions submitted to your preprint server will also be automatically processed by Europe PMC plus. During approval you will be given the choice to have Europe PMC plus automatically process all later versions, or to be notified to check each one. Your selection to automatically or manually check later versions can be changed at any time.`}
        </p>
      </div>
      <H id="orcidclaiming">Can I claim my preprint to my ORCID record?</H>
      <div>
        <p>
          {`Yes, preprints can be claimed to your ORCID record. Find out more about `}
          <A href={`${europepmcUrl}/Help#linktoORCID`} target="_blank">
            how to claim a preprint in Europe PMC to your ORCID
          </A>
          .
        </p>
      </div>
      <H id="publication">
        How do I identify if a preprint has subsequently been published?
      </H>
      <div>
        <p>
          {`Europe PMC links preprints to published versions of the article, and vice-versa. If a peer-reviewed, published version of the article is matched to the preprint, a link is provided in the orange notification bar above the abstract.`}
        </p>
        <p>
          {`You can search for preprints which have been published in a journal using the search query `}
          <A
            href={`${europepmcUrl}/search?query=HAS_PUBLISHED_VERSION%3AY`}
            target="_blank"
          >
            HAS_PUBLISHED_VERSION:Y
          </A>
          {`, or peer-reviewed journal articles which are based on a preprint using the query `}
          <A
            href={`${europepmcUrl}/search?query=HAS_PREPRINT%3AY`}
            target="_blank"
          >
            HAS_PREPRINT:Y
          </A>
          .
        </p>
      </div>
      <H id="remove">How do I remove my preprint from Europe PMC?</H>
      <div>
        <p>
          {`We cannot amend the record that is held by the preprint provider, but if the provider removes the preprint and updates CrossRef, the record will be subsequently removed in Europe PMC.`}
        </p>
      </div>
    </Content>
  </GuidePage>
)
