import React from 'react'
import * as mime from 'mime-types'
import { H2, H3, H4 } from '@pubsweet/ui'
import { TypesWithNames } from '../../upload-files'
import { Table } from '../../ui'
import { Head, GuidePage, Menu, MenuLink, Content } from './UserGuideElements'

const fileList = TypesWithNames.manuscriptTypes.concat(TypesWithNames.fileTypes)

const mimetypes = fileList.reduce((types, obj) => {
  const { mime: type } = obj
  const mimetype = type.substring(0, type.indexOf('/'))
  if (mime.extension(type) && !types.some(t => t === mimetype)) {
    types.push(mimetype)
  }
  return types
}, [])

export const AllowedFiles = () => (
  <GuidePage>
    <Menu>
      <ul>
        {mimetypes.map(type => {
          const title = type.charAt(0).toUpperCase() + type.slice(1)
          return (
            <li key={title}>
              <MenuLink id={`${title}_files`}>{title} files</MenuLink>
            </li>
          )
        })}
      </ul>
    </Menu>
    <Content>
      <H2>Accepted file types</H2>
      <H3>Manuscript text file</H3>
      <p>This must be either a PDF or Microsoft Word file.</p>
      <H3>Figures and supplemental files</H3>
      {mimetypes.map(type => {
        const title = type.charAt(0).toUpperCase() + type.slice(1)
        return (
          <React.Fragment key={title}>
            <Head h={H4} iconSize={2.5} open>{`${title} files`}</Head>
            <Table>
              <tbody>
                <tr>
                  <th>Kind of document</th>
                  <th>File extension</th>
                  <th>MIME type</th>
                </tr>
                {fileList
                  .filter(({ mime: subtype }) => subtype.startsWith(type))
                  .map(
                    ({ mime: subtype, name }) =>
                      mime.extension(subtype) && (
                        <tr key={subtype}>
                          <td>{name}</td>
                          <td>.{mime.extension(subtype)}</td>
                          <td>{subtype}</td>
                        </tr>
                      ),
                  )}
              </tbody>
            </Table>
          </React.Fragment>
        )
      })}
    </Content>
  </GuidePage>
)
