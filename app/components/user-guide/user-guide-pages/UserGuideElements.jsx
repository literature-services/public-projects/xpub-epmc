import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link } from '@pubsweet/ui'
import { ExpandingHead } from '../../ui'

export const GuidePage = styled.div`
  display: flex;
  padding-top: calc(${th('gridUnit')} * 5);
`
export const Menu = styled.div`
  flex: 0 0 240px;
  padding-right: calc(${th('gridUnit')} * 5);
  ul {
    position: sticky;
    top: calc(${th('gridUnit')} * 4);
    margin: 0;
    padding: 0;
    list-style-type: none;
    li {
      margin-bottom: calc(${th('gridUnit')} * 1.5);
      a {
        font-weight: 600;
      }
    }
  }
  @media screen and (max-width: 900px) {
    display: none;
  }
`
export const Content = styled.div`
  flex: 1 1 1000px;
  & > h2 {
    margin-top: 0;
  }
`

export const MenuLink = ({ children, id }) => (
  <Link
    onClick={() => document.getElementById(id).scrollIntoView(true)}
    to={`#${id}`}
  >
    {children}
  </Link>
)

export class Head extends React.Component {
  state = {
    curr: false,
    open: !!this.props.open,
    id: this.props.id
      ? this.props.id
      : this.props.children.split(' ').join('_'),
  }
  componentDidMount() {
    this.openElement()
  }
  componentDidUpdate() {
    const loc = window.location.hash
    const { curr, id } = this.state
    const newCurr = loc === `#${id}`
    if (curr !== newCurr) {
      this.openElement()
    }
  }
  openElement() {
    const loc = window.location.hash
    const { open, id } = this.state
    const curr = loc === `#${id}`
    this.setState({ open: !!open || curr, curr })
    if (curr) {
      const el = document.getElementById(id)
      el.scrollIntoView(true)
    }
  }
  render() {
    const { children, h, iconSize = 3, className } = this.props
    const { id, open } = this.state
    return (
      <ExpandingHead
        className={className}
        h={h}
        iconSize={iconSize}
        id={id}
        open={open}
        to={!open ? `#${id}` : '#'}
      >
        {children}
      </ExpandingHead>
    )
  }
}
