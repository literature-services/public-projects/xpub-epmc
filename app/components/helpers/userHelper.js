const { localStorage } = window

const userHelper = {
  setToken: token => {
    localStorage.setItem('token', token)
  },
  clearCurrentUser: () => {
    localStorage.removeItem('token')
  },
}

export default userHelper
