/* eslint-disable no-template-curly-in-string */
import React from 'react'
import { debounce } from 'lodash'
import { withRouter } from 'react-router-dom'
import styled, { createGlobalStyle } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import moment from 'moment'
import { Button, ErrorText, H2, H3, RadioGroup } from '@pubsweet/ui'
import { SearchSelect, Center, LoadingIcon } from '../ui'
import { checkFunders, isBoolean } from './funderChecks'

const GrantInfo = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')};
  span {
    margin-right: ${th('gridUnit')};
  }
`
const PlanSDateInfo = createGlobalStyle`
  #planSDateInfo {
    display:block;
  }
`

function mapGrants(grants) {
  if (Array.isArray(grants)) {
    return grants.map(grant => {
      const pi = {
        surname: grant.Person.FamilyName,
        email: grant.Person.Email,
      }
      if (grant.Person.Initials) pi.givenNames = grant.Person.Initials
      if (grant.Person.Title) pi.title = grant.Person.Title
      const g = {
        fundingSource: grant.Grant.Funder.Name,
        awardId: grant.Grant.Id,
        title: grant.Grant.Title,
        pi,
      }
      if (grant.Grant.StartDate) {
        g.startDate = moment
          .utc(grant.Grant.StartDate, 'YYYY-MM-DD')
          .toISOString()
      }
      if (grant.Grant.Category) {
        g.categories = Array.isArray(grant.Grant.Category)
          ? grant.Grant.Category
          : [grant.Grant.Category]
      }
      return g
    })
  }
}

class GrantSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: '',
      hitcount: 0,
      page: 0,
      availableGrants: [],
      selectedGrants: this.props.selectedGrants,
      grantsErr: '',
      loading: false,
      showDateCheck: false,
      dateCheckDate: '',
      dateCheckVal: null,
    }
    this.onQueryChange = this.onQueryChange.bind(this)
    this.searchGrist = debounce(this.searchGrist.bind(this), 250)
    this.onLoadMore = this.onLoadMore.bind(this)
    this.grantsAdded = this.grantsAdded.bind(this)
    this.changePlanS = this.changePlanS.bind(this)
  }
  componentDidMount() {
    const { selectedGrants, funders, decision } = this.props
    if (selectedGrants && selectedGrants.length > 0) {
      const { showDateCheck, dateCheckDate, dateCheckVal } = checkFunders(
        funders,
        selectedGrants,
      )
      this.setState({ showDateCheck, dateCheckDate, dateCheckVal })
      if (decision !== 'extant' && showDateCheck && !isBoolean(dateCheckVal)) {
        this.props.disableNext(true)
      }
    }
  }
  async changePlanS(planSVal) {
    const { planS, isPlanS } = this.props
    if (planS && !planSVal) {
      await this.props.deleteNote(planS.id)
    } else if (isPlanS && !planSVal) {
      await this.props.deleteNote(null)
    }
    if (!planS && planSVal) {
      await this.props.newNote({ notesType: 'planS' })
    }
  }
  async grantsAdded(grants) {
    if (grants.length > 0) {
      const { funders, decision } = this.props
      const {
        showDateCheck,
        dateCheckDate,
        dateCheckVal,
        isPlanS,
        grantList,
      } = checkFunders(funders, grants, this.state.dateCheckVal)
      this.setState({
        selectedGrants: grantList,
        grantsErr: '',
        showDateCheck,
        dateCheckDate,
        dateCheckVal,
      })
      if (isPlanS || (!isPlanS && !showDateCheck)) {
        await this.changePlanS(isPlanS)
      }
      if (decision !== 'extant' && showDateCheck && !isBoolean(dateCheckVal)) {
        this.props.disableNext(true)
      } else {
        this.props.disableNext(false)
      }
      // change grants last, important
      await this.props.changedGrants(grantList)
    } else {
      await this.changePlanS(false)
      this.setState({
        selectedGrants: [],
        grantsErr: 'At least one grant is required.',
        showDateCheck: false,
        dateCheckVal: null,
        dateCheckDate: '',
      })
      // change grants last, important
      await this.props.changedGrants([])
    }
  }
  async searchGrist(newState) {
    const { query } = this.state
    const url = `/grist/rest/api/search?query=gid:%22${query}%22%7Cpi:%22${query}%22&page=1`
    const response = await fetch(url, {
      headers: new Headers({
        Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      }),
    })
    const json = await response.json()
    const hitcount = json.HitCount
    if (hitcount) {
      if (parseInt(hitcount, 10) === 0) {
        this.setState(newState)
      } else if (parseInt(hitcount, 10) === 1) {
        newState.availableGrants = mapGrants([json.RecordList.Record])
        newState.hitcount = hitcount
        this.setState(newState)
      } else {
        newState.availableGrants = mapGrants(json.RecordList.Record)
        newState.hitcount = hitcount
        this.setState(newState)
      }
    } else {
      this.setState({
        grantsErr:
          "We're experiencing a problem with our grants search. Please try again later.",
      })
    }
  }

  formatDate = d => moment(d).format('D MMM YYYY')

  async onLoadMore() {
    const { query } = this.state
    const page = this.state.page + 1
    const url = `/grist/rest/api/search?query=gid:%22${query}%22%7Cpi:%22${query}%22&page=${page}`
    const response = await fetch(url, {
      headers: new Headers({
        Authorization: `Bearer ${window.localStorage.getItem('token')}`,
      }),
    })
    const json = await response.json()
    const availableGrants = this.state.availableGrants.concat(
      mapGrants(
        Array.isArray(json.RecordList.Record)
          ? json.RecordList.Record
          : [json.RecordList.Record],
      ),
    )
    this.setState({
      page,
      availableGrants,
      loading: false,
    })
  }
  onQueryChange(e) {
    const query = e.target.value
    this.setState({ query: e ? query : '', loading: true })
    const newState = {
      page: 1,
      availableGrants: [],
      hitcount: 0,
      loading: false,
    }
    if (query.trim().length > 2) {
      this.searchGrist(newState)
    } else {
      this.setState(newState)
    }
  }

  async onRadioSelect(v) {
    this.props.disableNext(false)
    this.setState({ dateCheckVal: v === 'true' })
    const { isPlanS, grantList } = checkFunders(
      this.props.funders,
      this.state.selectedGrants,
      v === 'true',
    )
    await this.changePlanS(isPlanS)
    // change grants last, important
    await this.props.changedGrants(grantList)
  }

  render() {
    const {
      query,
      loading,
      grantsErr,
      hitcount,
      availableGrants,
      selectedGrants,
      showDateCheck,
      dateCheckDate,
      dateCheckVal,
    } = this.state
    return (
      <div>
        <H2>Funding</H2>
        <H3>Add all grants that support this manuscript</H3>
        <SearchSelect
          displaySelected="`${option.fundingSource}, ${option.awardId} (${option.pi.surname}): ${option.title}`"
          icon="chevron_down"
          invalidTest={grantsErr}
          label="Search by Grant # or by PI surname followed by initial(s), e.g. Smith A"
          onInput={this.onQueryChange}
          optionsOnChange={this.grantsAdded}
          placeholder="Search by grant ID or PI surname"
          query={query}
          selectedOptions={selectedGrants}
        >
          {loading && (
            <Center>
              <LoadingIcon />
            </Center>
          )}
          {availableGrants.map(grant => (
            <SearchSelect.Option
              data-option={grant}
              key={`${grant.pi.title ? `${grant.pi.title}_` : ''}${
                grant.pi.givenNames ? `${grant.pi.givenNames}_` : ''
              }${grant.pi.surname}_${grant.awardId}_${grant.fundingSource}`}
              propKey={`${grant.pi.title ? `${grant.pi.title}_` : ''}${
                grant.pi.givenNames ? `${grant.pi.givenNames}_` : ''
              }${grant.pi.surname}_${grant.awardId}`}
            >
              {grant.title}
              <GrantInfo>
                <span>
                  <b>PI:</b>
                  {` ${grant.pi.title ? `${grant.pi.title} ` : ''}${
                    grant.pi.givenNames ? `${grant.pi.givenNames} ` : ''
                  }${grant.pi.surname}`}
                </span>
                <span>
                  <b>Funder:</b>
                  {` ${grant.fundingSource}`}
                </span>
                <span>
                  <b>ID:</b>
                  {` ${grant.awardId}`}
                </span>
              </GrantInfo>
            </SearchSelect.Option>
          ))}
          {hitcount > availableGrants.length && (
            <SearchSelect.Option propKey="loadMore">
              <Center>
                {loading ? (
                  <LoadingIcon />
                ) : (
                  <Button
                    onClick={e => {
                      e.stopPropagation()
                      this.setState({ loading: true })
                      this.onLoadMore()
                    }}
                  >
                    Load more results
                  </Button>
                )}
              </Center>
            </SearchSelect.Option>
          )}
        </SearchSelect>

        {this.props.decision !== 'extant' && showDateCheck && (
          <React.Fragment>
            <PlanSDateInfo />
            <H3>
              When was the manuscript submitted to the publishing journal?
            </H3>
            <RadioGroup
              name="manuscriptSubmissionDate"
              onChange={v => this.onRadioSelect(v)}
              options={[
                {
                  value: 'false',
                  label: `Before ${this.formatDate(dateCheckDate)}`,
                },
                {
                  value: 'true',
                  label: `After ${this.formatDate(dateCheckDate)}`,
                },
              ]}
              value={
                isBoolean(dateCheckVal) ? dateCheckVal.toString() : dateCheckVal
              }
            />
          </React.Fragment>
        )}

        {this.state.grantsErr && <ErrorText>{grantsErr}</ErrorText>}
      </div>
    )
  }
}

export default withRouter(GrantSearch)
