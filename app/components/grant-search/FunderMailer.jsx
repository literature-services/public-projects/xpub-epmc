import React from 'react'
import { createBrowserHistory } from 'history'
import { Button, H3 } from '@pubsweet/ui'
import { Notification, Buttons } from '../ui'
import { checkFunders } from './funderChecks'

const FunderMailer = ({ close }) => (
  <React.Fragment>
    <Notification type="success">Email message sent</Notification>
    <H3>Thank you for contacting your funder.</H3>
    <p>
      We will email you once your funder has confirmed your exception and you
      can complete your submission.
    </p>
    <Buttons>
      <Button
        onClick={() => createBrowserHistory({ forceRefresh: true }).push('/')}
        primary
      >
        Exit
      </Button>
    </Buttons>
  </React.Fragment>
)

export const mailFunders = ({ grants = [], title, username, funders }) => {
  const { mailList } = checkFunders(funders, grants)
  const recipients = mailList.map(g => g.fundingSource)
  const ids = mailList.map(g => g.awardId)
  const message = `Dear ${recipients.join(', ')},\n
Please confirm my license exception, associated with grant${
    ids.length > 1 ? 's' : ''
  } ${ids.join(
    ', ',
  )}, to the Europe PMC Helpdesk, and inform them of the specific license type that has been approved for the submission of the paper "${title}"\n
Thank you,
${username.title ? `${username.title} ` : ''}${username.givenNames} ${
    username.surname
  }`
  return {
    recipients: ['helpdesk'].concat(recipients),
    message,
    subject: 'License policy exception confirmation requested',
    showSuccess: FunderMailer,
  }
}
