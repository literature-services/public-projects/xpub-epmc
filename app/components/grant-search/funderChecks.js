const isBoolean = val => typeof val === 'boolean'

const strategyFactory = ({ required = 'date', startDate, value, strategy }) => {
  const planSDate =
    value || (strategy && strategy.value) || '2021-01-01T00:00:00.000Z'
  const defaultStrategy = new Date(startDate) > new Date(planSDate)
  return {
    required,
    planSDate,
    isPlanS: isBoolean(strategy) ? strategy : defaultStrategy,
  }
}

const categoryStrategy = ({ value = 'Plan S', categories = [] }) => {
  const strategy = categories.some(c => c === value)
  const obj = { required: 'categories', strategy }
  return strategyFactory(obj)
}

const submitStrategy = ({ value, dateCheckVal }) => {
  const obj = { value, required: 'boolean', strategy: dateCheckVal }
  return strategyFactory(obj)
}

const makeFunderList = funders => {
  const funderObject = {}
  const strategies = {
    'grant date': strategyFactory,
    'submit date': submitStrategy,
    category: categoryStrategy,
  }
  funders.forEach(f => {
    const { funderName, checkStrategy, checkValue } = f
    funderObject[funderName] = obj => {
      if (checkValue) obj.value = checkValue
      return strategies[checkStrategy](obj)
    }
  })
  return funderObject
}

const checkFunders = (funders, grants = [], dateCheck) => {
  const funderCheckList = makeFunderList(funders)
  const mailList = []
  const grantList = []
  let showDateCheck = false
  let dateCheckDate = null
  let dateCheckVal = isBoolean(dateCheck) ? dateCheck : null
  const isPlanS = grants.reduce((result, grant) => {
    const { fundingSource, startDate, categories } = grant
    const strategy = funderCheckList[fundingSource]
    if (strategy) {
      const { required, planSDate } = strategy({})
      if (required === 'boolean') {
        showDateCheck = true
        dateCheckDate = planSDate
        dateCheckVal = isBoolean(dateCheck) ? dateCheck : grant.dateChecked
        if (isBoolean(dateCheckVal)) {
          const { isPlanS } = strategy({ dateCheckVal })
          grant.dateChecked = dateCheckVal
          if (isPlanS) {
            grantList.push(grant)
            mailList.push(grant)
            return isPlanS
          }
        }
      } else if (required === 'categories') {
        if (categories) {
          const { isPlanS } = strategy({ categories })
          if (isPlanS) {
            grantList.push(grant)
            mailList.push(grant)
            return isPlanS
          }
        }
      } else if (required === 'date') {
        const { isPlanS } = strategy({ startDate })
        if (isPlanS) {
          grantList.push(grant)
          mailList.push(grant)
          return isPlanS
        }
      }
    }
    grantList.push(grant)
    return result
  }, false)
  return {
    showDateCheck,
    dateCheckDate,
    dateCheckVal,
    isPlanS,
    grantList,
    mailList,
  }
}

module.exports = { checkFunders, isBoolean }
