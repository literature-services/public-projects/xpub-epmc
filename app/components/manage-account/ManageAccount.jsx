import React from 'react'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { H1, H2, Icon, Link } from '@pubsweet/ui'
import { Loading, LoadingIcon, Page } from '../ui/'
import AccountDetails from './AccountDetailsContainer'
import MergeAccount from './MergeAccount'
import { GET_USER, GET_ROLES_BY_TYPE } from './operations'

const Container = styled.div`
  .reset-pass {
    margin-top: 20px;
  }
  .search-again {
    float: right;
    font-size: 14px;
  }
`

const ManageAccount = props => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_USER}
    variables={{ id: props.location.pathname.split('/')[2] }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const userData = data
      return (
        <Query
          fetchPolicy="cache-and-network"
          query={GET_ROLES_BY_TYPE}
          variables={{ organization: true }}
        >
          {({ data, loading }) => {
            if (loading) {
              return (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )
            }
            return (
              <Page>
                <Container>
                  <H1>Manage User: {props.location.pathname.split('/')[2]}</H1>
                  <Link className="search-again" to="/manage/users">
                    <Icon color="currentColor" size={2}>
                      search
                    </Icon>
                    Search again
                  </Link>
                  <H2>Edit profile and access</H2>
                  <AccountDetails
                    {...props}
                    roles={data.rolesByType}
                    user={userData.epmc_user}
                  />
                  <H2>Merge and delete</H2>
                  <p>Merge data from another account to this one</p>
                  <MergeAccount {...props} user={userData.epmc_user} />
                </Container>
              </Page>
            )
          }}
        </Query>
      )
    }}
  </Query>
)

export default ManageAccount
