import React from 'react'
import { Field } from 'formik'
import { graphql, compose } from 'react-apollo'
import URLSearchParams from 'url-search-params'
import {
  H1,
  Link,
  Button,
  Checkbox,
  TextField,
  Icon,
  ErrorText,
} from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { withTheme } from 'styled-components'
import { Page, Notification, NoClick, BoldCheck } from '../ui'
import SignInFooter from '../SignInFooter'
import UserCore from '../UserCore'
import { GET_NOTE } from './operations'

const Header = styled(H1)`
  margin-bottom: calc(${th('gridUnit')} * 3);
`
const Flex = styled.div`
  display: flex;
  align-items: center;
  margin: calc(${th('gridUnit')} * 3) auto;
`
const Signin = styled.p`
  margin: 0;
  font-size: ${th('fontSizeHeading4')};
  a {
    font-weight: 600;
  }
  margin-left: calc(${th('gridUnit')} * 2);
`
const AcceptDiv = styled.div``
const Notice = styled.p`
  margin: ${th('gridUnit')} 0 0;
  text-indent: calc(3 * ${th('gridUnit')});
`
const ShowPassword = styled(Checkbox)`
  position: absolute;
  right: 0;
  top: 0;
  font-size: ${th('fontSizeBaseSmall')};
`
const PasswordDetails = styled.div`
  display: flex;
  align-items: center;
  width: 50%;
  max-width: 100%;
  @media screen and (max-width: 1000px) {
    width: 600px;
  @media screen and (max-width: 800px) {
    display: block;
    width: 408px;
  }
`
const PasswordField = styled.div`
  position: relative;
  width: 408px;
  max-width: 100%;
  margin-right: calc(${th('gridUnit')} * 1.5);
  @media screen and (max-width: 800px) {
    width: 100%;
  }
`
const PasswordRules = styled(ErrorText)`
  display: flex;
  align-items: center;
  color: ${props => (props.success ? th('colorSuccess') : th('colorError'))};
`

const PasswordIcon = withTheme(({ theme, valid }) => (
  <Icon color={valid ? theme.colorSuccess : theme.colorError} size={2.5}>
    {valid ? 'check' : 'x'}
  </Icon>
))

class Signup extends React.Component {
  state = {
    accept: false,
    emptyPassword: true,
    validPassword: false,
    defaultValues: null,
  }
  componentDidMount() {
    if (this.props.getNote) {
      const { getNote } = this.props.getNote
      if (getNote) {
        const reviewer = getNote.content ? JSON.parse(getNote.content) : {}
        this.setDefaultUser(reviewer)
      }
    }
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.getNote &&
      prevProps.getNote.loading &&
      !this.props.getNote.loading
    ) {
      const { error, getNote } = this.props.getNote
      if (!error) {
        const reviewer = getNote.content ? JSON.parse(getNote.content) : {}
        this.setDefaultUser(reviewer)
      }
    }
  }
  setDefaultUser = reviewer => {
    const { name } = reviewer
    const { values } = this.props
    values.givenNames = (name && name.givenNames) || ''
    values.surname = (name && name.surname) || ''
    values.email = reviewer.email || ''
    this.setState({ defaultValues: values })
  }
  setButtonRef = button => {
    this.button = button
  }
  toggleAccept = () => {
    const { accept, showPassword } = this.state
    this.setState({
      accept: !accept,
      showPassword,
    })
  }
  toggleShowPassword = () => {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword,
    })
  }
  validatePassword = e => {
    const validPassword = e.target.value && e.target.value.length >= 8
    const emptyPassword = !e.target.value
    this.setState({
      emptyPassword,
      validPassword,
    })
  }
  PasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      {...props.field}
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      type="password"
    />
  )
  PlainPasswordInput = props => (
    <TextField
      invalidTest={props.invalidTest}
      label="Password"
      onBlur={e => this.validatePassword(e)}
      onKeyUp={e => this.validatePassword(e)}
      {...props.field}
    />
  )
  render() {
    const {
      defaultValues,
      showPassword,
      validPassword,
      emptyPassword,
      accept,
    } = this.state
    const {
      getNote,
      values,
      handleSubmit,
      location,
      submitCount,
      ...props
    } = this.props
    if (getNote && getNote.loading) return '...'
    return (
      <Page>
        <Header>Create a Europe PMC plus account</Header>
        {values.success && (
          <React.Fragment>
            <NoClick />
            <Notification type="success">{values.success}</Notification>
          </React.Fragment>
        )}
        {values.error && (
          <Notification type="error">{values.error}</Notification>
        )}
        <form onSubmit={handleSubmit}>
          <UserCore
            handleSubmit={handleSubmit}
            values={defaultValues || values}
            {...props}
          >
            <PasswordDetails>
              <PasswordField>
                {!this.state.showPassword && (
                  <Field component={this.PasswordInput} name="password" />
                )}

                {this.state.showPassword && (
                  <Field component={this.PlainPasswordInput} name="password" />
                )}
                <ShowPassword
                  checked={showPassword}
                  label="Show password"
                  onChange={() => this.toggleShowPassword()}
                />
                {(submitCount || !emptyPassword) && (
                  <PasswordRules success={validPassword}>
                    <PasswordIcon valid={validPassword} /> 8 character minimum
                  </PasswordRules>
                )}
              </PasswordField>
            </PasswordDetails>
          </UserCore>
          <AcceptDiv className="accept">
            <BoldCheck
              checked={accept}
              label="I have read and accept the privacy notice."
              onChange={() => this.toggleAccept()}
            />
            <Notice>
              {`See `}
              <Link
                target="_blank"
                to="//www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus"
              >
                Privacy notice
              </Link>
              {` for Europe PMC’s Advanced User Services.`}
            </Notice>
          </AcceptDiv>
          <Flex>
            <Button
              disabled={!accept}
              primary
              ref={this.setButtonRef}
              type="submit"
            >
              Create account
            </Button>
            <Signin>
              {`Already have an account? `}
              <br />
              <Link to={`/login${location.search}`}>Sign in.</Link>
            </Signin>
          </Flex>
        </form>
        <SignInFooter />
      </Page>
    )
  }
}

export default compose(
  graphql(GET_NOTE, {
    name: 'getNote',
    skip: props => {
      const next = new URLSearchParams(props.location.search)
      const params =
        next.get('next') &&
        new URLSearchParams(next.get('next').replace('/dashboard', ''))
      const id = params && params.get('accept')
      return !id
    },
    options: props => {
      const next = new URLSearchParams(props.location.search)
      const params =
        next.get('next') &&
        new URLSearchParams(next.get('next').replace('/dashboard', ''))
      const id = params && params.get('accept')
      return { skip: !id, variables: { id } }
    },
  }),
)(Signup)
