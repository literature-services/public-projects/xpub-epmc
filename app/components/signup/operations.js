import gql from 'graphql-tag'

export const GET_NOTE = gql`
  query GetNote($id: ID!) {
    getNote(id: $id) {
      content
    }
  }
`

export const SIGNUP_USER = gql`
  mutation($input: SignupUserInput) {
    epmc_signupUser(input: $input) {
      user {
        email
      }
      token
    }
  }
`
