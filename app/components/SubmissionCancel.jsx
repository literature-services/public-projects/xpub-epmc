import React from 'react'
import { Mutation } from 'react-apollo'
import { Button } from '@pubsweet/ui'
import { Portal, Buttons } from './ui'
import { USER_MANUSCRIPTS } from './dashboard/operations'
import {
  GET_MANUSCRIPT,
  DELETE_MANUSCRIPT,
  RECOVER_MANUSCRIPT,
} from './operations'

const DefaultShowComponent = ({ callback, close, deleteMan }) => (
  <Portal style={{ width: '600px' }} transparent>
    <p>
      {`Are you sure you want to cancel this submission? The submission and all of its details will be removed.`}
    </p>
    <Buttons right>
      <Button onClick={() => deleteMan(callback)} primary>
        Yes
      </Button>
      <Button onClick={() => close()}>No</Button>
    </Buttons>
  </Portal>
)

const SubmissionCancel = ({
  callback,
  close,
  manuscriptId,
  refetch = [
    {
      query: GET_MANUSCRIPT,
      variables: { id: manuscriptId },
    },
    {
      query: USER_MANUSCRIPTS,
    },
  ],
  showComponent: ShowComponent = DefaultShowComponent,
}) => (
  <Mutation mutation={DELETE_MANUSCRIPT} refetchQueries={() => refetch}>
    {(deleteManuscript, { data }) => {
      const deleteMan = async callBack => {
        await deleteManuscript({
          variables: {
            id: manuscriptId,
          },
        })
        callBack && callBack()
      }
      return (
        <Mutation mutation={RECOVER_MANUSCRIPT} refetchQueries={() => refetch}>
          {(recoverManuscript, { data }) => {
            const recoverMan = async callBack => {
              await recoverManuscript({
                variables: {
                  id: manuscriptId,
                },
              })
              callBack && callBack()
            }
            return (
              <ShowComponent
                callback={callback}
                close={close}
                deleteMan={deleteMan}
                recoverMan={recoverMan}
              />
            )
          }}
        </Mutation>
      )
    }}
  </Mutation>
)

export default SubmissionCancel
