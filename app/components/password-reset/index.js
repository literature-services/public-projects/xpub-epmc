import PasswordResetContainer from './PasswordResetContainer'
import PasswordResetEmailContainer from './PasswordResetEmailContainer'

export {
  PasswordResetContainer as PasswordReset,
  PasswordResetEmailContainer as PasswordResetEmail,
}
