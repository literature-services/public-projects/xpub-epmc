import React from 'react'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import config from 'config'
import { H1, Link, Button, TextField } from '@pubsweet/ui'
import { Page, Notification } from '../ui/'

const { appName } = config['pubsweet-client']['password-reset']

const PasswordInput = props => (
  <TextField
    className="double-column last-column"
    label="Password *"
    {...props.field}
    type="password"
  />
)
const ConfirmPasswordInput = props => (
  <TextField
    className="double-column"
    label="Confirm Password *"
    {...props.field}
    type="password"
  />
)

const PasswordReset = ({ errors, handleSubmit, touched, status }) => (
  <Page>
    <H1>Reset your {appName} password</H1>
    <p>Enter a new password for your {appName} account</p>
    {errors.password && touched.password && (
      <Notification type="error">{errors.password}</Notification>
    )}
    {!errors.password && errors.confirmPassword && touched.confirmPassword && (
      <Notification type="error">{errors.confirmPassword}</Notification>
    )}

    {!isEmpty(errors) && typeof errors === 'string' && (
      <Notification type="error">
        {errors === 'jwt expired' ? (
          <React.Fragment>
            {`Your password reset link has expired. Please `}
            <Link to="/password-reset">reset your password</Link>
            {` again.`}
          </React.Fragment>
        ) : (
          errors
        )}
      </Notification>
    )}

    {status && status.reset && (
      <Notification type="success">
        <strong>Password reset!</strong>
        <br />
        Your {appName} account password has been reset.{' '}
        <Link to="/login">Sign in using your new password</Link>.
      </Notification>
    )}

    <form onSubmit={handleSubmit} style={{ maxWidth: '60ch' }}>
      <Field
        component={PasswordInput}
        disabled={status && status.reset}
        name="password"
      />
      <Field
        component={ConfirmPasswordInput}
        disabled={status && status.reset}
        name="confirmPassword"
      />
      <Button disabled={status && status.reset} primary type="submit">
        Reset password
      </Button>
    </form>
  </Page>
)

// used by consumers
export default PasswordReset
