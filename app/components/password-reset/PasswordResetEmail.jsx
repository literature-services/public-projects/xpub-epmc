import React from 'react'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import config from 'config'
import { H1, H2, Button, TextField } from '@pubsweet/ui'
import { Page, Portal, A, Buttons, Notification } from '../ui/'

const { appName } = config['pubsweet-client']['password-reset']

const EmailInput = props => <TextField label="Email address" {...props.field} />

const PasswordResetEmail = ({
  errors,
  handleSubmit,
  handleReset,
  touched,
  status,
}) => (
  <Page>
    <H1>Reset your password</H1>
    {errors.email && touched.email && (
      <Notification type="error">{errors.email}</Notification>
    )}
    {!isEmpty(errors) && typeof errors === 'string' && (
      <Notification type="error">{errors}</Notification>
    )}
    <form onSubmit={handleSubmit} style={{ maxWidth: '60ch' }}>
      <Field component={EmailInput} name="email" />
      <Button primary type="submit">
        Send reset link
      </Button>
      {status && status.email && (
        <Portal transparent>
          <H2>{`We sent an email to ${status.email}`}</H2>
          <p>{`Please click the link in the email to reset the password of your ${appName} account.`}</p>
          <Buttons right>
            <Button onClick={handleReset} primary>
              Done
            </Button>
          </Buttons>
        </Portal>
      )}
    </form>
    <p>
      {`If you no longer have access to the email address for your account, contact the Europe PMC helpdesk at `}
      <A href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</A>
      {` or `}
      <span style={{ whiteSpace: 'no-wrap' }}>+44 1223 494118.</span>
    </p>
  </Page>
)

// used by consumers
export default PasswordResetEmail
