import React from 'react'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'

export const FullTextLabel = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  display: inline-block;
  background-color: ${th('colorSuccess')};
  color: ${th('colorTextReverse')};
  padding: 0 ${th('gridUnit')};
`

const Label = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  display: inline-flex;
  align-items: center;
  background-color: ${th('colorFurniture')};
  color: ${th('colorTextReverse')};
  padding: 0 ${th('gridUnit')};
  margin-right: ${th('gridUnit')};
  font-weight: normal;
`

const Container = styled.span`
  display: inline-flex;
  padding-right: calc(${th('gridUnit')} / 2);
  svg {
    stroke: ${props => props.color || props.theme.colorText};
    width: calc(${props => props.size} * ${th('gridUnit')});
    height: calc(${props => props.size} * ${th('gridUnit')});
  }
  ${override('ui.Icon')};
`

const CovidIcon = ({ color, size = 3, ...props }) => (
  <Container color={color} role="img" size={size} {...props}>
    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <style type="text/css">
        {`.covid0{fill: currentColor; stroke:none;} 
        .covid1{fill:none;stroke:currentColor;stroke-miterlimit:10;}`}
      </style>
      <path
        className="covid0"
        d="M12.2,4.9c-3.9,0-7.1,3.2-7.1,7.1s3.2,7.1,7.1,7.1c3.9,0,7.1-3.2,7.1-7.1S16.1,4.9,12.2,4.9z M8.4,14.5
  c-0.6,0-1.1-0.5-1.1-1.1c0-0.6,0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1C9.5,14,9,14.5,8.4,14.5z M11.4,10.4c-1.1,0-2.1-0.9-2.1-2.1
  c0-1.1,0.9-2.1,2.1-2.1c1.1,0,2.1,0.9,2.1,2.1C13.4,9.5,12.5,10.4,11.4,10.4z M14.1,16.9c-1.7,0-3-1.4-3-3s1.4-3,3-3s3,1.4,3,3
  S15.7,16.9,14.1,16.9z"
      />
      <g>
        <g>
          <line className="covid1" x1="11.8" x2="11.8" y1="18.7" y2="21.1" />
          <circle className="covid0" cx="11.8" cy="22.2" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="7.1" x2="5.4" y1="16.7" y2="18.3" />
          <circle className="covid0" cx="4.7" cy="19.1" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="5.3" x2="2.9" y1="11.8" y2="11.8" />
          <circle className="covid0" cx="1.8" cy="11.8" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="7.3" x2="5.7" y1="7.1" y2="5.4" />
          <circle className="covid0" cx="4.9" cy="4.7" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="12.2" x2="12.2" y1="5.3" y2="2.9" />
          <circle className="covid0" cx="12.2" cy="1.8" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="16.9" x2="18.6" y1="7.3" y2="5.7" />
          <circle className="covid0" cx="19.3" cy="4.9" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="18.7" x2="21.1" y1="12.2" y2="12.2" />
          <circle className="covid0" cx="22.2" cy="12.2" r="1.7" />
        </g>
        <g>
          <line className="covid1" x1="16.7" x2="18.3" y1="16.9" y2="18.6" />
          <circle className="covid0" cx="19.1" cy="19.3" r="1.7" />
        </g>
      </g>
    </svg>
  </Container>
)

export const PreprintLabel = ({ version, subjects = [] }) => (
  <Label>
    {subjects && subjects.includes('COVID-19') && (
      <CovidIcon
        color="currentColor"
        size={2}
        strokeWidth={2}
        title="COVID-19 Preprint"
      />
    )}
    {'Preprint '}
    {version && version > 0 ? ` v${Math.round(version)}` : ''}
  </Label>
)
