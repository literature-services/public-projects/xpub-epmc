import React from 'react'
import styled, { keyframes } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from '@pubsweet/ui'

const spin = keyframes`
  0% {
    transform: rotate(0deg);
    transform-origin: 50% 50%;
  }

  100% {
    transform: rotate(360deg);
    transform-origin: 50% 50%;
  }
`
const SpinIcon = styled(Icon)`
  &:hover {
    cursor: wait;
  }

  line {
    stroke-linejoin: round;
  }

  circle {
    animation: ${spin} 2s infinite linear;
    stroke-dasharray: 16;
    stroke-dashoffset: 0;
    stroke-linejoin: round;
  }
`

const LoadingIcon = props => <SpinIcon {...props}>plus_circle</SpinIcon>

const Loading = styled.div`
  padding: calc(${th('gridUnit')} * 2) 0;
  width: 100%;
  justify-content: center;
  align-items: center;
  color: ${th('colorPrimary')};
  display: inline-flex;
`

export { LoadingIcon, Loading }
