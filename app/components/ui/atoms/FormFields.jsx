import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Checkbox } from '@pubsweet/ui'

export const BoldCheck = styled(Checkbox)`
  font-weight: 600;
`

export const Fieldset = styled.fieldset`
  border: 0;
  padding: 0.01em 0 0 0;
  margin: 0;
  min-width: 0;

  legend {
    padding: 0;
    font-weight: 600;
    margin-top: ${th('gridUnit')};
    margin-bottom: calc(${th('gridUnit')} * 2);
  }
`
