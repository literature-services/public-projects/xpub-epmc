import React from 'react'
import styled from 'styled-components'
import { Icon, Action } from '@pubsweet/ui'

const Close = styled.p`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin: 0;
  & + h2 {
    margin-top: 0;
  }
`
const CloseButton = ({ size = 4, ...props }) => (
  <Action {...props} aria-label="Close" title="Close">
    <Icon color="currentColor" size={size}>
      x
    </Icon>
  </Action>
)

const CloseModal = ({ className, ...props }) => (
  <Close className={className}>
    <CloseButton {...props} />
  </Close>
)

export { CloseModal, CloseButton }
