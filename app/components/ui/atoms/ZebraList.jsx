import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const ZebraList = styled.ul`
  margin: 0;
  max-width: 100%;
  padding: 0;
  list-style-type: none;
  *:focus {
    outline: none;
    box-shadow: none;
  }
  *:focus-visible {
    outline-offset: -2px;
    outline: 2px ${th('borderStyle')} rgba(32, 105, 156, 0.8);
  }
`

const ZebraListItem = styled.li`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-top: 0;
  background-color: ${th('colorTextReverse')};
  padding: ${th('gridUnit')};
  &:first-child {
    border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  }
  &:nth-child(odd) {
    background-color: ${th('colorBackgroundHue')};
  }
`

export { ZebraList, ZebraListItem }
