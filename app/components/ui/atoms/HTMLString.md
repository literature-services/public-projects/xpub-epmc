A utility to translate strings with HTML and special characters into the unescaped version for display.

```js
<HTMLString string="<b>Look!</b>, here is an example &amp; everything."/>
```
