import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const SplitPage = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  min-height: calc(100vh - (${th('gridUnit')} * 28));
  box-sizing: border-box;

  @media screen and (max-width: 995px) {
    min-height: calc(
      100vh -
        (
          ${th('gridUnit')} *
            ${props => (props.withHeader ? '31.125' : '23.125')}
        )
    );
  }
  @media screen and (max-width: 870px) {
    height: auto;
    flex-direction: column-reverse;
    align-items: center;
  }
`
const StepPanel = styled.div`
  flex: 1 1 1000px;
  margin-bottom: 0;
  & > div {
    max-width: 1000px;
    margin: 0 0 0 auto;
    padding: 0 calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 4);
    background-color: ${th('colorBackground')};
  }
  @media screen and (max-width: 870px) {
    flex: 1 1 auto
    width: 100%;
    & > div {
      max-width: 100%;
    }
  }
`
const InfoPanel = styled.div`
  flex: 1 1 500px;
  margin-bottom: 0;
  background-color: ${th('colorBackgroundHue')};
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  box-shadow: -4px 0px 4px -4px ${th('colorBorder')};
  min-height: 100%;
  z-index: 1;
  & > div {
    max-width: 500px;
    margin: 0 auto 0 0;
    padding: 0 calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 2);
  }

  @media screen and (max-width: 870px) {
    min-height: 0;
    box-shadow: 0 2px 2px -2px ${th('colorBorder')};
    border-left: 0;
    border-bottom: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorBorder')};
    flex: 0 0 auto;
    width: 100%;
    & > div {
      max-width: 100%;
    }

    .createInfo {
      display: none;
    }

    #more-info {
      display: flex;
      align-items: center;
      width: 100%;
      padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 3);
      font-weight: 600;
      cursor: default;

      &.showInfo + .createInfo {
        display: block;
      }
    }
  }
`
export { SplitPage, StepPanel, InfoPanel }
