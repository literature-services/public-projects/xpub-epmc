import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'

const Center = styled.div`
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  justify-content: center;
`
const A = styled.a`
  ${override('ui.Link')};
`
const B = styled.strong`
  font-weight: 600;
`
const Callout = styled.div`
  margin-top: calc(${th('gridUnit')} * 4);
  background-color: ${props =>
    props.hue ? th('colorBackgroundHue') : th('colorTextReverse')};
  border: 1px solid ${th('colorPrimary')};
  padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 3)
    ${th('gridUnit')};
  & > *:first-child {
    margin-top: 0;
  }
`
const ReaderText = styled.span`
  clip: rect(1px, 1px, 1px, 1px);
  clip-path: inset(50%);
  height: 1px;
  width: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
`
export { Center, A, B, Callout, ReaderText }
