import React from 'react'
import styled from 'styled-components'
import { Icon } from '@pubsweet/ui'
import { th, override } from '@pubsweet/ui-toolkit'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: calc(
    ${props => (props.inline ? '0' : props.theme.gridUnit)} * 3
  );
  width: ${props => (props.width ? props.width : 'auto')};
  max-width: 100%;
  ${override('ui.Select')};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  display: block;
  ${override('ui.Label')};
  ${override('ui.Select.Label')};
`

const Dropdown = styled.div`
  max-width: 100%;
  box-sizing: border-box;
  position: relative;
  ${override('ui.Select.Dropdown')};
`
const borderColor = ({ theme, validationStatus = 'default' }) =>
  ({
    error: theme.colorError,
    success: theme.colorSuccess,
    default: theme.colorBorder,
  }[validationStatus])

const DropdownText = styled.select`
  width: 100%;
  border: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};
  height: calc(${th('gridUnit')} * 6);
  line-height: calc(${th('gridUnit')} * 6);
  border-radius: 6px;
  font-family: inherit;
  font-size: inherit;
  padding: 0 ${th('gridUnit')};
  -moz-appearance: none;
  -webkit-appearance: none;
  color: ${th('colorText')}
  background-color: ${th('colorBackgroundHue')};
  &:disabled, &[disabled] {
    opacity: 0.6;
  }
  &:disabled + *, &[disabled] + * {
    opacity: 0.6;
  }
  ${override('ui.Select.DropdownText')};
`
const DropdownIcon = styled(Icon)`
  box-sizing: border-box;
  background-color: ${th('colorBackgroundHue')};
  background-clip: padding-box;
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};
  height: calc(${th('gridUnit')} * 6);
  display: inline-flex;
  align-items: center;
  pointer-events: none;
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  ${override('ui.Select.DropdownIcon')};
`

const DefaultDropIcon = ({ icon }) => (
  <DropdownIcon color="currentColor">{icon}</DropdownIcon>
)

class Select extends React.Component {
  componentDidMount() {
    // generate a unique ID to link the label to the input
    // note this may not play well with server rendering
    this.inputId = `textfield-${Math.round(Math.random() * 1e12).toString(36)}`
  }
  render() {
    const {
      label,
      value = '',
      icon = 'chevrons_down',
      dropIcon: DropIcon = DefaultDropIcon,
      options = [],
      inline,
      width,
      ...props
    } = this.props
    return (
      <Root inline={inline} width={width}>
        {label && <Label htmlFor={this.inputId}>{label}</Label>}
        <Dropdown>
          <DropdownText id={this.inputId} value={value} {...props}>
            {options.map(opt =>
              typeof opt === 'object' ? (
                <option key={opt.value} value={opt.value}>
                  {opt.label}
                </option>
              ) : (
                <option key={opt} value={opt}>
                  {opt}
                </option>
              ),
            )}
          </DropdownText>
          <DropIcon icon={icon} />
        </Dropdown>
      </Root>
    )
  }
}

export default Select
