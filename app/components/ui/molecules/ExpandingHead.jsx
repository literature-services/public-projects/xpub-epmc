import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Link, Action, Icon } from '@pubsweet/ui'

const Head = styled.h3`
  &.h4Style {
    font-size: ${th('fontSizeHeading4')};
    margin-top: calc(${th('gridUnit')} * 3);
    margin-bottom: calc(-${th('gridUnit')} / 2);
  }
  .ispan {
    white-space: nowrap;
    span {
      vertical-align: sub;
      margin: 0;
    }
    span:first-of-type {
      display: inline;
    }
    span:last-child {
      display: none;
    }
  }
  &:not(.open) + * {
    clip: rect(1px, 1px, 1px, 1px);
    clip-path: inset(50%);
    height: 1px;
    width: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
  }
  &.open .ispan {
    span:first-of-type {
      display: none;
    }
    span:last-child {
      display: inline;
    }
  }
`
const R = ({ size }) => (
  <Icon color="currentColor" size={size} strokeWidth={3}>
    chevron-right
  </Icon>
)

const D = ({ size }) => (
  <Icon color="currentColor" size={size} strokeWidth={3}>
    chevron-down
  </Icon>
)

const Clicker = ({ children, ...props }) => {
  if (props.to) {
    return <Link {...props}>{children}</Link>
  }
  return (
    <Action
      {...props}
      style={{
        fontSize: 'inherit',
        lineHeight: 'inherit',
        fontWeight: 'inherit',
      }}
    >
      {children}
    </Action>
  )
}

const toggleOpen = secHead => {
  const expanded = secHead.getAttribute('aria-expanded')
  secHead.setAttribute('aria-expanded', expanded === 'true' ? 'false' : 'true')
  secHead.parentElement.classList.toggle('open')
}

const ExpandingHead = ({
  children,
  h: H,
  id,
  iconSize = 3,
  className,
  open,
  to,
}) => {
  const words = children.split(' ')
  const last = words.pop()
  return (
    <Head
      as={H}
      className={`${open ? 'open' : ''} ${className}`}
      id={id}
      key={children}
    >
      <Clicker
        aria-expanded={open}
        onClick={e => toggleOpen(e.currentTarget)}
        to={to}
      >
        {words.join(' ')}{' '}
        <span className="ispan">
          {last}
          <R size={iconSize} />
          <D size={iconSize} />
        </span>
      </Clicker>
    </Head>
  )
}

export default ExpandingHead
