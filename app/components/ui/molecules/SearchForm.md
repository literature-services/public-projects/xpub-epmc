A form combining a text input and a button side-by-side.

```js
initialState = { 
  query: '',
  event: '',
}

;<SearchForm
  buttonLabel="Search"
  label="Search for something"
  name="Search"
  onChange={event => setState({ value: event.target.value })}
  onSubmit={event => setState({ event: event })}
  placeholder="Your query"
  value={state.query}
/>
```

The default button text is "Search", but this can be changed.

```js
initialState = { 
  value: '',
  event: '',
}

;<SearchForm
  buttonLabel="Submit"
  label="Submit something"
  name="Submit"
  onChange={event => setState({ value: event.target.value })}
  onSubmit={event => setState({ event: event })}
  placeholder="Your input"
  value={state.value}
/>
```