import gql from 'graphql-tag'

export const GET_PROP = gql`
  query FindProp($name: String!) {
    findProp(name: $name) {
      value
    }
  }
`

export const LOGIN_USER = gql`
  mutation($input: SigninUserInput) {
    epmc_signinUser(input: $input) {
      user {
        id
        admin
        email
      }
      token
    }
  }
`

export const GET_USER = gql`
  query($email: String!) {
    userByEmail(email: $email) {
      id
    }
  }
`

export const GET_USER_ID = gql`
  query($id: ID!) {
    epmc_user(id: $id) {
      id
      identities {
        type
        email
        name {
          title
          givenNames
          surname
        }
        meta {
          publisher
        }
      }
      teams
    }
  }
`

export const ManuscriptFragment = gql`
  fragment ManuscriptFragment on Manuscript {
    id
    version
    status
    updated
    deleted
    decision
    formState
    pdfDepositState
    claiming {
      id
      givenNames
      surname
    }
    organization {
      name
    }
    journal {
      id
      journalTitle
      meta {
        nlmta
        pmcStatus {
          startDate
          endDate
        }
        pubmedStatus
        firstYear
        endYear
      }
    }
    meta {
      title
      articleType
      articleIds {
        pubIdType
        id
      }
      publicationDates {
        type
        date
        jatsDate {
          season
          day
          month
          year
        }
      }
      fundingGroup {
        fundingSource
        awardId
        title
        categories
        pi {
          surname
          givenNames
          title
          email
        }
        startDate
        dateChecked
      }
      releaseDelay
      subjects
      unmatchedJournal
      notes {
        id
        notesType
        content
      }
    }
    files {
      id
      updated
      type
      label
      filename
      url
      mimeType
    }
    teams {
      role
      teamMembers {
        user {
          id
        }
        alias {
          name {
            title
            surname
            givenNames
          }
        }
      }
    }
  }
`

export const GET_MANUSCRIPT = gql`
  query GetManuscript($id: ID!) {
    manuscript(id: $id) {
      ...ManuscriptFragment
    }
  }
  ${ManuscriptFragment}
`

export const UPDATE_MANUSCRIPT = gql`
  mutation UpdateManuscript($data: ManuscriptInput!) {
    updateManuscript(data: $data) {
      ... on Manuscript {
        status
      }
      ... on Error {
        type
        message
      }
    }
  }
`

export const REJECT_MANUSCRIPT = gql`
  mutation RejectManuscript($data: NewNoteInput!) {
    rejectManuscript(data: $data) {
      manuscript {
        ...ManuscriptFragment
      }
      errors {
        type
        message
      }
    }
  }
  ${ManuscriptFragment}
`

export const DELETE_NOTE = gql`
  mutation DeleteNote($id: ID!) {
    deleteNote(id: $id)
  }
`

export const CREATE_NOTE = gql`
  mutation CreateNote($data: NewNoteInput!) {
    createNote(data: $data)
  }
`

export const UPDATE_NOTE = gql`
  mutation UpdateNote($data: NoteInput!) {
    updateNote(data: $data)
  }
`

export const GET_PREPRINT_SELECTIONS = gql`
  query($id: ID!) {
    getPreprintSelections(manuscriptId: $id) {
      versionApproval
      addLicense
    }
  }
`

export const CHECK_DUPES = gql`
  query($id: ID!, $articleIds: [String], $title: String!) {
    checkDuplicates(id: $id, articleIds: $articleIds, title: $title) {
      manuscripts {
        ...ManuscriptFragment
      }
    }
  }
  ${ManuscriptFragment}
`

export const REPLACE_MANUSCRIPT = gql`
  mutation ReplaceManuscript($keepId: ID!, $throwId: ID!) {
    replaceManuscript(keepId: $keepId, throwId: $throwId)
  }
`

export const DELETE_MANUSCRIPT = gql`
  mutation DeleteManuscript($id: ID!) {
    deleteManuscript(id: $id)
  }
`

export const RECOVER_MANUSCRIPT = gql`
  mutation RecoverManuscript($id: ID!) {
    recoverManuscript(id: $id)
  }
`
export const CLAIM_MANUSCRIPT = gql`
  mutation ClaimManuscript($id: ID!) {
    claimManuscript(id: $id) {
      manuscript {
        id
        updated
        claiming {
          id
          givenNames
          surname
        }
      }
      errors {
        message
      }
    }
  }
`
export const UNCLAIM_MANUSCRIPT = gql`
  mutation UnclaimManuscript($id: ID!) {
    unclaimManuscript(id: $id) {
      manuscript {
        id
        updated
        claiming {
          id
          givenNames
          surname
        }
      }
      errors {
        message
      }
    }
  }
`
