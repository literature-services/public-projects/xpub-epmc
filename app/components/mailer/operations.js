import gql from 'graphql-tag'

export const SEND_EMAIL = gql`
  mutation SendEmail(
    $manuscriptId: ID!
    $manuscriptVersion: Float!
    $to: [ID]!
    $subject: String!
    $message: String!
    $cc: String
    $bcc: Boolean
  ) {
    epmc_email(
      manuscriptId: $manuscriptId
      manuscriptVersion: $manuscriptVersion
      to: $to
      subject: $subject
      message: $message
      cc: $cc
      bcc: $bcc
    )
  }
`
