import { withFormik } from 'formik'
import { graphql, compose } from 'react-apollo'
import * as yup from 'yup'
import MailerForm from './MailerForm'
import { SEND_EMAIL } from './operations'

const handleSubmit = async (
  values,
  { props, setSubmitting, setErrors, resetForm, setStatus },
) => {
  try {
    await props.sendEmail({
      variables: {
        manuscriptId: props.manuscript.id,
        manuscriptVersion: props.manuscript.version,
        to: values.recipients,
        subject: values.subject,
        message: values.message,
        cc: values.cc,
        bcc: values.bcc,
      },
      refetchQueries: props.refetch || [],
    })
    setStatus('success')
  } catch (e) {
    if (e.graphQLErrors) {
      setSubmitting(false)
      setErrors(e.graphQLErrors[0].message)
    }
  }
}

const enhancedFormik = withFormik({
  initialValues: {
    recipients: [],
    subject: '',
    message: '',
    cc: '',
    bcc: false,
  },
  mapPropsToValues: props => ({
    recipients: props.recipients,
    subject: props.subject,
    message: props.message,
    cc: props.cc,
    bcc: props.bcc,
  }),
  validationSchema: yup.object().shape({
    recipients: yup.array().required('At least one recipient is required'),
    subject: yup.string().required('Subject is required'),
    message: yup.string().required('Message is required'),
    cc: yup.string(),
    bcc: yup.boolean(),
  }),
  displayName: 'send-email',
  handleSubmit,
})(MailerForm)

export default compose(graphql(SEND_EMAIL, { name: 'sendEmail' }))(
  enhancedFormik,
)
