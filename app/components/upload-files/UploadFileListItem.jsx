import React from 'react'
import { Mutation } from 'react-apollo'
import Dropzone from 'react-dropzone'
import styled from 'styled-components'
import { debounce } from 'lodash'
import { th, override } from '@pubsweet/ui-toolkit'
import { Action, TextField, Icon, Checkbox, ErrorText } from '@pubsweet/ui'
import { LoadingIcon, Select, NoClick } from '../ui'
import { UserContext } from '../App'
import { GET_MANUSCRIPT } from '../operations'
import { SubmissionTypes, FileTypes, ManuscriptTypes } from './uploadtypes'
import FileLightbox from './FileLightbox'
import {
  DELETE_FILE_MUTATION,
  UPDATE_FILE_MUTATION,
  COPY_FILE_MUTATION,
  REPLACE_MANUSCRIPT_FILE_MUTATION,
} from './operations'

const FileListItem = styled.li`
  display: flex;
  flex-wrap: no-wrap;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
  & > * {
    flex: 1 1 28%;
    padding: 0 calc(${th('gridUnit')} * 3);
  }
  @media screen and (max-width: 1190px) and (min-width: 870px),
    screen and (max-width: 750px) {
    flex: 1 1 30%;
    padding: calc(${th('gridUnit')} * 2);
    margin: ${th('gridUnit')};
    background-color: ${th('colorBackgroundHue')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-radius: ${th('borderRadius')};
    flex-direction: column;
    align-items: flex-start;
    justify-content: stretch;

    & > * {
      width: 100%;
      padding: 0;
      margin-bottom: calc(${th('gridUnit')} * 3);
      min-height: calc(${th('gridUnit')} * 9);
    }
  }
`
const LabeledDiv = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 3);
  div {
    display: flex;
    align-items: center;
    min-height: calc(${th('gridUnit')} * 6);
  }
  & > label {
    display: block;
    font-size: ${th('fontSizeBaseSmall')};
    ${override('ui.Label')};
  }
`
const Label = styled.span`
  display: block;
  font-size: ${th('fontSizeBaseSmall')};
  color: ${th('colorText')};
  ${override('ui.Label')};
`
const Drop = styled.div`
  width: auto;
  height: auto;
  border: 0;
  outline: none;
  input {
    width: 100%;
    left: -100% !important;
  }
`
const ButtonDiv = styled.div`
  flex: 1 1 16%;
  padding: 0;
  display: flex;
  flex-wrap: no-wrap;
  align-items: flex-start;
  justify-content: space-between;
  margin-bottom: 0;
  button:hover {
    text-decoration: none;
  }
  @media screen and (max-width: 1190px) and (min-width: 870px),
    screen and (max-width: 750px) {
    div {
      margin-bottom: 0;
    }
  }
`
const Delete = styled.div`
  width: calc(${th('gridUnit')} * 6);
  justify-content: space-around;
  color: ${th('colorPrimary')};
  cursor: pointer;
  ${override('ui.Action')};
`

const ReplaceButton = ({
  accept,
  manuscriptId,
  fileId,
  loading,
  setLoading,
  setNotification,
}) => (
  <Mutation
    awaitRefetchQueries
    fetchPolicy="no-cache"
    mutation={REPLACE_MANUSCRIPT_FILE_MUTATION}
    refetchQueries={() => [
      {
        query: GET_MANUSCRIPT,
        variables: { id: manuscriptId },
      },
    ]}
  >
    {(replaceManuscriptFile, { data }) => {
      const replaceFileHandler = async (fileId, files) => {
        if (files && files.length > 0) {
          if (files.some(f => f.size === 0)) {
            const emptyFiles = files
              .filter(f => f.size === 0)
              .map(f => f.name)
              .join(', ')
            this.setNotification(
              `The file size of ${emptyFiles} is zero. No files were uploaded. Please check and upload the corrected file(s).`,
              'warning',
            )
            return
          }
          setLoading(true)
          if (
            files[0].type ===
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
          ) {
            setNotification('Transforming manuscript file for preview.', 'info')
          }
          await replaceManuscriptFile({
            variables: {
              fileId,
              files,
            },
          })
          setLoading(false)
          setNotification('')
        } else {
          setNotification('Only one file is allowed.', 'error')
        }
      }
      return (
        <LabeledDiv>
          <label htmlFor="mreplace">Replace</label>
          <Dropzone
            accept={accept}
            multiple={false}
            onDrop={files => {
              replaceFileHandler(fileId, files)
            }}
          >
            {({ getRootProps, getInputProps }) => (
              <Drop {...getRootProps()}>
                <input id="mreplace" {...getInputProps()} />
                {loading ? (
                  <div>
                    <LoadingIcon />
                  </div>
                ) : (
                  <Delete>
                    <Icon color="currentColor" size={3}>
                      upload
                    </Icon>
                  </Delete>
                )}
              </Drop>
            )}
          </Dropzone>
        </LabeledDiv>
      )
    }}
  </Mutation>
)

class UploadFileListItem extends React.Component {
  state = {
    label: this.props.file.label ? this.props.file.label : '',
    type: this.props.file.type ? this.props.file.type : '',
    loading: false,
    tooLong: false,
  }
  componentDidMount() {
    this._mounted = true
  }
  componentWillUnmount() {
    this._mounted = false
  }

  static contextType = UserContext
  render() {
    const currentUser = this.context
    const { label, type, loading, tooLong } = this.state
    const { types, file, manuscript, pdfSend, setNotification } = this.props
    return (
      <Mutation
        awaitRefetchQueries
        mutation={DELETE_FILE_MUTATION}
        refetchQueries={() => [
          {
            query: GET_MANUSCRIPT,
            variables: { id: manuscript },
          },
        ]}
      >
        {(deleteFile, { data }) => {
          const deleteFileHandler = id => {
            deleteFile({ variables: { id } })
          }
          if (file.type === 'manuscript') {
            return (
              <Mutation
                awaitRefetchQueries
                mutation={COPY_FILE_MUTATION}
                refetchQueries={() => [
                  {
                    query: GET_MANUSCRIPT,
                    variables: { id: manuscript },
                  },
                ]}
              >
                {(copyFile, { data }) => {
                  const addPDFLoadFile = async () => {
                    copyFile({
                      variables: {
                        id: file.id,
                        type: 'pdf4load',
                      },
                    })
                  }
                  const onPDFCheck = e => {
                    if (e.target.checked) {
                      addPDFLoadFile()
                    } else if (pdfSend) {
                      deleteFileHandler(pdfSend.id)
                    }
                  }
                  return (
                    <FileListItem>
                      {loading && <NoClick />}
                      <LabeledDiv>
                        <Label>File</Label>
                        <div>
                          <FileLightbox file={file} />
                        </div>
                      </LabeledDiv>
                      <LabeledDiv>
                        <Label>File type</Label>
                        <div>
                          <span>manuscript</span>
                        </div>
                      </LabeledDiv>
                      <LabeledDiv>
                        {currentUser.admin &&
                          file.mimeType === 'application/pdf' && (
                            <React.Fragment>
                              <Label>Use in PMC</Label>
                              <div>
                                <Checkbox
                                  checked={pdfSend}
                                  label="Use this as PMC PDF"
                                  name="pdf4load"
                                  onChange={e => onPDFCheck(e)}
                                />
                              </div>
                            </React.Fragment>
                          )}
                      </LabeledDiv>
                      <ButtonDiv>
                        <ReplaceButton
                          accept={ManuscriptTypes.join(', ')}
                          fileId={file.id}
                          loading={loading}
                          manuscriptId={manuscript}
                          setLoading={v =>
                            this._mounted && this.setState({ loading: v })
                          }
                          setNotification={setNotification}
                        />
                        {currentUser.admin && this.props.delete && (
                          <LabeledDiv>
                            <Action onClick={() => deleteFileHandler(file.id)}>
                              <Label>Remove</Label>
                              <Delete>
                                <Icon color="currentColor" size={3.25}>
                                  x
                                </Icon>
                              </Delete>
                            </Action>
                          </LabeledDiv>
                        )}
                      </ButtonDiv>
                    </FileListItem>
                  )
                }}
              </Mutation>
            )
          }
          return (
            <Mutation
              awaitRefetchQueries
              mutation={UPDATE_FILE_MUTATION}
              refetchQueries={() => [
                {
                  query: GET_MANUSCRIPT,
                  variables: { id: manuscript },
                },
              ]}
            >
              {(updateFile, { data }) => {
                const updateFileHandler = debounce(
                  async ({ type, label }) =>
                    updateFile({
                      variables: {
                        id: file.id,
                        type,
                        label,
                      },
                    }),
                  100,
                )
                const onPropChange = e => {
                  const key = e.target.name
                  const val = e.target.value
                  const obj = {}
                  obj[key] = val
                  if (this._mounted) {
                    if (key === 'label' && val.length > 100) {
                      this.setState({ tooLong: true })
                    } else {
                      this.setState(Object.assign({ tooLong: false }, obj))
                      updateFileHandler(obj)
                    }
                  }
                }
                return (
                  <FileListItem>
                    <LabeledDiv>
                      <Label>File</Label>
                      <div>
                        <FileLightbox file={file} />
                      </div>
                    </LabeledDiv>
                    <React.Fragment>
                      <Select
                        icon="chevron_down"
                        invalidTest={!file.type}
                        label="Select file type"
                        name="type"
                        onChange={onPropChange}
                        options={types}
                        value={type}
                      />
                      <div>
                        <TextField
                          invalidTest={
                            (!file.label &&
                              SubmissionTypes.some(
                                t => t.value === file.type,
                              )) ||
                            tooLong
                          }
                          label="Label to appear in text"
                          name="label"
                          onBlur={onPropChange}
                          onChange={e =>
                            this._mounted &&
                            this.setState({ label: e.target.value })
                          }
                          placeholder="F1, Fig. 1, Figure 1"
                          value={label}
                        />
                        {tooLong && (
                          <ErrorText>
                            Label must be under 100 characters
                          </ErrorText>
                        )}
                      </div>
                    </React.Fragment>
                    <ButtonDiv>
                      <ReplaceButton
                        accept={FileTypes.join(', ')}
                        fileId={file.id}
                        loading={loading}
                        manuscriptId={manuscript}
                        setLoading={v =>
                          this._mounted && this.setState({ loading: v })
                        }
                        setNotification={setNotification}
                      />
                      <LabeledDiv>
                        <Action onClick={() => deleteFileHandler(file.id)}>
                          <Label>Remove</Label>
                          <Delete>
                            <Icon color="currentColor" size={3.25}>
                              x
                            </Icon>
                          </Delete>
                        </Action>
                      </LabeledDiv>
                    </ButtonDiv>
                  </FileListItem>
                )
              }}
            </Mutation>
          )
        }}
      </Mutation>
    )
  }
}

export default UploadFileListItem
