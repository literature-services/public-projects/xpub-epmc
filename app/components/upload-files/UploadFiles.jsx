import React from 'react'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import Dropzone from 'react-dropzone'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { H3, Icon } from '@pubsweet/ui'
import config from 'config'
import { LoadingIcon, Notification, NoClick } from '../ui'
import UploadFileListItem from './UploadFileListItem'
import { UPLOAD_MUTATION } from './operations'
import { FileTypes, ManuscriptTypes, XMLTypes, fileSort } from './uploadtypes'
import { GET_MANUSCRIPT } from '../operations'

const UploadDiv = styled.div`
  padding: calc(${th('gridUnit')} * 3) 0;
  &:after {
    content: '';
    display: block;
    clear: both;
  }
`
const DropArea = styled.div`
  width: 100%;
  text-align: center;
  padding: calc(${th('gridUnit')} * 6);
  border: 2px dashed ${th('colorBorder')};
  cursor: default;
  outline: none;
  span {
    color: ${th('colorPrimary')};
  }
  &:hover {
    border-color: ${lighten('colorPrimary', 20)};
    background-color: ${th('colorTextReverse')};
    span {
      color: ${lighten('colorPrimary', 20)};
    }
  }
`
const DropMore = styled(DropArea)`
  padding: calc(${th('gridUnit')} * 3);
  align-items: center;
  justify-content: center;
`
const FileList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0 0 calc(${th('gridUnit')} * 6);
  max-width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;

  @media screen and (max-width: 1190px) and (min-width: 870px),
    screen and (max-width: 750px) {
    flex-flow: row wrap;
    justify-content: flex-start;
    align-items: flex-start;
  }
`

class UploadFiles extends React.Component {
  state = { notify: '', loading: false }

  setNotification = (message, type) => {
    if (message) {
      this.setState({ notify: { message, type } })
    } else {
      this.setState({ notify: '' })
    }
  }

  render() {
    const {
      manuscript,
      version,
      types,
      checked = false,
      pdfSend,
      files,
    } = this.props
    const { loading, notify } = this.state
    const sorted = fileSort(files)
    return (
      <Mutation
        awaitRefetchQueries
        fetchPolicy="no-cache"
        mutation={UPLOAD_MUTATION}
        refetchQueries={result => {
          if (result.data.uploadFiles) {
            return [
              {
                query: GET_MANUSCRIPT,
                variables: { id: manuscript },
              },
            ]
          }
          return []
        }}
      >
        {(uploadFiles, { data }) => {
          const fileUploadHandler = async (files, type) => {
            if (files.length > 0) {
              if (files.some(f => f.size === 0)) {
                const emptyFiles = files
                  .filter(f => f.size === 0)
                  .map(f => f.name)
                  .join(', ')
                this.setNotification(
                  `The file size of ${emptyFiles} is zero. No files were uploaded. Please check and upload the corrected file(s).`,
                  'warning',
                )
                return
              }
            }

            if (type === 'manuscript' && files.length === 0) {
              this.setNotification(
                'Only one file is allowed for manuscript.',
                'error',
              )
              this.setState({ loading: false })
              return
            }

            this.setState({ loading: true })
            if (
              type === 'manuscript' &&
              files[0].type ===
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ) {
              this.setNotification(
                'Transforming manuscript file for preview.',
                'info',
              )
            }
            const { data } = await uploadFiles({
              variables: {
                id: manuscript,
                version,
                files,
                type,
              },
            })
            if (!data.uploadFiles) {
              this.setNotification('File is not an allowed file type.', 'error')
              this.setState({ loading: false })
            } else {
              this.setNotification('')
              this.setState({ loading: false })
            }
          }
          return (
            <React.Fragment>
              {loading && <NoClick />}
              {notify && (
                <Notification type={notify.type}>{notify.message}</Notification>
              )}
              <UploadDiv>
                {files.length > 0 && (
                  <div>
                    <FileList>
                      {sorted.map((file, i, arr) =>
                        file.type === 'manuscript' ? (
                          <UploadFileListItem
                            delete={arr
                              .slice(0, i)
                              .some(f => f.type === 'manuscript')}
                            file={file}
                            key={file.url}
                            manuscript={manuscript}
                            pdfSend={pdfSend}
                            setNotification={this.setNotification}
                          />
                        ) : (
                          <UploadFileListItem
                            checked={checked}
                            file={file}
                            key={file.url}
                            manuscript={manuscript}
                            setNotification={this.setNotification}
                            types={types}
                          />
                        ),
                      )}
                    </FileList>
                  </div>
                )}
                {files && files.length > 0 ? (
                  <Dropzone
                    accept={
                      types.some(t => t.value === 'PMC')
                        ? FileTypes.concat(XMLTypes).join(', ')
                        : FileTypes.join(', ')
                    }
                    onDrop={files => {
                      fileUploadHandler(files)
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <DropMore {...getRootProps()}>
                        <input {...getInputProps()} id="file_drop" />
                        {loading ? (
                          <LoadingIcon size={6} />
                        ) : (
                          <Icon color="currentColor" size={6}>
                            upload
                          </Icon>
                        )}
                        <label htmlFor="file_drop">
                          <p>
                            {`Drop additional files here, or click to select from your computer.`}
                          </p>
                        </label>
                      </DropMore>
                    )}
                  </Dropzone>
                ) : (
                  <Dropzone
                    accept={ManuscriptTypes.join(', ')}
                    multiple={false}
                    onDrop={files => {
                      fileUploadHandler(files, config.file.type.manuscript)
                    }}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <DropArea {...getRootProps()}>
                        <input {...getInputProps()} id="man_drop" />
                        {loading ? (
                          <LoadingIcon size={8} />
                        ) : (
                          <Icon color="currentColor" size={8}>
                            upload
                          </Icon>
                        )}
                        <label htmlFor="man_drop">
                          <H3>
                            {`Drop manuscript file here, or click to select from your computer.`}
                          </H3>
                        </label>
                        <p>
                          {`Only Microsoft Word or PDF file formats are accepted for the manuscript file.`}
                        </p>
                      </DropArea>
                    )}
                  </Dropzone>
                )}
              </UploadDiv>
            </React.Fragment>
          )
        }}
      </Mutation>
    )
  }
}

export default UploadFiles
