export { default } from './UploadFiles'
export { default as FileThumbnails } from './FileThumbnails'
export { default as FileLightbox } from './FileLightbox'
export {
  SubmissionTypes,
  ManuscriptTypes,
  FileTypes,
  XMLTypes,
  ReviewTypes,
  AllTypes,
  TypesWithNames,
  ImageTypes,
  fileSort,
} from './uploadtypes'
