import { compose } from 'recompose'
import config from 'config'
import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import { createBrowserHistory } from 'history'
import Login from './Login'
import { LOGIN_USER } from '../operations'
import redirectPath from './redirect'
import userHelper from '../helpers/userHelper'

export const handleSSO = (search, jwt) => {
  if (search && search.includes('?auth_client=')) {
    const domain = search.split(/=(.+)/)[1]
    const europepmcDomains = config['pubsweet-client']['europepmc-domains']
    if (europepmcDomains.includes(domain)) {
      window.location = `https://${domain}/accounts/plus_verify?token=${jwt}`
      return true
    }
  }
  return false
}

const handleSubmit = (values, { props, setSubmitting, setErrors }) =>
  props
    .epmc_signinUser({ variables: { input: values } })
    .then(({ data, errors }) => {
      if (!errors) {
        userHelper.setToken(data.epmc_signinUser.token)
        const { search } = props.location
        if (handleSSO(search, data.epmc_signinUser.token)) {
          return
        }
        let path = ''
        if (search && search.includes('?next=')) {
          path = search.split(/=(.+)/)[1]
            ? search.split(/=(.+)/)[1]
            : redirectPath({ location: props.location })
        }
        createBrowserHistory({ forceRefresh: true }).push(path)
        setSubmitting(true)
      }
    })
    .catch(e => {
      if (e.graphQLErrors) {
        setSubmitting(false)
        setErrors(e.graphQLErrors[0].message)
      }
    })

const enhancedFormik = withFormik({
  initialValues: {
    email: '',
    password: '',
  },
  mapPropsToValues: props => ({
    email: props.email,
    password: props.password,
  }),
  displayName: 'login',
  handleSubmit,
  passwordReset: true,
})(Login)

export default compose(graphql(LOGIN_USER, { name: 'epmc_signinUser' }))(
  enhancedFormik,
)
