import React from 'react'
import { Query } from 'react-apollo'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import moment from 'moment'
import { ErrorText, H1, Link, Button, TextField, Checkbox } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Page, Notification, A } from '../ui'
import SignInFooter from '../SignInFooter'
import { GET_PROP } from '../operations'
import Holiday from './Holiday'

const Flex = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: calc(${th('gridUnit')} * 3);
`
const Signup = styled.p`
  margin: 0;
  font-size: ${th('fontSizeHeading4')};
  a {
    font-weight: 600;
  }
  margin-left: calc(${th('gridUnit')} * 2);
`
const ResetPassword = styled.p`
  margin-bottom: 0;
  a {
    font-weight: 600;
  }
`
const ShowPassword = styled(Checkbox)`
  position: absolute;
  right: 0;
  top: 0;
  font-size: ${th('fontSizeBaseSmall')};
`
const PasswordField = styled.div`
  position: relative;
  max-width: 500px;
`
const Container = styled.form`
  margin: 0 0;
  max-width: 350px;
  margin-top: calc(${th('gridUnit')} * 3);
`
const EmailInput = props => <TextField label="Email address" {...props.field} />
const PasswordInput = props => (
  <TextField label="Password" {...props.field} type="password" />
)

const PlainPasswordInput = props => (
  <TextField
    invalidTest={props.invalidTest}
    label="Password"
    {...props.field}
  />
)

class Login extends React.Component {
  state = {
    showPassword: false,
  }
  toggleShowPassword = () => {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword,
    })
  }
  render() {
    const {
      errors,
      isSubmitting,
      handleSubmit,
      signup = true,
      passwordReset = true,
      location,
    } = this.props
    return (
      <Page>
        <H1>Sign in with your Europe PMC plus account</H1>
        <Query
          fetchPolicy="cache-and-network"
          query={GET_PROP}
          variables={{ name: 'homeNotification' }}
        >
          {({ data, loading }) => {
            if (loading || !data) return null
            const {
              kind,
              display,
              message,
              startDate,
              stopDate,
            } = data.findProp.value
            if (!display) return null
            if (
              (startDate &&
                moment().isBefore(`${startDate.date}T${startDate.time}`)) ||
              (stopDate &&
                moment().isAfter(`${stopDate.date}T${startDate.time}`))
            )
              return null
            return <Notification type={kind}>{message}</Notification>
          }}
        </Query>
        <Query
          fetchPolicy="cache-and-network"
          query={GET_PROP}
          variables={{ name: 'holidayNotification' }}
        >
          {({ data, loading }) => {
            if (loading || !data) return null
            const { display, startDate, stopDate } = data.findProp.value
            if (!display) return null
            if (moment().isAfter(`${stopDate.date}T${startDate.time}`))
              return null
            return (
              <Holiday
                startDate={`${startDate.date}T${startDate.time}`}
                stopDate={`${stopDate.date}T${startDate.time}`}
              />
            )
          }}
        </Query>
        {!isEmpty(errors) && <Notification type="error">{errors}</Notification>}
        <Container onSubmit={handleSubmit}>
          <Field component={EmailInput} name="email" />
          <PasswordField>
            {!this.state.showPassword && (
              <Field component={PasswordInput} name="password" />
            )}

            {this.state.showPassword && (
              <Field component={PlainPasswordInput} name="password" />
            )}
            <ShowPassword
              checked={this.state.showPassword}
              label="Show password"
              onChange={() => this.toggleShowPassword()}
            />
          </PasswordField>
          <Flex>
            <Button disabled={isSubmitting} primary type="submit">
              Login
            </Button>
            {signup && (
              <Signup>
                {`Don’t have an account? `}
                <Link to={`/signup${location.search}`}>Create an account.</Link>
              </Signup>
            )}
          </Flex>
        </Container>
        {passwordReset && (
          <ResetPassword>
            {`Trouble logging in? `}
            <Link to="/password-reset">Reset your password</Link>
            {` or contact `}
            <A href="mailto:helpdesk@europepmc.org">helpdesk@europepmc.org</A>
          </ResetPassword>
        )}
        <SignInFooter />
      </Page>
    )
  }
}

// used by tests
export { Login, ErrorText, Signup, ResetPassword }

// used by consumers
export default Login
