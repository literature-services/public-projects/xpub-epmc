import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { europepmcUrl } from 'config'
import { A, FlexBar, LeftSide, RightSide } from './ui'

const Foot = styled(FlexBar)`
  border-top: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  * {
    font-size: ${th('fontSizeBaseSmall')};
  }
  @media screen and (max-width: 995px) {
    height: auto;
    flex-wrap: wrap;
    z-index: 3;
    & > div:first-child {
      & > div {
        text-align: center;
        padding-bottom: 0;
      }
      & + div div {
        text-align: center;
        margin: 0 auto;
        padding-top: calc(${th('gridUnit')} / 2);
        padding-left: 0;
      }
    }
  }
`
const Footer = () => (
  <Foot>
    <LeftSide>
      <div>
        For assistance, see the <A href="/user-guide">User guide</A>,{' '}
        <A href="mailto:helpdesk@europepmc.org">
          email the Europe PMC Helpdesk
        </A>
        , or call +44 1223 494118.
      </div>
    </LeftSide>
    <RightSide>
      <div>
        <A href={`${europepmcUrl}`} target="_blank">
          Europe PMC
        </A>
        <A
          href="https://www.ebi.ac.uk/data-protection/privacy-notice/europe-pmc-plus"
          target="_blank"
        >
          Privacy Notice
        </A>
        <A href={`${europepmcUrl}/Accessibility`} target="_blank">
          Accessibility
        </A>
      </div>
    </RightSide>
  </Foot>
)

export default Footer
