import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { Button, Action, Icon } from '@pubsweet/ui'
import { Table, TextArea, Buttons, NoClick, LoadingIcon } from '../ui'
import { RETAG_MANUSCRIPT } from './operations'

const TBody = styled.tbody`
  vertical-align: top;
  width: 100%;
  max-width: 100%;
  box-sizing: border-box;
  th {
    white-space: nowrap;
  }
  td {
    textarea {
      width: 100%;
      resize: vertical;
    }
  }
  button {
    display: flex;
    align-items: center;
  }
`
const formatEmail = formData => `<table style="text-align: left; border-collapse: collapse" cellpadding="5px">
  <tbody valign="top">
    <tr>
      <th style="border:1px solid #CCCCCC;">Location</th>
      <th style="border:1px solid #CCCCCC;">Selected text</th>
      <th style="border:1px solid #CCCCCC; width: 60%;">Error description</th>
    </tr>
    ${formData
      .map(
        row =>
          `<tr>
            <td style="border:1px solid #CCCCCC;">${row.location}</td>
            <td style="border:1px solid #CCCCCC;">${row.quote}</td>
            <td style="border:1px solid #CCCCCC;">${row.text}</td>
          </tr>`,
      )
      .join('')}
  </tbody>
</table>`

class TaggerError extends React.Component {
  state = {
    formData: this.props.formData.slice(),
    sending: false,
  }
  update = (i, e) => {
    const newData = this.state.formData.slice()
    newData[i][e.target.name] = e.target.value
    this.setState({ formData: newData })
  }
  render() {
    const { close, manuscript, history } = this.props
    const { formData, sending } = this.state
    return (
      <Mutation mutation={RETAG_MANUSCRIPT}>
        {(retagManuscript, { data }) => {
          const send = async () => {
            this.setState({ sending: true })
            await retagManuscript({
              variables: {
                data: {
                  manuscriptId: manuscript.id,
                  manuscriptVersion: manuscript.version,
                  notesType: 'userMessage',
                  content: JSON.stringify({
                    to: ['tagger'],
                    subject: 'Fix tagging errors',
                    message: formatEmail(formData),
                  }),
                },
              },
            })
            close()
            history.push('/')
          }
          return (
            <React.Fragment>
              {sending && <NoClick />}
              <Table
                style={{ marginTop: '1rem', width: '100%', maxWidth: '100%' }}
              >
                <TBody>
                  <tr>
                    <th>Location</th>
                    <th>Selected text</th>
                    <th>Error description</th>
                  </tr>
                  {formData.map((row, i) => {
                    const { location, quote, text } = row
                    return (
                      <tr key={row.id}>
                        <td>
                          <TextArea
                            name="location"
                            onChange={e => this.update(i, e)}
                            rows={4}
                            value={location}
                          />
                        </td>
                        <td style={{ width: '30%' }}>
                          <TextArea
                            name="quote"
                            onChange={e => this.update(i, e)}
                            rows={4}
                            value={quote}
                          />
                        </td>
                        <td style={{ width: '50%' }}>
                          <TextArea
                            name="text"
                            onChange={e => this.update(i, e)}
                            rows={4}
                            value={text}
                          />
                          <Action
                            onClick={() => {
                              formData.splice(i, 1)
                              this.setState({ formData })
                            }}
                          >
                            <Icon color="currentColor" size={2.5}>
                              x
                            </Icon>
                            Delete row
                          </Action>
                        </td>
                      </tr>
                    )
                  })}
                </TBody>
              </Table>
              <p>
                <Action
                  onClick={() => {
                    formData.push({
                      id: formData.length,
                      location: '',
                      quote: '',
                      text: '',
                    })
                    this.setState({ formData })
                  }}
                  style={{ display: 'flex', alignItems: 'center' }}
                >
                  <Icon color="currentColor">plus</Icon>
                  Add row
                </Action>
              </p>
              <Buttons right>
                <Button disabled={sending} onClick={() => send()} primary>
                  {sending ? (
                    <span>
                      <LoadingIcon color="currentColor" size={2} /> Creating
                      package
                    </span>
                  ) : (
                    'Send for retagging'
                  )}
                </Button>
                <Button
                  disabled={sending}
                  onClick={() => {
                    this.setState({ formData: this.props.formData })
                    close()
                  }}
                >
                  Cancel
                </Button>
              </Buttons>
            </React.Fragment>
          )
        }}
      </Mutation>
    )
  }
}

export default withRouter(TaggerError)
