const getNearestId = (loc, doc, type) => {
  const d = type === 'xml' ? doc : document
  const result = d.evaluate(loc, doc, null, XPathResult.FIRST_ORDERED_NODE_TYPE)
  if (result.singleNodeValue && result.singleNodeValue.id) {
    return result.singleNodeValue.id
  }
  const newLoc = loc.slice(0, loc.lastIndexOf('/'))
  return getNearestId(newLoc, doc)
}

const getLineIndex = (str, line) => {
  let pos = str.indexOf('\n')
  while (line - 1 > 1 && pos !== -1) {
    line -= 1
    pos = str.indexOf('\n', pos + 1)
  }
  return pos
}

const xmlGetId = (xml, a) => {
  if (a.quote.indexOf(' id="') >= 0) {
    const rx = / id="([0-9A-Za-z]+)"/
    const [, id] = a.quote.match(rx) || []
    return id
  }
  const [, line] = a.ranges[0].start.match(/span\[([0-9]+)\]$/) || []
  const pos = line ? getLineIndex(xml, line) : 0
  const searchStr = xml.slice(0, pos + a.ranges[0].startOffset)
  if (searchStr.lastIndexOf(' id="') >= 0) {
    const split = searchStr.split(' id="').pop()
    const i = split.indexOf('"')
    return split.slice(0, i)
  }
  return false
}

const getLocations = async (url, xUrl, annotations) => {
  if (annotations && url && xUrl) {
    const hresponse = await fetch(url)
    const html = await hresponse.text()
    const outer = document.createElement('div')
    const test = document.createElement('div')
    const escQuote = document.createElement('textarea')
    const escText = document.createElement('textarea')
    test.innerHTML = html
    outer.appendChild(test)
    const xresponse = await fetch(xUrl)
    const xml = await xresponse.text()
    const locate = await annotations.map(a => {
      const { type } = a.file
      escQuote.textContent = a.quote
      escText.textContent = a.text
      const obj = {
        id: a.id,
        quote: escQuote.innerHTML,
        text: escText.innerHTML,
        location: '',
      }
      if (type === 'tempHTML') {
        // Search HTML for ids
        const loc = a.ranges[0].start
        obj.location = getNearestId(loc, outer)
      } else if (type === 'PMC') {
        // Search XML for ids
        const id = xmlGetId(xml, a)
        if (id) obj.location = id
      } else {
        obj.location = 'PDF'
      }
      return obj
    })
    return locate
  }
}

const locateError = async a => {
  const { type, url } = a.file
  if (type === 'tempHTML') {
    // Search HTML for ids
    const response = await fetch(url)
    const html = await response.text()
    const outer = document.createElement('div')
    const test = document.createElement('div')
    test.innerHTML = html
    outer.appendChild(test)
    const loc = a.ranges[0].start
    return getNearestId(loc, outer)
  } else if (type === 'PMC') {
    // Search XML for id
    const response = await fetch(url)
    const xml = await response.text()
    const id = xmlGetId(xml, a)
    return id || null
  } else if (type === 'schematron') {
    const response = await fetch(url)
    const xml = await response.text()
    const doc = new window.DOMParser().parseFromString(xml, 'text/xml')
    const loc = a.xpath
    return getNearestId(loc, doc, 'xml')
  }
}

const checkLoading = async () => {
  await new Promise(resolve => setTimeout(resolve, 350))
  const check = document.querySelector('.annotator.loaded')
  if (check) {
    return true
  }
  await new Promise(resolve => setTimeout(resolve, 350))
  return checkLoading()
}

module.exports = { getLocations, locateError, checkLoading }
