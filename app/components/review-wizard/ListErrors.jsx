import React from 'react'
import { withRouter } from 'react-router'
import moment from 'moment'
import styled, { createGlobalStyle } from 'styled-components'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { H4, Action } from '@pubsweet/ui'
import { ZebraList, ZebraListItem } from '../ui'
import { locateError } from './locateErrors'

const Annotations = styled.ul`
  font-size: ${th('fontSizeBaseSmall')};
  list-style-type: none;
  padding-left: 0;
  margin-left: 0;
  li {
    padding: calc(${th('gridUnit')} * 2);
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
    border-radius: calc(${th('borderRadius')} * 3);
    background-color: ${lighten('colorWarning', 51)};
    & + li {
      margin-top: calc(${th('gridUnit')} * 2);
    }
    p {
      font-size: ${th('fontSizeBaseSmall')};
      margin-bottom: 0;
    }
    &:after {
      content: '';
      display: block;
      clear: both;
    }
  }
`
const TimeStamp = styled.div`
  float: right;
  margin: 0 0;
  button {
    font-size: calc(${th('gridUnit')} * 1.5);
  }
`

const ErrorScroll = createGlobalStyle`
  @keyframes highlight {
    0% { border: 3px solid ${th('colorWarning')}; }
    100% { border: 3px solid transparent; }
  }
  .annotator .errorScroll {
    border: 3px solid transparent;
    animation: highlight 6s ease-out alternate;
  }
`

const ListItem = styled(ZebraListItem)`
  font-size: ${th('fontSizeBaseSmall')};
  button {
    font-size: ${th('fontSizeBaseSmall')};
  }
`

const checkLoading = async () => {
  await new Promise(resolve => setTimeout(resolve, 350))
  const check = document.querySelector('.annotator.loaded')
  if (check) {
    return true
  }
  await new Promise(resolve => setTimeout(resolve, 350))
  return checkLoading()
}

const WarnList = ({ warnings, xml, history }) => (
  <ZebraList>
    {warnings.map(s => {
      if (xml) {
        const loc = s.indexOf('(/article')
        const text = s.slice(0, loc)
        const link = s.slice(loc + 1, s.length - 1)
        const a = {
          file: {
            type: 'schematron',
            url: xml.url,
          },
          xpath: link,
        }
        return (
          <ListItem key={s}>
            {text}{' '}
            <Action
              onClick={async () => {
                if (history.search !== '?xml') {
                  history.push({ search: '?xml' })
                  await checkLoading()
                }
                const settings = {
                  behavior: 'smooth',
                  inline: 'center',
                  block: 'center',
                }
                const id = await locateError(a)
                const hl = document.getElementById(id)
                if (hl) {
                  hl.classList.remove('errorScroll')
                  hl.scrollIntoView(settings)
                  hl.classList.add('errorScroll')
                }
              }}
            >
              {link}
            </Action>
          </ListItem>
        )
      }
      return <ListItem key={s}>{s}</ListItem>
    })}
  </ZebraList>
)

const AList = ({ list, pane, history }) => (
  <Annotations>
    {list.map(a => (
      <li key={a.id}>
        <em>{a.quote}</em>
        <p>{a.text}</p>
        <TimeStamp>
          <Action
            onClick={async () => {
              if (history.search !== pane) {
                history.push({ search: pane })
                await checkLoading()
              }
              const settings = {
                behavior: 'smooth',
                inline: 'center',
                block: 'center',
              }
              let hl = document.querySelector(
                `span[data-annotation-id="${a.id}"]`,
              )
              if (!hl) {
                const id = await locateError(a)
                hl = document.getElementById(id)
              }
              if (hl) {
                hl.classList.remove('errorScroll')
                hl.scrollIntoView(settings)
                hl.classList.add('errorScroll')
              }
            }}
          >
            {moment(a.updated).format('D/M/YY H:m')}
          </Action>
        </TimeStamp>
      </li>
    ))}
  </Annotations>
)

const PaneList = withRouter(AList)
export const ListWarnings = withRouter(WarnList)

const ListErrors = ({ annotations }) => {
  const list = { tempHTML: [], pdf4print: [], PMC: [] }
  annotations.forEach(a => list[a.file.type].push(a))
  return (
    <React.Fragment>
      <ErrorScroll />
      {list.tempHTML.length > 0 && (
        <React.Fragment>
          <H4>Web preview errors</H4>
          <PaneList list={list.tempHTML} pane="?web" />
        </React.Fragment>
      )}
      {list.pdf4print.length > 0 && (
        <React.Fragment>
          <H4>PDF preview errors</H4>
          <PaneList list={list.pdf4print} pane="?pdf" />
        </React.Fragment>
      )}
      {list.PMC.length > 0 && (
        <React.Fragment>
          <H4>XML errors</H4>
          <PaneList list={list.PMC} pane="?xml" />
        </React.Fragment>
      )}
    </React.Fragment>
  )
}

export default ListErrors
