import React from 'react'
import styled from 'styled-components'
import { Action, Button, H2, Icon, Link } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Portal, Buttons, Notification } from '../ui'
import GifOne from '../../assets/annotation1.gif'
import GifTwo from '../../assets/annotation2.gif'
import GifThree from '../../assets/annotation3.gif'

const Panel = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  & > div {
    padding: calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 3);
  }
`
const ImgP = styled.p`
  text-align: center;
  img {
    max-width: 100%;
  }
`
const WithIcon = styled(Action)`
  display: flex;
  align-items: center;
`
const toggleNext = div => {
  div.parentNode.classList.toggle('hidden')
  div.parentNode.nextElementSibling.classList.toggle('hidden')
}

const togglePrev = div => {
  div.parentNode.classList.toggle('hidden')
  div.parentNode.previousElementSibling.classList.toggle('hidden')
}

const PanelButtons = ({ next, prev }) => (
  <Buttons style={{ flexDirection: !next ? 'row' : 'row-reverse' }}>
    {next && (
      <WithIcon onClick={e => toggleNext(e.currentTarget.parentNode)}>
        Next
        <Icon color="currentColor" size={2.5}>
          chevron-right
        </Icon>
      </WithIcon>
    )}
    {prev && (
      <WithIcon onClick={e => togglePrev(e.currentTarget.parentNode)}>
        <Icon color="currentColor" size={2.5}>
          chevron-left
        </Icon>
        Previous
      </WithIcon>
    )}
  </Buttons>
)

const ReviewInstructions = ({ close, preprint }) => (
  <Portal transparent>
    <H2>Reporting errors</H2>
    <Panel>
      <div>
        <p>
          {`Highlight text in the web preview screen, and click the button that appears, with a pencil icon, to create an annotation and report errors.`}
        </p>
        <ImgP>
          <img
            alt={`A user's cursor highlights text in yellow, causing a button to appear. The user clicks the button with the cursor to show a text box, where the user types an annotation for the text and clicks a "save" button.`}
            src={GifOne}
          />
        </ImgP>
        <PanelButtons next />
      </div>
      <div className="hidden">
        <p>
          {`By hovering over existing annotations, you can click a pencil icon to edit, or an 'X' icon to delete them.`}
        </p>
        <ImgP>
          <img
            alt="Hovering over yellow highlighted text displays the previous annotation, as well as a pencil and an X icon. CLicking the pencil opens a text box, and clicking the X removes the annotationa and the highlight."
            src={GifTwo}
          />
        </ImgP>
        <PanelButtons next prev />
      </div>
      <div className="hidden">
        <p>
          {`Go to the PDF preview and perform the same actions to report PDF-specific rendering errors.`}
        </p>
        <ImgP>
          <img
            alt={`A cursor clicks the 'PDF preview' button, then highlights text to add an annotation to the PDF shown.`}
            src={GifThree}
          />
        </ImgP>
        <PanelButtons prev />
      </div>
    </Panel>
    <p>
      {`Please report only errors or omissions that impact the scientific accuracy of your article.`}
    </p>
    {preprint ? (
      <Notification type="info">
        {`If you wish to report errors that you have corrected in a later version of your preprint, please see the `}
        <Link target="_blank" to="/user-guide/preprintfaqs#versions">
          Preprint FAQs
        </Link>{' '}
        {`to find out why we are still asking you to approve this older version.`}
      </Notification>
    ) : (
      <p>
        {`If you need to update figures or tables, please highlight the figure or table label, and provide an annotation to that effect. Helpdesk staff will contact you with instructions for uploading updated files when we process your review.`}
      </p>
    )}
    <Buttons right>
      <Button onClick={() => close()}>Close</Button>
    </Buttons>
  </Portal>
)

export default ReviewInstructions
