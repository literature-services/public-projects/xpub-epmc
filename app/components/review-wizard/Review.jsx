import React from 'react'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import parse from 'html-react-parser'
import { th, lighten } from '@pubsweet/ui-toolkit'
import { Action, Button, H1, H2, H3, H4, Icon } from '@pubsweet/ui'
import {
  B,
  Buttons,
  LoadingIcon,
  PreviewPage,
  PreviewPanel,
  EditPanel,
  PanelHeader,
  PanelContent,
  SectionContent as Content,
  SectionHeader as Header,
  Toggle as Toggles,
  Notification,
  NoClick,
} from '../ui'
import UploadFiles, {
  SubmissionTypes,
  ReviewTypes,
  AllTypes,
  FileThumbnails,
  FileLightbox,
} from '../upload-files'
import ManuscriptPreview from '../ManuscriptPreview'
import PDFViewer from '../../component-pdf-viewer'
import { GET_MANUSCRIPT } from '../operations'
import Citation from '../Citation'
import HTMLPreview from './HTMLPreview'
import ReviewForm from './ReviewForm'
import ReviewFooter from './ReviewFooter'
import Annotator from './Annotator'
import { ListWarnings } from './ListErrors'
import { CONVERT_XML } from './operations'

const PreviewPageDiv = styled(PreviewPage)`
  .show-mobile {
    display: none;
  }
  .pad {
    padding: 0 calc(2 * ${th('gridUnit')});
  }
  @media screen and (max-width: 870px) {
    .show-mobile {
      display: inline-block;
    }
    .hide-mobile {
      display: none;
    }
  }
`
const PreviewPanelDiv = styled.div`
  top: calc(-${th('gridUnit')} * 25) !important;
  @media screen and (max-width: 1305px) {
    top: calc(-${th('gridUnit')} * 28) !important;
  }
  @media screen and (max-width: 896px) {
    top: calc(-${th('gridUnit')} * 31) !important;
  }
`
const PreviewPanelHeader = styled(PanelHeader)`
  height: calc(${th('gridUnit')} * 10);
  h1 {
    margin-bottom: ${th('gridUnit')};
  }
  @media screen and (max-width: 870px) {
    height: auto;
  }
`
const Instructions = styled.div`
  padding: 0 calc(2 * ${th('gridUnit')});
  margin-bottom: calc(4 * ${th('gridUnit')});
  font-style: italic;
`
const Toggle = styled(Toggles)`
  height: calc(${th('gridUnit')} * 7);
  &.hide-mobile button {
    margin-left: calc(${th('gridUnit')} * 3);
  }
  @media screen and (max-width: 870px) {
    height: auto;
  }
`
const PreviewError = styled.div`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorError')};
  background-color: ${lighten('colorError', 100)};
  padding: ${th('gridUnit')} calc(${th('gridUnit')} * 2);
  margin: ${th('gridUnit')} 0;
  white-space: pre-wrap;
  overflow-wrap: break-word;
`
const ButtonSpacer = styled.div`
  height: calc(${th('gridUnit')} * 30) !important;
  @media screen and (max-width: 1305px) {
    height: calc(${th('gridUnit')} * 33) !important;
  }
  @media screen and (max-width: 896px) {
    height: calc(${th('gridUnit')} * 36) !important;
  }
  display: flex;
  align-items: flex-end;
  padding-bottom: calc(${th('gridUnit')} * 2);
`
const ManuscriptDiv = styled.div`
  h4 {
    margin: 0 0 ${th('gridUnit')};
  }
  a {
    font-size: ${th('fontSizeBaseSmall')};
    display: block;
    margin: ${th('gridUnit')} 0;
  }
`
const PDFNotif = styled(Notification)`
  margin-top: 0;
`
const LicenseCheckP = styled.p`
  font-size: ${th('fontSizeBaseSmall')};
  margin-top: 0;
`
const LicenseCheckUL = styled.ul`
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: calc(${th('gridUnit')} * 2);
  margin-bottom: 0;
`
const AnnotatePDF = Annotator(PDFViewer)
const AnnotateHTML = Annotator(HTMLPreview)

const LicenseCheck = ({ metaFile, license }) => {
  if (license && !metaFile) {
    return (
      <React.Fragment>
        <Notification type="warning">
          License added during tagging!
        </Notification>
        <Content>
          <LicenseCheckUL>
            <li>Is a license present in the source?</li>
            <li>
              Does the url <em>{license}</em> match the license text?
            </li>
          </LicenseCheckUL>
          <LicenseCheckP>
            Please mark an error if either is not the case.
          </LicenseCheckP>
        </Content>
      </React.Fragment>
    )
  }
  return null
}

class Review extends React.Component {
  state = {
    open: true,
    xopen: false,
    pane: 'web',
    showManuscript: false,
    showAll: false,
    startProcessing: false,
    notif: false,
  }
  static getDerivedStateFromProps(props, state) {
    return { pane: props.location.search.substr(1) || state.pane }
  }
  componentDidMount() {
    this._mounted = true
    this.notifTimer = null
    if (this.page && this.props.manuscript) {
      const { files, status, formState } = this.props.manuscript
      if (files && !files.find(f => f.type === 'PMC')) {
        this.setState({ showAll: true })
      }
      if (['file-error', 'tagging', 'repo-triage'].includes(status)) {
        this.setState({ pane: 'files', open: status === 'repo-triage' })
      }
      if (status === 'xml-triage' && formState) {
        this.setState({ pane: 'files' })
      }
      if (status === 'xml-qa') {
        this.setState({ xopen: true })
      }
      if (['xml-triage', 'xml-error', 'xml-corrected'].includes(status)) {
        this.setState({ open: false })
      }
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.showSuccess && !prevProps.showSuccess) {
      this.showMessage('success', 'Status successfully changed', true)
    }
  }
  componentWillUnmount() {
    this._mounted = false
    clearTimeout(this.notifTimer)
  }
  showMessage = (type, message, timeout) =>
    new Promise((resolve, reject) => {
      if (this.page && this._mounted) {
        window.scrollY = 0
        window.pageYOffset = 0
        document.scrollingElement.scrollTop = 0
        this.setState({ notif: { type, message } })
        if (timeout) {
          if (this.notifTimer) {
            clearTimeout(this.notifTimer)
          }
          this.notifTimer = setTimeout(() => {
            if (this._mounted) {
              this.setState({ notif: false }, resolve())
            }
          }, 3000)
        }
        resolve()
      }
    })
  setRef = page => {
    this.page = page
  }
  render() {
    const { manuscript, currentUser, review } = this.props
    const {
      open,
      xopen,
      pane,
      showManuscript,
      showAll,
      notif,
      startProcessing,
    } = this.state
    const {
      files: allfiles,
      meta,
      status,
      version,
      teams,
      pdfDepositState,
      organization,
    } = manuscript
    const preprint = organization.name === 'Europe PMC Preprints'
    if (teams && allfiles) {
      const sourceFile =
        allfiles.find(file => file.type === 'source') ||
        allfiles.find(file => file.type === 'manuscript')
      const files = allfiles.filter(
        f => !f.type || ReviewTypes.some(rev => rev.value === f.type),
      )
      const originalFiles = allfiles.filter(f =>
        SubmissionTypes.some(sub => sub.value === f.type),
      )
      const combinedFiles = allfiles.filter(
        f =>
          f.type === 'manuscript' || AllTypes.some(all => all.value === f.type),
      )
      const html = files.find(file => file.type === 'tempHTML')
      const pdf =
        allfiles.find(f => f.type === 'pdf4load') ||
        allfiles.find(f => f.type === 'pdf4print')
      const xml = files.find(f => f.type === 'PMC') || null
      const reviewer =
        teams &&
        teams.find(t => t.role === 'reviewer') &&
        teams.find(t => t.role === 'reviewer').teamMembers[0]
      const badNameCheck = xml && xml.filename.split('.')[0] !== manuscript.id
      const { notes = [] } = meta
      const metaFile =
        allfiles.some(f => f.type === 'metadata') ||
        notes.some(n => n.notesType === 'planS')
      const license =
        notes.find(n => n.notesType === 'openAccess') &&
        notes.find(n => n.notesType === 'openAccess').content
      const sNote = notes.find(n => n.notesType === 'schematron')
      const schematron = sNote && JSON.parse(sNote.content)
      if (
        !currentUser.admin &&
        !(currentUser.external && manuscript.status === 'xml-qa') &&
        !(currentUser.tagger && manuscript.status === 'tagging') &&
        !(
          reviewer &&
          currentUser.id === reviewer.user.id &&
          manuscript.status === 'xml-review'
        )
      ) {
        this.props.history.push(`/submission/${manuscript.id}/submit`)
        return null
      }
      let heading = ''
      switch (status) {
        case 'tagging':
          heading = 'Tag XML and upload files'
          break
        case 'file-error':
          heading = 'Correct file loading errors'
          break
        case 'xml-qa':
          heading = 'QA tagging and web versions'
          break
        case 'xml-triage':
          heading = 'Triage reported errors'
          break
        case 'xml-corrected':
          heading = 'Check corrected errors'
          break
        case 'xml-review':
          heading = 'Review final web versions'
          break
        case 'xml-error':
          heading = 'Correct reviewer-reported errors'
          break
        case 'repo-triage':
          heading = 'Fix repository loading errors'
          break
        default:
          heading = ''
          break
      }
      return (
        <React.Fragment>
          {notif && (
            <React.Fragment>
              <NoClick />
              <Notification fullscreen type={notif.type}>
                {notif.message}
              </Notification>
            </React.Fragment>
          )}
          <PreviewPageDiv ref={this.setRef}>
            <PreviewPanel
              style={{
                flex: showManuscript && '1 1 1000px',
                width: showManuscript && '50%',
                maxWidth: showManuscript && '50%',
              }}
            >
              <PreviewPanelDiv style={{ maxWidth: showManuscript && '1000px' }}>
                <PreviewPanelHeader>
                  <H1>{heading}</H1>
                </PreviewPanelHeader>
                {pane !== 'files' && status !== 'tagging' && (
                  <Instructions>
                    <B>
                      {`To report an error, select text and describe the error in the comment box that appears.`}
                    </B>
                    {` Please only report errors or omissions that impact the scientific accuracy of your article.${
                      pdf && pdf.type !== 'pdf4load'
                        ? ' Only rendering errors need to be reported in the PDF preview.'
                        : ''
                    }`}
                  </Instructions>
                )}
                <Toggle>
                  {(currentUser.admin || currentUser.tagger) && (
                    <Action
                      className={pane === 'files' ? 'current' : ''}
                      onClick={() => {
                        this.props.history.push({ search: '?files' })
                        this.setState({ pane: 'files' })
                      }}
                    >
                      XML files
                    </Action>
                  )}
                  {[
                    'file-error',
                    'xml-qa',
                    'xml-triage',
                    'xml-corrected',
                    'xml-error',
                    'repo-triage',
                  ].includes(status) && (
                    <Action
                      className={pane === 'xml' ? 'current' : ''}
                      disabled={!xml}
                      onClick={() => {
                        this.props.history.push({ search: '?xml' })
                        this.setState({ pane: 'xml' })
                      }}
                    >
                      XML view
                    </Action>
                  )}
                  <Action
                    className={pane === 'web' ? 'current' : ''}
                    disabled={!html}
                    onClick={() => {
                      this.props.history.push({ search: '?web' })
                      this.setState({ pane: 'web' })
                    }}
                  >
                    Web preview
                  </Action>
                  <Action
                    className={pane === 'pdf' ? 'current' : ''}
                    disabled={!pdf}
                    onClick={() => {
                      this.props.history.push({ search: '?pdf' })
                      this.setState({ pane: 'pdf' })
                    }}
                  >
                    PDF preview
                  </Action>
                  <Action
                    className={
                      pane === 'original'
                        ? 'current show-mobile'
                        : 'show-mobile'
                    }
                    onClick={() => {
                      this.props.history.push({ search: '?original' })
                      this.setState({ pane: 'original' })
                    }}
                    primary={pane !== 'original'}
                  >
                    Submitted file
                  </Action>
                </Toggle>
                <PanelContent>
                  {pane === 'files' && (
                    <Mutation
                      mutation={CONVERT_XML}
                      refetchQueries={() => [
                        {
                          query: GET_MANUSCRIPT,
                          variables: { id: manuscript.id },
                        },
                      ]}
                    >
                      {(convertXML, { data }) => {
                        const generatePreviews = async () => {
                          if (!pdfDepositState) {
                            this.setState({ startProcessing: true })
                          }
                          await convertXML({
                            variables: { id: xml.id },
                          })
                          this.setState({ startProcessing: false })
                        }
                        const XMLButtons = () => (
                          <Buttons left top>
                            <Button
                              disabled={!xml}
                              onClick={() => generatePreviews()}
                              style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                              }}
                            >
                              {(pdfDepositState || startProcessing) && (
                                <LoadingIcon color="currentColor" size={2} />
                              )}
                              Process
                              {(pdfDepositState || startProcessing) &&
                                'ing'}{' '}
                              XML
                            </Button>
                            <Button
                              onClick={() =>
                                this.setState({ showAll: !showAll })
                              }
                            >
                              {showAll ? 'Hide ' : 'Show '}
                              submission files
                            </Button>
                          </Buttons>
                        )
                        return (
                          <React.Fragment>
                            <XMLButtons />
                            {manuscript.formState && (
                              <PreviewError>
                                {parse(manuscript.formState)}
                              </PreviewError>
                            )}
                            <UploadFiles
                              checked
                              files={showAll ? combinedFiles : files}
                              manuscript={manuscript.id}
                              pdfSend={allfiles.find(
                                f => f.type === 'pdf4load',
                              )}
                              types={showAll ? AllTypes : ReviewTypes}
                              version={version}
                            />
                            <XMLButtons />
                          </React.Fragment>
                        )
                      }}
                    </Mutation>
                  )}
                  {pane === 'pdf' && pdf && (
                    <React.Fragment>
                      {pdf.type === 'pdf4load' && (
                        <PDFNotif type="info">
                          The original {preprint ? 'preprint' : 'manuscript'}{' '}
                          file will be made available for download in Europe
                          PMC. No errors can be reported.
                        </PDFNotif>
                      )}
                      {status === 'tagging' || pdf.type === 'pdf4load' ? (
                        <PDFViewer url={pdf.url} />
                      ) : (
                        <React.Fragment>
                          <AnnotatePDF
                            file={pdf}
                            reload={this.props.reload}
                            revId={(review && review.id) || null}
                            userId={currentUser.id}
                          />
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                  {pane === 'web' && html && (
                    <React.Fragment>
                      {status === 'tagging' ? (
                        <HTMLPreview url={html.url} />
                      ) : (
                        <React.Fragment>
                          <AnnotateHTML
                            file={html}
                            reload={this.props.reload}
                            revId={(review && review.id) || null}
                            userId={currentUser.id}
                          />
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                  {pane === 'xml' && xml && (
                    <React.Fragment>
                      <React.Fragment>
                        <AnnotateHTML
                          file={xml}
                          reload={this.props.reload}
                          revId={(review && review.id) || null}
                          userId={currentUser.id}
                          xml
                        />
                      </React.Fragment>
                    </React.Fragment>
                  )}
                  {pane === 'original' && sourceFile && (
                    <ManuscriptPreview
                      file={sourceFile}
                      original={allfiles.find(
                        file => file.type === 'manuscript',
                      )}
                    />
                  )}
                </PanelContent>
              </PreviewPanelDiv>
            </PreviewPanel>
            <EditPanel
              style={{
                flex: showManuscript && '1 1 1000px',
                width: showManuscript && '50%',
                maxWidth: showManuscript && '50%',
              }}
            >
              <div
                style={{
                  maxWidth: showManuscript && '1000px',
                  border: showManuscript && '0',
                  backgroundColor: showManuscript && 'transparent',
                }}
              >
                {showManuscript && sourceFile ? (
                  <PanelContent className="pad">
                    <ButtonSpacer className="hide-mobile">
                      <Button
                        onClick={() => this.setState({ showManuscript: false })}
                      >
                        Close manuscript file
                      </Button>
                    </ButtonSpacer>
                    <ManuscriptPreview
                      file={sourceFile}
                      original={allfiles.find(
                        file => file.type === 'manuscript',
                      )}
                    />
                  </PanelContent>
                ) : (
                  <React.Fragment>
                    <PreviewPanelHeader>
                      <H2>
                        {preprint ? 'Check & approve' : 'Compare & approve'}
                      </H2>
                    </PreviewPanelHeader>
                    <PanelContent>
                      {status === 'xml-qa' && xml && (
                        <React.Fragment>
                          <Action
                            onClick={() => this.setState({ xopen: !xopen })}
                            style={{ width: '100%', textDecoration: 'none' }}
                            title={`${xopen ? 'Hide' : 'Show'} files`}
                          >
                            <Header>
                              <H3>XML file</H3>
                              <Icon color="currentColor">
                                chevron-{xopen ? 'down' : 'right'}
                              </Icon>
                            </Header>
                          </Action>
                          {xopen && (
                            <Content>
                              <ManuscriptDiv>
                                <FileLightbox file={xml} />
                                {schematron && schematron.length > 0 && (
                                  <React.Fragment>
                                    <H4>Validation warnings</H4>
                                    <ListWarnings
                                      warnings={schematron}
                                      xml={xml}
                                    />
                                  </React.Fragment>
                                )}
                              </ManuscriptDiv>
                            </Content>
                          )}
                        </React.Fragment>
                      )}
                      <React.Fragment>
                        {preprint && (
                          <React.Fragment>
                            <Header>
                              <H3>Preprint citation</H3>
                            </Header>
                            <Content>
                              <Citation
                                journal={manuscript.journal}
                                metadata={manuscript.meta}
                                version={manuscript.version}
                              />
                            </Content>
                          </React.Fragment>
                        )}
                        {(!preprint || status === 'xml-qa') && (
                          <React.Fragment>
                            <Action
                              onClick={() => this.setState({ open: !open })}
                              style={{ width: '100%', textDecoration: 'none' }}
                              title={`${open ? 'Hide' : 'Show'} files`}
                            >
                              <Header>
                                <H3>Submitted files</H3>
                                <Icon color="currentColor">
                                  chevron-{open ? 'down' : 'right'}
                                </Icon>
                              </Header>
                            </Action>
                            {open && (
                              <Content>
                                {sourceFile && (
                                  <ManuscriptDiv>
                                    <H4>Manuscript file</H4>
                                    <FileLightbox
                                      file={allfiles.find(
                                        f => f.type === 'manuscript',
                                      )}
                                    />
                                    <Button
                                      className="hide-mobile"
                                      onClick={() =>
                                        this.setState({ showManuscript: true })
                                      }
                                    >
                                      View manuscript file alongside preview
                                    </Button>
                                  </ManuscriptDiv>
                                )}
                                <FileThumbnails files={originalFiles} />
                              </Content>
                            )}
                          </React.Fragment>
                        )}
                      </React.Fragment>
                      {['xml-qa', 'xml-triage'].includes(status) && (
                        <LicenseCheck license={license} metaFile={metaFile} />
                      )}
                      <ReviewForm
                        manuscript={manuscript}
                        previews={!!html && !!pdf}
                        review={review}
                      />
                      {(currentUser.admin || currentUser.tagger) &&
                        badNameCheck && (
                          <Notification type="warning">
                            The XML file name does not match the EMSID
                          </Notification>
                        )}
                    </PanelContent>
                  </React.Fragment>
                )}
              </div>
            </EditPanel>
          </PreviewPageDiv>
          <ReviewFooter
            currentUser={currentUser}
            files={originalFiles}
            manuscript={manuscript}
            previews={!!html && !!pdf}
            review={review}
            showMessage={this.showMessage}
          />
        </React.Fragment>
      )
    }
    return null
  }
}

export default Review
