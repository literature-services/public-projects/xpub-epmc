import React from 'react'
import { Query } from 'react-apollo'
import { createBrowserHistory } from 'history'
import { Page, Loading, LoadingIcon } from '../ui'
import { GET_MANUSCRIPT } from '../operations'
import SubmissionHeader from '../SubmissionHeader'
import Review from './Review'
import { CURRENT_REVIEW } from './operations'

const ReviewWithHeader = SubmissionHeader(Review)

const ReviewPage = ({ match, ...props }) => (
  <Query
    fetchPolicy="cache-and-network"
    query={GET_MANUSCRIPT}
    variables={{ id: match.params.id }}
  >
    {({ data, loading }) => {
      if (loading) {
        return (
          <Page>
            <Loading>
              <LoadingIcon />
            </Loading>
          </Page>
        )
      }
      if (!data) {
        createBrowserHistory({ forceRefresh: true }).go()
        return null
      }
      if (!data.manuscript) {
        createBrowserHistory().push('/')
        return null
      }
      return (
        <ReviewContent manuscript={data.manuscript} match={match} {...props} />
      )
    }}
  </Query>
)

class ReviewContent extends React.Component {
  state = { showSuccess: false }
  componentDidUpdate(prevProps) {
    if (prevProps.manuscript.status !== this.props.manuscript.status) {
      this.showSuccess()
    }
  }
  showSuccess = () => this.setState({ showSuccess: true })
  render() {
    const { showSuccess } = this.state
    const { manuscript, match, ...props } = this.props
    return (
      <Query
        query={CURRENT_REVIEW}
        variables={{ manuscriptId: match.params.id }}
      >
        {({ data: { currentReview }, loading, refetch }) => (
          <ReviewWithHeader
            key={`${manuscript.id}${manuscript.status}`}
            manuscript={manuscript}
            match={match}
            reload={() => refetch()}
            review={currentReview}
            saved={new Date()}
            showSuccess={showSuccess}
            {...props}
          />
        )}
      </Query>
    )
  }
}

export default ReviewPage
