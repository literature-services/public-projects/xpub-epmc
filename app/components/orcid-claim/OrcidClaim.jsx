/* global thorApplicationNamespace */

import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { thorApiUrl } from 'config'
import { LoadingIcon } from './../ui'
import orcidIcon from './orcid-brands.svg'
/**
e.g. Citation (starred fields are mandatory)
{
    "title": "this is my work",
    "workType": "preprint",
    "publicationYear": "",
    "workExternalIdentifiers": [
        {
            "workExternalIdentifierType": "doi",
            "workExternalIdentifierId": "asdasd09123"
        }
    ],
    "url": "",
    "shortDescription": "",
    "clientDbName": ""
}
*/

const BtnText = styled.div`
  display: inline !important;
  white-space: nowrap;
  margin-left: calc(${th('gridUnit')} * 0.5);
`

const BtnIcon = styled.img`
  max-width: calc(${th('gridUnit')} * 2);
`
const BtnContentContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const DefaultShowComponent = ({ onClick, isLoading }) => (
  <Button className="orcid-component" disabled={isLoading} onClick={onClick}>
    <BtnContentContainer>
      {isLoading ? <LoadingIcon /> : <BtnIcon src={orcidIcon} />}
      <BtnText>
        {isLoading
          ? 'Claiming. The operation could take a few minutes.'
          : 'Claim manuscript with ORCID'}
      </BtnText>
    </BtnContentContainer>
  </Button>
)

const loadScript = (
  className,
  url,
  scriptLoaded = () => {},
  isAsync = false,
) => {
  const script = document.createElement('script')
  script.classList.add(className)
  script.src = url
  script.async = isAsync
  script.onload = scriptLoaded
  document.body.appendChild(script)
}

class OrcidClaim extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      loadingTimeoutEvent: window.setTimeout(
        () => !this.state.claimed && this.callErrorCallbackIfRequired(false),
        60000,
      ),
      claimed: false,
      userData: null,
    }
    if (document.getElementsByClassName('thor-script').length === 0) {
      loadScript(
        'thor-script',
        'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
      )
      loadScript(
        'thor-script',
        `${thorApiUrl}/resources/js/dataclaiming/ebithor-claiming.1.3.js`,
        () => thorApplicationNamespace.loadClaimingInfo(),
      )
    }
    if (window.addEventListener) {
      window.addEventListener('message', this.handleThorEvent, false)
    } else {
      window.attachEvent('onmessage', this.handleThorEvent)
    }
  }
  componentDidMount() {
    if (typeof thorApplicationNamespace === 'undefined') return
    thorApplicationNamespace.loadClaimingInfo()
  }
  componentWillUnmount() {
    if (window.addEventListener) {
      window.removeEventListener('message', this.handleThorEvent, false)
    } else {
      window.detachEvent('onmessage', this.handleThorEvent)
    }
    if (this.state.loadingTimeoutEvent !== null)
      window.clearTimeout(this.state.loadingTimeoutEvent)
  }
  handleThorEvent = event => {
    try {
      if (typeof thorApplicationNamespace === 'undefined') return
      switch (event.data) {
        case thorApplicationNamespace.notify.fatalError:
          this.callErrorCallbackIfRequired(false, event)
          break
        case thorApplicationNamespace.notify.errorOccurred: // Called after error is returned from Thor
          this.state.claimed ||
          this.checkIfAlreadyClaimed(this.props.citation, this.state.userData)
            ? this.callSuccessCallback()
            : this.callErrorCallbackIfRequired(false, event)
          break
        case thorApplicationNamespace.notify.claimEnd: // Called after the claim is done into orcid record
          this.setState({ claimed: true })
          break
        case thorApplicationNamespace.notify.popupClosed:
          this.callErrorCallbackIfRequired(true)
          break
        case thorApplicationNamespace.notify.loadingComplete:
          if (
            thorApplicationNamespace.isSignedIn() &&
            thorApplicationNamespace.userData !== null
          ) {
            this.setState({
              userData: JSON.parse(
                JSON.stringify(thorApplicationNamespace.userData),
              ),
            })
            this.state.claimed ||
            this.checkIfAlreadyClaimed(
              this.props.citation,
              thorApplicationNamespace.userData,
            )
              ? this.callSuccessCallback()
              : this.claimToOrcid()
          } else this.callErrorCallbackIfRequired(false)
          break
        default:
          break
      }
    } catch (e) {
      this.callErrorCallbackIfRequired(false, e)
      throw e
    }
  }
  checkIfAlreadyClaimed = (newWork, userData) => {
    if ('works' in userData && userData.works instanceof Array) {
      const allWorkIds = []
      userData.works.forEach(work =>
        work.workExternalIdentifiers.forEach(workId =>
          allWorkIds.push(
            `${workId.workExternalIdentifierType}<:>${workId.workExternalIdentifierId}`,
          ),
        ),
      )
      return newWork.workExternalIdentifiers.reduce(
        (result, workId) =>
          result ||
          allWorkIds.indexOf(
            `${workId.workExternalIdentifierType}<:>${workId.workExternalIdentifierId}`,
          ) > -1,
        false,
      )
    }
    return false
  }
  claimToOrcid = () => {
    thorApplicationNamespace.orcIdWork = this.props.citation
    thorApplicationNamespace.addWork()
  }
  loginToOrcid = () => {
    thorApplicationNamespace.notifyClientApp(
      thorApplicationNamespace.notify.loginClick,
    )
    const url = thorApplicationNamespace.getLoginUrl()
    window.open(url, 'ORCID')
  }
  logoutFromOrcid = () => {
    const url = thorApplicationNamespace.getLogoutUrl()
    thorApplicationNamespace.callWS(url, () => {}, '', 'GET')
    thorApplicationNamespace.callWSJsonP(
      thorApplicationNamespace.thorForceLogoutUrl,
      null,
      null,
    )
  }
  callSuccessCallback = () => {
    this.props.successCallback(this.state.userData)
    this.setState({ claimed: true })
    this.callErrorCallbackIfRequired(false)
    this.logoutFromOrcid()
  }
  callErrorCallbackIfRequired = (isLoading, error = null) => {
    const timeoutEvent = this.state.loadingTimeoutEvent
    this.setState({
      isLoading,
      loadingTimeoutEvent: isLoading
        ? window.setTimeout(
            () =>
              !this.state.claimed &&
              this.callErrorCallbackIfRequired(false, 'Request timed out'),
            300000,
          )
        : null,
    })
    if (timeoutEvent !== null) window.clearTimeout(timeoutEvent)
    if (!isLoading && error !== null) {
      this.props.errorCallback(error)
      this.logoutFromOrcid()
    }
  }
  render() {
    const ShowComponent = this.props.showComponent
    return (
      <ShowComponent
        isLoading={this.state.isLoading}
        onClick={() => {
          try {
            this.loginToOrcid()
          } catch (e) {
            this.callErrorCallbackIfRequired(false, e)
            throw e
          }
        }}
      />
    )
  }
}

OrcidClaim.propTypes = {
  citation: PropTypes.shape({
    title: PropTypes.string.isRequired,
    workType: PropTypes.string.isRequired,
    workExternalIdentifiers: PropTypes.arrayOf(
      PropTypes.exact({
        workExternalIdentifierType: PropTypes.string.isRequired,
        workExternalIdentifierId: PropTypes.string.isRequired,
      }),
    ).isRequired,
    publicationYear: PropTypes.string,
    url: PropTypes.string,
    shortDescription: PropTypes.string,
    clientDbName: PropTypes.string,
  }).isRequired,
  showComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
  errorCallback: PropTypes.func,
  successCallback: PropTypes.func,
}

OrcidClaim.defaultProps = {
  showComponent: DefaultShowComponent,
  errorCallback: () => {},
  successCallback: () => {},
}

export default OrcidClaim
