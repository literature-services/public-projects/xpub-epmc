import React from 'react'
import { Query } from 'react-apollo'
import { Icon, Action } from '@pubsweet/ui'
import { states } from 'config'
import { Portal, CloseModal } from './ui'
import { ManuscriptMutations } from './SubmissionMutations'
import PreprintReview from './PreprintReview'
import { GET_PREPRINT_SELECTIONS } from './operations'

const EditIcon = () => (
  <Icon color="currentColor" size={1.9} style={{ verticalAlign: 'bottom' }}>
    edit-3
  </Icon>
)

class PreprintSelections extends React.Component {
  state = {
    edit: null,
  }
  render() {
    const { manuscript, setStatus } = this.props
    const { notes } = manuscript.meta
    const license = notes && notes.find(n => n.notesType === 'openAccess')
    const licenseLink = license && license.content
    const openAccess =
      licenseLink && licenseLink.search(/\/(by-nd|by-nc-nd)\//i) < 0
    const mark = states.indexOf('xml-complete')
    const current = states.indexOf(manuscript.status)
    return (
      <Query
        fetchPolicy="cache-and-network"
        query={GET_PREPRINT_SELECTIONS}
        variables={{ id: manuscript.id }}
      >
        {({ data, loading, error }) => {
          if (loading || !data) {
            return '...'
          }
          const { versionApproval, addLicense } = data.getPreprintSelections
          return (
            <React.Fragment>
              {versionApproval && (
                <Action
                  onClick={() => this.setState({ edit: 'versionApproval' })}
                  style={{ display: 'block' }}
                >
                  {versionApproval === 'approved'
                    ? 'Automatic updates'
                    : 'Manual updates'}
                  <EditIcon />
                </Action>
              )}
              {addLicense && !openAccess && (
                <Action onClick={() => this.setState({ edit: 'addLicense' })}>
                  {addLicense === 'approved' ? (
                    'Open license'
                  ) : (
                    <React.Fragment>
                      Restricted license <EditIcon />
                    </React.Fragment>
                  )}
                </Action>
              )}
              {openAccess && (
                <Action onClick={() => this.setState({ edit: 'addLicense' })}>
                  {'Open license'}
                </Action>
              )}
              {this.state.edit && (
                <Portal style={{ width: '740px' }} transparent>
                  <CloseModal onClick={() => this.setState({ edit: null })} />
                  <PreprintReview
                    complete={() => {
                      if (current > mark) {
                        setStatus('repo-ready')
                      }
                      this.setState({ edit: null })
                    }}
                    manuscript={manuscript}
                    refetch={[
                      {
                        query: GET_PREPRINT_SELECTIONS,
                        variables: { id: manuscript.id },
                      },
                    ]}
                    show={this.state.edit}
                    singleView={
                      (this.state.edit === 'versionApproval' &&
                        versionApproval) ||
                      (this.state.edit === 'addLicense' &&
                        addLicense &&
                        addLicense) ||
                      true
                    }
                  />
                </Portal>
              )}
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

const PreprintSelectionsWithMutations = ManuscriptMutations(PreprintSelections)

export default PreprintSelectionsWithMutations
