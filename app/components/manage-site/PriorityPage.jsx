import React from 'react'
import moment from 'moment'
import styled from 'styled-components'
import URLSearchParams from 'url-search-params'
import { Query } from 'react-apollo'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Link } from '@pubsweet/ui'
import { pageSize, ftpAddr, ftp_tagger } from 'config'
import {
  A,
  Loading,
  LoadingIcon,
  Notification,
  Pagination,
  Toggle,
  ZebraList,
  ZebraListItem,
} from '../ui'
import { timeSince, PaginationPane } from '../dashboard'
import ManagementBase from './ManagementBase'
import { PRIORITY_PREPRINTS, PRIORITY_LIST } from './operations'

const taggerName = ftp_tagger.username

const S = styled.span`
  padding: ${th('gridUnit')};
  display: table-cell;
`

const Console = ({ history }) => {
  const params = new URLSearchParams(history.location.search)
  const tier = params.get('tier')
  const currentPage = params.get('page') ? parseInt(params.get('page'), 10) : 1
  const page = params.has('page') ? currentPage - 1 : 0
  const ordinals = ['Corrections', 'First', 'Second', 'Third', 'Fourth']

  const onPageEntered = p => {
    history.push(`/manage/priority?tier=${tier}&page=${p}`)
  }
  return (
    <React.Fragment>
      <Query fetchPolicy="network-only" query={PRIORITY_PREPRINTS}>
        {({ data, loading }) => {
          if (loading || !data) {
            return (
              <Loading>
                <LoadingIcon />
              </Loading>
            )
          }
          const { preprintPriority } = data
          if (preprintPriority.length === 0) {
            return (
              <Notification type="info">
                No preprints need tagging.
              </Notification>
            )
          }
          if (!tier || !preprintPriority.some(r => r.type === tier)) {
            const lowest = preprintPriority.reduce((b, r) => {
              const n = parseInt(r.type, 10)
              if (n < b) return n
              return parseInt(b, 10)
            }, 4)
            history.push(`/manage/priority?tier=${lowest}`)
          }
          return (
            <Toggle>
              {preprintPriority.map(row => (
                <Link
                  className={tier === row.type && 'current'}
                  key={row.type}
                  to={`/manage/priority?tier=${row.type}`}
                >
                  {ordinals[parseInt(row.type, 10)]}
                  {row.type === '0' ? ' ' : ' tier '}({row.count})
                </Link>
              ))}
            </Toggle>
          )
        }}
      </Query>
      {tier && (
        <Query
          fetchPolicy="network-only"
          query={PRIORITY_LIST}
          variables={{ tier, page, pageSize }}
        >
          {({ data, loading }) => {
            if (loading || !data) {
              return (
                <Loading>
                  <LoadingIcon />
                </Loading>
              )
            }
            const { manuscripts, total } = data.preprintPriorityList
            const PageNavigation = () => (
              <Pagination
                currentPage={currentPage}
                onPageEntered={onPageEntered}
                pageSize={pageSize}
                totalSize={total}
              />
            )
            return (
              <React.Fragment>
                <PaginationPane
                  style={{
                    justifyContent: 'space-between',
                    alignItems: 'baseline',
                  }}
                >
                  <H2>
                    {ordinals[parseInt(tier, 10)]}
                    {tier === '0' ? ' ' : ' tier '} preprints
                  </H2>
                  <PageNavigation />
                </PaginationPane>
                <ZebraList
                  style={{
                    width: '100%',
                    display: 'table',
                    borderCollapse: 'collapse',
                  }}
                >
                  {manuscripts.map(m => (
                    <ZebraListItem
                      key={m.id}
                      style={{ width: '100%', display: 'table-row' }}
                    >
                      <S>
                        {m.id} v{m.version}
                      </S>{' '}
                      <S>
                        <Link to={`/submission/${m.id}/review`}>
                          Submission page
                        </Link>
                      </S>{' '}
                      <S>
                        <A
                          href={`ftp://${ftpAddr}/${taggerName}/New/_Preprints/${moment(
                            m.updated,
                          ).format('YYYY-MM-DD')}/${m.id}v${m.version}.tar.gz`}
                        >
                          FTP package
                        </A>
                      </S>{' '}
                      <S>Waiting for tagging: {timeSince(m.updated)}</S>
                    </ZebraListItem>
                  ))}
                </ZebraList>
                <PaginationPane>
                  <PageNavigation />
                </PaginationPane>
              </React.Fragment>
            )
          }}
        </Query>
      )}
    </React.Fragment>
  )
}

const ConsoleTitle = ManagementBase(Console)

const ManagementConsole = ({ ...props }) => (
  <ConsoleTitle pageTitle="Preprint priority" {...props} />
)

export default ManagementConsole
