import React from 'react'
import { createBrowserHistory } from 'history'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H1, Link } from '@pubsweet/ui'
import { Page, Toggle } from '../ui'

const Top = styled.div`
  display: flex
  justify-content: space-between;
  align-items: center;
  & > *:first-child {
    margin-right: calc(${th('gridUnit')} * 4);
  }
  @media screen and (max-width: 800px) {
    display: block;
  }
`

const ManagementBase = BaseComponent => ({
  children,
  currentUser,
  hideSearchBoxes,
  pageTitle,
  ...props
}) => {
  if (
    !currentUser.admin &&
    !(window.location.pathname === '/manage/priority' && currentUser.tagger)
  ) {
    createBrowserHistory().push('/')
    return null
  }
  return (
    <Page>
      <Top>
        <H1>{pageTitle}</H1>
        {currentUser.admin && (
          <Toggle>
            <Link
              className={window.location.pathname === '/manage' && 'current'}
              to="/manage"
            >
              Console
            </Link>
            <Link
              className={
                window.location.pathname === '/manage/metrics' && 'current'
              }
              to="/manage/metrics"
            >
              Metrics
            </Link>
            <Link
              className={
                window.location.pathname === '/manage/priority' && 'current'
              }
              to="/manage/priority"
            >
              Priority
            </Link>
            <Link
              className={
                window.location.pathname === '/manage/users' && 'current'
              }
              to="/manage/users"
            >
              Users
            </Link>
          </Toggle>
        )}
      </Top>
      <BaseComponent currentUser={currentUser} {...props}>
        {children}
      </BaseComponent>
    </Page>
  )
}

export default ManagementBase
