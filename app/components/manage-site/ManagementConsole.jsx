import React from 'react'
import moment from 'moment'
import { Query } from 'react-apollo'
import styled, { withTheme } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon, H2, Link } from '@pubsweet/ui'
import { B, Loading, LoadingIcon, Table, Notification } from '../ui'
import ManagementBase from './ManagementBase'
import ConfigurationForm from './ConfigurationForm'
import { JOB_LIST, PROPS } from './operations'

const THolder = styled.div`
  overflow-x: auto;
  table {
    width: 100%;
  }
`

const Form = styled.div`
  h3 ~ div > label {
    margin-top: 0.5rem;
    font-size: ${th('fontSizeBase')};
  }
`
const Desc = styled.span`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
  span {
    font-size: ${th('fontSizeBaseSmall')};
    margin-left: calc(${th('gridUnit')} * 2);
  }
`

const Status = withTheme(({ theme, status }) => (
  <Icon color={status ? theme.colorSuccess : theme.colorError}>
    {status ? 'check' : 'x'}
  </Icon>
))

const Console = () => (
  <React.Fragment>
    <H2>Job status</H2>
    <Query fetchPolicy="no-cache" query={JOB_LIST}>
      {({ data, loading }) => {
        if (loading) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }
        if (!data) {
          return (
            <Notification type="error">
              Error loading jobs from database
            </Notification>
          )
        }
        const { getJobs } = data
        return (
          <THolder>
            <Table>
              <thead>
                <tr>
                  <th>Job</th>
                  <th>Running</th>
                  <th>Status</th>
                  <th>Last successful run</th>
                </tr>
              </thead>
              <tbody>
                {getJobs.map(job => (
                  <tr key={job.name}>
                    <td>
                      {' '}
                      <Desc>
                        <B>
                          <Link to={`/manage/job/${job.name}`}>{job.name}</Link>{' '}
                        </B>
                        <span>{job.description}</span>
                      </Desc>
                    </td>
                    <td style={{ textAlign: 'center' }}>
                      {job.running === true && <LoadingIcon />}
                    </td>
                    <td style={{ textAlign: 'center' }}>
                      <Status status={job.lastStatus === 'pass'} />
                    </td>
                    <td>{moment(job.lastPass).format('DD/MM/YYYY HH:mm')}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </THolder>
        )
      }}
    </Query>
    <H2>Site configuration</H2>
    <Query fetchPolicy="no-cache" query={PROPS}>
      {({ data, loading }) => {
        if (loading) {
          return (
            <Loading>
              <LoadingIcon />
            </Loading>
          )
        }
        if (!data) {
          return (
            <Notification type="error">
              Error loading properties from database
            </Notification>
          )
        }
        const { getProps } = data
        return (
          <Form>
            {getProps.map(prop => {
              const { ...props } = prop
              return <ConfigurationForm key={prop.name} {...props} />
            })}
          </Form>
        )
      }}
    </Query>
  </React.Fragment>
)

const ConsoleTitle = ManagementBase(Console)

const ManagementConsole = ({ ...props }) => (
  <ConsoleTitle pageTitle="Console" {...props} />
)

export default ManagementConsole
