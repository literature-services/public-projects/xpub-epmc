import React from 'react'
import moment from 'moment'
import { Query } from 'react-apollo'
import styled from 'styled-components'
import { Action, H2, H3, Link } from '@pubsweet/ui'
import { th, override } from '@pubsweet/ui-toolkit'
import { METRICS, COUNT_SET, WEEK_METRICS, STATS } from './operations'
import { Loading, LoadingIcon, Table, Select } from '../ui'
import ManagementBase from './ManagementBase'
import { States } from '../dashboard'

const MetricTable = styled.div`
  overflow-x: auto;
  table {
    width: 100%;
  }
  th {
    white-space: nowrap;
  }
`
const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  & > * {
    margin-bottom: ${th('gridUnit')};
    margin-right: calc(${th('gridUnit')} * 3);
  }
  .fullWidth {
    width: 100%;
    margin-right: 0;
  }
  & > *:last-child {
    margin-right: 0;
  }
  label {
    display: flex;
    align-items: baseline;
    margin-bottom: ${th('gridUnit')};
    span {
      font-size: ${th('fontSizeBaseSmall')};
      display: block;
      ${override('ui.Label')};
      ${override('ui.Select.Label')};
      margin-right: ${th('gridUnit')};
    }
    span,
    div {
      margin-bottom: 0;
    }
  }
`
const Desc = styled.p`
  font-size: ${th('fontSizeBaseSmall')};
  margin: 0.8em auto;
`
const SmallAction = styled(Action)`
  font-size: ${th('fontSizeBaseSmall')};
`

class Metrics extends React.Component {
  state = {
    startMonth: 1,
    endMonth: 6,
    showDesc: false,
    set: this.props.location.search.substr(1) || 'manuscript',
  }
  render() {
    const { startMonth, endMonth, showDesc, set } = this.state
    const preprint = set !== 'manuscript'
    const type = preprint ? 'Preprints' : 'Manuscripts'
    const allmonths = Math.floor(
      moment(new Date()).diff(new Date('2019-05-01'), 'months', true),
    )
    const ranges = Array.from(
      Array(Math.ceil(allmonths / 6)).keys(),
    ).map(n => ({ startMonth: (n + 1) * 6 - 5, endMonth: (n + 1) * 6 }))
    const dataRows = [
      {
        prop: 'submitted',
        text: `Total ${type.toLowerCase()} full text received`,
        desc: `${type} that have reached status 'submitted' in this period`,
      },
      {
        prop: 'xml_tagging',
        text: `${type} tagged`,
        desc: `Sum of count of XMLs uploaded by tagger first time only (no corrected XMLs) + count of re-tagging notes added by admin`,
      },
      {
        prop: 'xml_tagging_within_3_days',
        text: 'Number of manuscripts tagged in 3 days',
        desc: `Count of manuscripts above where the tagging date is less than 4 days after the date sent to tagging`,
      },
      {
        prop: 'xml_tagging_within_3_days_perc',
        text: 'Percent of manuscripts tagged in 3 days',
        desc: `% of the above out of the total count`,
      },
      {
        prop: 'external_qa',
        text: `${type} external QA`,
        desc: `Number of ${type.toLowerCase()} moved by external_qa to 'xml-review' or 'xml-triage' in this period`,
      },
      {
        prop: 'xml_errors',
        text: `${type} with errors`,
        desc: `Of the above, those moved to 'xml-triage'`,
      },
      {
        prop: 'xml_review',
        text: `Total ${type.toLowerCase()} processed`,
        desc: `${type} that have moved between 'submitted' and 'xml-review' in this period`,
      },
      {
        prop: 'xml_review_within_10_days',
        text: 'Number of manuscripts processed in 10 days',
        desc: `Count of manuscripts above where the 'xml-review' date is less than 11 days after the 'submitted' date`,
      },
      {
        prop: 'xml_review_within_10_days_perc',
        text: 'Percent of manuscripts processed in 10 days',
        desc: `% of the above out of the total count`,
      },
      {
        prop: 'xml_review_within_3_days',
        text: 'Number of manuscripts processed in 3 days',
        desc: `Count of manuscripts above where the 'xml-review' date is less than 4 days after the 'submitted' date`,
      },
      {
        prop: 'xml_review_within_3_days_perc',
        text: 'Percent of manuscripts processed in 3 days',
        desc: `% of the above out of the total count`,
      },
      {
        prop: 'published',
        text: `${type} published to Europe PMC`,
        desc: `${type} that have reached status 'published' in this period`,
      },
      {
        prop: 'ncbi_ready_median',
        text: 'Median processing days from submission to NCBI packet',
        desc: `The median number of days it took ${type.toLowerCase()} in this period to move from 'submitted' to 'repo-ready'`,
      },
    ]
    if (preprint) {
      dataRows.splice(12, 1)
      dataRows.splice(2, 2)
      dataRows.splice(5, 4)
    }
    const sets = ['manuscript', 'preprint', 'funder_preprint', 'COVID_preprint']
    const pretty = s => s[0].toUpperCase() + s.slice(1).replace(/_/g, ' ')
    const getDate = n =>
      moment()
        .subtract(n, 'months')
        .format('MMM YYYY')
    const monthSets = ranges.map(o => ({
      value: JSON.stringify(o),
      label: `${getDate(o.endMonth)} - ${getDate(o.startMonth)}`,
    }))
    return (
      <React.Fragment>
        <Select
          icon="chevron_down"
          label="Metrics submission set"
          onChange={e => {
            const v = e.target.value
            this.setState({ set: v })
            this.props.history.push({ search: `?${v}` })
          }}
          options={sets.map(s => ({ value: s, label: pretty(s) }))}
          value={set}
          width="400px"
        />
        <Flex className="fullWidth" style={{ alignItems: 'baseline' }}>
          <H2>{pretty(set)} metrics</H2>
          <label>
            <span>Date range</span>
            <Select
              icon="chevron_down"
              onChange={e => {
                this.setState(JSON.parse(e.target.value))
              }}
              options={monthSets}
              value={JSON.stringify({ startMonth, endMonth })}
              width="216px"
            />
          </label>
        </Flex>
        <Flex>
          <Query
            fetchPolicy="cache-and-network"
            query={METRICS}
            variables={{ startMonth, endMonth, preprint, set }}
          >
            {({ data, loading }) => {
              if (loading) {
                return (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )
              }
              if (!data) {
                this.props.history.go()
                return null
              }

              const { getMetrics } = data
              const headerCols = getMetrics.map(metrix => metrix.display_mth)
              return (
                <React.Fragment>
                  <MetricTable className="fullWidth">
                    <Table>
                      <tbody>
                        <tr>
                          <th style={{ textAlign: 'right' }}>
                            <SmallAction
                              onClick={() =>
                                this.setState({ showDesc: !showDesc })
                              }
                            >
                              {showDesc ? 'Hide ' : 'Show '}descriptions
                            </SmallAction>
                          </th>
                          {headerCols.map(col => (
                            <th key={col}>{col}</th>
                          ))}
                        </tr>
                        {dataRows.map(row => (
                          <tr key={row.prop}>
                            <td>
                              {row.text}
                              {showDesc && <Desc>{row.desc}</Desc>}
                            </td>
                            {getMetrics.map(metrix => (
                              <td key={metrix.display_mth}>
                                {!metrix[row.prop] && metrix[row.prop] !== 0
                                  ? '--'
                                  : metrix[row.prop]}
                                {row.prop.endsWith('perc') && '%'}
                              </td>
                            ))}
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </MetricTable>
                  {!preprint && (
                    <div>
                      <H3>Publisher submission counts</H3>
                      <Query
                        fetchPolicy="network-only"
                        query={STATS}
                        variables={{ startMonth, endMonth }}
                      >
                        {({ data, loading }) => {
                          if (loading || !data) {
                            return (
                              <Loading>
                                <LoadingIcon />
                              </Loading>
                            )
                          }
                          const { publisherMetrics } = data
                          const combined = publisherMetrics.reduce(
                            (obj, curr) => {
                              const { name, user_id, display_mth, count } = curr
                              if (obj[name] && count) {
                                obj[name].months[display_mth] = count
                              } else {
                                obj[name] = {
                                  name,
                                  user_id,
                                  months: headerCols.reduce((o, v) => {
                                    o[v] = 0
                                    return o
                                  }, {}),
                                }
                                if (display_mth && count) {
                                  obj[name].months[display_mth] = count
                                }
                              }
                              return obj
                            },
                            {},
                          )
                          return (
                            <MetricTable>
                              <Table>
                                <tbody>
                                  <tr>
                                    <th>Publisher</th>
                                    {headerCols.map(col => (
                                      <th key={col}>{col}</th>
                                    ))}
                                  </tr>
                                  {Object.keys(combined).map(p => (
                                    <tr key={combined[p].name}>
                                      <td>
                                        <Link
                                          to={`/manage-account/${combined[p].user_id}`}
                                        >
                                          {combined[p].name}
                                        </Link>
                                      </td>
                                      {headerCols.map(m => (
                                        <td key={m}>{combined[p].months[m]}</td>
                                      ))}
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            </MetricTable>
                          )
                        }}
                      </Query>
                    </div>
                  )}
                </React.Fragment>
              )
            }}
          </Query>
          <Query
            fetchPolicy="network-only"
            query={COUNT_SET}
            variables={{ preprint, set }}
          >
            {({ data, loading }) => {
              if (loading || !data) {
                return (
                  <Loading>
                    <LoadingIcon />
                  </Loading>
                )
              }
              const { countSetByStatus } = data
              const counts = countSetByStatus.filter(row => row.count > 0)
              return (
                <div>
                  <H3>Current statuses</H3>
                  <Table>
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Count</th>
                      </tr>
                    </thead>
                    <tbody>
                      {counts.map(row => (
                        <tr key={row.type}>
                          <td>{States.admin[row.type].status}</td>
                          <td>{row.count}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              )
            }}
          </Query>
          {preprint && (
            <Query
              fetchPolicy="network-only"
              query={WEEK_METRICS}
              variables={{ preprint, set }}
            >
              {({ data, loading }) => {
                if (loading || !data) {
                  return (
                    <Loading>
                      <LoadingIcon />
                    </Loading>
                  )
                }
                const { weeklyMetrics } = data

                const headers = weeklyMetrics.map(metrix => metrix.display_mth)
                return (
                  <div>
                    <H3>Past month by week</H3>
                    <Table>
                      <tbody>
                        <tr>
                          <th style={{ textAlign: 'right' }}>Week starting</th>
                          {headers.map(col => (
                            <th key={col}>{col}</th>
                          ))}
                        </tr>
                        {dataRows.map(row => (
                          <tr key={row.prop}>
                            <td>{row.text}</td>
                            {weeklyMetrics.map(metrix => (
                              <td key={metrix.display_mth}>
                                {!metrix[row.prop] && metrix[row.prop] !== 0
                                  ? '--'
                                  : metrix[row.prop]}
                                {row.prop.endsWith('perc') && '%'}
                              </td>
                            ))}
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                )
              }}
            </Query>
          )}
        </Flex>
      </React.Fragment>
    )
  }
}

const MetricsTitle = ManagementBase(Metrics)

const MetricsPage = ({ ...props }) => (
  <MetricsTitle pageTitle="Metrics" {...props} />
)

export default MetricsPage
