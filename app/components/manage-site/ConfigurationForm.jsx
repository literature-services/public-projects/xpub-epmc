/* eslint-disable no-case-declarations, react/no-array-index-key */
// Disable cases for InputField and array index for ArrayBuilder
import React from 'react'
import { Mutation } from 'react-apollo'
import styled, { withTheme } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  Action,
  Button,
  ErrorText,
  Heading,
  Icon,
  RadioGroup,
  TextField,
} from '@pubsweet/ui'
import { Buttons, BoldCheck, Fieldset, TextArea as Area } from '../ui'
import { SAVE_PROP, PROPS } from './operations'

const FieldArray = styled(Fieldset)`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-items: flex-start;
  margin-bottom: 2rem;
  & > div, & > fieldset, & > button {
    display: inline-block;
    margin: calc(${th('gridUnit')}/2);
  }
  & > fieldset
    legend {
      font-size: ${th('fontSizeBaseSmall')};
      margin-top: 0;
      margin-bottom: 0;
      & + div {
        padding: 0 ${th('gridUnit')};
      }
    }
  }
`
const TextArea = styled(Area)`
  background-color: #ffffff !important;
  margin: 0 0 1rem;
`

const InputField = ({ type, opts, format, hiddenLabel, ...props }) => {
  switch (type) {
    case 'boolean':
      const { value, ...rest } = props
      return <BoldCheck checked={value} {...rest} />
    case 'string':
      if (opts) {
        return (
          <Fieldset>
            <legend>{props.label}</legend>
            <RadioGroup
              options={opts.map(o => ({ label: o, value: o }))}
              {...props}
            />
          </Fieldset>
        )
      }
      if (format === 'block') {
        return <TextArea rows={2} {...props} />
      }
      return <TextField {...props} />
    default:
      return <TextField {...props} />
  }
}

const FormItem = ({ name, updateVal, ...props }) => (
  <InputField
    onChange={e => {
      let tracked = props.value
      if (e.target) {
        const { type, value = '', checked = false } = e.target
        tracked = type === 'checkbox' ? checked : value
      } else {
        tracked = e
      }
      if (props.format && props.format === 'time') {
        if (/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/.test(tracked)) {
          const date = new Date()
          const tzo = -date.getTimezoneOffset()
          const dif = tzo >= 0 ? '+' : '-'
          const pad = num => {
            const norm = Math.floor(Math.abs(num))
            return (norm < 10 ? '0' : '') + norm
          }
          const tz = `${dif + pad(tzo / 60)}:${pad(tzo % 60)}`
          tracked += tz
        }
      }
      updateVal({ name, value: tracked })
    }}
    {...props}
  />
)

const IconButton = props => (
  <Button style={{ display: 'inline-flex', alignItems: 'center' }} {...props}>
    <Icon
      color="currentColor"
      size={2}
      strokeWidth={3}
      style={{ paddingLeft: 0 }}
    >
      plus
    </Icon>
    Add one
  </Button>
)

const RemoveIcon = withTheme(({ theme, onClick }) => (
  <Action
    onClick={onClick}
    style={{ color: theme.colorError }}
    title="Remove item"
  >
    <Icon color="currentColor">x</Icon>
  </Action>
))

const ArrayBuilder = ({ name, schema, value, updateVal }) => {
  if (schema.type === 'object') {
    const keys = schema.required ? schema.required.slice() : []
    Object.keys(schema.properties)
      .sort()
      .forEach(k => !keys.includes(k) && keys.push(k))
    return (
      <React.Fragment>
        {value.map((v, i) => (
          <FieldArray
            key={name + i}
            style={{ justifyContent: 'space-between' }}
          >
            {keys.map(k => (
              <SchemaSorter
                key={k}
                name={k}
                schema={schema.properties[k]}
                updateVal={val => {
                  const { name: n, value: v } = val
                  value[i][n] = v
                  updateVal({ name, value })
                }}
                value={v[k]}
              />
            ))}
            <RemoveIcon
              onClick={() => {
                value.splice(i, 1)
                updateVal({ name, value })
              }}
            />
          </FieldArray>
        ))}
        <Buttons right>
          <IconButton
            onClick={() => {
              const obj = {}
              schema.required.forEach(k => {
                obj[k] = schema.properties[k].default || undefined
              })
              value.push(obj)
              updateVal({ name, value })
            }}
          />
        </Buttons>
      </React.Fragment>
    )
  }
  return (
    <FieldArray>
      <legend>{schema.description}s</legend>
      {value.map((entry, i) => (
        <React.Fragment key={name + i}>
          <SchemaSorter
            hiddenLabel
            name={`${name}_${i}`}
            schema={schema}
            updateVal={val => {
              value[i] = val.value
              updateVal({ name, value })
            }}
            value={entry}
          />
          <RemoveIcon
            onClick={() => {
              value.splice(i, 1)
              updateVal({ name, value })
            }}
          />
        </React.Fragment>
      ))}
      <IconButton
        onClick={() => {
          value.push(schema.default || '')
          updateVal({ name, value })
        }}
      />
    </FieldArray>
  )
}

const SchemaSorter = ({
  hiddenLabel,
  name,
  schema,
  value = {},
  updateVal,
  level,
}) => {
  switch (schema.type) {
    case 'object':
      if (schema.properties) {
        const keys = schema.required ? schema.required.slice() : []
        Object.keys(schema.properties)
          .sort()
          .forEach(k => !keys.includes(k) && keys.push(k))
        const Parent = level === 3 ? React.Fragment : FieldArray
        return (
          <Parent>
            {level > 3 && <legend>{schema.description}</legend>}
            {keys.map(k => (
              <SchemaSorter
                key={k}
                level={level + 1}
                name={k}
                schema={schema.properties[k]}
                updateVal={val => {
                  const { name: n, value: v } = val
                  value[n] = v
                  const empty = ['""', '{}', '[]']
                  const str = JSON.stringify(v)
                  if (empty.includes(str)) delete value[n]
                  updateVal({ name, value })
                }}
                value={value[k]}
              />
            ))}
          </Parent>
        )
      }
      return null
    case 'array':
      return (
        <ArrayBuilder
          name={name}
          schema={schema.items}
          updateVal={updateVal}
          value={Object.keys(value).length === 0 ? [] : value}
        />
      )
    default:
      return (
        <FormItem
          aria-label={hiddenLabel && schema.description}
          format={schema.format}
          label={!hiddenLabel && schema.description}
          name={name}
          opts={schema.enum}
          type={schema.type}
          updateVal={updateVal}
          value={
            value === Object(value) && Object.keys(value).length === 0
              ? ''
              : value
          }
        />
      )
  }
}

class ConfigurationForm extends React.Component {
  state = {
    value: this.props.value,
    original: null,
    error: null,
    touched: false,
    refresh: false,
  }
  componentDidMount() {
    this.setState({ original: JSON.parse(JSON.stringify(this.props.value)) })
  }
  updateVal = ({ name, value }) => {
    this.setState({ value, error: null, touched: true })
  }
  handleError = err => {
    this.setState({ error: err.message })
  }
  cancelChanges = () => {
    const { original } = this.state
    this.setState({ value: original, refresh: true }, () => {
      this.setState({
        original: JSON.parse(JSON.stringify(original)),
        touched: false,
        refresh: false,
        error: null,
      })
    })
  }
  render() {
    const { name, description, schema } = this.props
    const { value, touched, refresh, error } = this.state
    return (
      <Mutation
        mutation={SAVE_PROP}
        onError={this.handleError}
        refetchQueries={[{ query: PROPS }]}
      >
        {(updateProp, { data }) => {
          const submitForm = async e => {
            e.preventDefault()
            const data = { name, schema, value }
            const success = await updateProp({ variables: { data } })
            if (success) {
              this.setState({
                touched: false,
                original: JSON.parse(JSON.stringify(value)),
              })
            }
          }
          return (
            <form id={`form_${name}`} onSubmit={e => submitForm(e)}>
              <Heading level={3}>{name}</Heading>
              <p>{description}</p>
              {refresh ? null : (
                <SchemaSorter
                  level={3}
                  name={name}
                  schema={JSON.parse(schema)}
                  updateVal={this.updateVal}
                  value={value}
                />
              )}
              {error && <ErrorText>{error}</ErrorText>}
              <Buttons right>
                <Button disabled={!touched} primary type="submit">
                  Save changes
                </Button>
                {touched && (
                  <Button onClick={() => this.cancelChanges()}>Cancel</Button>
                )}
              </Buttons>
            </form>
          )
        }}
      </Mutation>
    )
  }
}

export default ConfigurationForm
