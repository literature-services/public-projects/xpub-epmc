import React from 'react'
import { withRouter } from 'react-router'
import { Mutation } from 'react-apollo'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import { Button, CheckboxGroup, TextField } from '@pubsweet/ui'
import { Buttons, TextArea } from './ui'
import { REJECT_MANUSCRIPT } from './operations'

export const Fieldset = styled.fieldset`
  border: 0;
  padding: 0;
  margin-bottom: calc(${th('gridUnit')} * 3);
  & > label {
    ${override('ui.Label')};
  }
`
export const adminOptions = [
  {
    value: 'title',
    label: 'Manuscript title matching submission',
  },
  {
    value: 'affiliations',
    label: 'Complete title page with authors & affiliations',
  },
  {
    value: 'figures',
    label: 'Figures, with captions',
  },
  {
    value: 'tables',
    label: 'Tables, with captions',
  },
  {
    value: 'supp',
    label: 'Supplementary material',
  },
  {
    value: 'refs',
    label: 'Complete reference list',
  },
  {
    value: 'display',
    label: 'Legible text and graphics',
  },
  {
    value: 'latin',
    label: 'Content in Latin script',
  },
  {
    value: 'grants',
    label: 'Appropriate grant linking',
  },
  {
    value: 'embargo',
    label: 'Embargo (Plan S change)',
  },
]

const messages = title => ({
  title: {
    subject: 'manuscript title mismatch',
    message: `We have found that the file you have submitted is for a manuscript entitled "[INSERT TITLE HERE]", while your submission was made under the title "${title}".\n\nPlease respond to this email if there is a reason the title and file do not match, and we should proceed with the selected information. Otherwise, we have returned the manuscript to a state where you can upload a different file, or fix the citation. To change the material and resubmit, please visit:`,
  },
  affiliations: {
    subject: 'incomplete title page',
    message:
      'The manuscript is missing a complete title page. We do not have author and affiliation information for this manuscript and are therefore unable to proceed with processing of this manuscript for Europe PMC.',
  },
  figures: {
    subject: 'missing figures',
    message:
      'This submission makes reference to figures that have not been uploaded with the manuscript.',
  },
  tables: {
    subject: 'missing tables',
    message:
      'This submission makes reference to tables that have not been uploaded with the manuscript.',
  },
  supp: {
    subject: 'missing supplementary material',
    message:
      'This submission makes reference to supplementary files that have not been uploaded with the manuscript.',
  },
  refs: {
    subject: 'incomplete reference list',
    message: 'The manuscript is missing a complete reference list.',
  },
  display: {
    subject: 'poor display of content',
    message:
      'The figures supplied are of low resolution and contain blurry or illegible text. Please provide images of higher quality. If you are unable to provide higher quality image files because such files are not available, please let us know.',
  },
  grants: {
    subject: 'linked grants mismatch',
    message: `There is a problem with the grants linked to the manuscript. [DESCRIBE PROBLEM HERE]\n\nTo provide the correct grants your submission should be linked to, please visit:`,
  },
  embargo: {
    subject: 'embargo and licensing requirements have changed',
    message: `Due to funding information found in your manuscript, your submission is subject to certain license and embargo requirements. For more information and to agree to these funder requirements, please return to your submission and edit the highlighted 'Funding & Access' section.\n\nYour submission can be checked and resubmitted at:`,
  },
  latin: {
    subject: 'The primary language uses non-Latin script',
    message:
      'Unfortunately your submission to Europe PMC cannot be processed, because we currently only accept peer-reviewed manuscripts written using the Latin script, or alphabet. If such a version is available, please contact the Europe PMC Helpdesk at helpdesk@europepmc.org.',
  },
})

const materialErrs = [
  'affiliations',
  'figures',
  'tables',
  'supp',
  'refs',
  'display',
]

const formatName = name =>
  `${name.title ? `${name.title} ` : ''}${name.givenNames} ${name.surname}`

class SubmissionErrorReport extends React.Component {
  state = {
    subject: '',
    people: [],
    selected: [],
    message: '',
  }
  componentDidMount() {
    const { teams } = this.props
    const submitter = teams.find(team => team.role === 'submitter')
      ? teams.find(team => team.role === 'submitter').teamMembers[0]
      : null
    const reviewer = teams.find(team => team.role === 'reviewer')
      ? teams.find(team => team.role === 'reviewer').teamMembers[0]
      : null
    const people = []
    if (submitter) {
      people.push({
        label: `${formatName(submitter.alias.name)} (Submitter${
          reviewer && reviewer.user.id === submitter.user.id ? '/Reviewer' : ''
        })`,
        value: JSON.stringify(submitter),
      })
    }
    if (reviewer && reviewer.user.id !== submitter.user.id) {
      people.push({
        label: `${formatName(reviewer.alias.name)} (Reviewer)`,
        value: JSON.stringify(reviewer),
      })
    }
    this.setState({ people, selected: [JSON.stringify(submitter)] })
    this.generateMessage()
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selected !== this.state.selected ||
      prevProps.checkedBoxes !== this.props.checkedBoxes
    ) {
      this.generateMessage()
    }
  }
  generateMessage = () => {
    const { checkedBoxes, currentUser, manuscript } = this.props
    const { id, meta } = manuscript
    const errors = messages(meta.title)
    const subject = `${id} submission error:${checkedBoxes.map(
      err => ` ${errors[err] ? errors[err].subject : ''}`,
    )}.`
    const link = `${window.location.origin}/submission/${id}/submit`
    let message = `Dear ${this.state.selected
      .map(s => formatName(JSON.parse(s).alias.name))
      .join(
        ', ',
      )},\n\nThank you for your submission to Europe PMC plus.\n\n${checkedBoxes
      .map(err => (errors[err] ? `${errors[err].message}\n\n` : ''))
      .join('')}`

    if (!checkedBoxes.some(err => err === 'latin')) {
      if (checkedBoxes.some(err => materialErrs.includes(err))) {
        message += `We have returned the manuscript to a state where you can upload and submit the missing material. To provide the required material and resubmit, please visit:\n\n`
      }
      message += `${link}\n\n`
    }

    message += `Kind regards,\n\n${formatName(
      currentUser.identities[0].name,
    )}\nEurope PMC Helpdesk`

    this.setState({ message, subject })
  }
  render() {
    const { subject, message, people, selected } = this.state
    const { close, history, manuscript } = this.props
    return (
      <Mutation mutation={REJECT_MANUSCRIPT}>
        {(rejectManuscript, { data }) => {
          const reject = async () => {
            await rejectManuscript({
              variables: {
                data: {
                  manuscriptId: manuscript.id,
                  manuscriptVersion: manuscript.version,
                  notesType: 'userMessage',
                  content: JSON.stringify({
                    to: selected.map(s => JSON.parse(s).user.id),
                    subject,
                    message,
                  }),
                },
              },
            })
            history.push('/')
          }
          return (
            <div>
              <Fieldset>
                <label>Recipient(s)</label>
                <CheckboxGroup
                  onChange={checked => this.setState({ selected: checked })}
                  options={people}
                  value={selected}
                />
              </Fieldset>
              <TextField
                label="Subject"
                onChange={e => this.setState({ subject: e.target.value })}
                value={subject}
              />
              <TextArea
                label="Message"
                onChange={e => this.setState({ message: e.target.value })}
                rows={15}
                value={message}
              />
              <Buttons right>
                <Button
                  disabled={selected.length === 0 || !message}
                  onClick={reject}
                  primary
                >
                  Send & Reject
                </Button>
                <Button onClick={close}>Cancel</Button>
              </Buttons>
            </div>
          )
        }}
      </Mutation>
    )
  }
}

export default withRouter(SubmissionErrorReport)
