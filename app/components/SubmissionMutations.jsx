import React from 'react'
import { Mutation, Query } from 'react-apollo'
import { Loading, LoadingIcon } from './ui'
import {
  CREATE_NOTE,
  UPDATE_NOTE,
  DELETE_NOTE,
  GET_MANUSCRIPT,
  UPDATE_MANUSCRIPT,
  GET_PROP,
} from './operations'

export const ManuscriptMutations = BaseComponent => ({
  manuscript,
  children,
  refetch = [
    {
      query: GET_MANUSCRIPT,
      variables: { id: manuscript.id },
    },
  ],
  ...props
}) => (
  <Mutation mutation={UPDATE_MANUSCRIPT} refetchQueries={() => refetch}>
    {(updateManuscript, { data }) => {
      const { id } = manuscript
      const changeCitation = async citationData =>
        updateManuscript({
          variables: { data: { id, ...citationData } },
        })
      const updateEmbargo = async releaseDelay =>
        updateManuscript({
          variables: { data: { id, meta: { releaseDelay } } },
        })
      const updateGrants = async fundingGroup =>
        updateManuscript({
          variables: { data: { id, meta: { fundingGroup } } },
        })
      const setStatus = async (status, callBack) => {
        const data = { id, status }
        if (status === 'repo-ready') {
          data.ncbiState = null
        }
        await updateManuscript({ variables: { data } })
        callBack && callBack()
      }
      return (
        <BaseComponent
          changeCitation={changeCitation}
          manuscript={manuscript}
          setStatus={setStatus}
          updateEmbargo={updateEmbargo}
          updateGrants={updateGrants}
          {...props}
        >
          {children}
        </BaseComponent>
      )
    }}
  </Mutation>
)

export const NoteMutations = BaseComponent => ({
  manuscript,
  children,
  refetch = [
    {
      query: GET_MANUSCRIPT,
      variables: { id: manuscript.id },
    },
  ],
  ...props
}) => (
  <Mutation
    awaitRefetchQueries
    mutation={UPDATE_NOTE}
    refetchQueries={() => refetch}
  >
    {(updateNote, { data }) => {
      const changeNote = async note => {
        const data = { ...note }
        if (!data.manuscriptVersion) {
          data.manuscriptVersion = manuscript.version
        }
        const { data: done } = await updateNote({
          variables: { data },
        })
        return done.updateNote
      }
      return (
        <Mutation
          awaitRefetchQueries
          mutation={CREATE_NOTE}
          refetchQueries={() => refetch}
        >
          {(createNote, { data }) => {
            const newNote = async note => {
              const data = { ...note }
              if (manuscript.id && !data.manuscriptId) {
                data.manuscriptId = manuscript.id
              }
              if (!data.manuscriptVersion) {
                data.manuscriptVersion = manuscript.version
              }
              const { data: done } = await createNote({
                variables: { data },
              })
              return done.createNote
            }
            return (
              <Mutation
                awaitRefetchQueries
                mutation={DELETE_NOTE}
                refetchQueries={() => refetch}
              >
                {(deleteNote, { data }) => {
                  const removeNote = async id => {
                    const { data: done } = await deleteNote({
                      variables: { id },
                    })
                    return done.deleteNote
                  }
                  return (
                    <BaseComponent
                      changeNote={changeNote}
                      deleteNote={removeNote}
                      manuscript={manuscript}
                      newNote={newNote}
                      {...props}
                    >
                      {children}
                    </BaseComponent>
                  )
                }}
              </Mutation>
            )
          }}
        </Mutation>
      )
    }}
  </Mutation>
)

export const WithFunders = BaseComponent => ({ ...props }) => (
  <Query query={GET_PROP} variables={{ name: 'planSFunders' }}>
    {({ data, loading }) => {
      if (loading || !data) {
        return (
          <Loading>
            <LoadingIcon />
          </Loading>
        )
      }
      const { value } = data.findProp
      return <BaseComponent funders={value} {...props} />
    }}
  </Query>
)
