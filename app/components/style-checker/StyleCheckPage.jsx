import React from 'react'
import Dropzone from 'react-dropzone'
import { graphql, compose } from 'react-apollo'
import styled from 'styled-components'
import { th, lighten, override } from '@pubsweet/ui-toolkit'
import { H1, H2, Icon, Button } from '@pubsweet/ui'
import { Page, A, LoadingIcon, HTMLString } from '../ui'
import { CHECK_STYLE } from './operations'

const DropArea = styled.div`
  width: 100%;
  text-align: center;
  padding: calc(${th('gridUnit')} * 3);
  border: 2px dashed ${th('colorBorder')};
  cursor: default;
  outline: none;
  align-items: center;
  justify-content: center;
  span {
    color: ${th('colorPrimary')};
  }
  &:hover {
    border-color: ${lighten('colorPrimary', 20)};
    background-color: ${th('colorTextReverse')};
    span {
      color: ${lighten('colorPrimary', 20)};
    }
  }
`

const Report = styled.div`
  .report-details {
    padding: 0 ${th('gridUnit')};
    border: 2px solid ${th('colorBorder')};
    background-color: ${th('colorTextReverse')};
    a {
      ${override('ui.Link')};
    }
    strong {
      font-weight: 600;
    }
    #failNotify:after {
      content: ' \\2718';
      color: ${th('colorError')};
    }
    #passNotify:after {
      content: ' \\2714';
      color: ${th('colorSuccess')};
    }
    .errortext {
      color: ${th('colorError')};
    }
    .warningtext {
      color: ${th('colorWarning')};
    }
  }
`

class StyleCheckPage extends React.Component {
  state = { loading: false, result: null }
  styleCheckFile = async (file, c) => {
    c.setState({ loading: true })
    const { checkStyle } = c.props
    const { data } = await checkStyle({ variables: { file } })
    const { checkStyle: result } = data
    c.setState({ result, loading: false })
  }
  render() {
    const { loading, result } = this.state
    return (
      <Page>
        <H1>Europe PMC plus style checker</H1>
        <p>
          {`Use this tool, derived from the NLM style checker, to confirm whether an XML file conforms to Europe PMC style. Europe PMC style is derived from PMC style—for author manuscripts, use the `}
          <A href="http://www.pubmedcentral.nih.gov/pmcdoc/tagging-guidelines/manuscript/style.html">
            NIH Manuscript Tagging Guidelines
          </A>
          {`, and for preprints, use the `}
          <A href="http://www.pubmedcentral.nih.gov/pmcdoc/tagging-guidelines/article/style.html">
            PMC Tagging guidelines
          </A>
          {` (though with the article-type "preprint").`}
        </p>
        <p>Upload your XML file. The results will be displayed momentarily.</p>
        <p>
          Hit{' '}
          <Button
            onClick={() => this.setState({ result: null, loading: false })}
          >
            Clear
          </Button>{' '}
          to return to a clean form.
        </p>
        {result ? (
          <Report>
            <H2>Stylechecker results</H2>
            <HTMLString element="div" string={result} />
          </Report>
        ) : (
          <Dropzone
            accept={('application/xml', 'text/xml')}
            multiple={false}
            onDrop={file => this.styleCheckFile(file, this)}
          >
            {({ getRootProps, getInputProps }) => (
              <DropArea {...getRootProps()}>
                <input {...getInputProps()} id="file_drop" />
                {loading ? (
                  <LoadingIcon size={6} />
                ) : (
                  <Icon color="currentColor" size={6}>
                    upload
                  </Icon>
                )}
                <label htmlFor="file_drop">
                  <p>
                    {`Drop your file here, or click to select from your computer.`}
                  </p>
                </label>
              </DropArea>
            )}
          </Dropzone>
        )}
      </Page>
    )
  }
}

export default compose(graphql(CHECK_STYLE, { name: 'checkStyle' }))(
  StyleCheckPage,
)
