import React from 'react'
import { HTMLString, PreprintLabel, FullTextLabel } from '../ui'
import { Result, Citation } from './PubMedSearchResult'

function Label(props) {
  if (props.fullText === 'N') {
    return null
  }
  return <FullTextLabel>Already in Europe PMC</FullTextLabel>
}

const PreprintSearchResult = ({ result, ...props }) => (
  <Result {...props}>
    <PreprintLabel />
    <HTMLString string={result.title} />
    <br />
    <Citation>
      <HTMLString
        string={`${result.authorString}. ${result.bookOrReportDetails.publisher}. ${result.pubYear}. PPRID: ${result.id} DOI: ${result.doi}`}
      />
    </Citation>
    <Label fullText={result.inEPMC} />
  </Result>
)

export default PreprintSearchResult
