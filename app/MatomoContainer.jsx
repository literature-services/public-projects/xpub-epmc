import React, { useEffect } from 'react'
import config from 'config'

const { baseUrl } = config['pubsweet-server']
const { siteId, containerId } = config.matomo

console.error(config.matomo)

// eslint-disable-next-line no-unused-vars
function MatomoContainer({ children }) {
  useEffect(() => {
    // eslint-disable-next-line no-multi-assign
    const _paq = (window._paq = window._paq || [])
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView'])
    _paq.push(['enableLinkTracking'])
    _paq.push(['setTrackerUrl', `${baseUrl}/matomo/matomo.php`])
    _paq.push(['setSiteId', siteId])
    const d = document
    const g = d.createElement('script')
    const s = d.getElementsByTagName('script')[0]
    g.async = true
    g.src = `${baseUrl}/piwik/matomo.js`
    s.parentNode.insertBefore(g, s)
  }, [])

  return <div>{children}</div>
}

function MatomoContainerWithTagManager({ children }) {
  useEffect(() => {
    // eslint-disable-next-line no-multi-assign
    const _mtm = (window._mtm = window._mtm || [])
    _mtm.push({ 'mtm.startTime': new Date().getTime(), event: 'mtm.Start' })
    // (function() {
    const d = document
    const g = d.createElement('script')
    const s = d.getElementsByTagName('script')[0]
    g.async = true
    g.src = `${baseUrl}/piwik/js/${containerId}.js`
    s.parentNode.insertBefore(g, s)
    // })();
  }, [])

  return <div>{children}</div>
}
export default MatomoContainerWithTagManager
