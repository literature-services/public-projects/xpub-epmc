import 'regenerator-runtime/runtime'

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import { Root } from 'pubsweet-client'
import { JournalProvider } from 'xpub-journal'
import { InMemoryCache, defaultDataIdFromObject } from 'apollo-cache-inmemory'

import theme from './epmc-theme'
import Routes from './routes'
import MatomoContainer from './MatomoContainer'
import './assets/favicon.ico'

const makeConfig = config => {
  config.cache = new InMemoryCache({
    typePolicies: {
      ArticleId: {
        keyFields: ['pubIdType', 'id'],
      },
    },
    dataIdFromObject(responseObject) {
      switch (responseObject.__typename) {
        case 'ArticleId':
          return `${responseObject.pubIdType}:${responseObject.id}`
        default:
          return defaultDataIdFromObject(responseObject)
      }
    },
  })
  return config
}

const render = () => {
  ReactDOM.render(
    <MatomoContainer>
      <AppContainer>
        <JournalProvider
          journal={{ name: 'Europe PMC plus' }}
          style={{ height: '100%' }}
        >
          <Root
            connectToWebSocket="false"
            makeApolloConfig={makeConfig}
            routes={<Routes />}
            theme={theme}
          />
        </JournalProvider>
      </AppContainer>
    </MatomoContainer>,
    document.getElementById('root'),
  )
}

render()

if (module.hot) {
  module.hot.accept('./routes', () => {
    render()
  })
}
