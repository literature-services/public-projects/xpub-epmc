import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default css`
  font-weight: 600;
  font-size: ${th('fontSizeBaseSmall')};
`
