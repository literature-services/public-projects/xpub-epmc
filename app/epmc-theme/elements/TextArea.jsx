import { css } from 'styled-components'

const th = name => props => props.theme[name]

const invalid = css`
  border-color: ${th('colorError')};
`

export default {
  Area: css`
    ${props => props.invalidTest && invalid};
  `,
}
