import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default css`
  margin-top: calc(-${th('gridUnit')} * 3);
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  display: flex;
  align-items: flex-start;
`
