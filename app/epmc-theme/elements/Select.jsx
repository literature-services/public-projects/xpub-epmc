import { css } from 'styled-components'

const th = name => props => props.theme[name]

const invalid = css`
  border-color: ${th('colorError')};
`
export default {
  DropdownText: css`
    background-color: ${th('colorTextReverse')};
    border-radius: ${th('borderRadius')};
    ${props => props.invalidTest && invalid};
  `,
  DropdownIcon: css`
    background-color: ${th('colorTextReverse')};
    padding: ${th('gridUnit')};
  `,
}
