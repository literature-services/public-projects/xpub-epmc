import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const level = [
  css`
    margin-top: calc(${th('gridUnit')} * 4);
    margin-bottom: calc(${th('gridUnit')} * 2);
  `,
  css`
    margin-top: calc(${th('gridUnit')} * 3.75);
    margin-bottom: calc(${th('gridUnit')} * 2);
  `,
  css`
    margin-top: calc(${th('gridUnit')} * 3.5);
    margin-bottom: calc(${th('gridUnit')} * 1.5);
  `,
  css`
    margin-top: calc(${th('gridUnit')} * 3);
    margin-bottom: calc(${th('gridUnit')} * 1.5);
  `,
  css`
    margin-top: calc(${th('gridUnit')} * 2);
    margin-bottom: ${th('gridUnit')};
  `,
  css`
    margin-top: calc(${th('gridUnit')} * 2);
    margin-bottom: ${th('gridUnit')};
  `,
]

export default css`
  color: ${th('colorText')};
  font-weight: 600;
  ${props => props.level && level[props.level - 1]};
`
