import { css } from 'styled-components'

export default css`
  svg {
    ${props => props.strokeWidth && `stroke-width: ${props.strokeWidth};`}
  }
`
