// Update with your config settings.
const config = require('config')
const path = require('path')

const connection = config['pubsweet-server'] && config['pubsweet-server'].db

const pool = {
  min: 0,
  max: 20,
  propagateCreateError: false 
}

module.exports = {
  development: {
    client: 'postgresql',
    connection,
    pool,
    migrations: {
      tableName: 'knex_migrations',
      directory: path.join(__dirname, '/server/xpub-model/migrations'),
    },
    seeds: {
      directory: path.join(__dirname, '/server/xpub-model/seeds'),
    },
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user: 'username',
      password: 'password',
    },
    pool,
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  production: {
    client: 'postgresql',
    connection,
    pool,
    migrations: {
      tableName: 'knex_migrations',
      directory: path.join(__dirname, '/server/xpub-model/migrations'),
    },
    seeds: {
      directory: path.join(__dirname, '/server/xpub-model/seeds'),
    },
  },
}
