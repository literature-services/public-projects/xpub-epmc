if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const path = require('path')
const components = require('./components.json')
const states = require('./states.json')
const withdrawalOptions = require('./withdrawalOptions.json')
const gql = require('graphql-tag')
const { pubsweetLogger } = require('../server/utils/loggers')

// This wrapping is needed to overcome the winston 3.x logger object's circular reference which
// is not allowed in node config object.
const logger = {
  error: (...args) => pubsweetLogger.error(...args),
  warn: (...args) => pubsweetLogger.warn(...args),
  info: (...args) => pubsweetLogger.info(...args),
  debug: (...args) => pubsweetLogger.debug(...args),
  startTimer: (...args) => pubsweetLogger.startTimer(...args),
}

module.exports = {
  schema: {},
  authsome: {
    mode: path.resolve(__dirname, 'authsome.js'),
    teams: {
      admin: {
        name: 'admin',
      },
      submitter: {
        name: 'submitter',
      },
      reviewer: {
        name: 'reviewer',
      },
    },
  },
  pubsweet: {
    components,
  },
  mailer: {
    from: 'helpdesk@europepmc.org',
    path: `${__dirname}/mailer`,
    transport: {
      sendmail: true,
    },
  },
  logger,
  'pubsweet-server': {
    db: {
      host: process.env.PGHOST,
      port: 5432,
      database: process.env.PGDATABASE,
      user: process.env.PGUSER,
      password: process.env.POSTGRES_PASSWORD,
      application_name: process.env.APP_NAME || 'pubsweet',
    },
    port: 3000,
    logger,
    uploads: 'uploads',
    graphiql: true,
    enableExperimentalGraphql: true,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    'login-redirect': '/',
    'redux-log': false,
    'password-reset': {
      graphql: {
        mutations: {
          emailPasswordResetLink: gql`
            mutation($email: String!) {
              epmc_emailPasswordResetLink(email: $email)
            }
          `,
          resetPassword: gql`
            mutation($token: String!, $newPassword: String!) {
              epmc_resetPassword(token: $token, newPassword: $newPassword)
            }
          `,
        },
      },
      appName: 'Europe PMC plus',
    },
    'europepmc-domains': [
      'europepmc.org',
      'test.europepmc.org',
      'beta.europepmc.org',
      'dev.europepmc.org',
      'staging.europepmc.org',
    ],
  },
  'epmc-email': {
    url: process.env.PUBSWEET_EMAIL_URL || 'http://localhost:3000/',
    sender: process.env.PUBSWEET_SENDER,
    system: process.env.PUBSWEET_SYSTEM_EMAIL,
    dev: process.env.PUBSWEET_DEV_EMAIL,
    externalQA: process.env.PUBSWEET_SYSTEM_EMAIL,
    listName: 'Europe PMC plus',
  },
  'pubsweet-component-ink-backend': {
    inkEndpoint:
      process.env.INK_ENDPOINT || 'http://inkdemo-api.coko.foundation/',
    email: process.env.INK_USERNAME,
    password: process.env.INK_PASSWORD,
    maxRetries: 500,
    recipes: {
      'editoria-typescript': '2',
    },
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    validations: path.resolve(__dirname, 'upload-validations.js'),
  },
  publicKeys: [
    'pubsweet-client',
    'pubsweet-server',
    'authsome',
    'validations',
    'file',
    'pageSize',
    'ftpAddr',
    'states',
    'notification',
    'withdrawalOptions',
    'europepmcUrl',
    "['grist-api'].url",
    'thorApiUrl',
    'epmcOrcidWebhookToolUrl',
    'ftp_tagger',
    'matomo',
  ],
  login: {
    enableMock: false,
  },
  organization: {
    europepmc_plus: 'Europe PMC Plus',
    europepmc_preprints: 'Europe PMC Preprints',
  },
  manuscript: {
    status: {
      initial: 'INITIAL',
    },
  },
  states,
  withdrawalOptions,
  user: {
    identity: {
      default: 'local',
      local: 'local',
    },
  },
  file: {
    type: {
      manuscript: 'manuscript',
    },
    url: {
      download: '/download',
    },
  },
  pageSize: process.env.PAGE_SIZE || 50,
  benmark: { logger },
  ftp_directory: 'ftpdata',
  ftpAddr: process.env.PUBSWEET_FTP_ADDR,
  users: [],
  ftp_account: [],
  ftp_tagger: {
    username: 'taggers',
    email: '',
  },
  ftp_healthcheck: {
    username: process.env.FTP_HEALTH_USER,
    password: process.env.FTP_HEALTH_PASSWORD,
    host: process.env.FTP_HEALTH_HOST,
    enabled: process.env.FTP_HEALTH_ENABLED,
  },
  ftp_bulkupload: {
    chokidar: {
      stabilityThreshold: 60000,
      pollInterval: 2000,
    },
  },
  'ebi-repository': {},
  'grist-api': {
    url: 'https://www.ebi.ac.uk/europepmc/GristAPI',
  },
  thorApiUrl: 'https://beta.plus.europepmc.org/europepmc/hubthor',
  epmcOrcidWebhookToolUrl:
    'https://beta.plus.europepmc.org/orcid/webtool/webhook_register',
  europepmcUrl: 'https://europepmc.org',
  ncbiFileTypes: [
    {
      value: 'citation',
      numeric: 2,
    },
    {
      value: 'supplement',
      numeric: 6,
    },
    {
      value: 'citationXML',
      numeric: 8,
    },
    {
      value: 'IMGsmall',
      numeric: 9,
    },
    {
      value: 'IMGview',
      numeric: 10,
    },
    {
      value: 'IMGprint',
      numeric: 11,
    },
    {
      value: 'supplement_tag',
      numeric: 21,
    },
    {
      value: 'pdf4load',
      numeric: 3,
    },
  ],
}
