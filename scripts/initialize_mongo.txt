use MSSRepo
db.getCollection('fs.chunks').createIndex({'files_id': "hashed"}, {background : true})
db.getCollection('fs.files').createIndex({'metadata.compositeId': 1}, {background : true})