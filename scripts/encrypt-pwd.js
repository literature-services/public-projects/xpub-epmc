const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')
const Identity = require('../server/xpub-model/entities/identity/data-access')

const knex = BaseModel.knex()

async function run() {
  try {
    const identities = await knex('identity')
      .select(['id', 'password_hash'])
      .whereRaw('length(password_hash)=?', [7])
    // strategy 1
    // /* eslint-disable no-await-in-loop */
    // /* eslint-disable no-restricted-syntax */
    // for (const identity of identities) {
    //   await knex('identity')
    //     .where({ id: identity.id })
    //     .update({
    //       password_hash: await Identity.hashPassword(identity.passwordHash),
    //     })
    // }

    // strategy 2
    await knex.raw(
      `CREATE TABLE identity_tmp(id UUID PRIMARY KEY, password_hash TEXT);`,
    )

    let sql = 'INSERT INTO identity_tmp VALUES '
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const identity of identities) {
      const newPasswordHash = await Identity.hashPassword(identity.passwordHash)
      sql += `('${identity.id}','${newPasswordHash}'),`
    }
    sql = sql.substring(0, sql.length - 1)
    await knex.raw(sql)

    await knex.raw(`
      UPDATE identity SET password_hash=identity_tmp.password_hash FROM identity_tmp WHERE identity.id=identity_tmp.id
    `)

    await knex.raw(`DROP TABLE identity_tmp;`)
  } catch (e) {
    logger.info(e)
  }
}

;(async () => {
  await run()
  await knex.destroy()
})()
