#!/bin/bash

APP_LABEL=$1
sleep 30
POD_STATUS=$(kubectl get pod -l app=$APP_LABEL)
echo "$POD_STATUS"

if echo "$POD_STATUS" | grep -o -e "CrashLoop" -e "Error" -e "ImagePull"
then
    sleep 250
    echo "Getting the POD status again !!"
    POD_STATUS=$(kubectl get pod -l app=$APP_LABEL)
    echo "$POD_STATUS"
    if echo "$POD_STATUS" | grep -o -e "CrashLoop" -e "Error" -e "ImagePull"
    then
        echo "ERROR: Deployment Failed !!!"
        exit 1
    fi    
fi

RUNNING_POD_COUNT=$(echo "$POD_STATUS" | grep -o "1/1     Running" | wc -l)
READY_POD_COUNT=$(echo "$POD_STATUS" | grep -o "0/1     Running" | wc -l)

echo "Running POD count - $RUNNING_POD_COUNT"
echo "Ready POD count - $READY_POD_COUNT"

COUNTER=0
if [ "$RUNNING_POD_COUNT" -eq 0 -a "$READY_POD_COUNT" -gt 0 ] || [ "$RUNNING_POD_COUNT" -eq 0 -a "$READY_POD_COUNT" -eq 0 ]
then
    echo "Condition 1 - No running POD"
	while [ $COUNTER -le 20 ]; do
	    POD_STATUS=$(kubectl get pod -l app=$APP_LABEL)
		RUNNING_POD_COUNT=$(echo "$POD_STATUS" | grep -o "1/1     Running" | wc -l)
		echo "Running POD count in the loop - $RUNNING_POD_COUNT"
		if [ "$RUNNING_POD_COUNT" -gt 0 ]
		then
	      echo "Deployment Successful !!!"
		  exit 0
		fi
		echo "Waiting for $APP_LABEL..."
		sleep 30
		COUNTER=$((COUNTER + 1))
	done	
else
    echo "Condition 2 - Running POD found"
	while [ $COUNTER -le 20 ]; do
		POD_STATUS=$(kubectl get pod -l app=$APP_LABEL)
		if echo "$POD_STATUS" | grep -e "Terminating"
		then
		  NON_RUNNING_POD_COUNT=$(echo "$POD_STATUS" | grep -o "0/1     Running" | wc -l)
		  if [ "$NON_RUNNING_POD_COUNT" -eq 0 ]
		  then
		    echo "Deployment Successful !!!"
		    exit 0
	      fi		
		fi
		echo "Waiting for $APP_LABEL..."
		sleep 30
		COUNTER=$((COUNTER + 1))
	done
fi

echo "ERROR: $APP_LABEL is not running"
exit 1