const saxon = require('saxon-js')
const fs = require('fs')
const path = require('path')
const { stringify } = require('flatted')

const xslPath = path.resolve(
  __dirname,
  '../server/xml-validation/xsl/stylechecker/nlm-stylechecker.xsl',
  // '../server/xml-validation/xsl/stylechecker/nlm-stylereporter.xsl',
)
const xslString = fs.readFileSync(xslPath).toString()

const env = saxon.getPlatform()
const xsl = env.parseXmlFromString(xslString)

// Generate local file base
const localBaseURI = path.resolve(
  __dirname,
  '../server/xml-validation/xsl/stylechecker/',
)
xsl._saxonBaseUri = `file://${localBaseURI}/`
const sef = saxon.compile(xsl)
const sefString = stringify(sef)

// search and replace file path to server file path
const replacedSef = sefString.replaceAll(
  localBaseURI,
  '/home/xpub/server/xml-validation/xsl/stylechecker',
)
fs.writeFileSync('nlm-stylechecker.540.compile.sef.json', replacedSef)
