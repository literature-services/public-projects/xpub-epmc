PROCESS_NAME=$1
NODE_FILE=$2

RUNNING=$(curl --silent --connect-timeout 20 "https://beta.plus.europepmc.org/healthcheck?datacenterName=HL" | grep "\"status\":\"OK\"")
if [ -n "$RUNNING" ] ; then
        echo "Health check PASS"
        APP_NAME=$PROCESS_NAME node --harmony-promise-finally $NODE_FILE
        exit 0
fi

echo "Health check failed. Cronjob will skip"
exit 0
