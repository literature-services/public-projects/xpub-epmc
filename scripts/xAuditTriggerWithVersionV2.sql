ALTER TABLE audit.manuscript_process_dates DROP CONSTRAINT manuscript_process_dates_pkey;

ALTER TABLE audit.manuscript_process_dates ADD CONSTRAINT manuscript_process_dates_uk UNIQUE (manuscript_id, manuscript_version);

DROP FUNCTION audit.populate_manuscript_process_dates;

CREATE FUNCTION audit.populate_manuscript_process_dates(pv_manuscript_id TEXT, pn_manuscript_version NUMERIC, new_status TEXT)
RETURNS void
LANGUAGE 'plpgsql'
AS $$
DECLARE
  c1 CURSOR FOR
    SELECT *
    FROM audit.manuscript_process_dates
    WHERE manuscript_id = pv_manuscript_id
      AND manuscript_version = pn_manuscript_version;
  curr_process_dates audit.manuscript_process_dates;
  new_process_dates audit.manuscript_process_dates;
BEGIN
  --Does the record already exist?
  OPEN c1;
  FETCH c1 INTO curr_process_dates;
  CLOSE c1;

  IF curr_process_dates.manuscript_id IS NULL THEN
    --RAISE NOTICE 'Manuscript is not found in the table';
    new_process_dates.manuscript_id=pv_manuscript_id;
    new_process_dates.manuscript_version=pn_manuscript_version;
  ELSE
    --RAISE NOTICE 'Manuscript is found in the table, submit date is:%', curr_process_dates.submitted_date;
    new_process_dates = curr_process_dates;
  END IF;

  IF new_status='submitted' THEN
    IF new_process_dates.submitted_date IS NULL THEN
      new_process_dates.submitted_date = CURRENT_TIMESTAMP;
    --RAISE NOTICE 'submitted date populated.';
    END IF;
  ELSEIF new_status='xml-review' THEN
    IF new_process_dates.xml_review_date IS NULL THEN
      new_process_dates.xml_review_date = CURRENT_TIMESTAMP;
    --RAISE NOTICE 'xml review date populated.';
    END IF;
  ELSEIF new_status='published' THEN
    IF new_process_dates.first_published_date IS NULL THEN
      new_process_dates.first_published_date = CURRENT_TIMESTAMP;
    --RAISE NOTICE 'first pub date populated.';
    END IF;
  ELSEIF new_status='repo-ready' THEN
    IF new_process_dates.ncbi_ready_date IS NULL THEN
      new_process_dates.ncbi_ready_date = CURRENT_TIMESTAMP;
    --RAISE NOTICE 'NCBI ready date populated.';
    END IF;
  END IF;

  IF curr_process_dates.manuscript_id IS NULL THEN
    --raise notice 'new manuscript id:%', new_process_dates.manuscript_id;
    INSERT INTO audit.manuscript_process_dates VALUES (new_process_dates.*);
  ELSE
    UPDATE audit.manuscript_process_dates
    SET submitted_date=new_process_dates.submitted_date,
        xml_review_date=new_process_dates.xml_review_date,
        first_published_date=new_process_dates.first_published_date,
        ncbi_ready_date=new_process_dates.ncbi_ready_date
    WHERE manuscript_id=pv_manuscript_id
      AND manuscript_version=pn_manuscript_version;

    --raise notice 'NCBI date set to:% for man id:%, version:%', new_process_dates.ncbi_ready_date, pv_manuscript_id, pn_manuscript_version;
  END IF;
END;
$$;

COMMENT ON FUNCTION audit.populate_manuscript_process_dates(TEXT, NUMERIC, TEXT) IS $body$
Populate the relevant date in manuscript_process_dates table with current system timestamp.
​
Arguments:
   pv_manuscript_id : Manuscript ID (EMS ID) which is being updated
   pn_manuscript_version: Version number of the manuscript being updated
   new_status : New Status which decides which date gets populated.
$body$;

CREATE OR REPLACE FUNCTION audit.if_modified_func() RETURNS TRIGGER AS $body$
DECLARE
  audit_row audit.audit_log;
  include_values boolean;
  log_diffs boolean;
  h_old_data hstore;
  h_changes hstore;
  excluded_cols text[] = ARRAY[]::text[];
BEGIN
  IF TG_WHEN <> 'AFTER' THEN
    RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
  END IF;

  audit_row = ROW(
    uuid_generate_v4(), -- id
        current_timestamp,                            -- created
    NULL,                                         -- user_id
    substring(TG_OP,1,1),                         -- action
    NULL,                                         -- original_data
    NULL,                                         -- changes
    NULL,                                         -- object_id
    --        TG_RELID,                                     -- object_id
    TG_TABLE_NAME::text,                          -- object_type
    NULL,                                          -- manuscript_id
    NULL                                          -- manuscript_version
    );

  IF TG_ARGV[1] IS NOT NULL THEN
    excluded_cols = TG_ARGV[1]::text[];
  ELSE
    excluded_cols = ARRAY['created', 'updated']::text[];
  END IF;

  IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.original_data = to_json(h_old_data);
    h_changes =  (hstore(NEW.*) - h_old_data) - excluded_cols;
    audit_row.user_id = NEW.updated_by;
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = NEW.id;
      audit_row.manuscript_version = NEW.version;
      IF NEW.status IN ('submitted', 'xml-review', 'published', 'repo-ready') THEN
        perform audit.populate_manuscript_process_dates(NEW.id, NEW.version, NEW.status);
      END IF;
    ELSE
      audit_row.object_id = NEW.id;
      audit_row.manuscript_id = NEW.manuscript_id;
      audit_row.manuscript_version = NEW.manuscript_version;
    END IF;
    IF h_changes = hstore('') THEN
      -- All changed fields are ignored. Skip this update.
      RETURN NULL;
    ELSE
      audit_row.changes = hstore_to_jsonb(h_changes);
    END IF;
  ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
    h_old_data = hstore(OLD.*) - excluded_cols;
    audit_row.user_id = OLD.updated_by;
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = OLD.id;
      audit_row.manuscript_version = OLD.version;
    ELSE
      audit_row.object_id = OLD.id;
      audit_row.manuscript_id = OLD.manuscript_id;
      audit_row.manuscript_version = OLD.manuscript_version;
    END IF;
    audit_row.original_data = hstore_to_jsonb(h_old_data);
  ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
    h_changes =  hstore(NEW.*) - excluded_cols;
    audit_row.changes = hstore_to_jsonb(h_changes);
    IF (TG_TABLE_NAME::text = 'manuscript') THEN
      audit_row.manuscript_id = NEW.id;
      audit_row.manuscript_version = NEW.version;
    ELSE
      audit_row.object_id = NEW.id;
      audit_row.manuscript_id = NEW.manuscript_id;
      audit_row.manuscript_version = NEW.manuscript_version;
    END IF;
    audit_row.user_id = NEW.updated_by;
  ELSE
    RAISE EXCEPTION '[audit.if_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
    RETURN NULL;
  END IF;
  INSERT INTO audit.audit_log VALUES (audit_row.*);
  RETURN NULL;
END;
$body$
  LANGUAGE plpgsql
  SECURITY DEFINER
  SET search_path = pg_catalog, public;


COMMENT ON FUNCTION audit.if_modified_func() IS $body$
Track changes to a table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: boolean, whether to log the query text. Default 't'.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a 'FOR EACH STATEMENT' rather than 'FOR EACH ROW' trigger if you do not
want to log row values.

Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger its self.
$body$;
