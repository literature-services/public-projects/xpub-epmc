#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const config = require('config')
const Identity = require('../server/xpub-model/entities/identity/data-access')
const User = require('../server/xpub-model/entities/user/data-access')
const Journal = require('../server/xpub-model/entities/journal/data-access')
const FtpAccount = require('../server/xpub-model/entities/ftpAccount/data-access')
const journals = require('./journals.json')

async function seed() {
  try {
    // const journals = config.get('journals')
    const natureJournal = await Journal.findByField('meta,pmjrid', '6011')

    if (natureJournal.length > 0) {
      logger.info('Will not run journals seed.')
    } else {
      logger.info('Seeding journals.')
      await Journal.upsertMulti(journals)
    }

    const users = config.get('users')
    await users.reduce(async (promise, user) => {
      await promise
      const idens = await user.identities.reduce(async (arr, identity) => {
        const [exists] = await Identity.findByField('email', identity.email)
        if (!exists) {
          identity.passwordHash = await Identity.hashPassword(identity.password)
          delete identity.password
          arr.push(identity)
        }
        return arr
      }, [])
      if (idens.length > 0) {
        await new User(user).save()
      }
      return true
    }, Promise.resolve())

    const ftp = config.get('ftp_account')
    const ftps = await FtpAccount.selectAll()
    const newRows = ftp.reduce((arr, s) => {
      if (!(ftps && ftps.some(a => a.username === s.username))) arr.push(s)
      return arr
    }, [])
    await FtpAccount.insert(newRows)

    logger.info('Seeding complete.')
  } catch (e) {
    logger.warn('Could not load any seeds', e)
  }
}

;(async () => {
  const beforeUpdate = Date.now()
  await seed()
  await User.knex().destroy()
  logger.info(`Seeding was finished in ${Date.now() - beforeUpdate} ms`)
})()
