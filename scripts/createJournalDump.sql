-- connect to the database container, run psql and run the following

\t on
\pset format unaligned
SELECT json_agg(t) FROM (SELECT "updated", "created", journal_title "journalTitle", "meta,publisher_name" "meta,publisherName", "meta,issn", "meta,nlmta", "meta,nlmuniqueid", "meta,pmjrid", "meta,pubmed_status" "meta,pubmedStatus", "meta,pmc_status" "meta,pmcStatus", "meta,first_year" "meta,firstYear", "meta,end_year" "meta,endYear" FROM journal) t \g journals.json

-- exit psql and the container and run `docker cp xpubepmc_postgres_1:/journals.json server/journals.json`
-- push the result to git if necessary