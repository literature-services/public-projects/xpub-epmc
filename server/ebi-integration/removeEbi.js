const logger = require('@pubsweet/logger')
const rfr = require('rfr')
const config = require('config')
const superagent = require('superagent')
require('superagent-proxy')(superagent)

const { FileManager } = rfr('server/xpub-model')
const { baseUrl } = config.get('pubsweet-server')
const { deleteUrl, testUrl, preprintAuth, plusAuth } = config.get(
  'ebi-repository',
)

module.exports.deleteManifest = async function deleteManifest(
  manuscriptId,
  repoId,
  userId,
  replace,
) {
  try {
    const files = await FileManager.findByManuscriptId(manuscriptId)
    const token = Date.parse(files.find(f => f.type === 'PMC').updated)
    const sendObj = {
      clientId: manuscriptId,
      id: repoId,
      callback: `${testUrl || baseUrl}/api/delete/?token=${token}${
        replace ? '&replace=true' : ''
      }`,
    }
    logger.info(`Withdrawing ${manuscriptId} from EBI`)
    let superagentRequest = superagent('DELETE', deleteUrl)
    if (process.env.superagent_http_proxy) {
      superagentRequest = superagentRequest.proxy(
        process.env.superagent_http_proxy,
      )
    }
    superagentRequest.set('Content-Type', 'application/json')
    superagentRequest.set(
      'Authorization',
      repoId.startsWith('PPR') ? preprintAuth : plusAuth,
    )
    superagentRequest.send(JSON.stringify([sendObj]))
    await superagentRequest
    logger.info(`${manuscriptId} withdrawal request sent to EBI`)
    return true
  } catch (err) {
    logger.error(err)
    return {
      id: manuscriptId,
      formState: `Error withdrawing from EBI: ${err}`,
      status: replace ? 'repo-triage' : 'withdrawal-triage',
      pdfDepositState: null,
    }
  }
}
