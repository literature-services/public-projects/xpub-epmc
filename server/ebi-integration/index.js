const { configureApp } = require('./app')

module.exports = {
  backend: () => configureApp,
}
