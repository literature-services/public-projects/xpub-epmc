const superagent = require('superagent')
const logger = require('@pubsweet/logger')
const config = require('config')

const { plusAuth, preprintAuth, getMintedFtIdUrl } = config.get(
  'ebi-repository',
)

/**
 * @param {*} manuscriptId
 * @param {*} isPreprint
 * @returns object {
 *  resultType: -1 error, 0 full text generated, 1 no full text generated, 2 no previous submission
 *  id: full text id
 * }
 */
const getMintedFtId = async (manuscriptId, isPreprint) => {
  const url = `${getMintedFtIdUrl}/${manuscriptId}`
  logger.info(
    `Request URL: ${url}, method GET for ${
      isPreprint ? 'preprint' : 'author manuscript'
    }`,
  )

  let superagentRequest = superagent('GET', url)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('Authorization', isPreprint ? preprintAuth : plusAuth)
    .send()

  if (process.env.superagent_http_proxy) {
    superagentRequest = superagentRequest.proxy(
      process.env.superagent_http_proxy,
    )
  }

  let result
  try {
    const res = await superagentRequest
    if (res && res.body) {
      logger.info(
        `Response status: ${res.status}, body: ${JSON.stringify(res.body)}`,
      )
    }
    if (res && res.status === 200 && res.body && res.body.id) {
      // full text generated
      result = {
        resultType: 0,
        id: res.body.id,
      }
    } else {
      // no full text generated
      result = { resultType: 1 }
    }
  } catch (e) {
    if (e.status === 404) {
      // no submission in EBI repo
      result = { resultType: 2 }
    } else {
      // other errors
      result = { resultType: -1 }
    }
    // logger.error(e.message)
    logger.error(JSON.stringify(e.response))
  }
  return result
}

module.exports = {
  getMintedFtId,
}
