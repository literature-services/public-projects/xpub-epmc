require('dotenv').config()
const logger = require('@pubsweet/logger')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
require('dotenv').config()
const superagent = require('superagent')
require('superagent-proxy')(superagent)
const https = require('https')
const config = require('config')
const getUser = require('../utils/user.js')
const { ManuscriptManager, FileManager } = require('../xpub-model')
const files = require('../utils/files.js')
const { checkJobStatus, uncaughtError } = require('../job-runner')

if (!process.env.ENABLE_CRONJOB_PDFCONVERTER) {
  logger.info(
    'ENABLE_CRONJOB_PDFCONVERTER not defined. pdf-converter cronjob exits.',
  )
  process.exit(0)
}

const { baseUrl } = config.get('pubsweet-server')
const pdfTransformerApi = config.get('ncbiPdfTransformerApi')

;(async () => {
  await checkJobStatus('pdf-converter', async () => {
    const beforeUpdate = Date.now()
    const adminUser = await getUser.getAdminUser()
    await getDeposits(adminUser.id)
    logger.info(
      `PDF conversion process finished in ${Date.now() - beforeUpdate} ms`,
    )
  })
  process.exit()
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('pdf-converter', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('pdf-converter', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

async function getDeposits(userId) {
  logger.info('Polling our database for manuscripts to be converted to PDF...')

  const resultSet = await ManuscriptManager.findByDepositStates([
    'WAITING_FOR_PDF_CONVERSION',
  ])
  await resultSet.reduce(async (promise, manuscript) => {
    await promise
    return deposit(manuscript, userId)
  }, Promise.resolve())

  logger.info('Polling NCBI for replies from the PDF conversion service...')
  const pdfDepositStates = ['COMMITTED', 'WAITING_FOR_RESULT']

  const manuscripts = await ManuscriptManager.findByDepositStates(
    pdfDepositStates,
  )

  await manuscripts.reduce(async (promise, manuscript) => {
    await promise
    logger.info(`Check status of conversion: ${manuscript.id}`)
    const attempt = manuscript.retryAttempt + 1
    await retryNcbiApiCall(userId, attempt, manuscript, null)
  }, Promise.resolve())
}

function getFile(url, dest, cb) {
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(dest)

    let agent
    if (process.env.superagent_http_proxy) {
      const HttpsProxyAgent = require('https-proxy-agent')
      const proxy = process.env.superagent_http_proxy
      agent = new HttpsProxyAgent(proxy)
    }
    const start = url.indexOf('//') + 2
    const newUrl = url.slice(start)
    const end = newUrl.indexOf('/')
    const host = newUrl.substring(0, end)
    const path = newUrl.substring(end)
    const requestOptions = {
      host,
      path,
      method: 'GET',
      agent,
    }

    https.get(requestOptions, response => {
      if (response.statusCode === 200) {
        const stream = response.pipe(file)
        stream
          .on('finish', () => {
            resolve(true)
          })
          .on('error', err => {
            // Handle errors
            fs.unlink(dest) // Delete the file async. (But we don't check the result)
            reject(err)
          })
      } else {
        logger.error(
          `Error retrieving pdf from ${url} . Status code: ${response.statusCode}`,
        )
        reject(
          new Error(
            `Error retriveing pdf from S3. Http response status code: ${response.statusCode},`,
          ),
        )
      }
    })
  })
}

async function uploadPDF(
  filename,
  manuscript,
  filePath,
  item_size,
  item,
  userId,
) {
  try {
    logger.info(
      `PDF file uploading to GRIDFS manusctipt ${manuscript.id} version ${manuscript.version}`,
    )
    await files.fileUpload(
      manuscript.id,
      manuscript.version,
      filename,
      `${manuscript.id}.pdf`,
      filePath,
      'application/pdf',
    )
    logger.info('PDF file uploaded to GRIDFS')
    const pdf4print = 'pdf4print'

    // use existing ID if it already exists
    const existing = manuscript.files.findIndex(
      file => file.type === pdf4print && !file.deleted,
    )
    const pdfFile = {
      manuscriptId: manuscript.id,
      manuscriptVersion: manuscript.version,
      url: `/download/${filename}`,
      size: item_size,
      filename: item,
      type: pdf4print,
      mimeType: 'application/pdf',
      updatedBy: userId,
    }
    if (existing > -1) {
      pdfFile.id = manuscript.files[existing].id
    }
    await FileManager.save(pdfFile, userId)
    await ManuscriptManager.update({
      id: manuscript.id,
      formState: null,
      pdfDepositState: null,
    })
  } catch (error) {
    await ManuscriptManager.update(
      {
        id: manuscript.id,
        pdfDepositState: null,
        formState: 'PDF result storage failure',
      },
      userId,
    )
    logger.error(error)
  }
}

async function processDeposit(manuscript, deposit, userId) {
  logger.info(`NCBI conversion status for : ${manuscript.id}: ${deposit.state}`)
  const errorStatus =
    (manuscript.formState === 'Generating PDF' && manuscript.status) ||
    (manuscript.status === 'tagging' && 'file-error') ||
    (manuscript.status === 'repo-ready' && 'repo-triage') ||
    manuscript.status
  if (deposit.state === 'RESULT_IS_READY') {
    logger.info(`PDF conversion is ready: ${manuscript.id}. Fetching...`)
    const [pdfItem] = deposit.items.filter(item => item.result)
    const { item, s3_get_url, item_size } = pdfItem

    const uuidv = await uuidv4()
    const filename = `${uuidv}.pdf`
    const filePath = `/tmp/${filename}`
    try {
      await getFile(s3_get_url, filePath)
      await uploadPDF(filename, manuscript, filePath, item_size, item, userId)
      if (
        manuscript.status === 'tagging' &&
        manuscript.formState !== 'Generating PDF'
      ) {
        const newMan = {
          id: manuscript.id,
          status: 'xml-qa',
          formState: null,
        }
        await ManuscriptManager.update(newMan, userId)
      }
      logger.info(
        `PDF retrieved and saved successfully for manuscript ${manuscript.id}`,
      )
    } catch (error) {
      await ManuscriptManager.update(
        {
          id: manuscript.id,
          pdfDepositState: null,
          formState: `PDF result not retrieved: ${JSON.stringify(error)}`,
          status: errorStatus,
        },
        userId,
      )
      logger.error(error)
    }
  } else if (deposit.state === 'NO_RESULT' || deposit.state === 'EXPIRED') {
    await ManuscriptManager.update(
      {
        id: manuscript.id,
        pdfDepositState: null,
        formState: `PDF ${deposit.state}: ${deposit.details}`,
        status: errorStatus,
      },
      userId,
    )
    logger.error(
      `Error getting result from PDF conversion service: ${deposit.details}`,
    )
  } else {
    logger.info(`Nothing found to be processed.`)
  }
}

function retryNcbiApiCall(userId, attempt, manuscript, depositObj) {
  const errorStatus =
    (manuscript.formState === 'Generating PDF' && manuscript.status) ||
    (manuscript.status === 'tagging' && 'file-error') ||
    (manuscript.status === 'repo-ready' && 'repo-triage') ||
    manuscript.status
  return new Promise(async (resolve, reject) => {
    try {
      if (manuscript.pdfDepositState === 'WAITING_FOR_PDF_CONVERSION') {
        const ncbiResponse = await ncbiApiCall('/deposits/', 'POST', depositObj)
        logger.info(JSON.stringify(ncbiResponse))
        const databaseResponse = await ManuscriptManager.update(
          {
            id: manuscript.id,
            pdfDepositState: ncbiResponse.state,
            pdfDepositId: ncbiResponse.deposit_id,
            retryAttempt: 0,
          },
          userId,
        )
        logger.info(
          `Deposit done. Stored depositId: ${ncbiResponse.deposit_id}`,
        )
        resolve(databaseResponse)
      } else {
        // pdfDepositStates is 'COMMITTED' or 'WAITING_FOR_RESULT'
        const ncbiResponse = await ncbiApiCall(
          '/deposits',
          'GET',
          depositObj,
          `/${manuscript.pdfDepositId}`,
        )
        const response = await processDeposit(manuscript, ncbiResponse, userId)
        resolve(response)
      }
    } catch (err) {
      if (attempt <= 3) {
        logger.error(`retry attempt: ${attempt} - ${manuscript.id}`, err)
        await ManuscriptManager.update(
          { id: manuscript.id, retryAttempt: attempt },
          userId,
        )
      } else {
        // update formState column with error message
        logger.error(
          `All retry attempts used, sending error - ${manuscript.id}`,
          err,
        )
        await ManuscriptManager.update(
          {
            id: manuscript.id,
            formState: `Unable to generate PDF from NCBI: ${err.message}`,
            status: errorStatus,
            pdfDepositState: null,
            retryAttempt: 0,
          },
          userId,
        )
      }
      resolve()
    }
  })
}

async function deposit(manuscript, userId) {
  const nxml =
    manuscript.files &&
    manuscript.files.find(file => !file.deleted && file.type === 'PMCfinal')
  const files =
    manuscript.files &&
    manuscript.files.filter(file => !file.deleted && file.type === 'IMGprint')
  const bigfiles = files.filter(file => file.size > 24000000)
  const errorStatus =
    (manuscript.formState === 'Generating PDF' && manuscript.status) ||
    (manuscript.status === 'tagging' && 'file-error') ||
    (manuscript.status === 'repo-ready' && 'repo-triage') ||
    manuscript.status
  if (bigfiles.length > 0) {
    const err = `Unable to generate PDF. The following TIFF files are too large:\n${bigfiles
      .map(f => f.filename)
      .join('\n')}`
    logger.error(err)
    await ManuscriptManager.update(
      {
        id: manuscript.id,
        formState: err,
        pdfDepositState: null,
        status: errorStatus,
      },
      userId,
    )
    return true
  } else if (nxml) {
    logger.info(`Start depositing ${manuscript.id}`)
    files.push(nxml)
    const depositObj = {
      action: 'commit',
      depositor: 'ebi',
      domain: 'ukpmcpa',
    }
    depositObj.items = files.map(file => ({
      item: `${baseUrl}${file.url}`,
      item_size: file.size,
    }))
    const attempt = manuscript.retryAttempt + 1
    await retryNcbiApiCall(userId, attempt, manuscript, depositObj)
    return true
  }
  logger.error(`No files to deposit for manuscript ${manuscript.id}`)
  await ManuscriptManager.update(
    {
      id: manuscript.id,
      formState: 'No NXML file to deposit for PDF conversion',
      pdfDepositState: null,
      status: errorStatus,
    },
    userId,
  )
  return true
}

function ncbiApiCall(uri, method, body, parameters) {
  let localParams = ''
  if (parameters) localParams = parameters
  const url = `${pdfTransformerApi.url}${uri}${localParams}`
  logger.info(`Calling ${url}`)
  return new Promise((resolve, reject) => {
    const superagentRequest = superagent(method, url)
    if (process.env.superagent_http_proxy) {
      superagentRequest.proxy(process.env.superagent_http_proxy)
    }
    superagentRequest
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')

    superagentRequest.send(body ? JSON.stringify(body) : '')

    superagentRequest.end((err, response) => {
      if (err) {
        reject(err)
      } else {
        resolve(response.body)
      }
    })
  })
}
