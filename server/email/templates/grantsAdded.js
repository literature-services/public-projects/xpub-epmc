const grantsAddedTemplate = (pi, grants, manInfo, user) => `
  <p>Dear ${pi.title ? `${pi.title} ` : ''}${
  pi.givenNames ? `${pi.givenNames} ` : ''
}${pi.surname},</p>
  <p>This message is to inform you that your grant${
    grants.length > 1 ? 's' : ''
  }, listed below, ha${grants.length > 1 ? 've' : 's'} been linked to a paper ${
  manInfo.status === 'link-existing' ? 'via' : 'deposited with'
} Europe PMC plus:</p>
  <ul>${grants
    .map(g => `<li>${g.fundingSource} ${g.awardId}, <em>${g.title}</em></li>`)
    .join('')}</ul>
  <p>The linked paper is <em>${manInfo.title}</em> (${manInfo.id}), linked by ${
  user.givenNames
} ${user.surname}.</p>
  <p>If you are satisfied that this link has been made correctly, no further action is required. ${
    manInfo.status === 'link-existing'
      ? '</p>'
      : `The paper will be uploaded to Europe PMC ${
          manInfo.releaseDelay === '0'
            ? 'immediately'
            : `${manInfo.releaseDelay} month${
                manInfo.releaseDelay === '1' ? '' : 's'
              }`
        } after publication.</p>`
  }
  <p>If you have any questions, or think this link has been made in error, please respond to this message for assistance.</p>
  <p>Sincerely,</p>
  <p>The Europe PMC Helpdesk</p>
`

const licenseAddedTemplate = (salutation, funder, title, license, link) => `
  <p>Dear ${salutation},</p>
  <p>Your funder${funder.length > 1 ? 's' : ''}, ${funder.join(' &amp; ')}, ha${
  funder.length > 1 ? 've' : 's'
} confirmed that your submission has a license exception. "${title}" will be made available on Europe PMC under a CC ${license} license.</p>
  <p>Please approve this change and continue your submission:</p>
  <p><a style="color:#20699C" href="${link}">${link}</a></p>
  <p>Best wishes,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = { grantsAddedTemplate, licenseAddedTemplate }
