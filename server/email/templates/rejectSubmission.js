const rejectSubmissionTemplate = (
  salutation,
  title,
  reviewer,
  message,
  link,
) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Submission rejected</h1>
  <p>Dear ${salutation},</p>
  <p>Your manuscript submission, <b>${title}</b> was rejected by the reviewer.<p>
  <p>The reviewer, ${reviewer}, sent the following message:</p>
  <blockquote style="white-space: pre-wrap;overflow-wrap: break-word;">${message}</blockquote>
  <p>Please go to your manuscript at <a style="color:#20699C" href="${link}">${link}</a> to correct any errors in the submission.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  rejectSubmissionTemplate,
}
