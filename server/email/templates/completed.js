const credit = link => `
<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 32px 0 0; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949; border-collapse: collapse;">
  <tbody style="background-color: #fff; border: 1px solid #20699C;" >
    <tr>
      <td align="left" valign="middle" style="width: 46px; padding: 32px 0 16px 32px;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/ORCID_iD.svg/240px-ORCID_iD.svg.png" style="width: 46px;" alt="ORCID iD:"/></td>
      <td align="left" valign="middle" style="padding: 32px 32px 16px 6px;"><h2 style="font-weight:600;font-size:24px; margin: 0;">Have you taken credit for this article?</h2></td>
    </tr>
    <tr>
      <td align="left" valign="top" colspan="2" style="padding: 0 32px 32px 32px;">
        <p>An ORCID iD is a unique identifier that distinguishes you from every other researcher, and makes it easier to find your work. Services based on ORCID benefit both authors and readers. <a href="https://europepmc.org/Help#howORCIDsused" style="color:#20699C">Learn more about how Europe PMC uses ORCID iDs</a>.</p>
        <p>If you are an author of this paper, you can claim it to your ORCID iD now:</p>
        <p><a style="display: inline-block; font-size: 16px; padding: 10px 14px 8px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 2px; text-decoration: none" href="${link}">Claim article with ORCID</a></p>
      </td>
    </tr>
    </tbody>
  </table>
`

const completedTemplate = (title, link, pmcid, claim) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Manuscript available in Europe PMC</h1>
  <p>Your completed manuscript, <b>${title}</b>, is now available in Europe PMC:</p>
  <p><a style="color:#20699C" href="${link}">${link}</a></p>
  <p>Your manuscript will also be made available in PubMed Central (PMC). <B>${pmcid}</B> may be used for any grant reporting purposes.</p>
  ${
    claim
      ? `<p>The manuscript has been successfully linked to your ORCID iD, and will appear on your <a href="https://europepmc.org/authors/${claim}" style="color:#20699C">author profile</a>. Other authors are also encouraged to link the paper to their ORCID iD—please forward this email to any co-authors!</p>`
      : ''
  }
  ${credit(`${link}?claimWithOrcid`)}
  ${
    claim
      ? ''
      : `<p>All authors are encouraged to link the article to their ORCID iD—please forward this email to any co-authors!</p>`
  }
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const completedPreprintTemplate = (title, version, link, claim) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Preprint displayed in Europe PMC</h1>
  <p>The full text of${
    version > 0 ? ` version ${version} of` : ''
  } your preprint, <b>${title}</b>, is now freely available in Europe PMC:</p>
  <p><a style="color:#20699C" href="${link}">${link}</a></p>
  ${
    claim
      ? `<p>The preprint has been successfully linked to your ORCID iD, and will appear on your <a href="https://europepmc.org/authors/${claim}" style="color:#20699C">author profile</a>. Other authors are also encouraged to link the preprint to their ORCID iD—please forward this email to any co-authors!</p>`
      : ''
  }
  ${credit(`${link}?claimWithOrcid`)}
  ${
    claim
      ? ''
      : `<p>All authors are encouraged to link the preprint to their ORCID iD—please forward this email to any co-authors!</p>`
  }
  <p>Please let us know if you have any questions.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const alreadyCompleteTemplate = (
  salutation,
  manId,
  title,
  pmcid,
  inEPMC,
  europepmcUrl,
) => `
<p>Dear ${salutation},</p>
<p>The full text of your manuscript submission, <b>${manId}: ${title}</b> has already been provided to Europe PMC. The full text meeting your funding requirements is available as ${pmcid}.</p>
${
  inEPMC
    ? `<p><a style="color:#20699C" href="${europepmcUrl}/article/PMC/${pmcid}">View this article on Europe PMC</a></p>`
    : ''
}
<p>Please use ${pmcid} for grant reporting purposes. No further action on your part is required. The funding information from your submission will be linked to the existing record.</p>
<p>Kind regards,</p>
<p>The Europe PMC Helpdesk</p>
`

module.exports = {
  completedTemplate,
  completedPreprintTemplate,
  alreadyCompleteTemplate,
}
