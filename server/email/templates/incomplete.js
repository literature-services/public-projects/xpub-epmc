const incompleteTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Your submission is incomplete</h1>
  <p>Dear ${salutation},</p>
  <p>Your Europe PMC plus submission, <b>${title}</b>, has not been completed.</p>
  <p>Please go to <a style="color:#20699C" href="${link}">${link}</a> to complete and submit your manuscript submission.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const stalledManuscriptXMLReviewTemplate = (salutation, title, link, month) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Final review</h1>
  <p>Dear ${salutation},</p>
  <p>The full text of your Europe PMC plus submission, <b>${title}</b>, will be displayed on Europe PMC in a week's time.</p>
  <p>If you’d like to review the web version before the article goes live please <a style="color:#20699C" href="${link}">click the link to review this submission</a>.Reviewing the article will also make it available in Europe PMC earlier, usually within 24 hours.
</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const stalledManuscriptInReviewTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Please review your submission</h1>
  <p>Dear ${salutation},</p>
  <p>Your Europe PMC plus submission, <b>${title}</b>, is still waiting for your review. This is a necessary step for processing the submission.</p>
  <p>Please <a style="color:#20699C" href="${link}">click the link to review this submission</a>.</p>
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, we would greatly appreciate it if you would reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`
const stalledPreprintTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Please review your preprint</h1>
  <p>Dear ${salutation},</p>
  <p>Your preprint, <b>${title}</b>, is searchable alongside peer-reviewed articles in Europe PMC. We would also like to display the full text of your preprint on the Europe PMC website for people to read. The web version of your preprint is still available for your review.</p>
  <p>Please click the button below to review your preprint.</p>
  <p style="text-align:center"><a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">Approve my preprint on Europe PMC</a></p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const stalledErrorTemplate = (salutation, title, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Please correct and continue your submission</h1>
  <p>Dear ${salutation},</p>
  <p>Errors were reported in your Europe PMC plus submission, <b>${title}</b>. Your submission cannot be processed until these errors are corrected.</p>
  <p>Please go to <a style="color:#20699C" href="${link}">${link}</a> to correct the errors and submit your manuscript again.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  incompleteTemplate,
  stalledManuscriptInReviewTemplate,
  stalledManuscriptXMLReviewTemplate,
  stalledPreprintTemplate,
  stalledErrorTemplate,
}
