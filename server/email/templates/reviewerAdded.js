const newReviewerTemplate = (salutation, title, submitter, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review manuscript submission</h1>
  <p>Dear ${salutation},</p>
  <p>A manuscript, <b>${title}</b>, was submitted to Europe PMC plus on your behalf by ${submitter}.</p>
  <p>You have been made the reviewer of this submission, which means you are responsible for ensuring its content is correct and for approving the final web version.</p>
  <p>Please <a style="color:#20699C" href="${link}">log in to or create a Europe PMC plus account</a> to accept this submission for review.</p>
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, we would greatly appreciate it if you would reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const setReviewerTemplate = (salutation, title, submitter, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review manuscript submission</h1>
  <p>Dear ${salutation},</p>
  <p>A manuscript, <b>${title}</b>, was submitted to Europe PMC plus on your behalf by ${submitter}.</p>
  <p>You have been made the reviewer of this manuscript, which means you are responsible for ensuring its content is correct and for approving the final web version.</p>
  <p>Please visit the following link to log in to your account and review the submission:<br/><a style="color:#20699C" href="${link}">${link}</a></p>
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, you can log in and reject it with a message to the submitter, or reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const checkReviewerTemplate = (salutation, title, submitter, link) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review manuscript submission</h1>
  <p>Dear ${salutation},</p>
  <p>${submitter}, the submitter of the manuscript submitted on your behalf, <b>${title}</b>, has addressed errors with the submission.</p>
  <p>Please visit the following link to log in to your account and review the corrected submission:<br/><a style="color:#20699C" href="${link}">${link}</a></p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const newReviewFinalTemplate = (
  salutation,
  title,
  submitter,
  link,
  finalReview,
) => `
  ${
    finalReview
      ? `<h1 style="font-weight:600;font-size:30px;line-height:34px;">Review web version</h1>`
      : ''
  }
  <p>Dear ${salutation},</p>
  <p>A manuscript, <b>${title}</b>, was submitted to Europe PMC plus on your behalf by ${submitter}.</p>
  <p>You have been made the reviewer of this submission, which means you are responsible for approving the final web version.</p>
  <p>${
    finalReview
      ? 'The web version of the submission is awaiting your final approval. P'
      : 'To ensure you are notified when the submission is ready for final approval, p'
  }lease <a style="color:#20699C" href="${link}">log in to or create a Europe PMC plus account</a> to accept this submission for review.</p>
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, we would greatly appreciate it if you would reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const setReviewFinalTemplate = (
  salutation,
  title,
  submitter,
  link,
  finalReview,
) => `
  ${
    finalReview
      ? `<h1 style="font-weight:600;font-size:30px;line-height:34px;">Review web version</h1>`
      : ''
  }
  <p>Dear ${salutation},</p>
  <p>${
    submitter
      ? `A manuscript, <b>${title}</b>, was submitted to Europe PMC plus on your behalf by ${submitter}.`
      : `You previously submitted the manuscript <b>${title}</b> to Europe PMC plus.`
  }</p>
  <p>You have been made the reviewer of this submission, which means you are responsible for approving the final web version.</p>
  ${
    finalReview
      ? `<p>Please proceed to <a style="color:#20699C" href="${link}">${link}</a> to approve or request corrections to the web version of your manuscript.</p>
  <p>Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p>`
      : '<p>You will be informed via email when this submission is ready for final review.</p>'
  }
  <p>If you do not think you are the correct person to review this submission, and you would like to reject the assignment, we would greatly appreciate it if you would reply to this email and let us know.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  newReviewerTemplate,
  setReviewerTemplate,
  checkReviewerTemplate,
  newReviewFinalTemplate,
  setReviewFinalTemplate,
}
