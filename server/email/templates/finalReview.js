const finalReviewTemplate = (salutation, title, link, releaseDelay) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review web version</h1>
  <p>Dear ${salutation},</p>
  <p>The web version of your Europe PMC plus submission, <b>${title}</b>, is now ready for your review.</p>
  <p>Please proceed to <a style="color:#20699C" href="${link}">${link}</a> to approve or request corrections to the web version of your manuscript. If no action is taken your manuscript will be auto-approved after 14 days.</p>
  <p>Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p>
  <p>Once approved, your manuscript will be made available in Europe PMC ${releaseDelay} after the final publication of the article. You will be notified when your article is freely available in Europe PMC.</p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const correctedReviewTemplate = (
  salutation,
  title,
  link,
  preprint,
  releaseDelay,
) => `
  <h1 style="font-weight:600;font-size:30px;line-height:34px;">Review your corrected ${
    preprint ? 'preprint' : 'manuscript'
  }</h1>
  <p>Dear ${salutation},</p>
  <p>The errors you reported have been corrected. Please proceed to <a style="color:#20699C" href="${link}">${title}</a> to ${
  preprint
    ? 'check and approve your preprint. '
    : 'approve or request additional corrections to the web version of your manuscript. Please note that only errors or omissions that impact the scientific accuracy of your article are eligible for correction.</p><p>'
}Once approved, your ${
  preprint ? 'preprint' : 'manuscript'
} will be made available${preprint ? ' for reading' : ''} in Europe PMC ${
  preprint
    ? 'within 24 hours'
    : `${
        releaseDelay > 0 ? `${releaseDelay} months` : 'immediately'
      } after the final publication of the article. You will be notified when your article is freely available in Europe PMC`
}.</p>
  ${preprint ? '<p>Please let us know if you have any questions.</p>' : ''}
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

const epmcDescription = `<h2 style="text-align: center"><img src="https://europepmc.org/img/Europe%20PMC%20Extended%20Logo%20CMYK.33decb46.png" alt="About Europe PMC" style="width: 250px;max-width:100%"/></h2>
  <p>Europe PMC is a worldwide, life sciences literature resource funded by <a style="color:#20699C" href="https://europepmc.org/Funders/">36 biomedical funders</a>, hosted at the <a style="color:#20699C" href="https://www.ebi.ac.uk">EMBL-EBI</a>, and <a style="color:#20699C" href="https://www.ncbi.nlm.nih.gov/pmc/about/pmci/">built in partnership with PMC</a> from the U.S. National Library of Medicine. Europe PMC has been including preprint abstracts since 2018 and now has over 450k biomedical preprint abstracts, from 24 preprint servers including bioRxiv, medRxiv and Research Square, freely available to search, alongside 7.9 million peer-reviewed full text articles and 40 million abstracts.</p>`

const aboutEuropePmcPreprintInitiative = `
  <h4 style="margin-bottom:0;">About the Europe PMC preprint initiative</h4>
  <p style="margin-top: 0;font-size: 14px;">
    In July 2020, Europe PMC began indexing the full text of COVID-19 related preprints,
    to be displayed, and fully searchable, alongside peer reviewed articles.
    For researchers, this means a greater number of search results can be returned.
    If a preprint is part of the Open access subset, the full text will also be available
    for programmatic searching, and text and data mining.
    The <a style="color:#20699C" href="https://europepmc.org/Preprints#preprint-indexing">COVID-19 preprint initiative</a> is currently supported by an award from <a style="color:#20699C" href="https://wellcome.org">Wellcome</a>.   
  </p>
`
const twoWeeksFromNow = new Date(+new Date() + 12096e5).toLocaleDateString(
  'en-gb',
  {
    day: 'numeric',
    month: 'short',
    year: 'numeric',
  },
)

const preprintReviewTemplateCovidNoLicence = ({
  id,
  salutation,
  title,
  link,
  version,
  existing,
  openAccess,
  unrestricted,
  fortnight,
  server,
}) => ({
  subject: `Make your preprint part of the COVID-19 collection`,
  body: `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  
  <h2 style="font-size: 20px">Europe PMC would like to display your posted preprint</h2>
  
  <p>
    <a href="https://europepmc.org" style="color:#20699C">Europe PMC</a> is a repository that aggregates scientific literature from journals and preprint servers.
     By displaying your preprint, <b>${title}${
    version ? ` (version ${version})` : ''
  }</b>, in Europe PMC, it becomes part of a larger 
    <a href="http://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22" style="color:#20699C">
    COVID-19 preprint collection</a>.  This is useful for current and future research, 
    and makes your work more discoverable.
  </p>
  
  <p>
    Help us unlock science’s potential to improve health and save lives during a global health crisis,
   display your preprint as part of the COVID-19 preprint initiative. 
   This initiative is funded by Wellcome and there is no charge to the author or reader for this service.
  </p>
  
  <p style="text-align:center">
    <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
      Make my preprint part of the COVID-19 collection
    </a>
  </p>
  
  <p>
    <i>
      Login to review your preprint, approve it for display, and make it available for text and data mining. Without this, your preprint will not be displayed.
    </i>
  </p>
  <h3>Have questions?</h3>
  
  <h4 style="margin-bottom:0;">How will this affect getting my preprint published?</h4>
  <p style="margin-top:0;font-size:14px;">
  Europe PMC is not a journal, it is a repository for previously posted or published content. Your preprint will still be eligible to be published in a journal, because Europe PMC will not publish it, but display a publicly available preprint. Consider checking the author guidelines of journals you wish to submit to. 
  </p>
  
  <h4 style="margin-bottom:0;">Why should I approve this when it’s already published?</h4>
  <p style="margin-top:0;font-size:14px;">
    If your preprint is already published, it is still valuable as part of the Europe PMC <a style="color:#20699C" href="https://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22">COVID-19 preprint collection</a>.
  Being part of the collection makes your preprint more discoverable, and as part of the Open access subset in Europe PMC,
   it will be available for programmatic searching and text mining. 
  Your preprint will also be linked to a published version when possible, 
  which allows researchers to see how research evolved over time, and assess the full value of different forms of publishing.  
  </p>
  
  <h3>About Europe PMC and the COVID-19 preprint initiative</h3>
  
  <h4 style="margin-bottom:0;">About Europe PMC</h4>
  <p style="margin-top:0;font-size:14px;">
    Europe PMC is an open access repository that provides free access to 
    more than 42 million biomedical and life sciences research articles PubMed Central and other sources, 
    as well as <a style="color:#20699C" href="https://europepmc.org/Preprints#preprint-servers">preprints from more than 30 different preprint servers</a>.
    The journal and preprint content is aggregated together, 
    and searchable at the same time, including free full text when the license allows.
  </p>
 
  ${aboutEuropePmcPreprintInitiative}
`,
})

const preprintReviewTemplateCovidNonDerivative = ({
  id,
  salutation,
  title,
  link,
  version,
  unrestricted,
  fortnight,
  server,
}) => ({
  subject: `Make your preprint available programmatically in Europe PMC`,
  body: `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  
  <h2 style="font-size: 20px">Allow your preprint to reach an even wider audience</h2>
  
  <p>
    As the corresponding author of <b>${title}${
    version ? ` (version ${version})` : ''
  }</b>, we wanted to let you know that <a style="color:#20699C" "https://europepmc.org/">Europe PMC</a>
    will display your preprint as part of the 
  <a style="color:#20699C" href="https://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22">COVID-19 preprint collection</a>. 
    Because the preprint is publicly available, we will be able to provide access to the free full text,
    alongside 42 million peer-reviewed articles in Europe PMC. 
  </p>
  
  <p>
  Help us unlock science’s potential to improve health and save lives during a global health crisis,
  by adding your preprint to the Open access subset in Europe PMC, 
  so that it becomes available for programmatic searching, and text and data mining. 
  This initiative is funded by Wellcome and there is no charge to the author or reader for this service.
  </p>
  
  
  <p style="text-align:center">
    <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
      Make my preprint available programmatically
    </a>
  </p>
  
  <p>
    <i>
      Login to review your preprint, and make it available for programmatic searching, and text and data mining. 
      Without this, your preprint will automatically display by ${twoWeeksFromNow},  
      but will not be added to the Open access subset.
    </i>
  </p>
  <h3>Have questions?</h3>
  
  <h4 style="margin-bottom:0;">How will this affect getting my preprint published?</h4>
  <p style="margin-top: 0;font-size: 14px;">
  Europe PMC is not a journal, it is a repository for previously posted or published content.
  Your preprint will still be eligible to be published in a journal, 
  because Europe PMC will not publish it, but display a publicly available preprint. 
  Consider checking the author guidelines of journals you wish to submit to. 
  </p>
  
  <h4 style="margin-bottom:0;">Why should I approve this when it’s already published?</h4>
  <p style="margin-top: 0;font-size: 14px;">
    If your preprint is already published, it is still valuable as part of the Europe PMC
    <a href="https://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22" style="color:#20699C">
    COVID-19 preprint collection</a>.
    Being part of the collection makes your preprint more discoverable, 
    and as part of the Open access subset in Europe PMC,
    it will be available for programmatic searching and text mining. 
    Your preprint will also be linked to a published version when possible, 
    which allows researchers to see how research evolved over time, 
    and assess the full value of different forms of publishing.  
  </p>

  <h4 style="margin-bottom:0;">How does Europe PMC have permission to display my preprint?</h4>
  <p style="margin-top: 0;font-size: 14px;">
  Europe PMC indexes preprints from more than 30 different preprint servers. 
  These preprints are publicly available, 
  and Europe PMC has permission to download the PDF and supplementary files, 
  and index the full text. </p>

  <h4 style="margin-bottom:0;">How does adding my preprint to the Open access subset work?</h4>
  <p style="margin-top: 0;font-size: 14px;">
  Adding your preprint to the 
    <a style="color:#20699C" href="https://europepmc.org/search?query=OPEN_ACCESS%3AY">Open access subset</a>
  modifies the “free to read” license,
  so that it becomes a less restrictive “free to read and use” license,
  within Europe PMC. This allows the preprint to become available for programmatic searching, 
  and text and data mining.</p>
  
  <h4 style="margin-bottom:0;">When will my preprint be displayed?</h4>
  <p style="margin-top: 0;font-size: 14px;">
  Your preprint is scheduled to be displayed in Europe PMC ${twoWeeksFromNow}.
  If you agree to make your preprint part of the Open access subset,
  then it will usually be displayed within 24 hours of approval.</p>

  
  <h3>About Europe PMC and the COVID-19 preprint initiative</h3>
  
  <h4 style="margin-bottom:0;">About Europe PMC</h4>
  <p style="margin-top: 0;font-size: 14px;">
    Europe PMC is an open access repository that provides free access
    to more than 42 million biomedical and life sciences research articles PubMed Central and other sources,
    as well as <a style="color:#20699c" href="https://europepmc.org/Preprints#preprint-servers">preprints from more than 30 different preprint servers</a>.
    The journal and preprint content is aggregated together, 
    and searchable at the same time, including free full text when the license allows.
  </p>
  
  ${aboutEuropePmcPreprintInitiative}
`,
})

const preprintReviewTemplateCovidFinalRevised = ({
  id,
  salutation,
  title,
  link,
  version,
  unrestricted,
  fortnight,
  server,
}) => ({
  subject: `Request to make your preprint available for meta research`,
  body: `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  
  <p style="font-weight:bold;">
    Europe PMC would like to add your COVID-19 preprint to the full text corpus of COVID-19 literature. This will aid research on future disease outbreaks.
  </p>
  
  <p style="text-align:center">
    <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
      Make my preprint available for meta research
    </a>
  </p>
  
  <p>
    <i>
      Login to review your preprint and approve its addition to the corpus. 
    </i>
  </p>
  
  <p style="margin-top: 0;">
    <a style="color:#20699C" href="https://europepmc.org/">Europe PMC</a> is an EMBL-EBI database of 
    scientific literature. It is a partner of PubMed / PMC and supported by 35 research funders.
  </p>

  <p>
    On your approval, your preprint, <b>${title}${
    version ? ` (version ${version})` : ''
  } </b> will become a part of a larger Europe PMC <a style="color:#20699c" href="http://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22">
    COVID-19 preprint collection</a>, used by many life science researchers. This initiative is funded by 
    Wellcome and there is no charge to the author or reader.
  </p>

  <h3>Have questions? See <a style="color:#20699c" href="https://plus.europepmc.org/user-guide/preprintfaqs">FAQ</a>.</h3>
`,
})

const preprintReviewTemplateCovidOpenLicence = ({
  id,
  salutation,
  title,
  link,
  version,
  existing,
  openAccess,
  unrestricted,
  fortnight,
  server,
}) => ({
  subject: `Review your preprint so it is displayed sooner in Europe PMC`,
  body: `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  
  <h2 style="font-size: 20px">Your preprint will reach an even wider audience</h2>
  
  <p>
    As the corresponding author of <b>${title}${
    version ? ` (version ${version})` : ''
  }</b>, we wanted to let you know that <a style="color:#20699C" href="https://europepmc.org/">Europe PMC</a>
    will display your preprint as part of the 
    <a style="color:#20699C" href="https://europepmc.org/search?query=FUNDER_INITIATIVE%3A%22COVID-19%22">COVID-19 preprint collection</a>
    on ${twoWeeksFromNow}. 
    Because the preprint is publicly available, we will be able to provide access to the free full text,
    alongside 42 million peer-reviewed articles in Europe PMC. 
    This means the preprint will be more discoverable, 
    and available for programmatic searching and deeper analysis through text and data mining.
  </p>
  
  <p>
    Thank you for helping to unlock science’s potential to improve health and save lives during a global health crisis.
    This initiative is funded by Wellcome and there is no charge to the author or reader for this service.
  </p>
  
  
  <p style="text-align:center">
    <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
      Review my preprint so it displays sooner
    </a>
  </p>
  
  <p>
    <i>
  Login to review and approve. Approximately 24 hours later, 
  the full text of your preprint will display as part of the COVID-19 preprint collection. 
  Without a review, your preprint will automatically display by ${twoWeeksFromNow}.
    </i>
  </p>

  <h3>Have questions?</h3>
  <h4 style="margin-bottom:0;">How will this affect getting my preprint published?</h4>
  <p style="margin-top: 0;font-size: 14px;">
    Europe PMC is not a journal, it is a repository for previously posted or published content.
    Your preprint will still be eligible to be published in a journal, 
    because Europe PMC will not publish it, but display a publicly available preprint. 
    If your preprint is published, the preprint will be linked to the published version when possible,
    allowing researchers to see how research evolved over time, 
    and to assess the full value of different forms of publishing. 
    Consider checking the author guidelines of journals you wish to submit to. 
  </p>
  
  <h4 style="margin-bottom:0;">How does Europe PMC have permission to display my preprint?</h4>
  <p style="margin-top:0;font-size: 14px;">
    Europe PMC indexes preprints from more than 30 different preprint servers.
    These preprints are publicly available, 
    and Europe PMC has permission to download the PDF and supplementary files,
    and index the full text.
  </p>

  <h3>About Europe PMC and the COVID-19 preprint initiative</h3>
  
  <h4 style="margin-bottom:0;">About Europe PMC</h4>
  <p style="margin-top:0;font-size:14px;">
    Europe PMC is an open access repository that provides free access
    to more than 42 million biomedical and life sciences research articles PubMed Central and other sources,
    as well as <a style="color:#20699c" href="https://europepmc.org/Preprints#preprint-servers">preprints from more than 30 different preprint servers</a>.
    The journal and preprint content is aggregated together, 
    and searchable at the same time, including free full text when the license allows.
  </p>
  
  ${aboutEuropePmcPreprintInitiative}
`,
})

const preprintReviewTemplate = ({
  id,
  salutation,
  title,
  link,
  version,
  unrestricted,
  fortnight,
  server,
}) => ({
  subject: `Enabling search and display of your preprint in Europe PMC`,
  body: `
<p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
<p>Dear ${salutation},</p>
<p><a href="https://europepmc.org" style="color:#20699C">Europe PMC</a> is funded by 35 international life sciences funders. Because your article acknowledges a grant from at least one of these funders, the full text of your preprint: <b>${title}${
    version ? ` (version ${version})` : ''
  }</b>, will be searchable alongside peer-reviewed articles in Europe PMC${
    unrestricted ? ', and freely available for programmatic text mining' : ''
  }. If you have questions, please see the <a href="https://plus.europepmc.org/user-guide/preprintfaqs" style="color:#20699C">Preprint FAQs</a>.</p>
<p>
  The full text will also be displayed on the Europe PMC website for people to read on ${fortnight}. If you’d like to review the formatting before the article goes live, please log into or create a Europe PMC plus account by clicking the button below. Reviewing the article will also make it available in Europe PMC earlier, usually within 24 hours.
</p>
<p style="text-align:center">
  <a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">
  Review my preprint display
  </a>
</p>
<p>Kind regards,</p>
<p>The Europe PMC Helpdesk</p>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 32px 0 0; font-family:'Open Sans',Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;color:#494949">
  <tr>
    <td align="left" style="padding: 32px 48px; background-color: #fff; border: 1px solid #E96012;" valign="top">
      ${epmcDescription}
    </td>
  </tr>
</table>
`,
})

const preprintVersionReviewTemplate = (id, salutation, title, server, link) => `
  <p style="margin:-32px 0 -12px; text-align: right; font-size: 12px;">${id}</p>
  <p>Dear ${salutation},</p>
  <p>A new version of your preprint, <b>${title}</b>, has been submitted to ${server}. Please click the button below, and log into your Europe PMC plus account to check and approve the new version.</p>
  <p style="text-align:center"><a style="display: inline-block; font-size: 20px; padding: 14px 18px 12px; background-color:#20699C; color: #fff; font-weight: 300; border-radius: 1px; text-decoration: none" href="${link}">Approve my preprint on Europe PMC</a></p>
  <p>Kind regards,</p>
  <p>The Europe PMC Helpdesk</p>
`

module.exports = {
  finalReviewTemplate,
  correctedReviewTemplate,
  preprintReviewTemplate,
  preprintReviewTemplateCovidOpenLicence,
  preprintReviewTemplateCovidNonDerivative,
  preprintReviewTemplateCovidNoLicence,
  preprintVersionReviewTemplate,
  preprintReviewTemplateCovidFinalRevised,
}
