const logger = require('@pubsweet/logger')
const superagent = require('superagent')
const config = require('config')
const { parseExpression } = require('cron-parser')
const { JobManager } = require('../xpub-model/')

const { DATA_CENTER: dataCenter } = process.env
const { internalServiceUrl } = config.get('pubsweet-server')

const checkJobStatus = async (name, logic, ftp) => {
  // Make trigger timestamp
  const triggered = new Date().toISOString()

  // For local running, skip healthcheck if data center not defined
  if (dataCenter) {
    // Perform healthcheck
    logger.info(`${name}: Running healthcheck`)
    const url = `${internalServiceUrl}/healthcheck?datacenterName=${dataCenter}&ftpExcluded=${
      ftp ? 0 : 1
    }`
    try {
      logger.info(`Healthcheck url: ${url}`)
      await superagent.get(url)
    } catch (error) {
      const { response, message } = error
      const { body, status } = response || {}
      const exitMessage = `Healthcheck failed. Exiting.\nStatus code: ${status}\nMessage: ${message}\nResponse body: ${body}`
      logger.error(exitMessage)
      return JobManager.update({
        name,
        triggered,
        dataCenter,
        exitMessage,
      })
    }
  }

  // Get job status from the database
  logger.info(`Checking ${name} status`)
  let job = {}
  try {
    job = await JobManager.find(name)
  } catch {
    const exitMessage = 'Job not found in database. Exiting.'
    logger.error(exitMessage)
    return JobManager.update({
      name,
      triggered,
      dataCenter,
      exitMessage,
    })
  }
  const { running, lastPass, triggered: prev, frequency, timeout, stale } = job
  // Check if trigger to run is stale
  const old = prev && new Date() - new Date(prev) > stale
  if (running && !old) {
    const exitMessage = `${name} is already running. Exiting.`
    logger.info(exitMessage)
    return JobManager.update({
      name,
      dataCenter,
      exitMessage,
    })
  }

  // Check if next pass exists between lastPass and current time
  const testRange = {
    currentDate: new Date(lastPass),
    endDate: new Date(),
    iterator: true,
  }
  const interval = await parseExpression(frequency, testRange)
  try {
    await interval.next()
    if (running && old) logger.info('Resetting long running job status.')
    logger.info(`${name} scheduled. Running:`)
  } catch (ignore) {
    const exitMessage = `${name} not yet scheduled to run. Exiting.`
    logger.info(exitMessage)
    return JobManager.update({
      name,
      triggered,
      dataCenter,
      exitMessage,
    })
  }

  // Mark job as running
  let status = 'fail'
  let exitMessage = ''
  await JobManager.update({
    name,
    running: true,
    dataCenter,
    triggered,
    exitMessage,
  })

  // Run job logic, with timeout
  const runTimeout = new Promise((resolve, reject) =>
    setTimeout(
      () => reject(new Error(`Timeout before ${name} could complete.`)),
      timeout,
    ),
  )
  const runJob = new Promise(async (resolve, reject) => {
    try {
      await logic()
      resolve()
    } catch (err) {
      reject(err)
    }
  })

  await Promise.race([runTimeout, runJob])
    .then(() => {
      status = 'pass'
    })
    .catch(err => {
      status = 'fail'
      logger.error(err.message)
      exitMessage = err.message
    })

  // Update status when job completes (or fails)
  return JobManager.update({
    name,
    running: false,
    lastStatus: status,
    lastPass: status === 'pass' ? new Date().toISOString() : lastPass,
    exitMessage,
  })
}

const uncaughtError = async (name, exitMessage) =>
  JobManager.update({
    name,
    running: false,
    lastStatus: 'fail',
    exitMessage,
  })

module.exports = { checkJobStatus, uncaughtError }
