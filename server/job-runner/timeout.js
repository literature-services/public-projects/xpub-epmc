const logger = require('@pubsweet/logger')
const { checkJobStatus } = require('../job-runner')

if (!process.env.ENABLE_CRONJOB_TIMEOUTTEST) {
  logger.info(
    'ENABLE_CRONJOB_TIMEOUTTEST not defined. timeout-test cronjob exits.',
  )
  process.exit(0)
}

;(async () => {
  await checkJobStatus('timeout-test', async () => {
    logger.info('Testing timeout function - starting 35 minute timer')
    return new Promise((resolve, reject) => {
      setTimeout(() => logger.info('5 minutes passed.'), 300000)
      setTimeout(() => logger.info('10 minutes passed.'), 600000)
      setTimeout(() => logger.info('15 minutes passed.'), 900000)
      setTimeout(() => logger.info('20 minutes passed.'), 1200000)
      setTimeout(() => logger.info('25 minutes passed.'), 1500000)
      setTimeout(() => {
        logger.info('35 minutes have passed. Timeout test not successful.')
        resolve()
      }, 2100000)
    })
  })
  process.exit(0)
})()
