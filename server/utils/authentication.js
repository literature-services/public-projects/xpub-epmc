const logger = require('@pubsweet/logger')
const jwt = require('jsonwebtoken')
const config = require('config')
const BearerStrategy = require('passport-http-bearer').Strategy
const AnonymousStrategy = require('passport-anonymous').Strategy
const LocalStrategy = require('passport-local').Strategy
const UserManager = require('../xpub-model')

const createToken = user => {
  logger.debug('Creating token for', user.email)
  let expiresIn = 72 * 3600
  if (config.has('pubsweet-server.tokenExpiresIn')) {
    expiresIn = config.get('pubsweet-server.tokenExpiresIn')
  }

  return jwt.sign(
    {
      email: user.email,
      id: user.id,
    },
    config.get('pubsweet-server.secret'),
    { expiresIn },
  )
}

const verifyToken = (token, done) => {
  jwt.verify(token, config.get('pubsweet-server.secret'), (err, decoded) => {
    if (err) return done(err)

    return done(null, decoded.id, {
      id: decoded.id,
      token,
    })
  })
}

const verifyTokenSync = async token =>
  jwt.verify(token, config.get('pubsweet-server.secret'))

const verifyPassword = (email, password, done) => {
  const errorMessage = 'Wrong email or password.'
  logger.debug('User finding:', email)
  UserManager.findByEmail(email)
    .then(user => {
      logger.debug('User found:', user.email)
      return Promise.all([user, user.validPassword(password)])
    })
    .then(([user, isValid]) => {
      if (isValid) {
        done(null, user, { id: user.id })
        return
      }
      logger.debug('Invalid password for user:', email)
      done(null, false, { message: errorMessage })
    })
    .catch(err => {
      logger.debug('User not found', err)
      if (err) {
        done(null, false, { message: errorMessage })
      }
    })
}

module.exports = {
  token: {
    create: createToken,
    verify: verifyToken,
    verifySync: verifyTokenSync,
  },
  strategies: {
    // no credentials
    anonymous: new AnonymousStrategy(),

    // JSON web token in "Bearer" HTTP header
    bearer: new BearerStrategy(verifyToken),

    // email + password
    local: new LocalStrategy(verifyPassword),
  },
}
