const fs = require('fs')
const logger = require('@pubsweet/logger')
const readline = require('readline')
const path = require('path')
const mime = require('mime-types')
const download = require('download')
const fetch = require('node-fetch')
const dateFormat = require('dateformat')
const rimraf = require('rimraf')

const gridFsClient = require('../gridFs/src/gridFsClient')

module.exports.getManifestFilename = function getManifestFilename(tmpPath) {
  return new Promise((resolve, reject) => {
    fs.readdir(tmpPath, (err, items) => {
      if (err) reject(err)
      let manifestExists = false
      for (let i = 0; i < items.length; i += 1) {
        if (!items[i].startsWith('.', 0) && items[i].indexOf('manifest') > -1) {
          manifestExists = true
          resolve(items[i])
        }
      }
      if (!manifestExists) reject(new Error('There is no manifest file.'))
    })
  })
}

module.exports.getManifestFileData = function getManifestFileData(
  tempFolder,
  manifestFileName,
) {
  return new Promise((resolve, reject) => {
    const files = []
    const manuscriptId = manifestFileName.match(/\d+/g)[0]

    const lineReader = readline.createInterface({
      input: fs.createReadStream(`${tempFolder}/${manifestFileName}`, {
        encoding: 'UTF-8',
      }),
    })
    lineReader
      .on('line', line => {
        if (line) {
          const row = line.split(/\t{1,}/).filter(x => x) // return only non empty tokens
          const fileName = row[row.length - 1]

          if (!fileName || !fs.existsSync(`${tempFolder}/${fileName}`)) {
            reject(new Error(`File ${fileName} does not exist`))
          } else {
            files.push({
              fileURI: `${tempFolder}/${fileName}`,
              filename: fileName,
              type: row[0],
              label: row[1],
              manuscriptId: `EMS${manuscriptId}`,
            })
          }
        }
      })
      .on('close', () => {
        resolve([`EMS${manuscriptId}`, files])
      })
  })
}

module.exports.checkFiles = function checkFiles(
  files,
  version,
  tmpPath,
  userId,
) {
  return new Promise((resolve, reject) => {
    const filesArr = []
    for (let i = 0; i < files.length; i += 1) {
      fs.access(files[i].fileURI, fs.F_OK, err => {
        if (err) {
          reject(new Error(`${files[i]} does not exist.`))
        }
      })
      const fileInfo = files[i]
      const {
        fileURI: file,
        filename,
        type: fileType,
        label: fileLabel,
        manuscriptId,
      } = fileInfo
      const stats = fs.statSync(file)
      const fileSizeInBytes = stats.size
      const extension = path.extname(file)
      const mimeType = mime.contentType(extension)
      if (fileType !== 'manifest') {
        filesArr.push({
          manuscriptId,
          manuscriptVersion: version,
          filename,
          mimeType,
          extension,
          type: fileType,
          size: fileSizeInBytes,
          url: file,
          label: fileLabel,
          updatedBy: userId,
        })
      }
    }
    resolve(filesArr)
  })
}

module.exports.renameFile = function renameFile(filepath) {
  return new Promise((resolve, reject) => {
    const datedFolder = dateFormat(new Date(), 'yyyy-mm-dd')
    const dir_path = path.dirname(filepath)
    const filename = path.basename(filepath)

    if (!fs.existsSync(`${dir_path}/${datedFolder}`)) {
      fs.mkdirSync(`${dir_path}/${datedFolder}`)
    }

    const oldPath = filepath
    const newPath = `${dir_path}/${datedFolder}/${filename}_${Date.now()}`

    fs.rename(oldPath, newPath, err => {
      if (err) reject(err)
    })
    logger.info(`File ${filename} has been moved to ${dir_path}/${datedFolder}`)
    resolve(newPath)
  })
}

module.exports.renameFileSuffix = function renameFile(filepath) {
  return new Promise((resolve, reject) => {
    const filename = path.basename(filepath)
    const newPath = `${filepath}.start`

    fs.rename(filepath, newPath, err => {
      if (err) reject(err)
    })
    logger.info(`File ${filename} has been renamed to ${filename}.start `)
    resolve(newPath)
  })
}

module.exports.moveErroneousFile = function moveErroneousFile(
  filepath,
  errorFolder,
) {
  return new Promise((resolve, reject) => {
    const filename = path.basename(filepath)
    const oldPath = filepath
    const newPath = `${errorFolder}/${filename}_${Date.now()}`

    fs.rename(oldPath, newPath, err => {
      if (err) reject(err)
      logger.error(`${filename}: File has been moved to the Error folder`)
    })
    resolve(newPath)
  })
}

module.exports.getDirAndFilename = function getDirAndFilename(filepath) {
  return new Promise((resolve, reject) => {
    if (filepath && filepath.length > 0) {
      const datedRegex = new RegExp(/\d{4}-\d{2}-\d{2}$/i)
      const pathSplit = filepath.split('/')
      if (pathSplit.length > 2) {
        const datedFolder = datedRegex.test(pathSplit.slice(-2)[0])
          ? pathSplit.slice(-2)[0]
          : null
        const processedFilename = pathSplit.slice(-1)[0]
        resolve([datedFolder, processedFilename])
      }
    }
    reject(new Error(`${filepath}: Incorrect filepath`))
  })
}

module.exports.downloadFile = function downloadFile(url, tmpPath) {
  return new Promise((resolve, reject) => {
    download(url, tmpPath)
      .then(() => {
        resolve(`${tmpPath}/${fs.readdirSync(tmpPath)[0]}`)
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports.getFilename = function getFilename(url) {
  return new Promise((resolve, reject) => {
    if (!url) reject(new Error('Something is wrong with the url'))
    const filename = url.substring(url.lastIndexOf('/') + 1)
    resolve(filename)
  })
}

module.exports.readData = function readData(url) {
  return new Promise((resolve, reject) => {
    fs.readFile(url, 'utf8', (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

module.exports.tidyUp = function tidyUp(tmpPath) {
  return new Promise((resolve, reject) => {
    rimraf(tmpPath, err => {
      if (err) reject(err)
      logger.info(`successfully deleted ${tmpPath}`)
      resolve(true)
    })
  })
}

module.exports.fetchFile = function fetchFile(fileUrl) {
  return new Promise((resolve, reject) => {
    let optionsfetch
    if (process.env.superagent_http_proxy) {
      const HttpsProxyAgent = require('https-proxy-agent')
      const proxy = process.env.superagent_http_proxy
      const agentProxy = new HttpsProxyAgent(proxy)
      optionsfetch = {
        method: 'GET',
        headers: {
          Accept: 'application/xml',
        },
        agent: agentProxy,
      }
    } else {
      optionsfetch = {
        method: 'GET',
        headers: {
          Accept: 'application/xml',
        },
      }
    }

    fetch(fileUrl, optionsfetch)
      .then(response => {
        if (!response.ok) {
          reject(new Error('Cannot retrieve file. Not a 2XX response'))
        }
        resolve(response.text())
      })
      .catch(err => {
        reject(err)
      })
  })
}

module.exports.normalizeFilename = function normalizeFilename({
  filename,
  type,
  label,
  manuscriptId,
}) {
  const filetype =
    (type.includes('pdf') && 'pdf') ||
    (type.includes('manuscript') && 'manuscript') ||
    type
  const ext = filename.split('.').pop()
  const replaced = label ? `-${label.replace(/[^\w]/g, '_')}` : ''
  const normedLabel = type.includes('pdf') ? '' : replaced

  return `${manuscriptId}-${filetype}${normedLabel}.${ext}`
}

// reduce gridFs request - retry upload with delay
async function fileUpload(
  manuscriptId,
  manuscriptVersion,
  uuid,
  filename,
  path,
  mime,
  n = 3,
) {
  try {
    await uploadFileToGridFs(
      manuscriptId,
      manuscriptVersion,
      uuid,
      filename,
      path,
      mime,
    )
    logger.info(`Uploaded file ${filename} into GridFs`)
    // fs.unlinkSync(path)
    return path
  } catch (err) {
    if (n === 1) {
      fs.unlinkSync(path)
      throw err
    }
    logger.error(`Upload error for ${filename}, delay then retry`)
    await new Promise(resolve => setTimeout(resolve, 1000))
    return fileUpload(
      manuscriptId,
      manuscriptVersion,
      uuid,
      filename,
      path,
      mime,
      n - 1,
    )
  }
}

function uploadFileToGridFs(
  manuscriptId,
  manuscriptVersion,
  filename,
  originalFileName,
  filePath,
  mimeType,
) {
  return gridFsClient.storeFileManuscript(
    manuscriptId,
    manuscriptVersion,
    originalFileName,
    mimeType,
    fs.createReadStream(filePath),
    filename,
  )
}

module.exports.fileUpload = fileUpload
