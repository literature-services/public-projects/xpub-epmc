module.exports.getFTPAccount = function getFTPAccount(userName) {
  return new Promise((resolve, reject) => {
    require('../xpub-model')
      .FtpAccountManager.findByFtpUsername(userName)
      .then(ftpAcc => {
        resolve(ftpAcc)
      })
      .catch(error => reject(error))
  })
}
