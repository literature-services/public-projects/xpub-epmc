const { createLogger, format, transports } = require('winston')

const { combine, label, printf, timestamp, errors } = format
const DailyRotateFile = require('winston-daily-rotate-file')

const dailyRotateFileTransportPubsweet = new DailyRotateFile({
  dirname: './logs',
  filename: 'xpub-epmc.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxFiles: '30d',
})

const dailyRotateFileTransportFTP = new DailyRotateFile({
  dirname: './logs/ftp',
  filename: 'xpub-epmc-ftp.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxFiles: '30d',
})

/**
 * Create thre loggers and register them in winston loggers
 */

const logFormat = printf(
  ({ level, message, label, timestamp, stack, durationMs }) =>
    `${timestamp} ${level}: ${
      typeof message === 'string' ? message.replace(/\n$/, '') : message
    }${durationMs ? ` Duration: ${durationMs}ms` : ''}${
      stack ? `\n${stack}` : ''
    }`,
)

const pubsweetLogger = createLogger({
  level: 'info',
  format: combine(
    errors({ stack: true }),
    label({ label: 'PUBSWEET' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportPubsweet],
})

const ftpLogger = createLogger({
  level: 'info',
  format: combine(
    errors({ stack: true }),
    label({ label: 'FTP' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

const bulkuploadLogger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'FTP-BULKUPLOAD-CRON' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

const bulkuploadMonitorLogger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'FTP-BULKUPLOAD-MONITOR' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

const taggerImportLogger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'FTP-TAGGER-IMPORT-CRON' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

const taggerImportMonitorLogger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'FTP-TAGGER-IMPORT-MONITOR' }),
    timestamp(),
    logFormat,
  ),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

const toTaggersLogger = createLogger({
  level: 'info',
  format: combine(label({ label: 'FTP-TO-TAGGERS' }), timestamp(), logFormat),
  transports: [new transports.Console(), dailyRotateFileTransportFTP],
})

module.exports = {
  pubsweetLogger,
  ftpLogger,
  bulkuploadLogger,
  bulkuploadMonitorLogger,
  taggerImportLogger,
  taggerImportMonitorLogger,
  toTaggersLogger,
}
