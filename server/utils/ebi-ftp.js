const fs = require('fs')
const { ftpLogger: logger } = require('../utils/loggers')

const Client = require('ftp')
const config = require('config')
const ftpUser = require('../utils/ftpUser')
const path = require('path')

function close(ftp) {
  ftp.end()
  ftp = null
  logger.info('connection terminated')
}

module.exports.renameTaggerFtpPackage = async function renameTaggerFtpPackage(
  username,
  folder,
  date,
  packageName,
) {
  const ftp = new Client()
  const parentRootPathFTP = `${
    config.get('taggers-upload-ftp')['receive-folder']
  }`
  const rootPathFTP = `${parentRootPathFTP}/Done`
  const ignoreNewFolder = `${parentRootPathFTP}/New`
  const errorFolder = `${parentRootPathFTP}/Error`
  const pathArr = [rootPathFTP, ignoreNewFolder, errorFolder]
  if (date) {
    const rootDatedPathFTP = `${parentRootPathFTP}/Done/${date}`
    pathArr.push(rootDatedPathFTP)
  }
  // create ftp folders if not present
  pathArr.forEach(dir => {
    ftp.mkdir(dir, true, err => {
      if (err) {
        logger.error(err)
      }
    })
  })

  const { host } = config.get('taggers-upload-ftp')
  const { username: user, password } = await ftpUser.getFTPAccount(username)

  return new Promise((resolve, reject) => {
    ftp.on('ready', () => {
      ftp.list(rootPathFTP, false, async (err, list) => {
        if (err || !list) {
          close(ftp)
          reject(err || new Error('FTP file list unavailable'))
        }
        // get array of tar.gz files only
        const [ftpPackageName, ftpPackageSuffix] = await checkTaggerFtpPackage(
          packageName,
        )
        const ftpFile = list.filter(
          file => file.name === ftpPackageName && file.type === '-',
        )[0]
        if (!ftpFile) {
          close(ftp)
          logger.error(
            `${ftpPackageName}: FTP file does not exist in ${rootPathFTP}`,
          )
          reject(
            new Error(
              `${ftpPackageName}: FTP file does not exist in ${rootPathFTP}`,
            ),
          )
        }
        const newFtpPath = date
          ? `${parentRootPathFTP}/${folder}/${date}/${ftpPackageName}${ftpPackageSuffix}`
          : `${parentRootPathFTP}/${folder}/${ftpPackageName}${ftpPackageSuffix}`
        // processed tar.gz file moved to appropriate folder
        ftp.rename(`${rootPathFTP}/${ftpPackageName}`, newFtpPath, err => {
          if (err) {
            logger.error(
              `${rootPathFTP}/${ftpPackageName}: FTP file could not be moved to ${newFtpPath}`,
            )
            close(ftp)
            reject(err)
          }
          logger.info(
            `${ftpPackageName}: FTP file has been moved to ${newFtpPath}`,
          )
          close(ftp)
          resolve(true)
        })
      })
    })
    ftp.on('error', () =>
      reject(new Error('Unable to connect to EBI FTP server')),
    )
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

function checkTaggerFtpPackage(packageName) {
  return new Promise((resolve, reject) => {
    const ftpPackageName = packageName.match(/(\S+)(\.start_\d+)$/i) || ['']
    if (
      ftpPackageName[1] &&
      ftpPackageName[1].length > 0 &&
      ftpPackageName[2] &&
      ftpPackageName[2].length > 0
    )
      resolve([ftpPackageName[1], ftpPackageName[2]])
    else
      reject(new Error(`${packageName}: PackageName failed naming validation`))
  })
}

module.exports.renameBulkFtpPackage = async function renameBulkFtpPackage(
  username,
  datedFolder,
  processedFilename,
  ftpDirPath,
) {
  try {
    const ftp = new Client()

    // create dated folder if not present
    ftp.mkdir(datedFolder, true, err => {
      if (err) {
        logger.error(err)
      }
    })
    const { host } = config.get('bulk-upload-ftp')
    const { username: user, password } = await ftpUser.getFTPAccount(username)

    return new Promise((resolve, reject) => {
      ftp.on('ready', () => {
        ftp.list(ftpDirPath, false, async (err, list) => {
          if (err || !list) {
            close(ftp)
            reject(err || new Error('FTP file list unavailable'))
          }
          const ftpPackageName = await checkBulkFtpPackage(processedFilename)

          // get array of tar.gz files only
          const ftpFile = list.filter(
            file => file.name === ftpPackageName && file.type === '-',
          )[0]
          if (!ftpFile) {
            close(ftp)
            logger.error(
              `${username}/${ftpPackageName}: FTP file does not exist.`,
            )
            reject(
              new Error(
                `${username}/${ftpPackageName}: FTP file does not exist.`,
              ),
            )
          }
          const newFtpPath = `${datedFolder}/${processedFilename}`

          // processed tar.gz file moved to appropriate folder
          ftp.rename(`${ftpDirPath}/${ftpPackageName}`, newFtpPath, err => {
            if (err) {
              logger.error(
                `${ftpPackageName}: FTP file could not be moved to ${username}/${newFtpPath}`,
              )
              close(ftp)
              reject(err)
            }
            logger.info(
              `${ftpPackageName}: FTP file has been moved to ${username}/${newFtpPath}`,
            )
            close(ftp)
            resolve(true)
          })
        })
      })
      ftp.on('error', () =>
        reject(new Error('Unable to connect to EBI FTP server')),
      )
      const keepalive = 10000
      const connTimeout = 60000
      const pasvTimeout = 60000
      ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
    })
  } catch (e) {
    logger.error(
      `${username}/${processedFilename}: Unable to rename bulk upload package on FTP. \n ${e}`,
    )
  }
}

function checkBulkFtpPackage(packageName) {
  return new Promise((resolve, reject) => {
    const file_ext = new RegExp(
      /((\.start\.loaded\.mid-EMS\d+)|(\.start\.ERROR)|(\.ERROR))\.\d{4}-\d{2}-\d{2}_\d{2}:\d{2}:\d{2}$/i,
    )
    if (file_ext.test(packageName)) resolve(packageName.split(file_ext)[0])
    else
      reject(
        new Error(
          `${packageName}: PackageName failed naming validation for Bulk Upload`,
        ),
      )
  })
}

module.exports.uploadTaggersPackage = function uploadTaggersPackage(
  username,
  filePath,
  preprint,
) {
  try {
    return new Promise(async (resolve, reject) => {
      let ftp = new Client()
      ftp.on('ready', () => {
        const profiler = logger.startTimer()
        const datedFolder = filePath.match(/\d{4}-\d{2}-\d{2}/)
        const stats = fs.statSync(filePath)
        const sizeInMB = stats.size / (1024 * 1024)

        const folderPath = preprint
          ? `New/_Preprints/${datedFolder}`
          : `New/${datedFolder}`

        // create dated folder if not present
        ftp.mkdir(folderPath, true, err => {
          if (err) {
            logger.error(err)
            reject(err)
          }
        })
        const filename = path.basename(filePath)
        ftp.put(filePath, `${folderPath}/${filename}`, err => {
          ftp.end()
          ftp = null
          if (err) {
            profiler.done({
              message: `${filename}: Error uploading taggers package ${filePath} to FTP`,
            })
            reject(err)
          }
          profiler.done({
            message: `${filename}: Taggers package ${filePath} uploaded to ${folderPath}/${filename}, size = ${sizeInMB &&
              sizeInMB.toFixed(2)}MB`,
          })
          resolve(true)
        })
      })
      ftp.on('error', () => {
        ftp.end()
        reject(new Error('Unable to connect to EBI FTP server'))
      })
      const { host } = config.get('taggers-upload-ftp')
      const { username: user, password } = await ftpUser.getFTPAccount(username)
      const keepalive = 10000
      const connTimeout = 60000
      const pasvTimeout = 60000
      ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
    })
  } catch (e) {
    const filename = path.basename(filePath)
    logger.error(`${filename}: Unable to upload taggers package on FTP.`)
  }
}
