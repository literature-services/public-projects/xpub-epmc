const Prometheus = require('prom-client')
const {
  checkDBStatus,
  getDBStatus,
  getDBStatusDatacenter,
} = require('./postgres-status')
const gridFsClient = require('../gridFs/src/gridFsClient')
const { checkFtpStatus } = require('./ftp-status')
const logger = require('@pubsweet/logger')

module.exports = app => {
  Prometheus.collectDefaultMetrics()

  app.get('/metrics', async (req, res) => {
    await checkDBStatus()
    res.set('Content-Type', Prometheus.register.contentType)
    res.end(Prometheus.register.metrics())
  })

  app.get('/postgres/healthcheck', async (req, res) => {
    const status = await getDBStatus()
    if (status === 1) {
      res.status(200).send('OK')
    } else {
      res.status(500).send('Error')
    }
  })

  app.get('/healthcheck', async (req, res) => {
    const startTime = new Date()
    let status = 'OK'
    let statusPostgres = 'OK'
    let statusFtp = 'OK'
    let statusDb
    let statusMongoDB

    const pgStartTime = new Date()
    try {
      let { datacenterName } = req.query
      if (datacenterName == null) {
        datacenterName = 'HX'
      }
      statusDb = await getDBStatusDatacenter(datacenterName)
    } catch (err) {
      logger.error('HEALTHCHECK:POSTGRES DOWN', err)
      statusDb = 0
    }
    const pgEndTime = new Date()
    const pgTime = pgEndTime - pgStartTime

    if (statusDb === 1) {
      logger.info(`HEALTHCHECK:Postgres connection time: ${pgTime}ms`)
    } else {
      status = 'KO'
      statusPostgres = 'KO'
    }

    const mongoStartTime = new Date()
    const countMongoDbFiles = await gridFsClient.checkConnection()
    const mongoEndTime = new Date()
    const mongoTime = mongoEndTime - mongoStartTime
    logger.info(`HEALTHCHECK:Mongo connection time: ${mongoTime}ms`)
    if (countMongoDbFiles < 0) {
      logger.error('HEALTHCHECK:MONGODB DOWN')
      status = 'KO'
      statusMongoDB = 'KO'
    } else {
      statusMongoDB = `Number of files ${countMongoDbFiles}`
    }

    let checkFtp = 1
    const { ftpExcluded } = req.query
    if (ftpExcluded == null || ftpExcluded === '0') {
      const ftpStartTime = new Date()
      checkFtp = await checkFtpStatus()
      const ftpEndTime = new Date()
      const ftpTime = ftpEndTime - ftpStartTime
      logger.info(`HEALTHCHECK:FTP connection time: ${ftpTime}ms`)
    }
    if (checkFtp === 0) {
      logger.error('HEALTHCHECK:FTP DOWN')
      status = 'KO'
      statusFtp = 'KO'
    }

    const endTime = new Date()

    const totalTime = endTime - startTime

    const checkingInfo = {
      status,
      statusMongoDB,
      statusPostgres,
      statusFtp,
      totalTime,
    }

    if (status === 'OK') {
      res.status(200).json(checkingInfo)
    } else {
      res.status(500).json(checkingInfo)
    }
  })
}
