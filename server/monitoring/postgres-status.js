const Prometheus = require('prom-client')
const BaseModel = require('@pubsweet/base-model')
const Configstore = require('configstore')
const rfr = require('rfr')
const logger = require('@pubsweet/logger')

const packageJson = rfr('./package.json')

const config = new Configstore(packageJson.name)

const knex = BaseModel.knex()

const dbStatus = new Prometheus.Gauge({
  name: 'postgres_status',
  help: 'Status of the PostgreSQL database: 1 means OK, 0 means Error',
  labelNames: ['status'],
})

const checkDBStatus = async () => {
  if (config.get('postgres_pretends_down')) {
    logger.info('checkDBStatus: Postgres pretends down!!!')
    dbStatus.set(0)
    return
  }
  const { rows } = await knex.raw('SELECT 1 as num')
  const status = rows[0].num === 1 ? 1 : 0
  dbStatus.set(status)
}

const getDBStatus = async () => {
  if (config.get('postgres_pretends_down')) {
    logger.info('getDBStatus: Postgres pretends down!!!')
    return 0
  }
  const { rows } = await knex.raw('SELECT 1 as num')
  return rows[0].num === 1 ? 1 : 0
}

const getDBStatusDatacenter = async dataCenterName => {
  if (config.get('postgres_pretends_down')) {
    logger.info('getDBStatus: Postgres pretends down!!!')
    return 0
  }
  const { rows } = await knex.raw(
    `SELECT status as num from config.datacenter_status where datacenter_name='${dataCenterName}'`,
  )
  return rows[0].num === 1 ? 1 : 0
}

module.exports = {
  checkDBStatus,
  getDBStatus,
  getDBStatusDatacenter,
}
