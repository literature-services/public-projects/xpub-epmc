const kue = require('kue')
const xsweetConverter = require('../xsweet-conversion/index.js')
const logger = require('@pubsweet/logger')

const queue = kue.createQueue()

queue.process('xsweet conversion', async (job, done) => {
  logger.info(`Working on job ${job.id}`)
  await convertMigratedFile(
    job.data.filePath,
    job.data.manuscriptId,
    job.data.userId,
    done,
  )
})

async function convertMigratedFile(filePath, manuscriptId, userId, done) {
  await xsweetConverter.xsweetConvert(filePath, manuscriptId, userId)
  done()
}
