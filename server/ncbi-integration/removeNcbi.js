const logger = require('@pubsweet/logger')
const config = require('config')
const path = require('path')
const moment = require('moment')
const Client = require('ftp')
const SftpClient = require('ssh2-sftp-client') // sftp
const { Readable } = require('stream')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { ManuscriptManager } = require('../xpub-model')
const getUser = require('../utils/user.js')

if (!process.env.ENABLE_CRONJOB_REMOVENCBI) {
  logger.info(
    'ENABLE_CRONJOB_REMOVENCBI not defined. removeNcbi cronjob exits.',
  )
  process.exit(0)
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('remove-ncbi', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('remove-ncbi', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

const withdrawalOptions = config.get('withdrawalOptions')
let user = null
;(async () => {
  await checkJobStatus('remove-ncbi', async () => {
    const updateTime = Date.now()
    logger.info(`Running manuscripts check to delete from NCBI`)
    user = await getUser.getAdminUser()
    await removalCheck()
    logger.info(
      `Manuscript deletion check was finished in ${Date.now() - updateTime} ms`,
    )
  })
  process.exit(0)
})()

async function removalCheck() {
  const manuscripts = await ManuscriptManager.findDeletionReady()
  if (manuscripts && manuscripts.length > 0) {
    await manuscripts.reduce(async (promise, m) => {
      await promise
      try {
        const filename = `${m.id.toLowerCase()}.${moment().format(
          'YYYY_MM_DD-HH_mm_SS',
        )}.wd.xml`
        const xml = await createXML(m)
        await sendXML(xml, filename)
        logger.info(`${m.id} withdrawal sent to NCBI`)
        await ManuscriptManager.update(
          {
            id: m.id,
            ncbiState: 'ncbi-withdrawal-sent',
          },
          user.id,
        )
      } catch (e) {
        logger.error(e.message)
        await ManuscriptManager.update(
          {
            id: m.id,
            status: 'withdrawal-triage',
            formState: `Unable to withdraw from NCBI: ${e.message}`,
          },
          user.id,
        )
      }
      return Promise.resolve()
    }, Promise.resolve())
    logger.info('All manuscript withdrawals sent')
    return true
  }
  logger.info('No manuscripts to withdraw.')
  return true
}

async function createXML(manuscript) {
  const { notes } = manuscript.meta
  const withdrawNote =
    notes && notes.find(n => n.notesType === 'withdrawReason')
  if (!withdrawNote) {
    throw new Error(`No reason given for withdrawing ${manuscript.id}`)
  }
  const noteContent = JSON.parse(withdrawNote.content)
  const { type, duplicate } = noteContent
  const reason = withdrawalOptions.find(o => o.value === type).label
  const fileContent = `<?xml version='1.0' encoding='UTF-8'?>
<withdrawal
  mid="${manuscript.id.toLowerCase()}"
  msgtype="${type}"
  reason="${reason}"${
    duplicate
      ? `
  newartid="${duplicate.replace('PMC', '')}"`
      : ''
  }
/>
`
  return fileContent
}

const { protocol } = config.get('ncbi-ftp')
const sendXML = protocol === 'sftp' ? sendXMLSftp : sendXMLFtp

async function sendXMLSftp(content, filename) {
  const { host, port, user, password } = config.get('ncbi-ftp')
  const sendFolder = config.get('ncbi-ftp')['send-folder']
  const ncbiConfig = {
    host,
    port,
    username: user,
    password,
    readyTimeout: 60000, // in ms
    keepaliveInterval: 10000, // in ms
  }
  const sftp = new SftpClient('NcbiClient')
  try {
    await sftp.connect(ncbiConfig)
    logger.info('Trying to send file to NCBI FTP')
    const stream = new Readable()
    stream._read = () => {}
    stream.push(content)
    stream.push(null)
    await sftp.put(stream, `${sendFolder}/${path.basename(filename)}`)
    sftp.end()
    return true
  } catch (err) {
    logger.error(`Error sending file ${filename} to NCBI ftp`, err)
    sftp.end()
    return new Error(`Unable to connect to NCBI FTP: ${err}`)
  }
}

async function sendXMLFtp(content, filename) {
  return new Promise((resolve, reject) => {
    let ftp = new Client()
    const stream = new Readable()
    stream._read = () => {}
    stream.push(content)
    stream.push(null)
    ftp.on('ready', () => {
      ftp.put(
        stream,
        `/${config.get('ncbi-ftp')['send-folder']}/${filename}`,
        err => {
          ftp.end()
          ftp = null
          if (err) {
            reject(err)
          }
          resolve(true)
        },
      )
    })
    const { host, user, password } = config.get('ncbi-ftp')
    ftp.connect({ host, user, password })
  })
}
