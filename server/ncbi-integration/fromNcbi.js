const Client = require('ftp') // ftp
const SftpClient = require('ssh2-sftp-client') // sftp
const config = require('config')
const fs = require('fs')
// const through = require('through2')
const { PassThrough } = require('stream')
const os = require('os')
const Dom = require('xmldom').DOMParser
const logger = require('@pubsweet/logger')
const _ = require('lodash')
const getUser = require('../utils/user.js')
const { ManuscriptManager } = require('../xpub-model')
const { errorDevEmail } = require('../email')
const { checkJobStatus, uncaughtError } = require('../job-runner')

if (!process.env.ENABLE_CRONJOB_FROMNCBI) {
  logger.info('ENABLE_CRONJOB_FROMNCBI not defined. fromNcbi cronjob exits.')
  process.exit(0)
}

const NCBI_RESPONSE_EXT = new RegExp(/\S+\.(l|w)d\.response\.xml$/i)

const { protocol } = config.get('ncbi-ftp')
const fromNcbi = protocol === 'sftp' ? fromNcbiSftp : fromNcbiFtp

;(async () => {
  await checkJobStatus('from-ncbi', async () => {
    logger.info('running from NCBI check')
    const beforeUpdate = Date.now()
    try {
      await fromNcbi()
    } catch (err) {
      try {
        await errorDevEmail('receiving from NCBI FTP', err)
        logger.info('Error email from NCBI process sent succesfully ')
      } catch (errEmail) {
        logger.error(
          `Error during Error email from NCBI process sent ${errEmail}`,
        )
      }
      throw new Error(err)
    }
    logger.info(
      `from NCBI check was finished in ${Date.now() - beforeUpdate} ms`,
    )
  })
  process.exit(0)
})()

async function close(client) {
  await client.end()
  client = null
  logger.info('connection terminated')
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    // close(ftp)
    await uncaughtError('from-ncbi', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    // close(ftp)
    await uncaughtError('from-ncbi', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

async function fromNcbiFtp() {
  const ftp = new Client()
  const ftpPromise = new Promise((resolve, reject) => {
    const updatedManuscripts = []
    ftp.on('ready', () => {
      ftp.list(
        `/${config.get('ncbi-ftp')['receive-folder']}`,
        false,
        async (err, list) => {
          if (err) {
            close(ftp)
            reject(err)
          }
          const responseFiles = list.filter(file =>
            NCBI_RESPONSE_EXT.test(file.name),
          )
          if (!responseFiles.length) {
            close(ftp)
            logger.info('No files to load')
            // reject(new Error('No files to load'))
            resolve(updatedManuscripts)
          }
          let counter = 0
          responseFiles.forEach((file, index, array) => {
            const resType =
              (file.name.match(/\.ld\./) && 'loading') ||
              (file.name.match(/\.wd\./) && 'withdrawal')
            const remoteFilePath = `/${
              config.get('ncbi-ftp')['receive-folder']
            }/${file.name}`
            ftp.get(remoteFilePath, (err, stream) => {
              if (err) {
                logger.error(remoteFilePath)
                logger.error(err.message)
                counter += 1
                if (counter === array.length) {
                  resolve(updatedManuscripts)
                }
              } else if (!stream) {
                logger.error(remoteFilePath)
                logger.error(`No FTP get stream`)
                counter += 1
                if (counter === array.length) {
                  resolve(updatedManuscripts)
                }
              } else {
                const path = `${os.tmpdir()}/${file.name}`
                const writeStream = stream.pipe(fs.createWriteStream(path))
                writeStream.on('finish', () => {
                  processFile(path).then(response => {
                    updateManuscriptNcbiStatus(
                      file.name,
                      response,
                      resType,
                    ).then(updatedManuscript => {
                      updatedManuscripts.push(updatedManuscript)
                      if (!updatedManuscript) {
                        counter += 1
                        if (counter === array.length) {
                          resolve(updatedManuscripts)
                        }
                      } else {
                        ftp.rename(
                          remoteFilePath,
                          `${remoteFilePath}.processed`,
                          err => {
                            if (err) {
                              logger.error(err)
                            }
                            counter += 1
                            if (counter === array.length) {
                              resolve(updatedManuscripts)
                            }
                          },
                        )
                      }
                    })
                  })
                })
              }
            })
          })
        },
      )
    })
    ftp.on('error', () => reject(new Error('Unable to connect to NCBI FTP')))
    const { host, user, password } = config.get('ncbi-ftp')
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })

  try {
    const result = await ftpPromise
    logger.info('connection terminated')
    ftp.end()
    return result
  } catch (err) {
    logger.error(err)
    logger.info('connection terminated')
    ftp.end()
  }
}

/**
 *
 * @returns array containing updated manuscript IDs
 */
async function fromNcbiSftp() {
  const { host, port, user, password } = config.get('ncbi-ftp')
  const sftp = new SftpClient('NcbiClient')
  const ncbiConfig = {
    host,
    port,
    username: user,
    password,
    readyTimeout: 60000, // in ms
    keepaliveInterval: 10000, // in ms
  }

  logger.info(
    JSON.stringify({
      ...ncbiConfig,
      ...{
        password: ncbiConfig.password && ncbiConfig.password.replace(/./g, '*'),
      },
    }),
  )

  try {
    await sftp.connect(ncbiConfig)
    const responseFiles = await sftp.list(
      `/${config.get('ncbi-ftp')['receive-folder']}`,
      file => NCBI_RESPONSE_EXT.test(file.name),
    )
    if (!responseFiles.length) {
      logger.info('No files to load')
      await sftp.end()
      return []
    }
    const updatedManuscripts = []
    const batches = _.chunk(responseFiles, 10) // in chunk of 10 which is max knex connections
    /* eslint-disable no-await-in-loop */
    /* eslint-disable no-restricted-syntax */
    for (const batch of batches) {
      const promisesForBatch = batch.map(async file => {
        try {
          const resType =
            (file.name.match(/\.ld\./) && 'loading') ||
            (file.name.match(/\.wd\./) && 'withdrawal')
          const remoteFilePath = `/${
            config.get('ncbi-ftp')['receive-folder']
          }/${file.name}`

          const path = `${os.tmpdir()}/${file.name}` // The temporary file name
          const dst = fs.createWriteStream(path)

          const fileWritten = new Promise(resolve => dst.on('finish', resolve))

          const pt = new PassThrough()
          const ostream = pt.pipe(dst)
          await sftp.get(remoteFilePath, ostream)
          await fileWritten
          const response = await processFile(path)
          const updatedManuscript = await updateManuscriptNcbiStatus(
            file.name,
            response,
            resType,
          )
          updatedManuscripts.push(updatedManuscript)
          if (updatedManuscript) {
            await sftp.rename(remoteFilePath, `${remoteFilePath}.processed`)
          }
        } catch (err) {
          logger.error(`Error processing NCBI file: ${file.name}`, err)
        }
      })
      await Promise.all(promisesForBatch)
    }
    return updatedManuscripts
  } catch (err) {
    logger.error('Error processing reciepts from NCBI sftp', err)
    await sftp.end()
  }
  await sftp.end()
}

/**
 * Parse response from NCBI response file
 * Return object of shape { status, pmcid, formState }
 **/
async function processFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      const doc = new Dom().parseFromString(data)
      if (doc) {
        const receipt = doc.getElementsByTagName('receipt')[0]
        resolve({
          status: parseInt(receipt.getAttribute('status'), 10),
          pmcid: receipt.getAttribute('pmcid'),
          formState: data.match(
            /==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s,
          )
            ? data.match(/==== BEGIN: STDERR ====(.*)==== END: STDERR ====/s)[0]
            : data,
        })
      } else {
        reject(new Error('xml parse error'))
      }
    })
  })
}

/**
 *
 * @param {*} fileName NCBI response file name
 * @param {*} response response object from NCBI result
 * @param {*} resType
 * @returns updated manuscript object
 */
async function updateManuscriptNcbiStatus(fileName, response, resType) {
  // filename example: ems76395.2019_01_01-08_30_06.ld.response.xml
  const manId = fileName.split('.')[0].toUpperCase()
  const adminUser = await getUser.getAdminUser()
  const manuscript = await ManuscriptManager.findById(manId, adminUser.id)
  if (!manuscript) {
    logger.info(`manuscript ${manId} not found in db`)
    return null
  }
  const { articleIds, fundingGroup } = manuscript.meta
  const manInput = { id: manId }
  if (response.status) {
    manInput.ncbiState = 'failure'
    manInput.formState = `NCBI ${resType} failed\n\n${response.formState}`
    manInput.status =
      resType === 'loading' ? 'repo-triage' : 'withdrawal-triage'
  } else {
    const newIds = articleIds.filter(
      aid => !['pre-pmc', 'pmcid'].includes(aid.pubIdType),
    )
    manInput.ncbiState = resType === 'withdrawal' ? 'withdrawn' : 'success'
    if (response.pmcid) {
      const newPMCID = response.pmcid.startsWith('PMC')
        ? response.pmcid
        : `PMC${response.pmcid}`
      newIds.push({ pubIdType: 'pmcid', id: newPMCID })
      manInput.meta = {
        articleIds: newIds,
      }
      if (
        resType !== 'withdrawal' &&
        manuscript.fundingState !== 'Grants linked'
      ) {
        manInput.fundingState = 'Linking grants'
      }
    }
  }
  if (resType === 'withdrawal' && !response.status) {
    if (fundingGroup.length > 0) {
      manInput.status = 'link-existing'
      const linkMan = await ManuscriptManager.update(manInput, adminUser.id)
      logger.info(`manuscript ${manId} successfully withdrawn`)
      return linkMan
    }
    await ManuscriptManager.update(manInput, adminUser.id)
    const deletedMan = await ManuscriptManager.delete(manId, adminUser.id)
    logger.info(`manuscript ${manId} successfully withdrawn`)
    return deletedMan
  }
  const newManuscript = await ManuscriptManager.update(manInput, adminUser.id)
  logger.info(`manuscript ${manId} updated`)
  return newManuscript
}
