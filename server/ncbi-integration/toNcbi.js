const moment = require('moment')
const logger = require('@pubsweet/logger')
const path = require('path')
const { exec } = require('child_process')
const http = require('http')
const fs = require('fs')
const config = require('config')
const Client = require('ftp')
const SftpClient = require('ssh2-sftp-client') // sftp
const md5File = require('md5-file')
const fileUtils = require('../utils/files')
const getUser = require('../utils/user')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { createTempDirNamed } = require('../utils/unTar')
const { ManuscriptManager } = require('../xpub-model')
const { errorDevEmail } = require('../email')

if (!process.env.ENABLE_CRONJOB_TONCBI) {
  logger.info('ENABLE_CRONJOB_TONCBI not defined. toNcbi cronjob exits.')
  process.exit(0)
}

// Pubsweet server is running locally from toNcbi's perspective
const pubsweetServer = config.get('pubsweet-server.internalServiceUrl')

// const ftpLocation = '/home/mselim/test'
const ncbiFileTypes = config.get('ncbiFileTypes')

const NCBI_PACKAGE_EXT = '.ld'

const sftp = new SftpClient('NcbiClient')

;(async () => {
  await checkJobStatus('to-ncbi', async () => {
    logger.info('TO NCBI process started')
    const beforeUpdate = Date.now()
    await toNcbi()
    logger.info(`ncbi check in ${Date.now() - beforeUpdate} ms`)
  })
  process.exit(0)
})()

async function toNcbi() {
  logger.info('Starting NCBI ready check.')
  const manuscripts = await ManuscriptManager.findNcbiReady()

  logger.info(`About to handle all manuscripts ready to send to NCBI`)

  return manuscripts.reduce(async (p, manuscript) => {
    await p
    if (
      manuscript.organization &&
      manuscript.organization.name === 'Europe PMC Plus'
    ) {
      return processManuscript(manuscript)
    }
    return Promise.resolve()
  }, Promise.resolve())
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('to-ncbi', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('to-ncbi', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

const processManuscript = async manuscript => {
  try {
    const citerefUrl = manuscript['meta,citerefUrl']
    const adminUser = await getUser.getAdminUser()
    const tmpPath = await createTempDirNamed(manuscript.id)
    await getFiles(tmpPath, manuscript)
    if (citerefUrl) {
      await createCitation(tmpPath, manuscript).catch(err => {
        throw new Error(err)
      })
    }
    await createManifest(tmpPath, manuscript).catch(err => {
      throw new Error(err)
    })
    const destination = await compress(tmpPath, manuscript.id).catch(err => {
      throw new Error(`Cannot compress files: ${err.message}`)
    })
    const stats = fs.statSync(destination)
    logger.info(
      `Created ${manuscript.id} package. Size in bytes: ${stats.size}`,
    )
    await send(destination, manuscript).catch(err => {
      throw new Error(err)
    })
    await tidyUp(tmpPath, manuscript).catch(err => {
      throw new Error(
        `Error while tidying up after another error: ${err.message}`,
      )
    })
    await ManuscriptManager.update(
      {
        id: manuscript.id,
        ncbiState: 'sent',
      },
      adminUser.id,
    )
    logger.info(`${manuscript.id} sent to NCBI!`)
    return true
  } catch (e) {
    logger.error(`${manuscript.id} NCBI sending error:\n ${e}`)
    try {
      await errorDevEmail('sending to NCBI FTP', e)
      logger.info('Error email to NCBI process sent succesfully ')
    } catch (errEmail) {
      logger.error(`Error during Error email to NCBI process sent ${errEmail}`)
    }
    return true
  }
}

const processFile = (file, tmpPath, manuscript) =>
  new Promise((resolve, reject) => {
    const fileOnDisk = fs.createWriteStream(`${tmpPath}/${file.filename}`)
    http
      .get(`${pubsweetServer}${file.url}`, response => {
        let stream = response.pipe(fileOnDisk)
        stream.on('finish', () => {
          resolve(true)
          stream = null
          logger.info(
            `${manuscript.id} file ${pubsweetServer}${file.url} downloaded`,
          )
        })
      })
      .on('error', err => {
        logger.error(
          `${manuscript.id} file error, ${pubsweetServer}${file.url}: ${err.message}`,
        )
        reject(err)
      })
  })

async function getFiles(tmpPath, manuscript) {
  logger.info(
    `[${manuscript.id}] About to handle NCBI files (tmpPath: ${tmpPath})`,
  )
  const files = getNcbiFiles(manuscript)

  logger.info(
    `[${manuscript.id}] NCBI files to sent: ${files
      .map(f => f.filename)
      .join(', ')})`,
  )
  const processedFiles = await Promise.all(
    files.map(file =>
      processFile(file, tmpPath, manuscript).catch(err => {
        throw new Error(err)
      }),
    ),
  )

  logger.info(`[${manuscript.id}] Done with NCBI files`)
  return processedFiles
}

async function createCitation(tmpPath, manuscript) {
  const aIds = manuscript['meta,articleIds']
  const volume = manuscript['meta,volume']
  const issue = manuscript['meta,issue']
  const location = manuscript['meta,location']
  const publicationDates = manuscript['meta,publicationDates']
  const citerefUrl = manuscript['meta,citerefUrl']
  const fulltextUrl = manuscript['meta,fulltextUrl']
  const { fpage, lpage, elocationId } = location
  const articleIds = aIds.filter(aid => aid.pubIdType !== 'pre-pmc')
  const text = `<citation>${
    volume
      ? `
  <volume>${volume}</volume>`
      : ''
  }${
    issue
      ? `
  <issue>${issue}</issue>`
      : ''
  }${
    fpage
      ? `
  <fpage>${fpage}</fpage>`
      : ''
  }${
    lpage
      ? `
  <lpage>${lpage}</lpage>`
      : ''
  }${
    elocationId
      ? `
  <elocation-id>${elocationId}</elocation-id>`
      : ''
  }${
    articleIds.length > 0
      ? articleIds
          .map(
            aid => `\n  <article-id pub-id-type="${aid.pubIdType}">${aid.id}</article-id>
  `,
          )
          .join('')
      : ''
  }${
    publicationDates.length > 0
      ? publicationDates
          .map(pDate =>
            pDate.jatsDate
              ? `<pub-date pub-type="${
                  elocationId && !fpage && pDate.type === 'ppub'
                    ? 'collection'
                    : pDate.type
                }">\n${Object.keys(pDate.jatsDate)
                  .sort()
                  .map(el =>
                    pDate.jatsDate[el]
                      ? `    <${el}>${pDate.jatsDate[el]}</${el}>\n`
                      : '',
                  )
                  .join('')}  </pub-date>`
              : '',
          )
          .join('\n  ')
      : ''
  }${
    citerefUrl
      ? `
  <citeref-url>${citerefUrl}</citeref-url>`
      : ''
  }${
    fulltextUrl
      ? `
  <fulltext-url>${fulltextUrl}</fulltext-url>`
      : ''
  }
</citation>`
  return new Promise((resolve, reject) => {
    fs.writeFile(`${tmpPath}/${manuscript.id}.cit`, text, err => {
      if (err) {
        reject(err)
      }
      resolve(tmpPath)
    })
  })
}

async function createManifest(tmpPath, manuscript) {
  const articleIds = manuscript['meta,articleIds']
  const pmcId =
    articleIds && articleIds.find(id => id.pubIdType === 'pmcid')
      ? articleIds.find(id => id.pubIdType === 'pmcid').id.substring(3)
      : 0
  const pmId =
    articleIds && articleIds.find(id => id.pubIdType === 'pmid')
      ? articleIds.find(id => id.pubIdType === 'pmid').id
      : 0
  const prePmc =
    articleIds && articleIds.find(id => id.pubIdType === 'pre-pmc')
      ? articleIds.find(id => id.pubIdType === 'pre-pmc').id.substring(3)
      : null
  const submitter = manuscript.teams.find(t => t.roleName === 'submitter')
  const subUser = submitter && submitter.users[0] && submitter.users[0]
  const identity = subUser && subUser.identities[0]
  const isPublisher = identity.meta && identity.meta.publisher
  const epubPublisher = isPublisher && isPublisher === 'NPG'
  const citerefUrl = manuscript['meta,citerefUrl']
  const createdDate = moment(manuscript.created).format('YYYY-MM-DD')
  const pubDates = manuscript['meta,publicationDates']
  const releaseDelay = manuscript['meta,releaseDelay']
    ? parseInt(manuscript['meta,releaseDelay'], 10)
    : 0
  const epubDate =
    pubDates.find(date => date.type === 'epub') &&
    pubDates.find(date => date.type === 'epub').date
  const ppubDate =
    pubDates.find(date => date.type === 'ppub') &&
    pubDates.find(date => date.type === 'ppub').date
  const startDate =
    (epubPublisher && epubDate && epubDate) ||
    (ppubDate && ppubDate) ||
    (epubDate && epubDate)
  const publishDate = moment(startDate)
    .add(releaseDelay, 'M')
    .format('YYYY-MM-DD')

  const journalNlmId = manuscript.journal['meta,nlmuniqueid']
  const files = getNcbiFiles(manuscript)
  if (citerefUrl) {
    files.push({ filename: `${manuscript.id}.cit`, type: 'citation' })
  }
  const hasPubPdf = files.find(file => file.type === 'pdf4load') ? '1' : '0'
  const blobList = await Promise.all(
    files.map(async file => {
      const { numeric } = ncbiFileTypes.find(type => type.value === file.type)
      const md5 = await md5File.sync(`${tmpPath}/${file.filename}`)
      return {
        numeric,
        md5,
        filename: file.filename,
      }
    }),
  )
  let text = `<?xml version="1.0"?>\n<!DOCTYPE pnihms-xdata PUBLIC "-//PNIHMS-EXCHANGE//DTD pNIHMS Exchange DTD//EN" "pnihms_exchange.dtd">\n
  <pnihms-xdata mid="${manuscript.id.toLowerCase()}" pubmed-id="${pmId}" pmc-id="${
    prePmc ? 0 : pmcId
  }" create-date="${createdDate}" publish-date="${publishDate}" pub-pdf="${hasPubPdf}" nlm-journal-id="${journalNlmId}"`
  if (prePmc) {
    text += ` pmc-id-preallocated="${prePmc}"`
  }
  if (isPublisher) {
    text += ` submitter-name="${subUser.givenNames} ${subUser.surname}" submitter-login="${isPublisher}" submitter-authority="publisher"`
  }
  text += `> \n<blobs>\n${blobList
    .map(
      file =>
        `  <blob md5="${file.md5}" name="${file.filename}" type="${file.numeric}" />`,
    )
    .join('\n')}</blobs>\n
  </pnihms-xdata>`

  logger.info(`[${manuscript.id}] NCBI meta xml:`)
  logger.info(`${text}`)

  return new Promise((resolve, reject) => {
    fs.writeFile(
      `${tmpPath}/${manuscript.id.toLowerCase()}.mxml`,
      text,
      err => {
        if (err) {
          reject(err)
        }
        resolve(tmpPath)
      },
    )
  })
}

function compress(tmpPath, id) {
  return new Promise((resolve, reject) => {
    const currentDate = new Date()
    // const date = currentDate.toISOString().split("T")[0]
    const [timestamp] = currentDate
      .toISOString()
      .replace(/-/g, '_')
      .replace('T', '-')
      .replace(/:/g, '_')
      .split('.')
    const dest = `${tmpPath}/${id.toLowerCase()}.${timestamp}${NCBI_PACKAGE_EXT}.zip`
    const cmd = `cd ${tmpPath} && zip -r ${dest} .`
    exec(cmd, err => {
      if (err) {
        // node couldn't execute the command
        reject(err)
      }
      global.destination = dest
      resolve(dest)
    })
  })
}

const { protocol } = config.get('ncbi-ftp')
const send = protocol === 'sftp' ? sendSftp : sendFtp

function sendFtp(dest, manuscript) {
  return new Promise((resolve, reject) => {
    let ftp = new Client()
    ftp.on('ready', () => {
      logger.info('Trying to send file to NCBI FTP')
      ftp.put(
        dest,
        `/${config.get('ncbi-ftp')['send-folder']}/${path.basename(dest)}`,
        err => {
          ftp.end()
          ftp = null
          if (err) {
            reject(err)
          }
          resolve(true)
        },
      )
    })
    ftp.on('error', err => {
      ftp.end()
      reject(new Error(`Unable to connect to NCBI FTP: ${err}`))
    })
    const { host, user, password } = config.get('ncbi-ftp')
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    logger.info(`Trying to connect to NCBI ftp ${host} ${user}:`)
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

async function sendSftp(dest, manuscript) {
  const { host, port, user, password } = config.get('ncbi-ftp')
  const sendFolder = config.get('ncbi-ftp')['send-folder']
  const ncbiConfig = {
    host,
    port,
    username: user,
    password,
    readyTimeout: 60000, // in ms
    keepaliveInterval: 10000, // in ms
  }
  try {
    await sftp.connect(ncbiConfig)
    logger.info('Trying to send file to NCBI FTP')
    await sftp.put(dest, `${sendFolder}/${path.basename(dest)}`)
    await sftp.end()
    return true
  } catch (err) {
    logger.error(`Error sending file ${dest} to NCBI ftp`, err)
    await sftp.end()
    return new Error(`Unable to connect to NCBI FTP: ${err}`)
  }
}

function tidyUp(tmpPath, manuscript) {
  const cmd = `rm -rf ${tmpPath}*`
  return new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        // node couldn't execute the command
        reject(err)
      }
      // logger.info(`Finished process`)
      resolve(manuscript)
    })
  })
}

function getNcbiFiles(manuscript) {
  return manuscript.files.reduce((list, file) => {
    if (!file.deleted && ncbiFileTypes.some(type => type.value === file.type)) {
      if (['supplement', 'pdf4load'].includes(file.type)) {
        file.filename = fileUtils.normalizeFilename(file)
      }
      list.push(file)
    }
    return list
  }, [])
}
