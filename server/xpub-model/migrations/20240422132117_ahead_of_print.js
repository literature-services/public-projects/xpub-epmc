exports.up = (knex, Promise) =>
  knex.schema.table('xpub.manuscript', t => {
    t.boolean('ahead_of_print')
  })

exports.down = (knex, Promise) =>
  knex.schema.table('xpub.manuscript', t => {
    t.dropColumn('ahead_of_print')
  })
