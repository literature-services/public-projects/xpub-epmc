/**
 * Tables in config/audit schema are kept unchanged.
 * Most tables in public are moved to config/xpub.
 * knex_migrations/knex_migrations_lock are kept in public schema.
 */

exports.up = async (knex, Promise) => {
  await knex.raw('CREATE SCHEMA IF NOT EXISTS config')
  await knex.raw('CREATE SCHEMA IF NOT EXISTS xpub')

  // From pub to config. Do we need this?
  await knex.raw('ALTER TABLE datacenter_status SET SCHEMA config')
  await knex.raw('ALTER TABLE ftp_account SET SCHEMA config')
  await knex.raw('ALTER TABLE privacy_notice SET SCHEMA config')

  // From pub to xpub
  await knex.raw('ALTER TABLE annotation SET SCHEMA xpub')
  await knex.raw('ALTER TABLE file SET SCHEMA xpub')
  await knex.raw('ALTER TABLE identity SET SCHEMA xpub')
  await knex.raw('ALTER TABLE journal SET SCHEMA xpub')
  await knex.raw('ALTER TABLE manuscript SET SCHEMA xpub')
  await knex.raw('ALTER TABLE note SET SCHEMA xpub')
  await knex.raw('ALTER TABLE organization SET SCHEMA xpub')
  await knex.raw('ALTER TABLE review SET SCHEMA xpub')
  await knex.raw('ALTER TABLE role SET SCHEMA xpub')
  await knex.raw('ALTER TABLE team SET SCHEMA xpub')
  await knex.raw('ALTER TABLE users SET SCHEMA xpub')

  // For sequences/id(s)
  await knex.raw(
    `ALTER SEQUENCE manuscript_emsid_seq SET SCHEMA xpub;
            ALTER FUNCTION merge SET SCHEMA xpub;
            ALTER FUNCTION trigger_set_manuscript_id SET SCHEMA xpub;
            ALTER FUNCTION trigger_set_timestamp SET SCHEMA xpub`,
  )
}

exports.down = async (knex, Promise) => {
  // From config back to public
  await knex.raw('ALTER TABLE config.datacenter_status SET SCHEMA public')
  await knex.raw('ALTER TABLE config.ftp_account SET SCHEMA public')
  await knex.raw('ALTER TABLE config.privacy_notice SET SCHEMA public')

  // From xpub back to public
  await knex.raw('ALTER TABLE xpub.annotation SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.file SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.identity SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.journal SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.manuscript SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.note SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.organization SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.review SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.role SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.team SET SCHEMA public')
  await knex.raw('ALTER TABLE xpub.users SET SCHEMA public')

  // For sequences/id(s)
  await knex.raw(
    `ALTER SEQUENCE xpub.manuscript_emsid_seq SET SCHEMA public;
        ALTER FUNCTION xpub.merge SET SCHEMA public;
        ALTER FUNCTION xpub.trigger_set_manuscript_id SET SCHEMA public;
        ALTER FUNCTION xpub.trigger_set_timestamp SET SCHEMA public`,
  )

  return knex.raw('DROP SCHEMA IF EXISTS xpub')
}
