exports.up = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.decimal('version')
      .notNullable()
      .defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('manuscript', t => {
    t.dropColumn('version')
  })
