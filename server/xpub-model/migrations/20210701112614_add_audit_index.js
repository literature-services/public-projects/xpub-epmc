exports.up = (knex, Promise) =>
  knex.raw(`CREATE INDEX IF NOT EXISTS audit_manuscript_index on audit.audit_log(manuscript_id);
CREATE INDEX IF NOT EXISTS audit_job_index on audit.job_log(job_name);`)

exports.down = (knex, Promise) =>
  knex.raw(`DROP INDEX IF EXISTS audit_manuscript_index;
DROP INDEX IF EXISTS audit_job_index;`)
