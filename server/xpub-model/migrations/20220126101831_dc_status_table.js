exports.up = (knex, Promise) =>
  knex.raw(`CREATE TABLE IF NOT EXISTS datacenter_status ();
ALTER TABLE datacenter_status ADD COLUMN IF NOT EXISTS status INTEGER;
ALTER TABLE datacenter_status ADD COLUMN IF NOT EXISTS datacenter_name VARCHAR(10) NOT NULL;`)

exports.down = (knex, Promise) => knex.raw(`DROP TABLE datacenter_status;`)
