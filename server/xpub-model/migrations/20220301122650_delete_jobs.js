exports.up = async (knex, Promise) =>
  knex.schema.table('config.job', t => {
    t.timestamp('deleted')
  })

exports.down = async (knex, Promise) =>
  knex.schema.table('config.job', t => {
    t.dropColumn('deleted')
  })
