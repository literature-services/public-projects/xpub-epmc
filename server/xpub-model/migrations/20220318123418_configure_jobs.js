exports.up = async (knex, Promise) => {
  await knex.schema.table('config.job', t => {
    t.integer('timeout')
      .notNullable()
      .defaultTo(1800000)
    t.integer('stale')
      .notNullable()
      .defaultTo(3600000)
  })
}

exports.down = async (knex, Promise) =>
  knex.schema.table('config.job', t => {
    t.dropColumn('timeout')
    t.dropColumn('stale')
  })
