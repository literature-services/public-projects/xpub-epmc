exports.up = async (knex, Promise) => {
  await knex.raw('CREATE SCHEMA IF NOT EXISTS config')
  return knex.schema.withSchema('config').createTable('job', t => {
    t.string('name')
      .notNullable()
      .unique()
    t.boolean('running').notNullable()
    t.string('last_status')
    t.timestamp('last_pass')
    t.string('frequency').notNullable()
    t.string('description')
  })
}

exports.down = async (knex, Promise) => {
  await knex.schema.dropTable('config.job')
  return knex.raw('DROP SCHEMA IF EXISTS config')
}
