exports.up = async (knex, Promise) =>
  knex.schema.withSchema('config').createTable('property', t => {
    t.string('name')
      .notNullable()
      .unique()
    t.string('description')
    t.jsonb('schema')
    t.jsonb('value')
  })

exports.down = async (knex, Promise) => {
  await knex.schema.dropTable('config.property')
}
