exports.up = (knex, Promise) =>
  knex.schema.table('journal', t => {
    t.specificType('other_titles', 'text[]').nullable()
  })

exports.down = (knex, Promise) =>
  knex.schema.table('journal', t => {
    t.dropColumn('other_titles')
  })
