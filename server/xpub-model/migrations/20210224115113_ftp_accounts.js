exports.up = (knex, Promise) =>
  knex.schema.createTable('ftp_account', t => {
    t.string('name').notNullable()
    t.string('description').notNullable()
    t.string('username')
      .notNullable()
      .unique()
    t.text('password').notNullable()
  })

exports.down = (knex, Promise) => knex.schema.dropTable('ftp_account')
