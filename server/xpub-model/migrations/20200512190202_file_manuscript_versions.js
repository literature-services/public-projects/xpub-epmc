exports.up = (knex, Promise) =>
  knex.schema.table('file', t => {
    t.decimal('manuscript_version')
      .notNullable()
      .defaultTo(0)
  })

exports.down = (knex, Promise) =>
  knex.schema.table('file', t => {
    t.dropColumn('manuscript_version')
  })
