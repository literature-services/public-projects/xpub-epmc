exports.up = (knex, Promise) =>
  knex.schema.table('config.job', t => {
    t.timestamp('last_run')
  })

exports.down = (knex, Promise) =>
  knex.schema.table('config.job', t => {
    t.dropColumn('last_run')
  })
