const lodash = require('lodash')
const logger = require('@pubsweet/logger')
const { db: buildQuery } = require('pubsweet-server')

const runQuery = async query => {
  const sql = query.toString()
  try {
    return await query
  } catch (error) {
    logger.warn('Error running database query', {
      query: sql,
      error: { message: error.message, ...error },
    })
    throw error
  }
}

const keyToCamelCase = snakeKey =>
  snakeKey
    .split(',')
    .map(lodash.camelCase)
    .join('.')

const rowToEntity = row => {
  if (lodash.isArray(row)) {
    return row.map(rowToEntity)
  }

  if (lodash.isPlainObject(row)) {
    return lodash.transform(row, (transformed, val, key) => {
      const camelKey = keyToCamelCase(key)
      lodash.set(transformed, camelKey, rowToEntity(val))
    })
  }

  return row
}

const entityToRow = (entity, columns) =>
  columns.reduce((row, columnName) => {
    const camelKey = keyToCamelCase(columnName)
    const value = lodash.get(entity, camelKey)
    return { ...row, [columnName]: value }
  }, {})

/**
 * Check the meta object whether citation information is complete
 * TODO Or check the manuscript object directly?
 * @param { meta }
 * @returns
 */
const isCitationReady = meta => {
  const ids = meta.articleIds || [] // id(s) to be checke
  const hasPmid =
    ids.some(aid => aid.pubIdType === 'pmid') &&
    meta.location &&
    Object.values(meta.location).some(v => !!v) &&
    (meta.volume || meta.issue)
  const hasPprid =
    ids.some(aid => aid.pubIdType === 'pprid') && meta.publicationDates
  const hasCitation =
    meta.volume &&
    Object.values(meta.location).some(v => !!v) &&
    meta.publicationDates &&
    meta.citerefUrl
  return !!(hasPmid || hasPprid || hasCitation)
}

const isCitationPartialReady = meta => {
  const ids = meta.articleIds || [] // id(s) to be checked
  const hasDoi = ids.some(aid => aid.pubIdType === 'doi')
  const hasPmid = ids.some(aid => aid.pubIdType === 'pmid')
  const hasPprid = ids.some(aid => aid.pubIdType === 'pprid')
  const hasPublicationDate =
    meta.publicationDates && meta.publicationDates.length > 0
  const hasUrl = meta.citerefUrl !== null

  // For preprints
  if (hasPprid) {
    return hasPublicationDate
  }

  // For author manuscripts with PMID
  if (hasPmid) {
    return hasDoi && hasPublicationDate
  }

  // For author manuscripts without PMID
  return !!(hasDoi && hasPublicationDate && hasUrl)
}

module.exports = {
  buildQuery,
  runQuery,
  rowToEntity,
  entityToRow,
  isCitationReady,
  isCitationPartialReady,
}
