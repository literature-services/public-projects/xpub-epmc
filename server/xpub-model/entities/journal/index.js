const lodash = require('lodash')
const logger = require('@pubsweet/logger')
const { PropManager } = require('../config')
const Journal = require('./data-access')

const empty = {
  journalTitle: '',
  publisher_name: '',
}

const JournalManager = {
  find: Journal.selectById,
  delete: Journal.delete,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async journal => {
    let { id } = journal
    if (id) {
      const updated = await Journal.update(journal)
      if (!updated) {
        throw new Error('Journal not found')
      }
    } else {
      id = await Journal.insert(journal)
    }

    return { ...journal, id }
  },
  selectWithNLM: async nlmId => Journal.selectByNlmId(nlmId),
  searchByTitle: async query => Journal.searchByTitle(query),
  preprintCheck: async journalId => {
    try {
      const preprintServers = await PropManager.find('preprintServers')
      const journal = await Journal.find(journalId)
      const nlmta = journal['meta,nlmta']
      const error = {
        type: 'error',
        message:
          'Europe PMC plus does not accept preprint submissions. COVID-19 preprints are ingested directly from preprint servers.',
      }
      return preprintServers.value.includes(nlmta) ? error : false
    } catch {
      logger.error(
        'Preprint server list not set; cannot check submission journal status.',
      )
      return false
    }
  },
}

module.exports = JournalManager
