const FtpAccount = require('./data-access')

const FtpAccountManager = {
  findByFtpUsername: FtpAccount.findByFtpUsername,
  findAll: FtpAccount.selectAll,
  findBulkUploaders: FtpAccount.selectBulkUploaders,
  modelName: 'FtpAccount',
  model: FtpAccount,
}

module.exports = FtpAccountManager
