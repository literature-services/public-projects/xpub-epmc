const Note = require('./data-access')

const NoteManager = {
  find: Note.selectById,
  findByManuscriptId: Note.selectByManuscriptId,
  findByManIdAndType: Note.selectByManuscriptIdAndType,
  delete: Note.delete,
  deleteByManuscriptId: Note.deleteByManuscriptId,
  getOrcidNotes: Note.getOrcidNotes,
  getLatestValue: async (mId, type, trx) => {
    const [versionNote] = await Note.selectByManuscriptIdAndType(
      mId,
      'versionList',
      trx,
    )
    const versionList = versionNote && JSON.parse(versionNote.content)
    const versions = versionList ? versionList.emsid : []
    versions.push(mId)
    return Note.getLatestValueByMidList(versions, type, trx)
  },
  create: async (note, userId, trx) => Note.insert(note, userId, trx),
  update: async (note, userId, trx) => {
    if (note.id) {
      const updated = await Note.update(note, userId, trx)
      if (!updated) {
        throw new Error('Note not found')
      }
      return updated
    }
  },
  modelName: 'Note',

  model: Note,
}

module.exports = NoteManager
