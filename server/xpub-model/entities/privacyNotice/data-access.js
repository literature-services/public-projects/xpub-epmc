const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow } = require('../util')

const columnNames = ['version', 'effectiveDate']

class PrivacyNotice extends EpmcBaseModel {
  static get tableName() {
    return 'config.privacyNotice'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        effectiveDate: { type: 'timestamp' },
        version: { type: 'string' },
      },
    }
  }

  static async selectById(id) {
    const [row] = await PrivacyNotice.query().where({ id })
    if (!row) {
      throw new Error('Privacy Notice not found')
    }
    return rowToEntity(row)
  }

  static async selectAll() {
    return PrivacyNotice.query()
  }

  static async selectLastVersion() {
    const row = await PrivacyNotice.query()
      .orderBy('version', 'desc')
      .first()
    if (!row) {
      throw new Error('Privacy Notice not found')
    }
    return rowToEntity(row)
  }

  static async insert(privacyNotice) {
    const row = entityToRow(privacyNotice, columnNames)
    row.id = uuid.v4()
    await PrivacyNotice.query().insert(row)
    return row.id
  }

  static update(privacyNotice) {
    const row = entityToRow(privacyNotice, columnNames)
    return PrivacyNotice.query()
      .update(row)
      .where('id', privacyNotice.id)
  }

  static delete(id) {
    return PrivacyNotice.query()
      .delete()
      .where({ id })
  }

  $beforeInsert() {
    delete this.created
  }
}
module.exports = PrivacyNotice
