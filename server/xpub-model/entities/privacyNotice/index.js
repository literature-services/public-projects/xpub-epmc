const lodash = require('lodash')
const PrivacyNotice = require('./data-access')

const empty = {
  version: '',
  effectiveDate: new Date(),
}

const PrivacyNoticeManager = {
  find: PrivacyNotice.selectById,
  findAll: async () => PrivacyNotice.selectAll(),

  delete: PrivacyNotice.delete,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async privacyNotice => {
    let { id } = privacyNotice
    if (privacyNotice.id) {
      const updated = await PrivacyNotice.update(privacyNotice)
      if (!updated) {
        throw new Error('privacyNotice not found')
      }
    } else {
      id = await PrivacyNotice.insert(privacyNotice)
    }

    return { ...privacyNotice, id }
  },
  findLastVersion: async () => PrivacyNotice.selectLastVersion(),
  model: PrivacyNotice,
}

module.exports = PrivacyNoticeManager
