const lodash = require('lodash')
const FileAccess = require('./data-access')
const fileUtils = require('../../../utils/files.js')
const tar = require('../../../utils/unTar.js')
const config = require('config')
const fs = require('fs')
const pdf = require('pdf-parse')
const { transaction } = require('objection')

const { baseUrl } = config.get('pubsweet-server')

// const dataAccess = new FileAccess()

const empty = {
  filename: '',
  label: '',
  type: 0,
  mime_type: '',
  size: 0,
  url: '',
}

const FileManager = {
  find: FileAccess.selectById,
  findByUrl: FileAccess.selectByUrl,
  findByManuscriptId: FileAccess.selectByManuscriptId,
  findByManIdAndType: FileAccess.selectByManIdAndType,
  delete: FileAccess.delete,
  deleteById: FileAccess.deleteById,
  deleteByManIdAndType: FileAccess.deleteByManIdAndType,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async (file, userId, trx) => {
    if (file.id) {
      const updated = await FileAccess.update(file, userId, trx)
      if (!updated) {
        throw new Error('File not found')
      }
    } else {
      file = await FileAccess.insert(file, userId, trx)
    }
    return { ...file }
  },
  modelName: 'File',
  model: FileAccess,
  getManuscriptText: async (manuscriptId, trx) => {
    const source = await FileAccess.selectByManIdAndType(
      manuscriptId,
      'source',
      trx,
    )
    const manFile = await FileAccess.selectByManIdAndType(
      manuscriptId,
      'manuscript',
      trx,
    )
    const file = source[0] || manFile[0]
    if (file.mimeType === 'application/pdf') {
      const tmpPath = await tar.createTempDir()
      const tmpFile = await fileUtils.downloadFile(baseUrl + file.url, tmpPath)
      const fileContent = await fs.readFileSync(tmpFile)
      const data = await pdf(fileContent)
      await fileUtils.tidyUp(tmpPath)
      return data.text
    }
    return fileUtils.fetchFile(baseUrl + file.url)
  },
  replace: async (fileId, user) => {
    const trx = await transaction.start(FileAccess.knex())
    try {
      const file = await FileAccess.selectById(fileId)
      const { type, label, manuscriptVersion, manuscriptId } = file
      if (type === 'manuscript') {
        // delete any derived source
        await FileAccess.deleteByManIdAndType(manuscriptId, 'source', user, trx)
        // delete any derived pdf4load
        await FileAccess.deleteByManIdAndType(
          manuscriptId,
          'pdf4load',
          user,
          trx,
        )
      }
      await FileAccess.deleteById(fileId, user, trx)
      return { id: manuscriptId, version: manuscriptVersion, type, label, trx }
    } catch (err) {
      if (trx) trx.rollback()
      throw err
    }
  },
}

module.exports = FileManager
