const { Model, raw } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity } = require('../util')

class File extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.file'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        manuscriptId: { type: 'string' },
        manuscriptVersion: { type: 'numeric' },
        filename: { type: 'string' },
        mimeType: { type: 'string' },
        type: { type: 'string' },
        size: { type: 'int' },
        url: { type: 'string' },
        label: { type: ['string', 'null'] },
        deleted: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const Manuscript = require('../manuscript/data-access')
    return {
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'xpub.file.manuscriptId',
          to: 'xpub.manuscript.id',
        },
      },
    }
  }

  static async selectById(id) {
    const rows = await File.query()
      .where({ id })
      .whereNull('deleted')
    if (!rows.length) {
      throw new Error('File not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByUrl(url) {
    const rows = await File.query().where('url', 'like', `%${url}`)
    if (!rows.length) {
      throw new Error('File not found')
    }
    return rowToEntity(rows[0])
  }

  static async selectByManuscriptId(manId, trx) {
    const rows = await File.query(trx)
      .where('manuscript_version', builder => {
        builder
          .select('version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', manId)
      })
      .where('manuscript_id', manId)
      .whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async selectByManIdAndType(manId, type, trx) {
    const rows = await File.query(trx)
      .where('manuscript_version', builder => {
        builder
          .select('version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', manId)
      })
      .where('manuscript_id', manId)
      .andWhere('type', type)
      .whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async selectAllByManuscriptId(manId, trx) {
    const rows = await File.query(trx)
      .where('manuscript_id', manId)
      .whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async selectAll() {
    const rows = await File.query().whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async selectMigratedUnpublished() {
    const files = await File.query()
      .alias('f')
      .select('f.*')
      .join({ m: 'xpub.manuscript' }, 'm.id', 'f.manuscript_id')
      .where('f.filename', 'like', '%.docx')
      .where('f.type', 'manuscript')
      .where(raw('cast(substring(f.manuscript_id from 4) as int) < ?', 90000))
      .where(raw('?? <> ?', ['m.status', 'published']))
      .whereNull('f.deleted')
      .whereNull('m.deleted')
    return files
  }

  static async insert(file, userId, trx) {
    file.updatedBy = userId
    file.id = uuid.v4()
    await File.query(trx).insert(file)
    return file.id
  }

  static update(file, userId, trx) {
    file.updatedBy = userId
    return File.query(trx)
      .update(file)
      .where('id', file.id)
      .whereNull('deleted')
  }

  static delete(id, userId) {
    return File.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }

  static deleteByManIdAndType(manuscript_id, type, userId, trx) {
    return File.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_version', builder => {
        builder
          .select('xpub.manuscript.version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', manuscript_id)
      })
      .where({ manuscript_id, type })
      .whereNull('deleted')
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await File.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_version', builder => {
        builder
          .select('xpub.manuscirpt.version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', id)
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }

  static async deleteById(id, userId, trx) {
    return File.query(trx).updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }
}
module.exports = File
