const lodash = require('lodash')
const Review = require('./data-access')

const empty = {
  open: false,
  comments: {},
  recommendation: '',
}

const transform = review => {
  const { user, ...other } = review
  return {
    user: {
      id: user.id,
      identities: [
        {
          name: {
            givenNames: user.givenNames,
            surname: user.surname,
          },
        },
      ],
    },
    ...other,
  }
}

const ReviewManager = {
  find: Review.selectById,
  getCurrent: async id => {
    const review = await Review.getCurrentByManuscriptId(id)
    return review ? transform(review) : null
  },
  getDeleted: async (id, trx) => {
    const reviews = await Review.getDeletedByManuscriptId(id, trx)
    return reviews ? reviews.map(r => transform(r)) : []
  },
  delete: Review.delete,
  deleteByManuscriptId: Review.deleteByManuscriptId,
  new: (props = {}) => lodash.merge({}, empty, props),
  save: async (review, userId) => {
    let { id } = review
    if (review.id) {
      const updated = await Review.update(review, userId)
      if (!updated) {
        throw new Error('Review not found')
      }
    } else {
      id = await Review.insert(review, userId)
    }

    return { ...review, id }
  },
  modelName: 'Review',

  model: Review,
}

module.exports = ReviewManager
