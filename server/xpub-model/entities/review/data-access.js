const { Model } = require('objection')
const uuid = require('uuid')
const EpmcBaseModel = require('../epmc-base-model')
const { rowToEntity, entityToRow } = require('../util')

const columnNames = [
  'recommendation',
  'comments',
  'open',
  'user_id',
  'manuscript_id',
  'manuscript_version',
]

class Review extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.review'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        updated: { type: 'timestamp' },
        deleted: { type: 'timestamp' },
        userId: { type: 'uuid' },
        manuscriptId: { type: 'string' },
        manuscriptVersion: { type: 'numeric' },
        open: { type: 'boolean' },
        comments: { type: 'string' },
        recommendation: { type: 'string' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    const Manuscript = require('../manuscript/data-access')
    const Annotation = require('../annotation/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'xpub.review.userId',
          to: 'xpub.users.id',
        },
      },
      manuscript: {
        relation: Model.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: ['xpub.review.manuscriptId', 'xpub.review.manuscriptVersion'],
          to: ['xpub.manuscript.id', 'xpub.manuscript.version'],
        },
      },
      annotations: {
        relation: Model.HasManyRelation,
        modelClass: Annotation,
        join: {
          from: 'xpub.review.id',
          to: 'xpub.annotation.reviewId',
        },
      },
    }
  }

  static async selectById(id) {
    const [row] = await Review.query()
      .where('id', id)
      .whereNull('deleted')
    if (!row) {
      throw new Error('Review not found')
    }
    return rowToEntity(row)
  }

  static async getCurrentByManuscriptId(manId) {
    const row = await Review.query()
      .where('manuscript_id', manId)
      .where('manuscript_version', builder => {
        builder
          .select('version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', manId)
      })
      .whereNull('deleted')
      .eager('[user, annotations.file]')
      .modifyEager('annotations', builder => {
        builder.whereNull('deleted')
      })
      .first()
    return rowToEntity(row)
  }

  static async getDeletedByManuscriptId(manId, trx) {
    const rows = await Review.query(trx)
      .where('manuscript_id', manId)
      .where('manuscript_version', builder => {
        builder
          .select('version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', manId)
      })
      .whereNotNull('deleted')
      .eager('[user, annotations.file]')
      .modifyEager('annotations', builder => {
        builder.whereNull('deleted')
      })
    return rows.map(rowToEntity)
  }

  static async selectAll() {
    const rows = await Review.query().whereNull('deleted')
    return rows.map(rowToEntity)
  }

  static async insert(review, userId) {
    const row = entityToRow(review, columnNames)
    row.id = uuid.v4()
    row.updated_by = userId
    await Review.query().insert(row)
    return row.id
  }

  static update(review, userId) {
    const row = entityToRow(review, columnNames)
    row.updated_by = userId
    return Review.query()
      .update(row)
      .where('id', review.id)
      .whereNull('deleted')
  }

  static delete(id, userId) {
    return Review.query().updateAndFetchById(id, {
      deleted: new Date().toISOString(),
      updatedBy: userId,
    })
  }

  static async deleteByManuscriptId(id, userId, trx) {
    await Review.query(trx)
      .update({
        deleted: new Date().toISOString(),
        updatedBy: userId,
      })
      .where('manuscript_version', builder => {
        builder
          .select('version')
          .from('xpub.manuscript')
          .where('xpub.manuscript.id', id)
      })
      .where('manuscript_id', id)
      .whereNull('deleted')
  }
}
module.exports = Review
