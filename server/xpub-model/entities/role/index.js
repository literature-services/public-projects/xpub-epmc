const Role = require('./data-access')

const RoleManager = {
  find: Role.selectByName,
  findAll: Role.selectAll,
  findByType: Role.selectByType,
  delete: Role.delete,
  save: async ({ name }) => {
    const role = new Role()
    role.updateProperties({ name })
    const saved = role.save()
    return { ...saved, name }
  },
  modelName: 'Role',
  model: Role,
}

module.exports = RoleManager
