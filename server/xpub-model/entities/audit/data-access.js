const { Model } = require('objection')
const EpmcBaseModel = require('../epmc-base-model')

const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()

class Audit extends EpmcBaseModel {
  static get tableName() {
    return 'audit.audit_log'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        userId: { type: 'uuid' },
        action: { type: 'string' },
        objectId: { type: 'uuid' },
        originalData: { type: 'object' },
        changes: { type: 'object' },
        objectType: { type: 'string' },
        manuscriptId: { type: 'string' },
        manuscriptVersion: { anyOf: [{ type: 'numeric' }, { type: 'null' }] },
      },
    }
  }

  static get relationMappings() {
    const User = require('../user/data-access')
    const Manuscript = require('../manuscript/data-access')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'audit.audit_log.userId',
          to: 'xpub.users.id',
        },
      },
      manuscript: {
        relation: Model.HasOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'audit.audit_log.manuscriptId',
          to: 'xpub.manuscript.id',
        },
      },
    }
  }

  static async qaCheck(manId, manVer) {
    const rows = await Audit.query()
      .select('created')
      .where('manuscriptId', manId)
      .where('manuscriptVersion', manVer)
      .whereRaw("original_data->>'status' = 'xml-qa'")
      .whereRaw(
        "changes->>'status' in ('xml-review', 'xml-triage', 'repo-ready')",
      )
    if (rows.length > 0) return true
    return false
  }

  static async publisherMetrics(startMonth, endMonth) {
    const stats = await knex.raw(`
with publishers as (
  select i.user_id, i.meta->>'publisher' as name
  from xpub.identity i
  where i.meta->>'publisher' is not null 
)
select p.name, p.user_id, display_mth, created_count as count from (
  select TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'YYYYMM') mth,
  TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'Mon YYYY') display_mth
  from generate_series(${startMonth}, ${endMonth}) as n
) dates
full outer join (
  select a.user_id, TO_CHAR(date_trunc('month', a.created AT TIME ZONE 'Europe/London') , 'YYYYMM') mth, count(*) created_count
  from audit.audit_log a where a.object_type='manuscript' and a.changes->>'id' is not null 
  and a.created AT TIME ZONE 'Europe/London' BETWEEN DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London') + -${endMonth} * INTERVAL '1 month' 
    and DATE_TRUNC('month', current_date AT TIME ZONE 'Europe/London') + -${startMonth -
      1} * INTERVAL '1 month'
  and a.user_id in (select p.user_id from publishers p)
  GROUP BY TO_CHAR(date_trunc('month', a.created AT TIME ZONE 'Europe/London') , 'YYYYMM'), a.user_id
) created on dates.mth = created.mth
right join publishers p on p.user_id = created.user_id
    `)
    const { rows } = stats
    return rows
  }

  static async getMetrics(startMonth, endMonth, orgId, subjCheck) {
    // display_mth, submitted, xml_review, xml_review_within_10_days, xml_review_within_10_days_perc,
    // xml_review_within_3_days, xml_review_within_3_days_perc, published, ncbi_ready_median, external_qa
    // xml_errors, xml_tagging, xml_tagging_within_3_days, xml_tagging_within_3_days_perc
    const metrics = await knex.raw(`
with external as (
  select au.* from audit.audit_log au
  inner join xpub.manuscript m on au.manuscript_id = m.id 
  where m.organization_id='${orgId}'${subjCheck ? ` and ${subjCheck}` : ''}
  and au.created between date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${endMonth} month'::interval)
    and date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${startMonth -
      1} month'::interval)
  and au.user_id not in (
    select user_id from xpub.team t where t.role_name='admin'
  )
  and au.changes->>'status' in ('xml-review', 'xml-triage', 'repo-ready')
  and au.original_data->>'status' = 'xml-qa'
), tag_diffs as (
 select manuscript_id, manuscript_version, tagged_date, sum(case when extract ('ISODOW' from date_range) < 6 then 1 else 0 end) as diff
  from (
    select d.manuscript_id, d.manuscript_version, d.tagged_date, 
    generate_series(d.tagging_date, d.tagged_date - '1 day'::interval, '1 day'::interval) as date_range
    from audit.manuscript_process_dates d 
    inner join xpub.manuscript m on d.manuscript_id = m.id where m.organization_id='${orgId}'${
      subjCheck ? ` and ${subjCheck}` : ''
    }
    and tagged_date between date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${endMonth} month'::interval)
    and date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${startMonth -
      1} month'::interval)
  ) get_diffs
  group by manuscript_id, manuscript_version, tagged_date
), xml_review_diffs as (
  select manuscript_id, manuscript_version, xml_review_date, sum(case when extract ('ISODOW' from date_range) < 6 then 1 else 0 end) as xml_review_diff
  from (
    select d.manuscript_id, d.manuscript_version, d.xml_review_date, 
    generate_series(d.submitted_date, d.xml_review_date - '1 day'::interval, '1 day'::interval) as date_range
    from audit.manuscript_process_dates d 
    inner join xpub.manuscript m on d.manuscript_id = m.id where m.organization_id='${orgId}'${
      subjCheck ? ` and ${subjCheck}` : ''
    }
    and xml_review_date between date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${endMonth} month'::interval)
    and date_trunc('month', current_date AT TIME ZONE 'Europe/London' - '${startMonth -
      1} month'::interval)
  ) get_diffs
  group by manuscript_id, manuscript_version, xml_review_date
)
select display_mth, submitted, xml_review, xml_review_within_10_days, 
  CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_10_days*100/xml_review END xml_review_within_10_days_perc,
  xml_review_within_3_days,
  CASE WHEN xml_review=0 THEN 0 ELSE xml_review_within_3_days*100/xml_review END xml_review_within_3_days_perc,
  published, ncbi_ready_median, external_qa, xml_errors, xml_tagging, xml_tagging_within_3_days,
  CASE WHEN xml_tagging=0 THEN 0 ELSE xml_tagging_within_3_days*100/xml_tagging END xml_tagging_within_3_days_perc
from (
  select TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'YYYYMM') mth,
    TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 month', 'Mon YYYY') display_mth,
    SUM(CASE WHEN TO_CHAR(d.submitted_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) submitted,
    SUM(CASE WHEN TO_CHAR(d.tagged_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) xml_tagging,
    SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) xml_review,
    SUM(CASE WHEN TO_CHAR(d.first_published_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN 1 ELSE 0 END) published,
    percentile_cont(0.5) within group ( order by CASE WHEN TO_CHAR(d.submitted_date, 'Mon YYYY') = TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY') THEN DATE_PART('DAY', ncbi_ready_date-submitted_date) ELSE NULL END) ncbi_ready_median
  from generate_series(${startMonth}, ${endMonth}) n, audit.manuscript_process_dates d 
  inner join xpub.manuscript m on d.manuscript_id = m.id where m.organization_id='${orgId}'${
      subjCheck ? ` and ${subjCheck}` : ''
    }
  group by TO_CHAR(date_trunc('month', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 month', 'Mon YYYY'), n.n
) as top
full outer join (
  select TO_CHAR(date_trunc('month', rev.xml_review_date AT TIME ZONE 'Europe/London') , 'YYYYMM') mth,
  SUM(CASE WHEN rev.xml_review_diff < 11 THEN 1 ELSE 0 END) xml_review_within_10_days,
  SUM(CASE WHEN rev.xml_review_diff < 4 THEN 1 ELSE 0 END) xml_review_within_3_days
  from xml_review_diffs rev
  group by mth
) xml on top.mth = xml.mth
full outer join (
  select TO_CHAR(date_trunc('month', ext.created AT TIME ZONE 'Europe/London') , 'YYYYMM') mth, count(*) external_qa,
  SUM(CASE WHEN ext.changes->>'status' = 'xml-triage' THEN 1 ELSE 0 END) xml_errors
  from external ext
  group by mth
) qa on top.mth = qa.mth
full outer join (
  select TO_CHAR(date_trunc('month', tag.tagged_date AT TIME ZONE 'Europe/London') , 'YYYYMM') mth,
  SUM(CASE WHEN tag.diff < 4 THEN 1 ELSE 0 END) xml_tagging_within_3_days
  from tag_diffs tag
  group by mth
) tagging on top.mth = tagging.mth
order by top.mth desc
`)

    const { rows } = metrics
    return rows
  }

  static async weeklyMetrics(orgId, subjCheck) {
    const metrics = await knex.raw(`
with external as (
  select au.* from audit.audit_log au
  inner join xpub.manuscript m on au.manuscript_id = m.id WHERE m.organization_id='${orgId}'${
      subjCheck ? ` and ${subjCheck}` : ''
    }
  and au.created between date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -4 * '1 week'::interval and current_timestamp AT TIME ZONE 'Europe/London'
  and au.user_id not in (
    select user_id from xpub.team t where t.role_name='admin'
  )
  and au.changes->>'status' in ('xml-review', 'xml-triage', 'repo-ready')
  and au.original_data->>'status' = 'xml-qa'
)
select display_mth, submitted, xml_review, published, external_qa, xml_tagging, xml_errors
from (
  select TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 week', 'IYYYIW') wk,
    TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n * INTERVAL '1 week', 'Mon DD') display_mth,
    SUM(CASE WHEN TO_CHAR(d.submitted_date, 'IYYYIW') = TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 week', 'IYYYIW') THEN 1 ELSE 0 END) submitted,
    SUM(CASE WHEN TO_CHAR(d.tagged_date, 'IYYYIW') = TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 week', 'IYYYIW') THEN 1 ELSE 0 END) xml_tagging,
    SUM(CASE WHEN TO_CHAR(d.xml_review_date, 'IYYYIW') = TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 week', 'IYYYIW') THEN 1 ELSE 0 END) xml_review,
    SUM(CASE WHEN TO_CHAR(d.first_published_date, 'IYYYIW') = TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 week', 'IYYYIW') THEN 1 ELSE 0 END) published
  from generate_series(0, 4) n, audit.manuscript_process_dates d 
  inner join xpub.manuscript m on d.manuscript_id = m.id where m.organization_id='${orgId}'${
      subjCheck ? ` and ${subjCheck}` : ''
    }
  group by TO_CHAR(date_trunc('week', current_date AT TIME ZONE 'Europe/London') + -1*n.n * INTERVAL '1 week', 'IYYYIW'), n.n
) as top
full outer join (
  select TO_CHAR(date_trunc('week', ext.created AT TIME ZONE 'Europe/London') , 'IYYYIW') wk, count(*) external_qa,
  SUM(CASE WHEN ext.changes->>'status' = 'xml-triage' THEN 1 ELSE 0 END) xml_errors
  from external ext
  group by wk
) qa on top.wk = qa.wk
order by top.wk desc
`)

    const { rows } = metrics
    return rows
  }
}

class JobAudit extends EpmcBaseModel {
  static get tableName() {
    return 'audit.job_log'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        created: { type: 'timestamp' },
        jobName: { type: 'string' },
        dataCenter: { type: 'string' },
        action: { type: 'string' },
        originalData: { type: 'object' },
        changes: { type: 'object' },
      },
    }
  }

  static get relationMappings() {
    const Job = require('../config/data-access')
    return {
      job: {
        relation: Model.HasOneRelation,
        modelClass: Job,
        join: {
          from: 'audit.job_log.job_name',
          to: 'config.job.name',
        },
      },
    }
  }

  static async jobLog(name) {
    // get last 50 log entries for a job
    const { results } = await JobAudit.query()
      .where('jobName', name)
      .orderBy('created', 'desc')
      .page(0, 50)
    return results || []
  }
}

module.exports = { Audit, JobAudit }
