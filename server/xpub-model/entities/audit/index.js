const { Audit, JobAudit } = require('./data-access')

const AuditManager = {
  modelName: 'Audit',
  model: Audit,
  qaCheck: Audit.qaCheck,
  getMetrics: Audit.getMetrics,
  weeklyMetrics: Audit.weeklyMetrics,
  publisherMetrics: Audit.publisherMetrics,
  jobLog: JobAudit.jobLog,
}

module.exports = AuditManager
