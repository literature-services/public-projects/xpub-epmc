const { Model } = require('objection')
// const uuid = require('uuid')
const bcrypt = require('bcrypt')
// const config = require('config')
const EpmcBaseModel = require('../epmc-base-model')
const User = require('../user/data-access')
const logger = require('@pubsweet/logger')

const BCRYPT_COST = 12

class Identity extends EpmcBaseModel {
  static get tableName() {
    return 'xpub.identity'
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'xpub.identity.user_id',
          to: 'xpub.users.id',
        },
      },
    }
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'uuid' },
        email: { type: 'string' },
        passwordHash: { type: 'string' },
        passwordResetToken: { type: 'string' },
        name: { type: 'object' },
        userId: { type: 'uuid' },
        identifier: { type: ['string', 'null'] },
        meta: {
          type: ['object', 'null'],
          properties: {
            ftpUserName: { type: ['string', 'null'] },
            publisher: { type: ['string', 'null'] },
          },
        },
        deleted: { type: 'timestamp' },
        updatedBy: { type: 'uuid' },
      },
    }
  }

  static async findByFtpUsername(value) {
    logger.debug('Finding', 'identity.meta', value)

    try {
      const result = await Identity.query()
        .whereJsonSupersetOf('meta', { ftpUserName: value })
        .eager('[user]')

      return result && result.length > 0 ? result[0] : null
    } catch (e) {
      logger.error(e)
    }
  }

  static async selectByUserId(userId) {
    return Identity.query().where('user_id', userId)
  }

  /*
  static get idColumn() {
    return 'email'
  }

  static get dbRefProp() {
    return 'id'
  }
*/
  static async isPasswordSame(password, hash) {
    let valid = false

    if (password.length >= 8) {
      try {
        const epmcPwd = require('epmc-pwd') // eslint-disable-line
        const encrypted = epmcPwd.generatePwd(password)
        // encryption 1
        // valid = encrypted === hash || (await bcrypt.compare(encrypted, hash))
        // encryption 2
        // valid = encrypted === hash || (await bcrypt.compare(password, hash))
        // encryption 3
        valid =
          encrypted === hash ||
          (await bcrypt.compare(password, hash)) ||
          (await bcrypt.compare(encrypted, hash))
      } catch (e) {
        valid = await bcrypt.compare(password, hash)
      }
    }

    return valid
  }

  static async hashPassword(password) {
    // let encrypted

    // encryption 1
    // try {
    //   const epmcPwd = require('epmc-pwd') // eslint-disable-line
    //   encrypted = epmcPwd.generatePwd(password)
    // } catch (e) {
    // encryption 1 and 3
    const encrypted = password
    // }

    const salt = await bcrypt.genSalt(
      process.env.BCRYPT_SALT_ROUNDS
        ? await parseInt(process.env.BCRYPT_SALT_ROUNDS, 10)
        : BCRYPT_COST,
    )
    const hash = await bcrypt.hash(encrypted, salt)

    return hash
  }

  static async updatePasswordResetToken(id, token) {
    const numUpdated = await Identity.query()
      .findById(id)
      .patch({
        passwordResetToken: token,
      })
    return numUpdated
  }
}

module.exports = Identity
