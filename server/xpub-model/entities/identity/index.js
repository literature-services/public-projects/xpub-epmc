const lodash = require('lodash')
const Identity = require('./data-access')

const empty = {}

const IdentityManager = {
  find: Identity.selectById,
  findByUserId: Identity.selectByUserId,
  findByFtpUsername: Identity.findByFtpUsername,
  delete: Identity.delete,
  new: () => lodash.cloneDeep(empty),
  save: async identity => {
    const identityToSave = new Identity(identity)
    return identityToSave.save()
  },
  modelName: 'Identity',
  model: Identity,
}

module.exports = IdentityManager
