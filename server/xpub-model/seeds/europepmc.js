const logger = require('@pubsweet/logger')
const Role = require('../entities/role/data-access')
const Organization = require('../entities/organization/data-access')

exports.seed = async (knex, Promise) => {
  // Set XSL flags - automatically regenerate saved transforms each deployment
  logger.info('Deleting cached XSL')
  await knex('config.xslt').update({ flag: true })
  // Checking preprint org
  const orgs = await Organization.selectAll()
  if (!orgs.find(org => org.name === 'Europe PMC Preprints')) {
    logger.info('Seeding preprint org.')
    await knex('xpub.organization').insert([
      {
        name: 'Europe PMC Preprints',
      },
    ])
  }
  // Checking all seeds
  const role = await Role.selectByName('admin')
  if (role) {
    logger.info('Good. Already seeded.')
    return
  }

  // Seed main organization
  await knex('xpub.organization').del()
  await knex('xpub.organization').insert([
    {
      name: 'Europe PMC Plus',
    },
    {
      name: 'Europe PMC Preprints',
    },
  ])

  // Seed roles
  await knex('xpub.role').insert([
    { name: 'admin', organization: true },
    { name: 'external-admin', organization: true },
    { name: 'submitter', organization: false },
    { name: 'reviewer', organization: false },
    { name: 'tagger', organization: true },
  ])

  // Seed versions
  await knex('config.privacy_notice').insert([
    { version: 2, effective_date: new Date('2018-05-24') },
    { version: 3, effective_date: new Date('2018-08-23') },
  ])
}
