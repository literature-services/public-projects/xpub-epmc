const logger = require('@pubsweet/logger')
const superagent = require('superagent')
const config = require('config')
require('superagent-proxy')(superagent)

const { url: gristUrl, username, password } = config.get('grist-api')

module.exports = app => {
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })

  app.get('/grist/rest/api/search', authBearer, async (req, res) => {
    res.set({ 'Content-Type': 'application/json' })

    const { page } = req.query
    const query = req.query.query.replace(/\\/g, '%255C')
    const url = `${gristUrl}/rest/get/secure/query=${query}%20AND%20epmc_funders:yes&page=${page}&showExtraInformation=true&resultType=core&format=json`

    try {
      logger.info('Querying GRIST')
      let superagentRequest = superagent('GET', url).auth(username, password)
      if (process.env.superagent_http_proxy) {
        superagentRequest = superagentRequest.proxy(
          process.env.superagent_http_proxy,
        )
      }
      const response = await superagentRequest
      res.status(200).send(response.body)
    } catch (error) {
      logger.error(`GRIST error: ${error}`)
      res.status(400).send({ error })
    }
  })
}
