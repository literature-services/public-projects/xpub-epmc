// const logger = require('@pubsweet/logger')
const config = require('config')
const {
  createProxyMiddleware,
  fixRequestBody,
} = require('http-proxy-middleware')

let agent
if (process.env.http_proxy) {
  const HttpProxyAgent = require('http-proxy-agent')
  const proxy = process.env.http_proxy
  agent = new HttpProxyAgent(proxy)
}

// eslint-disable-next-line no-unused-vars
const { url: matomoUrl } = config.matomo

module.exports = app => {
  app.use(
    '/piwik',
    createProxyMiddleware({
      target: matomoUrl,
      changeOrigin: true,
      agent,
      onProxyReq: fixRequestBody,
    }),
  )
}
