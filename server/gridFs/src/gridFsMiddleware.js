require('dotenv').config()
const formidable = require('formidable')
const uuidv1 = require('uuid/v1')
const config = require('config')
const { PassThrough } = require('stream')

const gridFsClient = require('./gridFsClient')
const utils = require('./utils')
const fs = require('fs')

const logger = (config && config.logger) || console

const Ops = Object.freeze({
  post: 1,
  get: 3,
  getStream: 4,
  delete: 5,
  postStream: 6,
})

const extractFileExtension = filename => filename.split('.').pop()

const validityCheck = (req, options) => {
  let errorVal
  if (!options) {
    errorVal = 'Options not provided'
    req.gridfs = { error: errorVal }
    return false
  }

  let supported = false
  Object.keys(Ops).forEach(key => {
    if (Ops[key] === options.op) {
      supported = true
    }
  })

  if (!supported) {
    errorVal = 'Operation not supported'
    req.gridfs = { error: errorVal }
    return false
  }

  return true
}

const handlePost = (req, next, fields, files) => {
  let filename = uuidv1()
  if (files.file && files.file.name) {
    const extension = extractFileExtension(files.file.name)
    if (extension) {
      filename += `.${extension}`
    }
  }
  const inputFileStream = fs.createReadStream(files.file.path)

  gridFsClient
    .storeFileManuscript(
      files.file.manuscriptId,
      files.file.manuscriptVersion,
      (files.file && files.file.name) || '',
      (files.file && files.file.type) || '',
      inputFileStream,
      filename,
    )
    .then(idFile => {
      req.gridfs = { post: { filename: `${idFile}` } }
      next()
    })
    .catch(errorDb => {
      req.gridfs = { error: errorDb }
      next()
    })
}

const handlePostStream = async (req, next, files, fileStream) => {
  let filename = uuidv1()
  if (files.file && files.file.name) {
    const extension = extractFileExtension(files.file.name)
    if (extension) {
      filename += `.${extension}`
    }
  }

  gridFsClient
    .storeFileManuscript(
      files.file.manuscriptId,
      files.file.manuscriptVersion,
      (files.file && files.file.name) || '',
      (files.file && files.file.type) || '',
      fileStream,
      filename,
    )
    .then(idFile => {
      req.gridfs = { post: { filename: `${idFile}` } }
      next()
    })
    .catch(errorDb => {
      req.gridfs = { error: errorDb }
      next()
    })
}

const getFileInfo = async req => {
  try {
    const metadataFile = await gridFsClient.getFileMetadata(req.params.filename)
    if (metadataFile) {
      // let filename = Buffer.from( metadataFile.metadata.originalFilename, 'base64').toString('utf8');
      const filename = metadataFile.metadata.originalFilename

      return {
        filename,
        contentType: metadataFile.contentType,
        size: metadataFile.length,
      }
    }
    logger.error(
      `GridFs getFileInfo not found for file : ${req.params.filename}`,
    )
    req.gridfs = {
      error: `GridFs getFileInfo not found for file : ${req.params.filename}`,
    }
    return {}
  } catch (errorDb) {
    logger.error(
      `GridFs getFileInfo error for file ${req.params.filename}: `,
      errorDb,
    )
    req.gridfs = { error: errorDb }
    return {}
  }
}

const handleGet = async (req, next) => {
  const { filename, contentType, size } = await getFileInfo(req)

  if (filename) {
    const tmpFile = utils.getTempPath(req.params.filename)
    const tmpWriteStream = fs.createWriteStream(tmpFile)
    gridFsClient
      .readFile(req.params.filename, tmpWriteStream)
      .then(idFile => {
        req.gridfs = {
          get: {
            path: tmpFile,
            originalName: filename,
            contentType,
            contentLength: size,
          },
        }

        next()
      })
      .catch(errorDb => {
        req.gridfs = { error: errorDb }
        next()
      })
  } else {
    req.gridfs = { error: 'File not existing' }
    next()
  }
}

const handleGetStream = async (req, next) => {
  const { filename, contentType, size } = await getFileInfo(req)

  if (filename) {
    gridFsClient
      .readFileStream(req.params.filename)
      .then(stream => {
        req.gridfs = {
          get: {
            stream,
            originalName: filename,
            contentType,
            contentLength: size,
          },
        }
        next()
      })
      .catch(errorDb => {
        req.gridfs = { error: errorDb }
        next()
      })
  } else {
    req.gridfs = { error: 'File not existing' }
    next()
  }
}

const handleDelete = async (req, next) => {
  if (!req.params.filename || req.params.filename === 'undefined') {
    req.gridfs = { error: 'File name not specified' }
    next()
  } else {
    gridFsClient
      .deleteFile(req.params.filename)
      .then(data => {
        if (data.numDeletion > 0) {
          req.gridfs = { delete: 'Success' }
        } else {
          req.gridfs = { error: 'File not existing' }
        }
        next()
      })
      .catch(errorDb => {
        req.gridfs = { error: errorDb }
        next()
      })
  }
}

const handleRequests = (req, next, options) => {
  if (!validityCheck(req, options)) {
    next()
    return
  }

  if (options.op === Ops.post) {
    const form = new formidable.IncomingForm()
    form.parse(req, (err, fields, files) => {
      if (err) {
        req.gridfs = { error: err }
        next()
      } else if (!files.file) {
        req.gridfs = { error: 'No file attached to post' }
        next()
      } else {
        handlePost(req, next, fields, files)
      }
    })
  } else if (options.op === Ops.postStream) {
    const form = new formidable.IncomingForm()
    const pass = new PassThrough()
    const files = { file: {} }
    form.onPart = part => {
      if (!part.filename) {
        form.handlePart(part)
        return
      }
      files.file.name = part.filename
      files.file.type = part.mime
      part.on('data', buffer => {
        pass.write(buffer)
      })
      part.on('end', () => {
        pass.end()
      })
    }
    form.parse(req, err => {
      if (err) {
        req.gridfs = { error: err }
        next()
      } else {
        handlePostStream(req, next, files, pass)
      }
    })
  } else if (options.op === Ops.get) {
    handleGet(req, next)
  } else if (options.op === Ops.getStream) {
    handleGetStream(req, next)
  } else if (options.op === Ops.delete) {
    handleDelete(req, next)
  }
}

module.exports = {
  middleware() {
    return options => (req, res, next) => {
      handleRequests(req, next, options)
    }
  },
  Ops,
}
