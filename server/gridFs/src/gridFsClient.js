/* eslint linebreak-style: 0 */
require('dotenv').config()
const mongo = require('mongodb')
const { v1: uuidv1 } = require('uuid')

let gfs
let connMongo
let collectionMetadata

const initDb = async () => {
  if (connMongo == null) {
    connMongo = await mongo.MongoClient.connect(process.env.GRIDFS_MONGO_DB_URL)
    gfs = new mongo.GridFSBucket(connMongo.db(process.env.GRIDFS_DBNAME), {
      bucketName: process.env.GRIDFS_BUCKET,
      // chunkSizeBytes: Number(process.env.GRIDFS_CHUNCKSIZE),
    })

    collectionMetadata = connMongo
      .db(process.env.GRIDFS_DBNAME)
      .collection(`${process.env.GRIDFS_BUCKET}.files`)
  }
}

const extractFileExtension = filename => filename.split('.').pop()

const streamToPromise = (stream, filename) =>
  new Promise((resolve, reject) => {
    stream.on('finish', () => resolve(filename))
    stream.on('error', error => reject(error))
  })

const getFileId = filename => {
  let idfileValue = uuidv1()
  const extension = extractFileExtension(filename)
  if (extension) {
    idfileValue += `.${extension}`
  }
  return idfileValue
}

const getManuscriptCompositeId = (manuscriptId, manuscriptVersion) => {
  let manuscriptVersionVal
  if (typeof manuscriptVersion === 'number') {
    manuscriptVersionVal = manuscriptVersion.toString()
  } else {
    manuscriptVersionVal = manuscriptVersion
  }

  if (manuscriptVersionVal.endsWith('.00') === false) {
    manuscriptVersionVal += '.00'
  }

  const manuscriptCompositeId = `${manuscriptId}/${manuscriptVersionVal}`
  return manuscriptCompositeId
}

const getFilesMetadata = filter =>
  new Promise((resolve, reject) => {
    gfs.find(filter).toArray((err, files) => {
      if (err) {
        reject(err)
      } else {
        resolve(files)
      }
    })
  })

/**
 * THIS METHOD SHOULD NOT BE USED. It is mstly for internal purpose. It stores a file for a specific manuscript  
 * 
 * @param filename composite id of the file i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf
 * @param mimeType mime type of the file i.e. application/pdf
 * @param readStream Input read stream from where to read the file content i.e. file system read strean
 * @param metadata document containing miscellaneous metadata connected with the file
 * @return Promise retrning the filename if the operation is successful, otherwise rejection error
 * @method storeFile

 */

const storeFile = async (filename, mimeType, readStream, metadata) => {
  await initDb()
  const writeStream = gfs.openUploadStreamWithId(filename, filename, {
    contentType: mimeType,
    metadata,
  })
  const retPromise = streamToPromise(writeStream, filename)
  readStream.pipe(writeStream)
  return retPromise
}

module.exports = {
  /**
 * It returns all the metadata information connected with all the files for a specific manuscript identified by id and version 
 * 
 * [{
  length: 46548,
  chunkSize: 15728640,
  uploadDate: 2021-03-24T12:05:28.886Z,
  filename: '4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf',
  md5: '9ec52c707e2afc2d33b4cecf53ad1145',
  contentType: 'application/pdf',
  metadata: { compositeId: 'EMS83002/0.00',
               manuscriptId: 'EMS83002',
               manuscriptVersion: '0.00',
               originalFilename: 'EMS83002-supplement-Supplementary_Figure_3.pdf' 
            }
},...
]
 *
 * @param manuscriptId manuscript id i.e. EMS45678
 * @param manuscriptVersion manuscript version i.e. 0
 * @return all the metadata information about the files connected with this manuscript
 * @method getFilesMetadataManuscript
 */

  async getFilesMetadataManuscript(manuscriptId, manuscriptVersion) {
    await initDb()
    const compositeIdManuscript = getManuscriptCompositeId(
      manuscriptId,
      manuscriptVersion,
    )
    return getFilesMetadata({
      'metadata.compositeId': compositeIdManuscript,
    })
  },

  /**
 * It returns the metadata information connected with a specified file of a manuscript 
 * 
 * {
  length: 46548,
  chunkSize: 15728640,
  uploadDate: 2021-03-24T12:05:28.886Z,
  filename: '4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf',
  md5: '9ec52c707e2afc2d33b4cecf53ad1145',
  contentType: 'application/pdf',
  metadata: { compositeId: 'EMS83002/0.00',
               manuscriptId: 'EMS83002',
               manuscriptVersion: '0.00',
               originalFilename: 'EMS83002-supplement-Supplementary_Figure_3.pdf' 
            }
}
 *
 * @param idfile composite identifier of a file in the database i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf
 * @return all the metadata information about the file if it is found, otherwise empty object
 * @method getFileMetadata
 */
  async getFileMetadata(idfile) {
    await initDb()
    const metadataInfos = await getFilesMetadata({ filename: idfile })
    if (metadataInfos.length > 0) {
      return metadataInfos[0]
    }
    return null
  },

  /**
 * It stores a file for a specific manuscript  
 * 
 * @param manuscriptId manuscript id i.e. EMS83002
 * @param manuscriptVersion manuscript version i.e. 0
 * @param filename name of the file i.e. EMS83002-supplement-Supplementary_Figure_3.pdf
 * @param mimeType mime type of the file i.e. application/pdf
 * @param readStream Input read stream from where to read the file content i.e. file system read stream,
 * @param idFileInput Id of a file if you have it as input. If it is null, a new fileId will be generated
 * @return Promise resolving to the composite id assigned to the file in the database if the operation succeeds (i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf) , otherwise rejection error
 * @method storeFileManuscript

 */

  async storeFileManuscript(
    manuscriptId,
    manuscriptVersion,
    filename,
    mimeType,
    readStream,
    idFileInput,
  ) {
    await initDb()
    let idfile
    if (idFileInput) {
      idfile = idFileInput
    } else {
      idfile = getFileId(filename)
    }
    const compositeIdManuscript = getManuscriptCompositeId(
      manuscriptId,
      manuscriptVersion,
    )
    const metadataManuscript = {
      compositeId: compositeIdManuscript,
      manuscriptId,
      manuscriptVersion,
      originalFilename: filename,
    }
    return storeFile(idfile, mimeType, readStream, metadataManuscript)
  },

  /**
   * It deletes all the files for a specific manuscript
   *
   * @param manuscriptId manuscript id i.e. EMS83002
   * @param manuscriptVersion manuscript version i.e. 0
   * @return List of Promises resolving to a list of objects containing both the number of files deleted and eventual id of the file deleted (i.e. [{numDeletion : 1, filename : 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf},{numDeletion : 1, filename : 4b25bf9b-3d41-4737-a009-0c89edf850er.tif}])
   * @method deleteAllFilesManuscript
   */
  async deleteAllFilesManuscript(manuscriptId, manuscriptVersion) {
    const filesMetadata = await this.getFilesMetadataManuscript(
      manuscriptId,
      manuscriptVersion,
    )
    const resultsDeletions = []
    filesMetadata.forEach(element => {
      resultsDeletions.push(this.deleteFile(element.filename))
    })
    return Promise.all(resultsDeletions)
  },

  /**
   * It reads the content of a file for a specific manuscript
   *
   * @param filename composite id of the file into the database i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf
   * @param writeStream Output write stream to use to write the file content read from the database i.e. file system write strean
   * @return Promise resolving to the id of the file in the database if the operation succeeds (i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf) , otherwise rejection error
   * @method readFile
   */
  async readFile(filename, writeStream) {
    await initDb()
    const readStream = gfs.openDownloadStream(filename)
    const retPromise = streamToPromise(writeStream, filename)
    readStream.pipe(writeStream)
    return retPromise
  },

  /**
   * It gets the reading stream the content of a file for a specific manuscript
   *
   * @param filename composite id of the file into the database i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf
   * @return Promise resolving to the reading stream for the specified file  (i.e. 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf) , otherwise rejection error
   * @method readFileStream
   */
  async readFileStream(filename) {
    await initDb()
    const readStream = gfs.openDownloadStream(filename)
    return readStream
  },
  /**
 * It deletes a file for a specific manuscript  
 * 
 * @param filename composite id of the file into the database 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf
 * @return Promise resolving to an object containing both the number of files deleted and eventual id of the file deleted (i.e. {numDeletion : 1, filename : 4b25bf9b-3d41-4737-a009-0c89edf850f7.pdf})
 * @method deleteFile

 */

  async deleteFile(filename) {
    await initDb()
    const fileMetadata = await this.getFileMetadata(filename)
    if (fileMetadata) {
      return new Promise((resolve, reject) => {
        gfs.delete(filename, error => {
          if (error) {
            resolve({ numDeletion: 0, filename: '' })
          } else {
            resolve({ numDeletion: 1, filename })
          }
        })
      })
    }
    return { numDeletion: 0, filename: '' }
  },

  /**

   * Method to open the connection to mongodb
   * @returns the connection to db if the operation is successful otherwise error
   * @method openConnection
   */
  async openConnection() {
    await initDb()
  },

  /**
   * Close the connection to mongodb
   * @returns success if the operation is successful otherwise error
   * @method closeConnection
   */

  async closeConnection() {
    await connMongo.close()
  },

  /**
   * Method to check if the connection to mongoDB is working
   * @returns The number of documents in the metadata files collection or -1 if there is an error
   */
  async checkConnection() {
    await initDb()
    let numFiles
    try {
      numFiles = await collectionMetadata.estimatedDocumentCount()
    } catch (err) {
      numFiles = -1
    }
    return numFiles
  },
}
