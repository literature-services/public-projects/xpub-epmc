const saxon = require('saxon-js')
const { buildCitationXmlFragment } = require('../xmlFinalize')

const refIDs = [{ id: 'R1', position: '1', pmid: 'pmid123' }]
const records = [
  {
    pmid: 'pmid123',
    doi: 'test doi with special chars <>',
    pmcid: 'pmcid321',
  },
]

test('create citations xml fragment', async () => {
  const doc = await buildCitationXmlFragment(refIDs, records)
  expect(doc).not.toBe(null)
  expect(doc.documentElement.nodeName).toEqual('citations')
  expect(
    saxon.XPath.evaluate('/citations/citation/pub-id[@pub-id-type="pmid"]', doc)
      .textContent,
  ).toBe('pmid123')
  expect(
    saxon.XPath.evaluate('/citations/citation/pub-id[@pub-id-type="doi"]', doc)
      .textContent,
  ).toBe('test doi with special chars <>')
})
