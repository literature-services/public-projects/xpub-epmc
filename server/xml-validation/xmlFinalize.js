const libxml = require('libxmljs')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const logger = require('@pubsweet/logger')
const config = require('config')
const saxon = require('saxon-js')
const fetch = require('node-fetch')
const { transaction } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const { default: axios } = require('axios')
const axiosRetry = require('axios-retry')
const _ = require('lodash')
const R = require('ramda')

const tar = require('../utils/unTar.js')
const fileUtils = require('../utils/files.js')
const Manuscript = require('../xpub-model/entities/manuscript')
const File = require('../xpub-model/entities/file')
const Note = require('../xpub-model/entities/note')
const { transform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')
const knex = BaseModel.knex()

const axiosInstance = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 60000, // 1 min
})

axiosRetry(axiosInstance, {
  retries: 3,
  retryDelay: axiosRetry.exponentialDelay,
  retryCondition: _error => true, // Retry on all failures
  onRetry: (retryCount, error, requestConfig) => {
    const {
      manuscriptConfig: { manuscriptId },
    } = requestConfig
    logger.error(`[${manuscriptId}] citation search error:`, error)
    logger.info(
      `[${manuscriptId}] citation search retry attempt: ${retryCount}`,
    )
  },
  shouldResetTimeout: true,
})

/**
 * Step 1: Extract references from xml
 * Step 2: Calling NCBI citation matcher API
 * Step 3: Calling NCBI ID converter api
 * Step 4: Build XML frament containing the matched citations
 * Step 5: Generate new XML containing matched citations
 * @param {*} manuscript
 * @param {*} userId
 * @param {*} bearer
 * @param {*} xslt
 */
const addCitation = async function addCitation(
  manuscript,
  userId,
  bearer,
  xslt = 'addCitation',
) {
  logger.info(`[${manuscript.id}] Adding citation`)
  const trx = await transaction.start(knex)
  const headers = new fetch.Headers({ Authorization: `Bearer ${bearer}` })
  try {
    const xml = manuscript.files.find(file => file.type === 'PMC')
    if (xml) {
      const { id: manId, meta, version, journal } = manuscript
      const { articleIds, notes, location, publicationDates, subjects } = meta
      const { journalTitle } = journal
      const { issn, nlmta } = journal.meta
      const { fpage, lpage, elocationId } = location || {}
      const license = notes && notes.find(n => n.notesType === 'openAccess')
      const licenseLink = license && license.content
      const openAccess =
        licenseLink && licenseLink.search(/\/(by-nd|by-nc-nd)\//i) < 0
      const addLicense = await Note.getLatestValue(manId, 'addLicense', trx)
      const subjGroup =
        subjects &&
        subjects.length > 0 &&
        (await saxon.getResource({
          text: `<subj-group subj-group-type="europepmc-category">${subjects
            .map(s => `<subject>${s}</subject>`)
            .join('')}</subj-group>`,
          type: 'xml',
        }))
      const xmlString = await fileUtils.fetchFile(baseUrl + xml.url)
      const xmlDoc = libxml.parseXml(xmlString)
      logger.debug('XML to transform read from DB')
      const { externalId, systemId } = await xmlDoc.getDtd()

      // Citation search
      const refs = xmlDoc.find(
        '//*[self::element-citation or self::mixed-citation]',
      )
      const refIDs = [] // { id, position, pmid }
      if (refs && refs.length > 0) {
        logger.info(
          `[${manId}] Getting pub-ids... Num of refs: ${refs.length} `,
        )
        // eslint-disable-next-line no-restricted-syntax
        for (const ref of refs) {
          const id = ref
            .get('ancestor::ref[1]') // Find the nearest ancestor.
            .get('@id')
            .value()
          // position is determined by counting the number of preceding element-citation or mixed-citation
          // under the same ancestor <ref>
          const position =
            ref.find(
              `preceding::*[self::element-citation or self::mixed-citation][ancestor::ref[@id="${id}"]]`,
            ).length + 1

          const searchString = buildCitationSearchString(ref)
          if (searchString) {
            try {
              // eslint-disable-next-line no-await-in-loop
              const pmid = await citationSearchRequestWithRetry(
                manId,
                searchString,
                bearer,
              )
              if (pmid) {
                refIDs.push({ id, position, pmid })
              }
            } catch (err) {
              logger.error(
                `[${manId}] Errors when calling the citation matcher API for ref id ${id}: ${err.message}`,
              )
              throw new Error(
                `Error calling citation matcher API for ref id ${id}`,
              )
            }
          } else {
            logger.info(`[${manId}] Ignore ref ID ${id} for citation search.`)
          }
        }
      }

      let matched
      if (refIDs.length > 0) {
        // Call ID converter to find other related IDs and generate XML fragment
        const pmidList = refIDs.map(l => l.pmid).join()
        logger.info(`[${manId}] Reference IDs to be converted ${pmidList}`)
        const idconv = await fetch(`${baseUrl}/idconv?pmid=${pmidList}`, {
          headers,
        })
        const { records } = await idconv.json()
        logger.info(
          `[${manId}] IDs conversion result: ${JSON.stringify(records)}`,
        )

        logger.info(
          `[${manId}] Build citation XML fragment with converted ids...`,
        )
        matched = await buildCitationXmlFragment(refIDs, records)
      } else {
        // matched will be left undefined
        logger.info(
          `[${manId}] No refs with valid PMID found for any references`,
        )
      }

      logger.debug(`XML checks to add references done for ${manId}`)
      const params = {
        externalId,
        systemId,
        journalTitle: journalTitle || '',
        nlmta: nlmta || '',
        volume: meta.volume || '',
        issue: meta.issue || '',
        fpage: fpage || '',
        lpage: lpage || '',
        elocationId: elocationId || '',
        permissions: '',
        subjects: subjGroup || '',
        addLicense: (!openAccess && addLicense) || '',
        version: (version > 0 && Math.round(version)) || '',
        matched,
        aheadOfPrint: manuscript.aheadOfPrint,
      }
      const pf = manuscript.files.find(file => file.type === 'metadata')
      if (pf) {
        saxon
          .getResource({ location: baseUrl + pf.url, type: 'xml' })
          .then(p => {
            logger.debug('Permissions file retrieved')
            params.permissions = p
          })
          .catch(e => logger.debug('Permissions file invalid'))
      }
      issn &&
        issn.forEach(n => {
          const t = n.type.toLowerCase().charAt(0)
          params[`${t}issn`] = n.id
        })
      articleIds &&
        articleIds.forEach(aid => {
          if (aid.pubIdType !== 'pre-pmc') {
            params[aid.pubIdType] = aid.id
          }
        })
      publicationDates &&
        publicationDates.forEach(pd => {
          params[pd.type] = pd.jatsDate
            ? Object.keys(pd.jatsDate)
                .filter(el => pd.jatsDate[el])
                .sort()
                .map(el => `${el}=${pd.jatsDate[el]}|`)
                .join('')
            : ''
        })
      logger.debug('XML generated. Trying to transform')
      const newXml = await transform(xmlString, xslt, trx, params)
      logger.debug('XML transformation done')
      const tmpPath = await tar.createTempDir()
      const tmpFilePath = `${tmpPath}/${manId}.xml`
      fs.writeFileSync(tmpFilePath, newXml)
      const uuid = uuidv4()
      const mimeType = 'application/xml'
      const newPath = `/download/${uuid}.xml`
      logger.debug(
        `GridFS uploading citation added XML for manuscript ${manId} version ${version}`,
      )
      const fileSize = fs.statSync(tmpFilePath).size
      await fileUtils.fileUpload(
        manId,
        version,
        `${uuid}.xml`,
        `${manId}.xml`,
        tmpFilePath,
        mimeType,
      )

      const fileInfo = {
        url: newPath,
        filename: `${manId}.xml`,
        type: 'citationXML',
        label: '',
        size: fileSize,
        mimeType,
        manuscriptId: manId,
        manuscriptVersion: version,
      }
      const existing = manuscript.files.find(
        file => file.type === 'citationXML',
      )
      if (existing) fileInfo.id = existing.id
      await File.save(fileInfo, userId, trx)
      logger.debug('XML uploaded')
      await Manuscript.update(
        { id: manId, pdfDepositState: 'CONVERTING_XML' },
        userId,
        trx,
      )
      await trx.commit()
      logger.info(`[${manId}] XML transformation process to add citation done`)
    } else {
      throw new Error('No XML file found')
    }
  } catch (err) {
    if (trx) {
      await trx.rollback()
    }
    logger.error(`[${manuscript.id}] Adding citation failed: `, err)
    await Manuscript.update(
      {
        id: manuscript.id,
        formState: `Error adding citation to XML: ${err.message}`,
        status: 'repo-triage',
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(err)
  }
}

/**
 * Build citation search strings for a single reference
 * @param {*} ref libxml node containing one referece in the source XML
 * return { id, position, searchString}
 */
const buildCitationSearchString = ref => {
  if (!ref || !ref.get('@publication-type')) {
    throw new Error(`Missing attribute publication-type in ${ref.name()}`)
  }
  const type = ref.get('@publication-type').value()
  if (['journal', 'book'].includes(type)) {
    const reftitle =
      ref.get(`*[contains(name(.), '-title')]`) &&
      ref.get(`*[contains(name(.), '-title')]`).text()
    const firstauthor =
      ref.get('.//*[self::collab or self::surname][1]') &&
      ref.get('.//*[self::collab or self::surname][1]').text()
    const source = ref.get('source') && ref.get('source').text()
    const year = ref.get('year') && ref.get('year').text()
    const volume = ref.get('volume') && ref.get('volume').text()
    const issue = ref.get('issue') && ref.get('issue').text()
    const page = ref.get('fpage')
      ? ref.get('fpage').text()
      : ref.get('elocation-id') && ref.get('elocation-id').text()
    const end = ref.get('lpage') && ref.get('lpage').text()

    const searchString = [
      firstauthor,
      source,
      year,
      volume,
      issue,
      page ? `${page}${end || ''}` : '',
      reftitle,
    ]
      .filter(R.compose(R.not, _.isEmpty))
      .join('+')
    return searchString
  }
}
/**
 * Issues a request to the xpub-epmc citation-search api.
 *
 * Return pmid if successful, undefined otherwise.
 */
const citationSearchRequestWithRetry = async (
  manuscriptId,
  searchString,
  bearer,
) => {
  const url = `${baseUrl}/hydra?query=${encodeURIComponent(searchString)}`
  logger.info(`[${manuscriptId}] Request url: ${url}`)
  const response = await axiosInstance({
    method: 'get',
    url,
    headers: {
      Authorization: `Bearer ${bearer}`,
    },
    manuscriptConfig: {
      manuscriptId,
    },
  })
  if (!response || response.status !== 200) {
    throw new Error('hydra search failed')
  }
  return response.data && response.data.pmid
}

/**
 * Build XML fragments with citation information
 * @param {*} refIDs References extracted from the original xml
 * @param {*} records List of IDs returned from NCBI citation mater api. Each record is an object
 * with different ID type
 * @returns Saxon XML document of citations
 */
const buildCitationXmlFragment = async (refIDs, records) => {
  if (!records || records.length === 0) {
    throw new Error('Invalid arguments when building citation XML fragment')
  }

  // Build XML fragment
  const doc = libxml.Document()
  const citations = doc.node('citations')
  records.forEach(ids => {
    const r = refIDs.find(i => i.pmid === ids.pmid)
    const { id, position } = r
    const citation = citations.node('citation').attr({ refid: id, position })
    Object.keys(ids)
      .filter(t => ['pmid', 'pmcid', 'doi'].includes(t))
      .forEach(t => citation.node('pub-id', ids[t]).attr({ 'pub-id-type': t }))
  })
  return saxon.getResource({
    text: citations.toString(),
    type: 'xml',
  })
}

module.exports = {
  addCitation,
  buildCitationXmlFragment,
  citationSearchRequestWithRetry,
}
