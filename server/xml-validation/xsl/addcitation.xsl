<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:ali="http://www.niso.org/schemas/ali/1.0/"
  exclude-result-prefixes="xsi xs ali">
  
  <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="no"/>
  <xsl:param name="externalId"/>
  <xsl:param name="systemId"/>
  <xsl:param name="journalTitle"/>
  <xsl:param name="nlmta"/>
  <xsl:param name="pissn"/>
  <xsl:param name="eissn"/>
  <xsl:param name="pmid"/>
  <xsl:param name="pmcid"/>
  <xsl:param name="doi"/>
  <xsl:param name="pprid"/>
  <xsl:param name="ppub"/>
  <xsl:param name="epub"/>
  <xsl:param name="volume"/>
  <xsl:param name="issue"/>
  <xsl:param name="fpage"/>
  <xsl:param name="lpage"/>
  <xsl:param name="elocationId"/>
  <xsl:param name="addLicense"/>
  <xsl:param name="permissions"/>
  <xsl:param name="subjects"/>
  <xsl:param name="version"/>
  <xsl:param name="matched"/>
  <xsl:param name="aheadOfPrint"/>
 
  <xsl:template match="/">
    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;!DOCTYPE article PUBLIC &quot;', $externalId ,'&quot; &quot;', $systemId, '&quot;>')"/>
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="article">
    <article>
      <xsl:apply-templates select="@*"/>
      <xsl:if test="$pissn = '' and $eissn = ''">
        <xsl:processing-instruction name="noissn"/>
      </xsl:if>
      <xsl:if test="$aheadOfPrint = 'true'">
        <xsl:processing-instruction name="aheadofprint-preload">true</xsl:processing-instruction>
      </xsl:if>
      <xsl:apply-templates select="node() | processing-instruction() | comment()" />
    </article>
  </xsl:template>

  <xsl:template match="journal-meta">
    <journal-meta>
      <journal-id journal-id-type="nlm-ta"><xsl:value-of select="$nlmta"/></journal-id>
      <journal-title-group>
        <journal-title><xsl:value-of select="$journalTitle"/></journal-title>
      </journal-title-group>
      <xsl:if test="$eissn != ''">
        <issn pub-type="epub"><xsl:value-of select="$eissn"/></issn>
      </xsl:if>
      <xsl:if test="$pissn != ''">
        <issn pub-type="ppub"><xsl:value-of select="$pissn"/></issn>
      </xsl:if>
      <xsl:if test="$pissn = '' and $eissn = ''">
        <issn pub-type="ppub"/>
      </xsl:if>
    </journal-meta>
  </xsl:template>
  
  <xsl:template match="article-meta">
    <article-meta>
      <xsl:apply-templates select="article-id[@pub-id-type='manuscript']"/>
      <xsl:call-template name="article-id">
        <xsl:with-param name="aid" select="$pmid"/>
        <xsl:with-param name="type" select="'pmid'"/>
      </xsl:call-template>
      <xsl:call-template name="article-id">
        <xsl:with-param name="aid" select="$pmcid"/>
        <xsl:with-param name="type" select="'pmcid'"/>
      </xsl:call-template>
      <xsl:call-template name="article-id">
        <xsl:with-param name="aid" select="$doi"/>
        <xsl:with-param name="type" select="'doi'"/>
      </xsl:call-template>
      <xsl:call-template name="article-id">
        <xsl:with-param name="aid" select="$pprid"/>
        <xsl:with-param name="type" select="'archive'"/>
      </xsl:call-template>
      <xsl:if test="$version != ''">
        <article-version-alternatives>
          <article-version article-version-type="status">preprint</article-version>
          <article-version article-version-type="number">
            <xsl:value-of select="$version"/>
          </article-version>
        </article-version-alternatives>
      </xsl:if>
      <xsl:apply-templates select="article-categories | title-group | contrib-group | aff | author-notes"/>
      <xsl:apply-templates select="pub-date[@pub-type='nihms-submitted']"/>
      <xsl:call-template name="pub-date">
        <xsl:with-param name="date" select="$ppub"/>
        <xsl:with-param name="type">
          <xsl:choose>
            <xsl:when test="$elocationId != '' and $fpage = ''">
              <xsl:text>collection</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>ppub</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="pub-date">
        <xsl:with-param name="date" select="$epub"/>
        <xsl:with-param name="type">
          <xsl:choose>
            <xsl:when test="$pprid != ''">
              <xsl:text>preprint</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>epub</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:if test="$volume != ''">
        <volume><xsl:value-of select="$volume"/></volume>
      </xsl:if>
      <xsl:if test="$issue != ''">
        <issue><xsl:value-of select="$issue"/></issue>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="$fpage != ''">
          <fpage><xsl:value-of select="$fpage"/></fpage>
          <lpage>
            <xsl:choose>
              <xsl:when test="$lpage != ''">
                <xsl:value-of select="$lpage"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$fpage"/>
              </xsl:otherwise>
            </xsl:choose>
          </lpage>
        </xsl:when>
        <xsl:when test="$elocationId != ''">
          <elocation-id><xsl:value-of select="$elocationId"/></elocation-id>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="history | pub-history"/>
      <xsl:choose>
        <xsl:when test="$pprid != '' and $permissions != ''">
          <xsl:apply-templates select="$permissions/permissions"/>
        </xsl:when>
        <xsl:when test="permissions">
          <xsl:apply-templates select="permissions"/>
        </xsl:when>
        <xsl:when test="$addLicense = 'approved'">
          <permissions>
            <ali:free_to_read xmlns:ali="http://www.niso.org/schemas/ali/1.0/"/>
            <xsl:call-template name="epmc-license"/>
          </permissions>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="self-uri | related-article | abstract | trans-abstract | kwd-group | funding-group | comment()"/>
    </article-meta>
  </xsl:template>
  
  <xsl:template name="article-id">
    <xsl:param name="type"/>
    <xsl:param name="aid"/>
    <xsl:if test="$aid != ''">
      <article-id pub-id-type="{$type}">
        <xsl:value-of select="$aid"/>
      </article-id>      
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="article-categories">
    <article-categories>
      <xsl:apply-templates select="subj-group[not(@subj-group-type='europepmc-category')]"/>
      <xsl:if test="$subjects != ''">
        <xsl:apply-templates select="$subjects/subj-group"/>
      </xsl:if>
    </article-categories>
  </xsl:template>

  <xsl:template match="subj-group[@subj-group-type='heading']/subject">
    <subject>
      <xsl:choose>
        <xsl:when test="/article/@article-type = 'preprint-removal'">Removal</xsl:when>
        <xsl:when test="/article/@article-type = 'preprint-withdrawal'">Withdrawal</xsl:when>
        <xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
      </xsl:choose>
    </subject>
  </xsl:template>
  
  <xsl:template name="pub-date">
    <xsl:param name="type"/>
    <xsl:param name="date"/>
    <xsl:if test="$date != ''">
      <pub-date pub-type="{$type}">
        <xsl:if test="contains($date, 'day=')">
          <day>
            <xsl:value-of select="substring-before(substring-after($date,'day='),'|')"/>
          </day>
        </xsl:if>
        <xsl:if test="contains($date, 'month=')">
          <month>
            <xsl:value-of select="substring-before(substring-after($date,'month='),'|')"/>
          </month>
        </xsl:if>
        <xsl:if test="contains($date, 'season=')">
          <season>
            <xsl:value-of select="substring-before(substring-after($date,'season='),'|')"/>
          </season>
        </xsl:if>
        <xsl:if test="contains($date, 'year=')">
          <year>
            <xsl:value-of select="substring-before(substring-after($date,'year='),'|')"/>
          </year>
        </xsl:if>
      </pub-date>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="permissions">
    <permissions>
      <xsl:apply-templates/>
      <xsl:if test="$addLicense = 'approved'">
        <xsl:if test="not(ali:free_to_read)">
          <ali:free_to_read xmlns:ali="http://www.niso.org/schemas/ali/1.0/"/>
        </xsl:if>
        <xsl:call-template name="epmc-license"/>
      </xsl:if>
    </permissions>
  </xsl:template>

  <xsl:template name="epmc-license">
    <license>      
      <ali:license_ref xmlns:ali="http://www.niso.org/schemas/ali/1.0/">https://europepmc.org/downloads/openaccess</ali:license_ref>
      <license-p>This preprint is made available via the <ext-link ext-link-type="uri" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://europepmc.org/downloads/openaccess">Europe PMC open access subset</ext-link>, for unrestricted research re-use and secondary analysis in any form or by any means with acknowledgement of the original preprint source.</license-p>
    </license>
  </xsl:template>
  
  <xsl:template match="element-citation|mixed-citation">
    <xsl:variable name="refid" select="ancestor::ref[1]/@id"/>
    <xsl:variable name="position" select="count(preceding::*[self::element-citation or self::mixed-citation][ancestor::ref[@id=$refid]]) + 1"/>
    <xsl:element name="{name(.)}">
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="node()[not(self::pub-id)]"/>
      <xsl:choose>
        <xsl:when test="$matched/citations/citation[@refid=$refid and @position=$position]"> 
          <xsl:apply-templates select="$matched/citations/citation[@refid=$refid and @position=$position]/pub-id"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="pub-id"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>
  <xsl:template match="(element-citation|mixed-citation)/text()[following-sibling::node()[1][self::pub-id]]" />
  
  <xsl:template match="license[ali:license_ref='https://europepmc.org/downloads/openaccess']"/>
  <xsl:template match="processing-instruction('properties')"/>
  <xsl:template match="processing-instruction('origin')">
    <xsl:processing-instruction name="origin">ukpmcpa</xsl:processing-instruction>
  </xsl:template>
  <xsl:template match="processing-instruction('aheadofprint-preload')" />
  
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="node() | processing-instruction() | comment()" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="@* | text() | comment() | processing-instruction()">
    <xsl:copy/>
  </xsl:template>
  
</xsl:stylesheet>