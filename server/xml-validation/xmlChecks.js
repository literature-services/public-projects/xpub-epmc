const logger = require('@pubsweet/logger')
const config = require('config')
const BaseModel = require('@pubsweet/base-model')
const { transaction } = require('objection')
const async = require('async')

const { ManuscriptManager, NoteManager } = require('../xpub-model')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const getUser = require('../utils/user.js')
const fileUtils = require('../utils/files.js')
const { token } = require('../utils/authentication')
const {
  processXML,
  testStep,
  nxmlStep,
  htmlStep,
  updateStep,
} = require('./xmlProcess')
const { addCitation } = require('./xmlFinalize')
const { schValidate } = require('./xmlValidate')
const { useTransform } = require('./transform')

const { baseUrl } = config.get('pubsweet-server')

if (!process.env.ENABLE_CRONJOB_XMLCONVERTER) {
  logger.info(
    'ENABLE_CRONJOB_XMLCONVERTER not defined. xml-converter cronjob exits.',
  )
  process.exit(0)
}

const knex = BaseModel.knex()

;(async () => {
  await checkJobStatus('xml-converter', xmlCheck)
  process.exit()
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('xml-converter', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('xml-converter', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

const beforeUpdate = Date.now()

async function xmlCheck() {
  // const beforeUpdate = Date.now()
  const mQueue = await ManuscriptManager.findByDepositStates([
    'ADDING_CITATION',
    'CONVERTING_XML',
    'VALIDATING_XML',
  ])
  logger.info(
    `XMl conversion started. ${mQueue.length} manuscripts to process.`,
  )
  if (mQueue && mQueue.length > 0) {
    const pQueue = async.queue(processTask, 1) // async queue with concurrency = 10
    // Push the manuscripts at once. If pushed seperated, then an empty arry would tigger an early drain
    // event and make the scrips exit early.
    pQueue.push([
      ...mQueue.filter(q => q.pdfDepositState === 'ADDING_CITATION'),
      ...mQueue.filter(q => q.pdfDepositState === 'CONVERTING_XML'),
      ...mQueue.filter(q => q.pdfDepositState === 'VALIDATING_XML'),
    ])
    pQueue.error((err, m) => {
      logger.error(`[${m.id}] XML processing experienced an error`, err)
    })
    await pQueue.drain()
  }
  logger.info(`XML processing finished in ${Date.now() - beforeUpdate} ms`)
}

async function processTask(m) {
  if (m.pdfDepositState === 'ADDING_CITATION') {
    await finalizeXml(m)
  } else if (m.pdfDepositState === 'CONVERTING_XML') {
    await convertXml(m)
  } else if (m.pdfDepositState === 'VALIDATING_XML') {
    await validateXml(m)
  }
}

async function finalizeXml(manuscript) {
  const user = await getUser.getAdminUser()
  const bearer = token.create(user)
  const addCitationXSL = await useTransform('addCitation')
  return addCitation(manuscript, user.id, bearer, addCitationXSL)
}

// Creating new NXML and HTML after tagging or finalizing
async function convertXml(m) {
  const adminUser = await getUser.getAdminUser()
  const styleCheckerXSL = await useTransform('styleChecker')
  const nxmlXSL = await useTransform('createNXML')
  const htmlXSL = await useTransform('createHTML')

  // Step 1: Validate and check style
  logger.info(`[${m.id}] Converting XML step 1: Checking style.`)
  const file =
    m.status !== 'tagging' &&
    m.files.some(f => !f.deleted && f.type === 'citationXML')
      ? m.files.find(f => !f.deleted && f.type === 'citationXML')
      : m.files.find(f => !f.deleted && f.type === 'PMC')
  const xml = await fileUtils.fetchFile(baseUrl + file.url)
  m.xmlFile = xml
  const styleResult = await processXML({
    xml,
    xsl: styleCheckerXSL,
    manuscript: m,
    step: testStep,
    adminUser,
  })
  if (!styleResult) {
    logger.error(`[${m.id}] Style check failed.`)
    return
  }

  // Step 2
  logger.info(`[${m.id}] Converting XML step 2: Create NXML.`)
  const nxml = await processXML({
    xml,
    xsl: nxmlXSL,
    manuscript: m,
    step: nxmlStep,
    adminUser,
  })
  if (nxml) {
    m.nxmlFile = nxml
  } else {
    logger.error(`[${m.id}] NXML creation failed.`)
    return
  }

  logger.info(`[${m.id}] Converting XML step 3: Create HTML`)
  const htmlResult = await processXML({
    xml,
    xsl: htmlXSL,
    manuscript: m,
    step: htmlStep,
    adminUser,
  })
  if (!htmlResult) {
    logger.error(`[${m.id}] HTML creation failed.`)
  }

  // Step 4
  logger.info(`[${m.id}] Converting XML step 4: Update manuscript.`)
  await processXML({ xml, manuscript: m, step: updateStep, adminUser })
  logger.info(`[${m.id}] XML conversion complete.`)
}

// Validate XML for one manuscript
async function validateXml(m) {
  const user = await getUser.getAdminUser()
  const trx = await transaction.start(knex)
  const errs = []
  const schematronXSL = await useTransform('schematron')
  let warnings = []
  try {
    const file = m.files.find(file => !file.deleted && file.type === 'PMC')
    logger.info(`[${m.id}] Schematron validating ${file.filename}`)
    warnings = await schValidate(file.url, schematronXSL)
  } catch (err) {
    errs.push(m.id)
    const subject = 'Fix tagging errors [schematron]'
    // Send to file-error status to mark validation is complete
    await ManuscriptManager.update(
      { id: m.id, pdfDepositState: '', status: 'file-error' },
      user.id,
      trx,
    )
    // Send back for tagging correction
    await ManuscriptManager.retag(
      {
        manuscriptId: m.id,
        manuscriptVersion: m.version,
        notesType: 'userMessage',
        content: JSON.stringify({
          to: ['tagger'],
          subject,
          message: err.message,
        }),
      },
      user.id,
      trx,
    )
    return trx.commit()
  }
  const note = {
    notesType: 'schematron',
    content: JSON.stringify(warnings),
    manuscriptId: m.id,
    manuscriptVersion: m.version,
  }
  const [exists] = await NoteManager.findByManIdAndType(m.id, 'schematron', trx)
  if (exists) {
    note.id = exists.idtype
    await NoteManager.update(note, user.id, trx)
  } else {
    await NoteManager.create(note, user.id, trx)
  }
  await ManuscriptManager.update(
    { id: m.id, pdfDepositState: 'CONVERTING_XML' },
    user.id,
    trx,
  )
  logger.info(`[${m.id}] Schematron validation passed.`)
  return trx.commit()
}

// module.export = { finalizeQueue }
