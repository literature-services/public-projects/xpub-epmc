const path = require('path')
const libxml = require('libxmljs')
const fs = require('fs')
const uuidv4 = require('uuid/v4')
const logger = require('@pubsweet/logger')
const config = require('config')
const { transaction } = require('objection')
const BaseModel = require('@pubsweet/base-model')
const tar = require('../utils/unTar.js')
const fileUtils = require('../utils/files.js')
const getUser = require('../utils/user.js')
const { createManifest } = require('../ebi-integration/toEbi')
const Manuscript = require('../xpub-model/entities/manuscript')
const File = require('../xpub-model/entities/file')
const Note = require('../xpub-model/entities/note')
const { transform } = require('./transform')
const { xsdValidate: validate } = require('./xmlValidate')

const { baseUrl } = config.get('pubsweet-server')
const xlink = 'http://www.w3.org/1999/xlink'
const ali = 'http://www.niso.org/schemas/ali/1.0/'
const knex = BaseModel.knex()

// Function for manually processing XML
const pushXML = async function pushXML(
  fileUrl,
  manuscriptId,
  userId,
  keepStatus,
) {
  const adminUser = await getUser.getAdminUser()
  await Manuscript.update(
    { id: manuscriptId, formState: null, pdfDepositState: 'CREATING_NXML' },
    userId,
  )
  logger.info(`Processing XML for ${manuscriptId}`)
  const trx = await transaction.start(knex)
  const man = await Manuscript.findById(manuscriptId, adminUser.id, trx)

  const xml = await fileUtils.fetchFile(baseUrl + fileUrl)

  const processObj = { xml, manuscript: man, adminUser, keepStatus, trx }

  processObj.step = testStep
  const success = await processXML(processObj)

  if (success) {
    processObj.step = nxmlStep
    const nxml = await processXML(processObj)

    if (nxml) {
      processObj.xml = nxml
      processObj.step = htmlStep
      const html = await processXML(processObj)

      if (html) {
        processObj.xml = xml
        processObj.step = updateStep
        // TRX commit happens in here
        await processXML(processObj)
        logger.info(`XML processing complete for ${manuscriptId}`)
      }
    }
  }
}

// Function for process XML in the queue, with a certian step
const processXML = async function processXML({
  xml,
  step,
  xsl,
  manuscript,
  adminUser,
  keepStatus,
  trx,
}) {
  logger.debug(`[${manuscript.id}] XML step processing start`)
  let commit = false
  const userId = adminUser.id
  try {
    if (!trx) {
      trx = await transaction.start(knex)
      commit = true
    }
    const result = await step({ xml, xsl, manuscript, userId, keepStatus, trx })
    if (trx && commit) await trx.commit()
    return result || true
  } catch (err) {
    const errorStatus =
      (keepStatus && manuscript.status) ||
      (['tagging'].includes(manuscript.status) && 'file-error') ||
      (['repo-ready', 'repo-processing'].includes(manuscript.status) &&
        'repo-triage') ||
      manuscript.status
    if (trx) await trx.rollback()
    await Manuscript.update(
      {
        id: manuscript.id,
        formState: err.message,
        status: errorStatus,
        pdfDepositState: null,
      },
      userId,
    )
    logger.error(`[${manuscript.id}] XML processing complete, with errors`, err)
    return false
  }
}

const testStep = async function testStep({
  xml,
  manuscript,
  trx,
  xsl: styleXSL = 'styleChecker',
}) {
  // Validate XML
  logger.info(`[${manuscript.id}] Validate against publishing XSD`)
  await validate(xml, 'publishing', 'XML')

  // Check XML against the stylechecker
  logger.info(`[${manuscript.id}] Validate against stylechecker`)
  const checked = await transform(xml, styleXSL, trx)
  const result = libxml.parseXml(checked)

  const styleErrors = result.find('//error')

  if (styleErrors.length > 0) {
    let styleErrString = `Style Errors: <br/><br/>`
    styleErrors.forEach((err, i) => {
      styleErrString += err
        .text()
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
      if (i !== styleErrors.length - 1) {
        styleErrString += `<br/><br/>`
      }
    })
    throw new Error(styleErrString)
  }
}

const nxmlStep = async function nxmlStep({
  xml,
  manuscript,
  trx,
  xsl: nxmlXSL = 'createNXML',
}) {
  // Transform XML to NXML
  logger.info(`[${manuscript.id}] Transform XML to NXML`)
  const nxml = await transform(xml, nxmlXSL, trx)
  // Validate NXML
  logger.info(`[${manuscript.id}] Validate NXML against archiving XSD`)
  await validate(nxml, 'archiving', 'NXML')
  // save converted NXML
  logger.info(`[${manuscript.id}] Save converted NXML`)
  await saveFile({ content: nxml, extension: 'nxml', manuscript, trx })

  return nxml
}

const htmlStep = async function htmlStep({
  xml,
  manuscript,
  trx,
  xsl: htmlXSL = 'createHTML',
}) {
  const filelist = getFilelist(manuscript.files)

  // Transform NXML to HTML:
  logger.info(`[${manuscript.id}] Transform NXML to HTML`)
  const htParams = { filelist, msspreview: 'true()' }
  const html = await transform(xml, htmlXSL, trx, htParams)
  // save converted HTML
  logger.info(`[${manuscript.id}] Saving converted HTML`)
  await saveFile({ content: html, extension: 'html', manuscript, trx })
  logger.info(`[${manuscript.id}] HTML saved`)
}

const updateStep = async function updateStep({
  xml,
  manuscript,
  trx,
  userId,
  keepStatus,
}) {
  logger.info(`[${manuscript.id}] XML processing, updateStep`)

  const manuscriptId = manuscript.id
  const xmlDoc = libxml.parseXml(xml)
  // Get article-type
  const articleType = xmlDoc.get('/article/@article-type').value()

  // Check for open access
  const license = xmlDoc.get(
    '//descendant-or-self::license[contains(@xlink:href, "creativecommons.org") or contains(ali:license_ref, "creativecommons.org")]/descendant-or-self::*[self::ali:license_ref or attribute::xlink:href]',
    { xlink, ali },
  )
  const { notes } = manuscript.meta
  const oAnote = notes && notes.find(n => n.notesType === 'openAccess')
  const planS = notes && notes.find(n => n.notesType === 'planS')
  let licenseLink = null
  let isOpen = false
  if (license) {
    licenseLink = license.get('@xlink:href', { xlink })
      ? license.get('@xlink:href', { xlink }).value()
      : license.text()
    isOpen =
      licenseLink.search(
        /\/(publicdomain|by|by-nc|by-nd|by-sa|by-nc-sa|by-nc-nd)\//i,
      ) >= 0
    if (isOpen) {
      if (!oAnote) {
        await Note.create(
          {
            notesType: 'openAccess',
            content: licenseLink,
            manuscriptId,
            manuscriptVersion: manuscript.version,
          },
          userId,
          trx,
        )
      } else if (oAnote.content !== licenseLink) {
        await Note.update(
          {
            id: oAnote.id,
            content: licenseLink,
            manuscriptVersion: manuscript.version,
          },
          userId,
          trx,
        )
      }
    } else if (oAnote) {
      await Note.delete(oAnote.id, userId, trx)
    }
  } else if (oAnote) {
    await Note.delete(oAnote.id, userId, trx)
  }

  // ***********************
  // Commit database changes
  // ***********************

  if (trx) await trx.commit()

  // *****************
  // File error checks
  // *****************

  // Check for preprint withdrawal/removal

  if (articleType === 'preprint' && manuscript.status === 'tagging') {
    // Find third paragraph
    const ptest = xmlDoc.find('//p')
    if (ptest.length <= 2) {
      throw new Error(
        `🔎 May be preprint withdrawal or removal notice. Please check, and if so change article type:\n\n\t• to 'preprint-withdrawal' if previous versions are accessible on the server\n\t• to 'preprint-removal' otherwise\n\nAfter XML processing with the correct article type, click the blue button and withdrawal and removal notices will be released immediately.`,
      )
    }
  }

  // Check for errors in licensing

  if (isOpen) {
    if (planS) {
      if (planS.content) {
        if (!licenseLink.includes(`/${planS.content}/`)) {
          throw new Error(
            `License in XML does not match Plan S exception (${planS.content
              .toUpperCase()
              .replace(/\//g, ' ')})`,
          )
        }
      } else if (!licenseLink.includes('/by/')) {
        throw new Error(
          'Submission with Plan S funding does not have CC BY license.',
        )
      }
    }
  }
  if (planS && !license) {
    throw new Error('Submission with Plan S funding has no license.')
  }
  if (planS && !licenseLink) {
    throw new Error('Submission with Plan S funding has no license URL.')
  }

  // Check for missing referenced files in xml

  // check for @xlink:href attribute
  const nodes = xmlDoc.find('//*[self::media or self::graphic][@xlink:href]', {
    xlink,
  })

  const fileNotFound = []
  let fileFound = false

  nodes.forEach(node => {
    const nodeTag = node.name()

    const filename = node.get('@xlink:href', { xlink }).value()

    if (nodeTag === 'media') {
      // check entire filename including extension
      fileFound = manuscript.files.find(el => el.filename === filename)
      if (fileFound === undefined) fileNotFound.push(filename)
    } else if (nodeTag === 'graphic') {
      // check that filename.tif and filename.jpg exist

      const tifFound = manuscript.files.find(
        el => el.filename === `${filename}.tif`,
      )

      const jpgFound = manuscript.files.find(
        el => el.filename === `${filename}.jpg`,
      )

      if (tifFound === undefined) fileNotFound.push(`${filename}.tif`)
      if (jpgFound === undefined) fileNotFound.push(`${filename}.jpg`)
      fileFound = tifFound && jpgFound
    }
  })

  if (fileNotFound.length > 0) {
    let errString = 'Files referenced in the xml are not in the database: \n'
    fileNotFound.forEach(file => {
      errString += `${file}\n`
    })
    throw new Error(errString)
  }

  // *********************
  // Successful processing
  // *********************

  // check for PDF, and/or whether it's time to send to repo
  const hasPDF = manuscript.files.some(f => f.type === 'pdf4load')

  const makePDF = !(hasPDF || manuscript.status === 'repo-ready')

  const obj = {
    id: manuscript.id,
    meta: { articleType },
    pdfDepositState: makePDF ? 'WAITING_FOR_PDF_CONVERSION' : null,
    formState:
      (makePDF && keepStatus && 'Generating PDF') ||
      (articleType.startsWith('preprint-') &&
        'Preprint withdrawal and removal notices will be released immediately.') ||
      null,
  }

  // Complete tagging and send for XML QA
  if (manuscript.status === 'tagging' && hasPDF && !keepStatus) {
    obj.status = 'xml-qa'
  }

  // Check, finalize, and send preprints to repo
  const updated = await Manuscript.review(obj, userId)
  if (
    updated.manuscript.status === 'repo-ready' &&
    manuscript.status !== 'repo-ready'
  ) {
    logger.info(
      `[${manuscript.id}] Set pdfDepositState status to ADDING_CITATION`,
    )
    await Manuscript.update(
      {
        id: manuscript.id,
        pdfDepositState: 'ADDING_CITATION',
      },
      userId,
    )
  }

  // Send complete submissions to the ebi fulltext repository
  if (manuscript.status === 'repo-ready') {
    logger.info(`[${manuscript.id}] create manifest`)
    await createManifest(manuscript.id, userId)
  }
}

async function saveFile({ content, extension, manuscript, trx }) {
  const adminUser = await getUser.getAdminUser()
  const tmpPath = await tar.createTempDir()
  const { id: manuscriptId, version } = manuscript
  // save converted.nxml to tmp folder
  fs.writeFileSync(`${tmpPath}/${manuscriptId}.${extension}`, content)
  const file = getFileInfo(
    `${tmpPath}/${manuscriptId}.${extension}`,
    adminUser.id,
  )
  const uuid = uuidv4()
  logger.debug(
    `GridFS uploading file ${file.filename} for manuscript ${manuscriptId} version ${version}`,
  )
  await fileUtils.fileUpload(
    manuscriptId,
    version,
    `${uuid}${file.extension}`,
    file.filename,
    file.url,
    file.mimeType,
  )
  file.url = `/download/${uuid}${file.extension}`
  file.manuscriptId = manuscriptId
  file.manuscriptVersion = version
  delete file.extension
  const [fileExists] = await File.findByManIdAndType(
    manuscriptId,
    file.type,
    trx,
  )
  if (fileExists) {
    file.id = fileExists.id
  }
  await File.save(file, adminUser.id, trx)
  await fileUtils.tidyUp(tmpPath)
  logger.debug(
    `Uploading of ${extension} file to GridFS and the database done for ${manuscriptId}.`,
  )
}

function getFileInfo(filepath, userId) {
  const filename = filepath.substring(filepath.lastIndexOf('/') + 1)
  const fileExt = path.extname(filepath)
  const fileSize = fs.statSync(filepath).size

  const fileInfo = {
    url: filepath,
    filename,
    type:
      (fileExt === '.nxml' && 'PMCfinal') ||
      (fileExt === '.html' && 'tempHTML') ||
      (fileExt === '.xml' && 'PMC'),
    label: '',
    size: fileSize,
    extension: fileExt,
    mimeType:
      (fileExt === '.html' && 'text/html; charset=utf-8') || 'application/xml',
    updatedBy: userId,
  }
  return fileInfo
}

function getFilelist(files) {
  const types = ['IMGview', 'supplement', 'supplement_tag']
  const list = files.reduce((list, file) => {
    if (!file.deleted && types.includes(file.type)) {
      if (file.type === 'supplement') {
        file.filename = fileUtils.normalizeFilename(file)
      }
      list.push(file)
    }
    return list
  }, [])
  return list
    .map(
      file =>
        `${file.filename.substring(
          0,
          file.filename.lastIndexOf('.'),
        )}:/api/files/${file.url.split('/').pop()};`,
    )
    .join('')
}

module.exports = {
  pushXML,
  processXML,
  testStep,
  nxmlStep,
  htmlStep,
  updateStep,
}
