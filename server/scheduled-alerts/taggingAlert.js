const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')
const config = require('config')
const { ref } = require('objection')
const { Audit } = require('../xpub-model/entities/audit/data-access')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { userMessage } = require('../email')

const knex = BaseModel.knex()

if (!process.env.ENABLE_CRONJOB_TAGGINGALERT) {
  logger.info(
    'ENABLE_CRONJOB_TAGGINGALERT not defined. taggingAlert cronjob exits.',
  )
  process.exit(0)
}

const { externalQA, system } = config['epmc-email']
const ftpTagger = config.get('ftp_tagger')

;(async () => {
  await checkJobStatus('tagging-alert', async () => {
    const updateTime = Date.now()
    logger.info(`Counting tagged manuscripts`)
    await taggingAlerts()
    await qaReceipt()
    logger.info(`Alerts were completed in ${Date.now() - updateTime} ms`)
  })
  await Audit.knex().destroy()
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('tagging-alert', `Uncaught Exception thrown: ${err}`)
    Audit.knex && (await Audit.knex().destroy())
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('tagging-alert', `Unhandled Rejection: ${reason}`)
    Audit.knex && (await Audit.knex().destroy())
    process.exit(1)
  })

async function taggingAlerts() {
  try {
    const tagging = await Audit.query()
      .alias('a')
      .select(
        'a.manuscriptId',
        'a.manuscriptVersion',
        'm.meta,subjects',
        'o.name',
      )
      .where('a.objectType', 'manuscript')
      .whereRaw(
        "a.\"created\"::date = current_date - interval '1 day' and a.changes->>'status' = 'tagging'",
      )
      .join({ m: 'xpub.manuscript' }, 'a.manuscriptId', 'm.id')
      .whereNull('m.deleted')
      .distinct('a.manuscriptId')
      .join({ o: 'xpub.organization' }, 'm.organizationId', 'o.id')
    logger.info(`${tagging.length} manuscripts sent to tagging yesterday.`)
    if (tagging.length > 0) {
      logger.info('Sending tagging count email.')
      await sendTaggingMessage(tagging)
    }
  } catch (e) {
    logger.error(`Unable to get or send tagging count: ${e}`)
  }
  try {
    const taggingDelay = await knex.raw(`
    select id, diff from (
      select id, sum(case when extract ('ISODOW' from date_range) < 6 then 1 else 0 end) as diff from (
        select m.id, generate_series(m.updated, current_date - '1 day'::interval, '1 day'::interval) as date_range from xpub.manuscript m 
        inner join xpub.organization o on m.organization_id = o.id
        where m.status = 'tagging' and m.deleted is null and o.name = 'Europe PMC Plus'
      ) diffs
      group by id
    ) all_diffs where diff > 3`)
    const { rows } = taggingDelay
    logger.info(`${rows.length} manuscripts in tagging over 3 days.`)
    if (rows.length > 0) {
      logger.info('Sending tagging delay alert.')
      await sendTaggingDelayAlert(rows)
    }
  } catch (e) {
    logger.error(`Unable to get or send tagging delay info: ${e}`)
  }
  return true
}

async function qaReceipt() {
  try {
    const qa = await Audit.query()
      .aliasFor(Audit, 'a')
      .select(
        'a.manuscriptId',
        ref('a.changes:status')
          .castText()
          .as('status'),
        'a.manuscriptVersion',
        'o.name',
      )
      .where('a.objectType', 'manuscript')
      .whereRaw(
        "a.\"created\"::date = current_date - interval '1 day' and a.changes->>'status' in ('xml-review', 'xml-triage', 'repo-ready') and a.original_data->>'status' = 'xml-qa'",
      )
      .joinRelation('manuscript', { alias: 'm' })
      .whereNull('m.deleted')
      .distinct('a.manuscriptId')
      .join({ o: 'xpub.organization' }, 'm.organizationId', 'o.id')
    logger.info(`${qa.length} manuscripts moved through QA yesterday.`)
    if (qa.length > 0) {
      logger.info('Sending QA receipt email.')
      await sendQAMessage(qa)
      return true
    }
  } catch (e) {
    logger.error(`Unable to get or send QA count: ${e}`)
  }
  return true
}

async function sendTaggingMessage(list) {
  const listM = list.filter(m => m.name === 'Europe PMC Plus')
  const listP = list.filter(
    m =>
      m.name === 'Europe PMC Preprints' &&
      !(m['meta,subjects'] && m['meta,subjects'].includes('COVID-19')),
  )
  const listC = list.filter(
    m =>
      m.name === 'Europe PMC Preprints' &&
      m['meta,subjects'] &&
      m['meta,subjects'].includes('COVID-19'),
  )
  const message = `${list.length} manuscript${
    list.length === 1 ? ' was' : 's were'
  } sent for tagging yesterday.${
    listM.length > 0
      ? `\n\n<b>Author manuscripts (${listM.length})</b>
${listM.map(m => m.manuscriptId).join('\n')}`
      : ''
  }${
    listP.length > 0
      ? `\n\n<b>Funder preprints (${listP.length})</b>
${listP
  .map(m => `${m.manuscriptId} v${Math.round(m.manuscriptVersion)}`)
  .join('\n')}`
      : ''
  }${
    listC.length > 0
      ? `\n\n<b>COVID-19 preprints (${listC.length})</b>
${listC
  .map(m => `${m.manuscriptId} v${Math.round(m.manuscriptVersion)}`)
  .join('\n')}`
      : ''
  }`
  const subject = `${list.length} submissions sent for tagging`
  await userMessage(externalQA, subject, message, null, ftpTagger.email, system)
  return true
}

async function sendTaggingDelayAlert(list) {
  const message = `Manuscripts awaiting tagging for more than 3 days:\n\n${list
    .map(m => m.id)
    .join('\n')}`
  const subject = 'Tagging delay alert'
  await userMessage(ftpTagger.email, subject, message, null, null, system)
  return true
}

async function sendQAMessage(list) {
  const listM = list.filter(m => m.name === 'Europe PMC Plus')
  const listP = list.filter(m => m.name === 'Europe PMC Preprints')
  const listME = listM.filter(m => m.status === 'xml-triage')
  const listMA = listM.filter(m => m.status !== 'xml-triage')
  const listPE = listP.filter(m => m.status === 'xml-triage')
  const listPA = listP.filter(m => m.status !== 'xml-triage')
  let message = `${list.length} manuscript${
    list.length === 1 ? '' : 's'
  } went through QA yesterday. Summary:\n\n<table cellpadding="5px" frame="void" rules="all" style="width: 100%; text-align: left; color: #494949; line-height: 150%">
  <thead>
    <tr style="text-align: center">
      <th colspan="2">Author manuscripts (${listM.length})</th>
      <th colspan="2">Preprints (${listP.length})</th>
    </tr>
    <tr>
      <th>Errors (${listME.length})</th><th>Approved (${listMA.length})</th>
      <th>Errors (${listPE.length})</th><th>Approved (${listPA.length})</th>
    </tr>
  </thead>
  <tbody><tr>`
  message += `<td valign="top">${
    listME.length > 0 ? listME.map(m => m.manuscriptId).join('<br/>') : 'None'
  }</td>`
  message += `<td valign="top">${
    listMA.length > 0 ? listMA.map(m => m.manuscriptId).join('<br/>') : 'None'
  }</td>`
  message += `<td valign="top">${
    listPE.length > 0
      ? listPE
          .map(m => `${m.manuscriptId} v${Math.round(m.manuscriptVersion)}`)
          .join('<br/>')
      : 'None'
  }</td>`
  message += `<td valign="top">${
    listPA.length > 0
      ? listPA
          .map(m => `${m.manuscriptId} v${Math.round(m.manuscriptVersion)}`)
          .join('<br/>')
      : 'None'
  }</td>`
  message += `</tr></tbody></table>`
  const subject = `QA receipt: ${list.length} submissions`
  await userMessage(externalQA, subject, message, null, null, system)
  return true
}
