const config = require('config')
const logger = require('@pubsweet/logger')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const Manuscript = require('../xpub-model/entities/manuscript')
const getUser = require('../utils/user.js')
const { createManifest } = require('../ebi-integration/toEbi')
const {
  incompleteEmail,
  stalledEmail,
  stalledErrorEmail,
  sendMail,
} = require('../email')

const { system } = config['epmc-email']

if (!process.env.ENABLE_CRONJOB_STALLEDCHECK) {
  logger.info(
    'ENABLE_CRONJOB_STALLEDCHECK not defined. stalledCheck cronjob exits.',
  )
  process.exit(0)
}

;(async () => {
  await checkJobStatus('check-stalled', async () => {
    const updateTime = Date.now()
    logger.info(`Running stalled manuscripts check`)
    await stalledCheck()
    logger.info(
      `Stalled manuscripts check was finished in ${Date.now() - updateTime} ms`,
    )
  })
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('check-stalled', `Uncaught Exception thrown: ${err}`)
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('check-stalled', `Unhandled Rejection: ${reason}`)
    process.exit(1)
  })

async function stalledCheck() {
  await alertIncomplete()
  await alertStalledPreprints()
  await alertStalledManuscripts()
  await alertNeedFix()
  return true
}

async function alertIncomplete() {
  const incomplete = await Manuscript.findIncomplete()

  if (incomplete && incomplete.length > 0) {
    await incomplete.reduce(async (promise, m) => {
      await promise
      try {
        const submitter =
          m.teams.find(t => t.roleName === 'submitter') &&
          m.teams.find(t => t.roleName === 'submitter').users[0]
        if (submitter) {
          const manInfo = {
            id: m.id,
            title: m['meta,title'],
            status: m.status,
          }
          logger.info(`Sending email for incomplete manuscript ${m.id}`)
          await incompleteEmail(manInfo, submitter)
          return Promise.resolve()
        }
        throw new Error('No submitter for manuscript')
      } catch (e) {
        logger.error(
          `Unable to send email for incomplete manuscript ${m.id}: ${e}`,
        )
        return Promise.resolve()
      }
    }, Promise.resolve())
    return true
  }
  logger.info('No incomplete manuscripts.')
  return true
}

async function alertStalledPreprints() {
  const stalledPreprints = (await Manuscript.findStalled()).filter(
    m => m.organization.name === 'Europe PMC Preprints',
  )
  const preprintEmailList = []

  if (stalledPreprints && stalledPreprints.length > 0) {
    await stalledPreprints.reduce(async (promise, m) => {
      await promise
      try {
        const reviewer = {}
        const openAccess = m.notes.some(n => n.notesType === 'openAccess')
        if (openAccess && m.status === 'xml-review') {
          logger.info(`Stalled ${m.id} is open access preprint`)
          const weekAgo = new Date()
          weekAgo.setDate(weekAgo.getDate() - 8)
          const updated = new Date(m.updated)
          if (weekAgo > updated) {
            // Release preprints updated more than 1 weeks ago
            logger.info(`Releasing preprint ${m.id} to repository`)
            const adminUser = await getUser.getAdminUser()
            const updated = await Manuscript.update(
              {
                id: m.id,
                status: 'repo-ready',
                ebiState:
                  'Releasing CC-licensed preprint after 2 week waiting period',
              },
              adminUser.id,
            )
            if (updated.status === 'repo-ready') {
              preprintEmailList.push(`${m.id} v${Math.round(m.version)}`)
              await createManifest(m.id, adminUser.id)
            }
          }
          return Promise.resolve()
        }
        const reviewerInfo =
          m.teams.find(t => t.roleName === 'reviewer') &&
          m.teams.find(t => t.roleName === 'reviewer').users[0]
        if (reviewerInfo) {
          reviewer.title = reviewerInfo.title
          reviewer.givenNames = reviewerInfo.givenNames
          reviewer.surname = reviewerInfo.surname
          reviewer.email = reviewerInfo.identities[0].email
        } else {
          throw new Error('No reviewer for preprint')
        }
        if (reviewer.email) {
          const manInfo = {
            id: m.id,
            title: m['meta,title'],
            status: m.status,
          }
          logger.info(`Sending email for stalled review preprint ${m.id}`)
          return stalledEmail(manInfo, reviewer, true)
        }
        throw new Error('No email for preprint reviewer')
      } catch (e) {
        logger.error(
          `Unable to send email for stalled review preprint ${m.id}: ${e}`,
        )
        return Promise.resolve()
      }
    }, Promise.resolve())
    if (preprintEmailList.length > 0) {
      const subject = 'Automatically released preprints'
      const html = `The following ${
        preprintEmailList.length
      } preprints were automatically released:<br/><br/>${preprintEmailList.join(
        '<br/>',
      )}`
      return sendMail(system, subject, html)
    }
    return true
  }
  logger.info('No stalled review preprints.')
  return true
}

async function alertStalledManuscripts() {
  const stalledManuscripts = (await Manuscript.findStalled()).filter(
    m => m.organization.name === 'Europe PMC Plus',
  )
  const emailList = []

  if (stalledManuscripts && stalledManuscripts.length > 0) {
    await stalledManuscripts.reduce(async (promise, m) => {
      await promise
      try {
        const reviewer = {}
        const reviewerNote = m.notes.find(
          n => n.notesType === 'selectedReviewer',
        )
        if (m.status === 'xml-review') {
          const weekAgo = new Date()
          weekAgo.setDate(weekAgo.getDate() - 8)
          const updated = new Date(m.updated)
          if (weekAgo > updated) {
            logger.info(`Releasing author manuscript ${m.id} to repository`)
            const adminUser = await getUser.getAdminUser()
            const updated = await Manuscript.update(
              {
                id: m.id,
                status: 'repo-ready',
                ebiState:
                  // TODO
                  'Releasing author manuscript after 2 week waiting period',
              },
              adminUser.id,
            )
            if (updated.status === 'repo-ready') {
              emailList.push(`${m.id}`)
              await createManifest(m.id, adminUser.id)
            }
          }
          return Promise.resolve()
        }
        if (reviewerNote) {
          const reviewerInfo = JSON.parse(reviewerNote.content)
          reviewer.token = reviewerNote.id
          reviewer.title = reviewerInfo.name.title
          reviewer.givenNames = reviewerInfo.name.givenNames
          reviewer.surname = reviewerInfo.name.surname
          reviewer.email = reviewerInfo.email
        } else {
          const reviewerInfo =
            m.teams.find(t => t.roleName === 'reviewer') &&
            m.teams.find(t => t.roleName === 'reviewer').users[0]
          if (reviewerInfo) {
            reviewer.title = reviewerInfo.title
            reviewer.givenNames = reviewerInfo.givenNames
            reviewer.surname = reviewerInfo.surname
            reviewer.email = reviewerInfo.identities[0].email
          } else {
            throw new Error('No reviewer for manuscript')
          }
        }
        if (reviewer.email) {
          const manInfo = {
            id: m.id,
            title: m['meta,title'],
            status: m.status,
          }
          logger.info(`Sending email for stalled review manuscript ${m.id}`)
          return stalledEmail(manInfo, reviewer, false)
        }
        throw new Error('No email for manuscript reviewer')
      } catch (e) {
        logger.error(
          `Unable to send email for stalled review manuscript ${m.id}: ${e}`,
        )
        return Promise.resolve()
      }
    }, Promise.resolve())
    if (emailList.length > 0) {
      const subject = 'Automatically released author manuscripts'
      const html = `The following ${
        emailList.length
      } author accepted manuscripts were automatically released:<br/><br/>${emailList.join(
        '<br/>',
      )}`
      return sendMail(system, subject, html)
    }
    return true
  }
  logger.info('No stalled review manuscripts.')
  return true
}
async function alertNeedFix() {
  const needFix = await Manuscript.findNeedFix()

  if (needFix && needFix.length > 0) {
    await needFix.reduce(async (promise, m) => {
      await promise
      try {
        const submitter =
          m.teams.find(t => t.roleName === 'submitter') &&
          m.teams.find(t => t.roleName === 'submitter').users[0]
        const reviewer =
          m.teams.find(t => t.roleName === 'reviewer') &&
          m.teams.find(t => t.roleName === 'reviewer').users[0]
        const users = submitter ? [submitter] : []
        if (
          !m.teams.some(t => t.userId === m.updatedBy) &&
          reviewer &&
          reviewer.id !== submitter.id
        ) {
          users.push(reviewer)
        }
        if (users.length > 0) {
          const manInfo = {
            id: m.id,
            title: m['meta,title'],
          }
          logger.info(`Sending email for stalled error manuscript ${m.id}`)
          return Promise.all(
            users.map(async u => stalledErrorEmail(manInfo, u)),
          )
        }
        throw new Error('No users to email')
      } catch (e) {
        logger.error(
          `Unable to send email for stalled error manuscript ${m.id}: ${e}`,
        )
        return Promise.resolve()
      }
    }, Promise.resolve())
    return true
  }
  logger.info('No stalled error manuscripts.')
  return true
}
