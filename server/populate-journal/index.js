const logger = require('@pubsweet/logger')
const parseString = require('xml2js').parseString // eslint-disable-line prefer-destructuring
const PromiseFtp = require('promise-ftp')
const Journal = require('../xpub-model/entities/journal/data-access')
const fs = require('fs')
const tar = require('../utils/unTar.js')
const httpUtil = require('../utils/http.js')
const { checkJobStatus, uncaughtError } = require('../job-runner')

const invokedByYarn = () => !!process.env.INVOKED_BY_YARN

if (!invokedByYarn()) {
  if (!process.env.ENABLE_CRONJOB_POPULATEJOURNAL) {
    logger.info(
      'ENABLE_CRONJOB_POPULATEJOURNAL not defined. populate-journal cronjob exits.',
    )
    process.exit(0)
  }
}

const ftp_uri = 'ftp.ncbi.nlm.nih.gov'
const xml_url =
  'https://www.ncbi.nlm.nih.gov/pmc/utils/nihms/pmc-mss-jrinfo.cgi'

;(async () => {
  if (invokedByYarn()) {
    const natureJournal = await Journal.findByField('meta,pmjrid', '6011')
    if (natureJournal.length > 0) {
      logger.info('Will not run journals seed.')
      teardownDBConnectionPool()
      process.exit(0)
    } else {
      logger.info('Running journals seed.')
      await populateJournalTB()
    }
  } else {
    await checkJobStatus('populate-journal', async () => {
      logger.info('Starting journal catalog update')
      await populateJournalTB()
    })
  }
  teardownDBConnectionPool()
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('populate-journal', `Uncaught Exception thrown: ${err}`)
    teardownDBConnectionPool()
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('populate-journal', `Unhandled Rejection: ${reason}`)
    teardownDBConnectionPool()
    process.exit(1)
  })

async function teardownDBConnectionPool() {
  logger.info('populate-journal: closing connection to the database...')
  Journal.knex && (await Journal.knex().destroy())
}

function timeDiff(beforeUpdate) {
  return Math.floor((Date.now() - beforeUpdate) / 60000)
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

async function populateJournalTB() {
  const beforeUpdate = Date.now()
  try {
    const [ftpXMLResult, pmcXMLResult] = await Promise.all([
      processFTP(),
      getXMLData(),
    ])
    const merged = ftpXMLResult.map(f => {
      const pmcStatus = pmcXMLResult.find(
        p => p['meta,pmjrid'] === f['meta,pmjrid'],
      )
      f['meta,pmc_status'] = pmcStatus && pmcStatus['meta,pmc_status']
      return f
    })
    await Journal.upsertMulti(merged)
    await sleep(3000)
    logger.info(
      `Journal table was updated in: ${timeDiff(beforeUpdate)} minutes`,
    )
    logger.info('Journals update is done.')
  } catch (e) {
    logger.error(e)
    throw e
  }
}

function processFTP() {
  return new Promise((resolve, reject) => {
    const ftp = new PromiseFtp()
    ftp
      .connect({ host: ftp_uri, user: '', password: '' })
      .then(() => ftp.get('pubmed/jourcache.xml'))
      .then(
        stream =>
          new Promise((resolve, reject) => {
            const tmpPath = tar.createTempDirSync()
            const tmpFilePath = `${tmpPath}/jourcache.xml`
            stream.once('close', () => resolve(tmpFilePath))
            stream.once('error', reject)
            stream.pipe(fs.createWriteStream(tmpFilePath))
          }),
      )
      .then(tmpFilePath => {
        ftp.end() // called when the connection has already closed
        logger.info('Fetching jourcache.xml journal data')
        const fetchFTPRes = parseFtpFile(tmpFilePath)
        const fetchFilter = fetchFTPRes.filter(
          a => a['meta,nlmuniqueid'] && a['meta,pmjrid'],
        )
        resolve(fetchFilter)
      })
      .catch(error => {
        logger.error(error)
        reject(error)
      })
  })
}

function parseFtpFile(file) {
  const data = fs.readFileSync(file)
  let mapping = []
  parseString(data.toString(), (err, result) => {
    const arr = result && result.JournalCache ? result.JournalCache.Journal : []

    if (arr !== undefined || arr.length > 0) {
      mapping = arr.map(x => {
        const nlm_unique_id_var = x.NlmUniqueID[0]

        const rObj = {}
        rObj['meta,pmjrid'] = x.$.jrid || null
        rObj['meta,nlmuniqueid'] = nlm_unique_id_var
        rObj['meta,firstYear'] = x.StartYear ? x.StartYear[0] : null
        rObj['meta,endYear'] = x.EndYear ? x.EndYear[0] : null
        rObj['meta,pubmed_status'] = x.ActivityFlag[0] === '1'
        rObj.journalTitle = x.Name ? x.Name[0]._ : ''

        rObj['meta,issn'] =
          x.Issn !== undefined
            ? x.Issn.map(t => {
                const issnsObj = {}
                issnsObj.id = t._
                issnsObj.type = t.$.type
                return issnsObj
              })
            : []

        if (x.MedAbbr) {
          rObj['meta,nlmta'] = x.MedAbbr[0]._
        } else if (x.IsoAbbr) {
          rObj['meta,nlmta'] = x.IsoAbbr.toString()
        } else {
          rObj['meta,nlmta'] = null
        }

        rObj.other_titles = x.Alias !== undefined ? x.Alias : []

        return rObj
      })
    }
    if (err) {
      throw new Error(err)
    }
  })
  return mapping
}

function getPMCparticpants(file) {
  const data = fs.readFileSync(file)
  let mapping = []
  parseString(data.toString(), (err, result) => {
    const arr = result && result.jinfo ? result.jinfo.journal : []
    mapping = arr.reduce((map, x) => {
      if (x.pmjrid) {
        const rObj = {}
        rObj['meta,pmjrid'] = x.pmjrid ? x.pmjrid[0] : null
        const emsAuth = x.authority.find(
          auth => auth.$.name === 'ukpmcpa' && auth.$.participation === 'pmc',
        )
        // rObj['meta,pmc_status'] = !!emsAuth && !emsAuth['end-date']
        rObj['meta,pmc_status'] =
          emsAuth &&
          JSON.stringify({
            start_date: emsAuth['start-date'] && emsAuth['start-date'][0],
            end_date: emsAuth['end-date'] && emsAuth['end-date'][0],
          })
        map.push(rObj)
      }
      return map
    }, [])
    if (err) {
      throw new Error(err)
    }
  })
  return mapping
}

function getXMLData() {
  return new Promise((resolve, reject) => {
    const tmpPath = tar.createTempDirSync()
    const tmpFilePath = `${tmpPath}/pmcjourcache.xml`
    const writeStreamToFile = fs.createWriteStream(tmpFilePath)
    httpUtil
      .get(xml_url)
      .pipe(writeStreamToFile)
      .on('finish', () => {
        logger.info('Fetching pmc-mss-jrinfo journal data')
        const pmcXMLResult = getPMCparticpants(tmpFilePath)
        resolve(pmcXMLResult)
      })
      .on('error', e => {
        logger.error(e)
        reject(e)
      })
  })
}
