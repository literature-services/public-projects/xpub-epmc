const logger = require('@pubsweet/logger')
const rfr = require('rfr')
const lodash = require('lodash')
const uploadFiles = require('./uploadFiles')
const { checkStyle } = require('./validateFiles')

const FileManager = rfr('server/xpub-model/entities/file')
const { pushXML } = rfr('server/xml-validation')

const resolvers = {
  Mutation: {
    checkStyle,
    uploadFiles,
    async updateFile(_, props, { user }) {
      if (!props.id) {
        return false
      }
      const existing = await FileManager.find(props.id)
      if (lodash.isMatch(existing, props)) {
        return false
      }

      const updatedFile = await FileManager.save(props, user)
      logger.debug('updated: ', updatedFile)
      return true
    },
    async copyFile(_, props, { user }) {
      if (!props.id) {
        return false
      }
      const existing = await FileManager.find(props.id)
      lodash.assign(existing, props)
      delete existing.id
      const doubleCheck = await FileManager.findByManIdAndType(
        existing.manuscriptId,
        existing.type,
      )
      if (doubleCheck) {
        existing.id = doubleCheck.id
      }
      const updatedFile = await FileManager.save(existing, user)
      logger.debug('updated: ', updatedFile)
      return true
    },
    async deleteFile(_, { id }, { user }) {
      const deletedFile = await FileManager.deleteById(id, user)
      logger.info('deletedFile: ', deletedFile)
      return true
    },
    async replaceManuscriptFile(_, { fileId, files }, { user }) {
      const trx = null
      try {
        const replace = await FileManager.replace(fileId, user)
        const { id, version, type, label, trx } = replace
        const complete = await uploadFiles(
          _,
          { id, version, files, type, label, trx },
          { user },
        )
        trx.commit()
        return complete
      } catch (err) {
        if (trx) trx.rollback()
        logger.error('Error replacing file', err)
        return false
      }
    },
    async convertXML(_, { id }, { user }) {
      try {
        const xmlFile = await FileManager.find(id)
        const keepStatus = true
        await pushXML(xmlFile.url, xmlFile.manuscriptId, user, keepStatus)
      } catch (error) {
        logger.error(error)
        return false
      }
      return true
    },
  },
}

module.exports = resolvers
