const uuidv1 = require('uuid/v1')
const logger = require('@pubsweet/logger')
const config = require('config')
const rfr = require('rfr')
const gridFsClient = require('../../../gridFs/src/gridFsClient')
const { timer } = require('benmark')

const { FileManager } = rfr('server/xpub-model')
// const { xsweetConvert } = rfr('server/xsweet-conversion')
// const { internalBaseUrl } = config.get('pubsweet-server')

const handleFile = async (
  file,
  manuscriptId,
  manuscriptVersion,
  type,
  user,
  trx,
) => {
  const { stream, filename, mimetype } = await file
  let newFilename = uuidv1()
  let extension = ''

  if (filename) {
    extension = filename.split('.').pop()
    if (extension) {
      newFilename += `.${extension}`
    }
  }

  try {
    await timer(`GridFs uploading file ${filename}`)(
      gridFsClient.storeFileManuscript,
    )(manuscriptId, manuscriptVersion, filename, mimetype, stream, newFilename)

    const metadataFile = await gridFsClient.getFileMetadata(newFilename)
    const size = metadataFile.length

    // case when manuscript is a docx file
    if (
      type === 'manuscript' &&
      mimetype ===
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ) {
      /* try {
        logger.info(`Starting XSweet conversion of ${filename}`)
        const fileUrl = `/download/${newFilename}`
        await xsweetConvert(
          fileUrl,
          manuscriptId,
          manuscriptVersion,
          user,
          internalBaseUrl,
          trx,
        )
        logger.info(
          `XSweet conversion of ${filename} for ${manuscriptId} is done`,
        )
      } catch (err) {
        logger.error(
          `Unable to convert ${manuscriptId} manuscript file ${filename}`,
        )
        logger.error(err)
      } */
      logger.info('XSweet temporarily disabled')
    }

    return { filename, url: newFilename, mimetype, size }
  } catch (err) {
    return { err }
  }
}

async function uploadFiles(
  _,
  { id, version, files, type = '', label = null, trx = null },
  { user },
) {
  const resolvedFiles = await files
  const savedFiles = []
  await resolvedFiles.reduce(async (promise, resolvedFile) => {
    await promise
    const { err, filename, url, mimetype, size } = await handleFile(
      resolvedFile,
      id,
      version,
      type,
      user,
      trx,
    )
    if (err || !filename) {
      logger.error('File database error: ', err)
      return Promise.resolve()
    }
    logger.info(
      `File database filename: ${url} (original filename: ${filename})`,
    )
    // Update database
    const file = {
      manuscriptId: id,
      manuscriptVersion: version,
      filename,
      mimeType: mimetype,
      url: `${config.file.url.download}/${url}`,
      type,
      size,
      label,
      updatedBy: user,
    }
    const savedFile = await FileManager.save(file, user, trx)
    logger.debug('savedFile: ', savedFile)
    savedFiles.push(savedFile)
    return Promise.resolve()
  }, Promise.resolve())

  const manuscript = {
    id,
    files: savedFiles,
  }
  logger.info('Manuscript updated: ', manuscript)
  return savedFiles.length > 0
}

module.exports = uploadFiles
