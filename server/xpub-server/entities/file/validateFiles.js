const rfr = require('rfr')

const { styleValidate } = rfr('server/xml-validation')

function getXMLString(stream) {
  const chunks = []
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(Buffer.from(chunk)))
    stream.on('error', err => reject(err))
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })
}

async function checkStyle(_, { file }, { user }) {
  const [resolved] = await file
  const { stream, filename } = await resolved
  const xmlString = await getXMLString(stream)
  return styleValidate(xmlString, filename)
}

module.exports = { checkStyle }
