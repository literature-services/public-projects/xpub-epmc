const rfr = require('rfr')
const logger = require('@pubsweet/logger')

const { NoteManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async getNote(_, { id }, { user }) {
      return NoteManager.find(id)
    },
    async getPreprintSelections(_, { manuscriptId }, { user }) {
      const versionApproval = await NoteManager.getLatestValue(
        manuscriptId,
        'versionApproval',
      )
      const addLicense = await NoteManager.getLatestValue(
        manuscriptId,
        'addLicense',
      )
      return { versionApproval, addLicense }
    },
  },

  Mutation: {
    async createNote(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }
      const saved = await NoteManager.create(data, user)
      logger.debug('saved: ', saved)
      return !!saved
    },

    // TODO restrict this in production
    async deleteNote(_, { id }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      await NoteManager.delete(id, user)
      return true
    },

    async updateNote(_, { data }, { user }) {
      if (!user) {
        throw new Error('Not logged in')
      }

      const updated = await NoteManager.update(data, user)
      return !!updated
    },
  },
}

module.exports = resolvers
