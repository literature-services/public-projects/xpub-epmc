const rfr = require('rfr')

const { RoleManager } = rfr('server/xpub-model')

const resolvers = {
  Query: {
    async rolesByType(_, { organization }, user) {
      if (!user) {
        throw new Error('You are not authenticated!')
      }
      const roleList = await RoleManager.findByType(organization)
      return roleList.map(role => role.name)
    },
  },
}

module.exports = resolvers
