const subjCheck = set => {
  let subjCheck = null
  if (set === 'COVID_preprint')
    subjCheck = `'COVID-19' = ANY(m."meta,subjects")`
  if (set === 'funder_preprint')
    subjCheck = `(m."meta,subjects" = '{}' or m."meta,subjects" is null)`
  return subjCheck
}

module.exports = { subjCheck }
