const superagent = require('superagent')
const config = require('config')
const GraphQLJSON = require('graphql-type-json')
require('superagent-proxy')(superagent)

const resolvers = {
  JSON: GraphQLJSON,

  Query: {
    async epmc_grist(_, { query, page }, ctx) {
      if (!ctx.user) {
        throw new Error('You are not authenticated!')
      }

      const url = `https://www.ebi.ac.uk/europepmc/GristAPI/rest/get/secure/query=${query}&page=${page}&showExtraInformation=true&format=json`

      let response
      try {
        let superagentRequest = superagent.get(url)
        if (process.env.superagent_http_proxy) {
          superagentRequest = superagentRequest.proxy(
            process.env.superagent_http_proxy,
          )
        }

        response = await superagentRequest.auth(
          config.get('grist-api').username,
          config.get('grist-api').password,
        )
      } catch (err) {
        return { error: { status: err.status } }
      }

      return response.body
    },
  },
}

module.exports = resolvers
