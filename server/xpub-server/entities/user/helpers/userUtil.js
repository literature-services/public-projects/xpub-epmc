const userUtil = {
  mapSqlToGraphql: (dbUser, privacyNoticeVersion) => {
    const name = {
      title: dbUser.title,
      givenNames: dbUser.givenNames,
      surname: dbUser.surname,
    }
    const graphQlUser = dbUser
    graphQlUser.identities.forEach(identity => {
      identity.name = name
    })
    if (graphQlUser.teams) {
      graphQlUser.teams = graphQlUser.teams
        .filter(team => !team.manuscriptId)
        .map(team => team.roleName)
    }
    if (privacyNoticeVersion) {
      graphQlUser.privacyNoticeReviewRequired =
        dbUser.privacyNoticeVersion !== privacyNoticeVersion
    }
    return graphQlUser
  },
}

module.exports = userUtil
