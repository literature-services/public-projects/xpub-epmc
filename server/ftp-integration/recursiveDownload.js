const Client = require('ftp')
const { ftpLogger: logger } = require('../utils/loggers')
const config = require('config')
const fs = require('fs')
const ftpUser = require('../utils/ftpUser')
const { errorDevEmail } = require('../email')
const { execSync } = require('child_process')
const FtpAccount = require('../xpub-model/entities/ftpAccount/data-access')
const async = require('async')

const pathModule = require('path')

/* eslint-disable */

/*
if (!process.env.ENABLE_CRONJOB_FROMFTPBULKUPLOAD) {
  logger.info(
    'ENABLE_CRONJOB_FROMFTP-BULKUPLOAD not defined. fromFtp-BulkUpload cronjob exits.',
  )
  process.exit(0)
}
*/

const BULK_UPLOAD_FILE_EXT = new RegExp(/\S+\.tar\.gz$/i)

;(async () => {
  const ftpBulkUploaders = await FtpAccount.selectBulkUploaders()

  // console.log(ftpBulkUploaders)

  /*ftpBulkUploaders.forEach(async user => {
    queue.push(user)
  })*/

  const bulkUploader = {
    name: 'beta_plus_upload',
    description: 'test beta_plus_upload',
    username: 'beta_plus_upload',
    password: 'GSFj5KC8',
  }

  queue.push(bulkUploader)
})()

const queue = async.queue(async user => {
  logger.info(
    `Bulk Upload: Initialising job for file download from FTP folder: ${user.username}`,
  )
  const ftp = new Client()

  const rootFolderLocal = `${process.env.HOME}/${config.get('ftp_directory')}`

  fs.access(rootFolderLocal, error => {
    if (error) {
      logger.error(`${rootFolderLocal} directory does not exist. ${error}`)
      process.exit(0)
    }
  })

  // create local directories
  const rootPathLocal = `${rootFolderLocal}/${user.username}`
  const cmd = `mkdir -p ${rootPathLocal}`
  execSync(cmd)

  const beforeUpdate = Date.now()

  try {
    const val = await checkNewFtpPackage(ftp, user, rootPathLocal)
    logger.info(
      `Bulk Upload: ${val} file(s) downloaded to local directory ${rootPathLocal} in ${Date.now() -
        beforeUpdate} ms`,
    )
  } catch (err) {
    errorDevEmail('Bulk Upload Cron Job: ', err)
    logger.error(err)
  }
  close(ftp)
  return true
})

queue.drain(() => {
  logger.info('Cron job completed.')
  process.exit(0)
})

queue.error((err, task) => {
  logger.error(`Error in task ${task}: ${err}`)
})

function close(ftp) {
  ftp.end()
  ftp = null
  logger.info('Ftp connection terminated')
}

process
  .on('uncaughtException', (err, origin) => {
    logger.error('Uncaught Exception thrown: ', err)
    logger.error('Exception thrown at: ', origin)
    process.exit(1)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    process.exit(1)
  })

async function checkNewFtpPackage(ftp, ftpBulkUploader, rootPathLocal) {
  const rootPathFTP = '/'
  const { host } = config.get('bulk-upload-ftp')
  const { username: user, password } = await ftpUser.getFTPAccount(
    ftpBulkUploader.username,
  )

  return new Promise((resolve, reject) => {
    ftp.on('ready', async () => {
      try {
        const targzFiles = await readDirRecursive(ftp, '')
        if (!targzFiles.length) {
          resolve(0) // No files to download to local FTP space
        }
        await downloadFtpFiles(targzFiles, ftp, rootPathFTP, rootPathLocal)
        resolve(targzFiles.length)
      } catch (err) {
        logger.error('Rejection with Bulk FTP file download', err)
        reject(err)
      }
    })
    ftp.on('error', () =>
      reject(new Error('Unable to connect to Bulk Upload FTP')),
    )
    const keepalive = 10000
    const connTimeout = 60000
    const pasvTimeout = 60000
    ftp.connect({ host, user, password, keepalive, connTimeout, pasvTimeout })
  })
}

function downloadFtpFiles(targzFiles, ftp, rootPathFTP, rootPathLocal) {
  let counter = 0
  return Promise.all(
    targzFiles.map(
      (file, index, array) =>
        new Promise((resolve, reject) => {
          const remoteFilePath = file //`${rootPathFTP}/${file.name}`
          ftp.get(remoteFilePath, (err, stream) => {
            if (err) {
              logger.error(remoteFilePath)
              logger.error(err.message)
              counter += 1
              if (counter === array.length) {
                reject(err.message)
              }
            } else if (!stream) {
              logger.error(remoteFilePath)
              logger.error(`No FTP get stream`)
              counter += 1
              if (counter === array.length) {
                reject(err.message)
              }
            } else {
              const pathName = pathModule.dirname(file)
              const fileName = pathModule.basename(file)
              const filePathLocal = `${rootPathLocal}${pathName}`

              console.log(file, '---', pathName, '---', filePathLocal)

              const cmd = `mkdir -p ${filePathLocal}`
              execSync(cmd)

              const downloadFilePath = `${filePathLocal}/${fileName}`
              const writeStream = stream.pipe(
                fs.createWriteStream(downloadFilePath),
              )
              writeStream.on('finish', () => {
                logger.info(
                  `Bulk Upload: FTP file has been successfully downloaded to local directory ${downloadFilePath}`,
                )
                resolve()
              })
            }
          })
        }),
    ),
  )
}

function readDirRecursive(ftp, startDir) {
  const readDirQueue = []
  const fileList = []

  //readDir() is called and passed starting directory to read from.
  const readDir = dir => {
    const getDirList = readDir => {
      return new Promise((resolve, reject) => {
        ftp.list(readDir, false, async (err, itemList) => {
          if (err) {
            close(ftp)
            reject(err)
          }

          // filter out directories which are 'dated folders'
          const datedRegex = new RegExp(/\d{4}-\d{2}-\d{2}$/i)
          const filteredList = itemList
            .filter(item => !(datedRegex.test(item.name) && item.type === 'd'))
            .map(item => {
              return { objPath: `${readDir}/${item.name}`, objType: item.type }
            })
          resolve(filteredList)
        })
      })
    }

    // Files are added to accumulating fileList array.
    // Directories are added to readDirQueue.
    // If directories exist on readDirQueue, next directory is shifted off to another readDir() call and returned to parent Promise.
    const processDirList = dirList => {
      for (const { objPath, objType } of dirList) {
        // if directory add to queue
        if (objType === 'd') {
          readDirQueue.push(objPath)
          // continue
        } else if (
          BULK_UPLOAD_FILE_EXT.test(objPath.split('/').pop()) &&
          objType === '-'
        ) {
          // push tar.gz files
          fileList.push(objPath)
        }
      }

      // if queue, process next item recursively
      if (readDirQueue.length > 0) {
        return readDir(readDirQueue.shift())
      }

      // finished - return file list
      return fileList
    }
    // read FS objects list from directory
    return getDirList(dir).then(processDirList)
  }

  // start reading at the top
  return readDir(startDir)
}
