const { bulkuploadMonitorLogger: logger } = require('../utils/loggers')

require('dotenv').config()
const htmlDecode = require('decode-html')
const uuidv4 = require('uuid/v4')
const lodash = require('lodash')
const mime = require('mime-types')
const fs = require('fs')
const readline = require('readline')
const chokidar = require('chokidar')
const libxml = require('libxmljs')
const path = require('path')
const { exec } = require('child_process')
const config = require('config')
const moment = require('moment')
const async = require('async')
const { transaction } = require('objection')
const {
  ManuscriptManager,
  NoteManager,
  FileManager,
  TeamManager,
  OrganizationManager,
} = require('../xpub-model/')
const Journal = require('../xpub-model/entities/journal/data-access')
const Identity = require('../xpub-model/entities/identity/data-access')
const fileUtils = require('../utils/files')
const ftpUtils = require('../utils/ebi-ftp.js')
const { createTempDir } = require('../utils/unTar')
// const { xsweetConvert } = require('../xsweet-conversion')
const { bulkUploaderEmail, errorDevEmail } = require('../email')

global.token = ''

// Initialize watcher.
const rootPath = `${process.env.HOME}/${config.get('ftp_directory')}/`
const ignoreTaggerFolder = `${rootPath}${config.get('ftp_tagger').username}/*`
// const { internalServiceUrl } = config.get('pubsweet-server')
const { states } = config

const watcher = chokidar.watch(`${rootPath}**/*.tar.gz`, {
  persistent: true,
  usePolling: true,
  ignored: ignoreTaggerFolder,
  awaitWriteFinish: {
    // eslint-disable-next-line prettier/prettier
    stabilityThreshold: config.get(
      'ftp_bulkupload.chokidar.stabilityThreshold',
    ),
    pollInterval: config.get('ftp_bulkupload.chokidar.pollInterval'),
  },
})

/**
 * 1. Move file filePath to a dated folder
 * 2. Delete folder tmpPath, if tmpPath is provided
 *
 * @param {*} filePath
 * @param {*} tmpPath
 * @param {*} manId manuscript ID
 * @param {*} isError Whether the current process has errors
 * @returns
 */
const tidyUp = async (filePath, tmpPath, manId, isError) => {
  try {
    const emsid = manId || 'UNKNOWN'
    let parentDir = filePath

    // Issue 162
    // go up directories until you find the ftp root path. This is where the dated folder should be created.
    do {
      parentDir = path.dirname(parentDir)
    } while (`${path.dirname(parentDir)}/` !== rootPath)

    const fileName = path.basename(filePath)
    const fileNameExt = isError ? 'ERROR.' : `loaded.mid-${emsid}.`
    const currentDate = moment(new Date()).format('YYYY-MM-DD')
    const dateExt = moment(new Date()).format('YYYY-MM-DD_HH:mm:ss')
    const datedFolder = `${parentDir}/${currentDate}`
    const datedFilePath = `${datedFolder}/${fileName}.${fileNameExt}${dateExt}`

    let cmd = `mkdir -p ${datedFolder}; mv ${filePath} ${datedFilePath}`
    if (tmpPath) cmd = cmd.concat(`; rm -rf ${tmpPath}`)

    return new Promise((resolve, reject) => {
      exec(cmd, { timeout: 30 * 1000 }, (err, stdout, stderr) => {
        if (err) {
          logger.error(`Nodejs could'd execute the command: ${cmd} `)
          // reject(err)
        }
        if (isError) {
          logger.info(`Error in Bulk import process for ${filePath}`)
        } else {
          logger.info(
            `Finished Bulk import process for ${filePath} as manuscript ${manId}.`,
          )
        }
        resolve(datedFilePath)
      })
    })
  } catch (e) {
    logger.error(
      `An error has happened while tidying bulkupload file ${filePath}${
        tmpPath ? `, temp directory: ${tmpPath}` : ''
      }`,
    )
  }
  return Journal.knex().destroy()
}

const queryIdentity = async path => {
  const rootParts = rootPath.split('/')
  const fileNameParts = path.split('/')
  const username = fileNameParts[rootParts.length - 1]
  const trx = await transaction.start(Identity.knex())
  try {
    const user = await Identity.findByFtpUsername(username, trx)
    trx.commit()
    return user
  } catch (err) {
    if (trx) trx.rollback()
    logger.error(`Cannot access database; sending ${path} back to queue`)
    await new Promise(resolve => setTimeout(resolve, 5000))
    queue.push(path)
    return true
  }
}

const queue = async.queue(async taskPath => {
  const profiler = logger.startTimer()
  const submitter = await queryIdentity(taskPath)
  if (submitter) {
    logger.info(`Initialising bulkupload processing job for file: ${taskPath}`)
    await runProcess(taskPath, submitter)
    logger.info(`Job complete for file: ${taskPath}`)
    profiler.done({ message: `bulk upload processing of file ${taskPath}` })
    return true
  }
  profiler.done({
    message: `bulk upload processing of file ${taskPath} with error`,
  })
  logger.error(`Submitter with details ${submitter} not found`)
})

queue.drain(() => {
  logger.info('All files processed.')
})

queue.error((err, task) => {
  logger.error(`Error in task ${task}: ${err.message}`)
})

process
  .on('uncaughtException', (err, origin) => {
    logger.error('Uncaught Exception thrown from bulk upload monitor: ', err)
    logger.error('Exception thrown at: ', origin)
    tidyUp(err.filePath, err.tmpPath, err.manId, true)
    process.exit(1)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    tidyUp(reason.filePath, reason.tmpPath, reason.manId, true)
  })

watcher
  .on('add', path => {
    logger.info(`New file ${path} has been detected`)
    queue.push(path)
  })
  .on('change', path => logger.debug(`File ${path} has been changed`))
  .on('unlink', path => logger.debug(`File ${path} has been removed`))

// More possible events.
watcher
  .on('addDir', path => logger.debug(`Directory ${path} has been added`))
  .on('unlinkDir', path => logger.debug(`Directory ${path} has been removed`))
  .on('error', error => {
    logger.error(`Watcher error: ${error}`)
    errorDevEmail('the FTP utility', `Watcher error: ${error}`)
  })
  .on('ready', () =>
    logger.info(
      `Initial scan complete. Ready for changes under folder ${rootPath}`,
    ),
  )
  .on('raw', (event, path, details) => {
    logger.debug('Raw event info:', event, path, details)
  })

// 'add', 'addDir' and 'change' events also receive stat() results as second
// argument when available: http://nodejs.org/api/fs.html#fs_class_fs_stats
watcher.on('change', (path, stats) => {
  if (stats) logger.debug(`File ${path} changed size to ${stats.size}`)
})

const runProcess = async (packagePath, submitter) => {
  const RootDirReg = new RegExp(`^${rootPath}${submitter.meta.ftpUserName}`)
  let packageName
  let tmpPath
  let manuscriptId
  let suffixedFilePath

  // verify if tar.gz id corrupted
  try {
    await validateCompressedFile(packagePath, submitter)
  } catch (error) {
    // tar.gz id corrupted
    if (submitter) {
      bulkUploaderEmail(submitter.email, error, packageName)
    }
    errorDevEmail(
      'the FTP utility',
      `Error with ftp-integration - bulk upload<br/>Package: ${packageName}<br/>${error}`,
    )
    logger.error('Error with ftp-integration - buldUpload.js', error.stack)
    return true
  }
  try {
    logger.info(`Starting Bulk Import Process for ${packagePath}`)
    packageName = path.basename(packagePath)
    tmpPath = await createTempDir()
    const tmpPathExt = await extractFiles(packagePath, tmpPath, submitter)
    // rename file - append .start suffix
    suffixedFilePath = await fileUtils.renameFileSuffix(packagePath)
    const parsedInfo = await parseManifest(tmpPathExt, packageName)

    const xmlObj = await parseXml(parsedInfo.metaXml, packageName)
    manuscriptId = await createManuscript(
      xmlObj,
      parsedInfo,
      tmpPath,
      submitter,
      packageName,
      suffixedFilePath,
    )
    const updatedFilepath = await tidyUp(
      suffixedFilePath,
      tmpPath,
      manuscriptId,
    )

    const ftpDirPath = path.dirname(suffixedFilePath.replace(RootDirReg, ''))
    await updateFTP(updatedFilepath, ftpDirPath, submitter)
  } catch (error) {
    if (submitter) {
      bulkUploaderEmail(submitter.email, error, packageName)
    }
    errorDevEmail(
      'the FTP utility',
      `Error with ftp-integration - bulk upload<br/>Package: ${packageName}<br/>${error}`,
    )
    logger.error('Error with ftp-integration - bulkUpload.js', error.stack)
    const updatedFilepath = await tidyUp(
      suffixedFilePath,
      tmpPath,
      manuscriptId,
      true,
    )
    const ftpDirPath = path.dirname(suffixedFilePath.replace(RootDirReg, ''))
    await updateFTP(updatedFilepath, ftpDirPath, submitter)
    return true
  }
}

const validateCompressedFile = (filePath, submitter) =>
  new Promise((resolve, reject) => {
    if (filePath.includes(' ')) {
      const errMsg = `file name contains spaces: ${path.basename(filePath)}`
      logger.error()
      reject(new Error(errMsg))
    } else {
      exec(`gunzip -t ${filePath}`, { timeout: 30 * 1000 }, async err => {
        if (err) {
          logger.error(
            `File: ${filePath} is corrupted and cannot be processed.`,
          )
          const updatedFilepath = await tidyUp(filePath, '', '', true)
          logger.info(`File ${filePath} has been moved to ${updatedFilepath}`)

          const RootDirReg = new RegExp(
            `^${rootPath}${submitter.meta.ftpUserName}`,
          )
          const ftpDirPath = path.dirname(filePath.replace(RootDirReg, ''))
          await updateFTP(updatedFilepath, ftpDirPath, submitter)
          reject(err)
        }
        resolve(true)
      })
    }
  })

const extractFiles = (source, dest, submitter) =>
  new Promise((resolve, reject) => {
    const cmd = `tar vzxf ${source} -C ${dest} --xform='s#^.+/##x'` // the xform removes all directories
    exec(cmd, async err => {
      if (err) {
        // node couldn't execute the command
        logger.error(`Error extracting FTP bulk upload file: ${source}`)
        const updatedFilepath = await tidyUp(source, dest, '', true)
        logger.info(`File ${source} has been moved to ${updatedFilepath}`)

        const RootDirReg = new RegExp(
          `^${rootPath}${submitter.meta.ftpUserName}`,
        )
        const ftpDirPath = path.dirname(source.replace(RootDirReg, ''))
        await updateFTP(updatedFilepath, ftpDirPath, submitter)
        reject(
          new Error(`Error extracting FTP bulk upload file: ${source}\n${err}`),
        )
      }
      resolve(dest)
    })
  })

const parseManifest = async (tempFolder, packageName) => {
  const files = []
  let metaXml = ''
  logger.info(`${packageName}: Parsing package manifest.`)
  const lineReader = readline.createInterface({
    input: fs.createReadStream(`${tempFolder}/manifest.txt`, {
      encoding: 'UTF-8',
    }),
  })

  const manifestList = await new Promise((resolve, reject) => {
    const lines = []
    lineReader
      .on('line', line => {
        if (/\S/.test(line)) {
          const row = line.trim().split(/\t{1,}/)
          const filename = row.pop()
          const [type, label] = row
          lines.push({ type, label, filename })
        }
      })
      .on('close', () => {
        resolve(lines)
      })
  })

  if (!manifestList.some(f => f.type === 'manuscript')) {
    throw new Error(`The package does not contain a manuscript file.`)
  }

  await manifestList.reduce(async (promise, file, i, mL) => {
    await promise
    if (file.type === 'manuscript') {
      if (mL.findIndex(f => f.type === 'manuscript') < i) {
        file.type = 'manuscript_secondary'
      }
    }
    const { type, label, filename } = file
    if (!filename || !fs.existsSync(`${tempFolder}/${filename}`)) {
      throw new Error(`Listed file ${filename || ''} is not in the package.`)
    }
    if (type === 'supplement' && label.length > 100) {
      throw new Error(
        `The label provided for file ${filename} exceeds the character limit. Please limit labels to 100 characters.`,
      )
    }
    if (type === 'bulksub_meta_xml') {
      metaXml = `${tempFolder}/${filename}`
    } else {
      files.push({
        fileURI: `${tempFolder}/${filename}`,
        filename,
        type,
        info: label,
      })
    }
    return Promise.resolve()
  }, Promise.resolve())

  return { metaXml, files }
}

const parseXml = (xmlFile, packageName) =>
  new Promise((resolve, reject) => {
    logger.info(`${packageName}: Parsing package metadata XML`)
    try {
      fs.readFile(xmlFile, 'utf8', (err, xml) => {
        if (err) {
          logger.error(`${packageName}: Cannot read XML file`)
          reject(err)
        }
        const xmlDoc = libxml.parseXml(xml)

        const permissions_node =
          xmlDoc.get('//permissions') &&
          xmlDoc
            .get('//permissions')
            .attr('xmlns:xlink', 'http://www.w3.org/1999/xlink')

        const permissions = permissions_node
          ? permissions_node
              .toString()
              .replace(/&lt;/g, '<')
              .replace(/&gt;/g, '>')
          : null

        const xlink = 'http://www.w3.org/1999/xlink'
        const ali = 'http://www.niso.org/schemas/ali/1.0/'
        const license = xmlDoc.get(
          '//descendant-or-self::license[contains(@xlink:href, "creativecommons.org") or contains(ali:license_ref, "creativecommons.org")]/descendant-or-self::*[self::ali:license_ref or attribute::xlink:href]',
          { xlink, ali },
        )
        let licenseLink = null
        let isOpen = false
        if (license) {
          licenseLink = license.get('@xlink:href', { xlink })
            ? license.get('@xlink:href', { xlink }).value()
            : license.text()
          isOpen =
            licenseLink.search(
              /\/(publicdomain|by|by-nc|by-nd|by-sa|by-nc-sa|by-nc-nd)\//i,
            ) >= 0
        }

        const journal_title_val = xmlDoc
          .get('//journal-meta/journal-title')
          .text()
        const nlmta_node = xmlDoc.get(
          '//journal-meta/*[self::journal-id[@journal-id-type="nlm-ta"] or self::nlm-ta]',
        )
        const journal_nlmta = nlmta_node ? nlmta_node.text() : null

        let reviewers = []
        xmlDoc.find('//contacts/person').forEach(node => {
          const email = node.get('@email').value()
          const corrpi =
            node.get('@corrpi = "yes"') || node.get('@person-type="reviewer"')
          if (email && !reviewers.find(reviewer => reviewer.email === email)) {
            reviewers.push({
              givenNames: node.get('@fname').value(),
              surname: node.get('@lname').value(),
              email,
              corrpi,
            })
          }
        })
        if (reviewers.length === 0) reviewers = null

        let versionList = []
        xmlDoc.find('//versionList/pprid').forEach(pprid => {
          versionList.push(pprid.text())
        })
        if (versionList.length === 0) versionList = null

        const title_value = lodash.escape(
          xmlDoc.get('*[self::title or self::manuscript-title]').text(),
        )

        const attnode = xmlDoc.get('/nihms-submit')
          ? xmlDoc.get('/nihms-submit/manuscript')
          : xmlDoc.get('/manuscript-submit')

        const articleIds = []
        const publicationDates = []
        const subjects = []
        xmlDoc.find('//subj-group/subject').forEach(subject => {
          subjects.push(subject.text())
        })
        const publisherPDF = !!attnode.get('@show_publisher_pdf = "yes"')
        const embargo = attnode.get('@*[starts-with(name(), "embargo")]')
          ? attnode.get('@*[starts-with(name(), "embargo")]').value()
          : 0
        const pprid = attnode.get('@pprid')
          ? attnode.get('@pprid').value()
          : null
        const preprint = !!pprid
        const version = attnode.get('@version')
          ? attnode.get('@version').value()
          : 0
        if (pprid) {
          articleIds.push({
            pubIdType: 'pprid',
            id: pprid,
          })
        }
        if (attnode.get('@version-date')) {
          const date = attnode.get('@version-date').value()
          if (date) {
            const jatsDate = date.split('-')
            publicationDates.push({
              type: 'epub',
              date: `${date}T00:00:00+00:00`,
              jatsDate: {
                year: jatsDate[0],
                month: jatsDate[1],
                day: jatsDate[2],
              },
            })
          }
        }
        if (attnode.get('@pmid') && attnode.get('@pmid').value()) {
          articleIds.push({
            pubIdType: '@pmid',
            id: attnode.get('@pmid').value(),
          })
        }
        if (attnode.get('@doi') && attnode.get('@doi').value()) {
          articleIds.push({
            pubIdType: 'doi',
            id: attnode.get('@doi').value(),
          })
        }

        const xmlObj = {
          title: htmlDecode(title_value),
          articleIds,
          journal: journal_title_val,
          journal_nlmta,
          releaseDelay: embargo,
          subjects,
          reviewers,
          permissions,
          publicationDates,
          publisherPDF,
          preprint,
          version,
          pprid,
          versionList,
          licenseLink,
          isOpen,
        }
        resolve(xmlObj)
      })
    } catch (e) {
      logger.error('Error parsing xml', e)
      reject(e)
    }
  })

const createManuscript = async (
  xmlObj,
  parsedInfo,
  tmpPath,
  submitter,
  packageName,
  packagePath,
) => {
  logger.info(`${packageName}: create Manuscript`)
  const {
    title,
    releaseDelay,
    subjects,
    articleIds,
    publicationDates,
    permissions,
    reviewers,
    journal_nlmta,
    journal,
    publisherPDF,
    preprint,
    version,
    pprid,
    versionList,
    licenseLink,
    isOpen,
  } = xmlObj
  const { files } = parsedInfo
  const manuscript = {
    version,
    meta: {
      title,
      releaseDelay,
      articleIds,
      publicationDates,
      subjects,
    },
  }
  const trx = await transaction.start(Journal.knex())
  try {
    // audrey (20190122): If there is not a single NLM TA match you should have the journal as unmatchedJournal
    // and let the helpdesk figure it out
    const journalSearch = journal_nlmta
      ? await Journal.query(trx).where('meta,nlmta', journal_nlmta)
      : undefined
    if (journalSearch === undefined || journalSearch.length !== 1) {
      manuscript.meta.unmatchedJournal = journal
    } else {
      manuscript.journalId = journalSearch[0].id
    }
    const existing = pprid
      ? await ManuscriptManager.findByArticleId(
          pprid,
          submitter.userId,
          true,
          trx,
        )
      : { manuscript: null }
    const organizationId = await OrganizationManager.getOrganizationID(
      preprint,
      trx,
    )
    const created = existing.manuscript
      ? { id: null }
      : await ManuscriptManager.create(
          manuscript,
          submitter.userId,
          organizationId,
          trx,
        )
    const manuscriptId = created.id || existing.manuscript.id

    let reviewerNoteId = null

    if (
      existing.manuscript &&
      existing.manuscript.id &&
      parseInt(version, 10) <= parseInt(existing.manuscript.version, 10)
    ) {
      logger.error(
        `Manuscript ${manuscriptId} version ${version} already exists`,
      )
      throw new Error(
        `Error in package ${packageName}. Manuscript version has already been imported`,
      )
    }

    if (permissions) {
      const fileURI = `${tmpPath}/metadata.xml`
      await fs.writeFileSync(fileURI, permissions)
      files.push({
        fileURI,
        filename: 'metadata.xml',
        type: 'metadata',
      })
      if (isOpen) {
        const existingNotes = existing.manuscript && existing.manuscript.notes
        const oAnote =
          existingNotes && existingNotes.find(n => n.notesType === 'openAccess')
        if (!oAnote) {
          await NoteManager.create(
            {
              notesType: 'openAccess',
              content: licenseLink,
              manuscriptId,
              manuscriptVersion: version,
            },
            submitter.userId,
            trx,
          )
        } else if (oAnote.content !== licenseLink) {
          await NoteManager.update(
            {
              id: oAnote.id,
              content: licenseLink,
              manuscriptVersion: version,
            },
            submitter.userId,
            trx,
          )
        }
      }
    }

    if (created.id && reviewers) {
      logger.info(`Adding reviewer for ${manuscriptId}`)
      let xmlReviewer
      if (reviewers.length === 1) {
        // if just one contact, keep this one
        ;[xmlReviewer] = reviewers
      } else {
        const corrpis = reviewers.filter(reviewer => reviewer.corrpi)
        if (corrpis && corrpis.length > 0) {
          ;[xmlReviewer] = corrpis // keep the first corrpi
        } else {
          // if there are not corrpis
          ;[xmlReviewer] = reviewers // keep the first contact
        }
      }
      const reviewer = {
        email: xmlReviewer.email,
        name: {
          givenNames: xmlReviewer.givenNames,
          surname: xmlReviewer.surname,
        },
      }

      if (!validateEmail(xmlReviewer.email)) {
        const error = new Error(
          `Reviewer's email is not valid: ${xmlReviewer.email} in package ${packageName}`,
        )
        error.submitter = submitter
        error.filePath = packagePath
        error.tmpPath = tmpPath
        throw error
      }

      const identities = await Identity.query(trx).where(
        'email',
        xmlReviewer.email,
      )
      if (identities && identities.length > 0) {
        // identity exists
        reviewer.id = identities[0].userId
      }
      const reviewerNote = await NoteManager.create(
        {
          notesType: 'selectedReviewer',
          content: JSON.stringify(reviewer),
          manuscriptId,
          manuscriptVersion: version,
        },
        submitter.userId,
        trx,
      )
      reviewerNoteId = reviewerNote.meta.notes[0].id
    } else if (created.id) {
      const error = new Error(
        `Error in package ${packageName}. There are no reviewers in the XML.`,
      )
      error.submitter = submitter
      error.filePath = packagePath
      error.tmpPath = tmpPath
      throw error
    }

    if (versionList) {
      logger.info(`Adding version information for ${manuscriptId}`)
      let notDupes = []
      let reviewerUserId = null
      const existingNotes = existing.manuscript && existing.manuscript.notes
      const existingNote =
        existingNotes &&
        existingNotes.find(n => n.notesType === 'notDuplicates')
      const oldVersions =
        existingNotes && existingNotes.find(n => n.notesType === 'versionList')
      if (existingNote) {
        notDupes = JSON.parse(existingNote.content)
        await NoteManager.delete(existingNote.id, submitter.userId, trx)
      }
      if (oldVersions) {
        await NoteManager.delete(oldVersions.id, submitter.userId, trx)
      }
      await versionList.reduce(async (promise, verId) => {
        await promise
        const verSearch = await ManuscriptManager.findByArticleId(
          verId,
          submitter.userId,
          true,
          trx,
        )
        const otherVer = verSearch.manuscript
        if (otherVer) {
          const existingTeam = await TeamManager.selectByManIdAndType(
            otherVer.id,
            'reviewer',
            trx,
          )
          if (existingTeam) reviewerUserId = existingTeam.userId
          if (!notDupes.includes(otherVer.id)) {
            notDupes.push(otherVer.id)
          }
          const { notes } = otherVer
          const dupeNote =
            notes && notes.find(n => n.notesType === 'notDuplicates')
          if (dupeNote) {
            const dupeNoteArray = JSON.parse(dupeNote.content)
            if (!dupeNoteArray.includes(manuscriptId)) {
              dupeNoteArray.push(manuscriptId)
            }
            await NoteManager.update(
              {
                id: dupeNote.id,
                content: JSON.stringify(dupeNoteArray),
                manuscriptVersion: otherVer.version,
              },
              submitter.userId,
              trx,
            )
          } else {
            await NoteManager.create(
              {
                notesType: 'notDuplicates',
                content: JSON.stringify([manuscriptId]),
                manuscriptId: otherVer.id,
                manuscriptVersion: otherVer.version,
              },
              submitter.userId,
              trx,
            )
          }
          const verNote =
            notes && notes.find(n => n.notesType === 'versionList')
          if (verNote) {
            const verNoteObj = JSON.parse(verNote.content)
            if (!verNoteObj.emsid.includes(manuscriptId))
              verNoteObj.emsid.push(manuscriptId)
            if (!verNoteObj.pprid.includes(pprid)) verNoteObj.pprid.push(pprid)
            await NoteManager.update(
              {
                id: verNote.id,
                content: JSON.stringify(verNoteObj),
                manuscriptVersion: otherVer.version,
              },
              submitter.userId,
              trx,
            )
          } else {
            await NoteManager.create(
              {
                notesType: 'versionList',
                content: JSON.stringify({
                  pprid: [pprid],
                  emsid: [manuscriptId],
                }),
                manuscriptId: otherVer.id,
                manuscriptVersion: otherVer.version,
              },
              submitter.userId,
              trx,
            )
          }
          return Promise.resolve()
        }
      }, Promise.resolve())
      await NoteManager.create(
        {
          notesType: 'versionList',
          content: JSON.stringify({
            pprid: versionList,
            emsid: notDupes,
          }),
          manuscriptId,
          manuscriptVersion: version,
        },
        submitter.userId,
        trx,
      )
      await NoteManager.create(
        {
          notesType: 'notDuplicates',
          content: JSON.stringify(notDupes),
          manuscriptId,
          manuscriptVersion: version,
        },
        submitter.userId,
        trx,
      )
      if (reviewerUserId && reviewerNoteId) {
        await TeamManager.addNewReviewer(reviewerNoteId, reviewerUserId, trx)
      }
    }

    logger.info(`Uploading files for ${manuscriptId}`)
    let manuscriptFile = {}
    await files.reduce(async (promise, file) => {
      await promise
      const { fileURI: filePath, filename, type, info: label } = file
      const stats = fs.statSync(filePath)
      const { size } = stats
      let extension = path.extname(filePath)
      if (extension === '.tgz') extension = '.gz'
      const mimeType = mime.contentType(extension)
      const uuid = uuidv4()
      // add file upload delay
      await new Promise(resolve => setTimeout(resolve, 500))
      logger.info(
        `GRIDFS FTP file upload for manuscript ${manuscriptId} version ${version}: ${filename}`,
      )
      await fileUtils.fileUpload(
        manuscriptId,
        version,
        `${uuid}${extension}`,
        filename,
        filePath,
        mimeType,
      )
      const uploadFile = {
        filename,
        label,
        type,
        mimeType,
        size,
        url: `/download/${uuid}${extension}`,
        manuscriptId,
        manuscriptVersion: version,
      }
      if (type === 'manuscript') {
        manuscriptFile = uploadFile
      }
      return FileManager.save(uploadFile, submitter.userId, trx)
    }, Promise.resolve())

    const { /* url, filename, */ mimeType } = manuscriptFile
    // case when manuscript is a docx file
    if (
      mimeType ===
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ) {
      /* try {
        logger.info(`Starting XSweet conversion of ${filename}`)
        await xsweetConvert(
          url,
          manuscriptId,
          version,
          submitter.userId,
          internalServiceUrl,
          trx,
        )
        logger.info(
          `XSweet conversion of ${filename} for ${manuscriptId} is done`,
        )
      } catch (error) {
        logger.error(`Unable to convert file ${filename}`)
      } */
      logger.info('XSweet temporarily disabled')
      // case when set to show publisher PDF
    } else if (mimeType === 'application/pdf' && publisherPDF) {
      delete manuscriptFile.id
      manuscriptFile.type = 'pdf4load'
      await FileManager.save(manuscriptFile, submitter.userId, trx)
    }

    const tagging = states.indexOf('tagging')
    const done = states.indexOf('published')
    const status = existing.manuscript && existing.manuscript.status
    const notProcessing =
      status &&
      (states.indexOf(status) < tagging || states.indexOf(status) >= done)
    if (created.id || notProcessing) {
      const submitMan = {
        id: manuscriptId,
        status: 'submitted',
        version,
      }
      logger.info(`FTP submitting manuscript ${manuscriptId}`)
      // trx is committed in .submit()
      await ManuscriptManager.submit(submitMan, submitter.userId, trx)
      logger.info(`Manuscript ${manuscriptId} successfully submitted`)
    } else {
      await trx.commit()
    }
    return manuscriptId
  } catch (err) {
    await trx.rollback()
    throw err
  }
}

const validateEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

const updateFTP = async (updatedFilepath, ftpDirPath, submitter) => {
  try {
    const [datedFolder, processedFilename] = await fileUtils.getDirAndFilename(
      updatedFilepath,
    )
    logger.info('Updating FTP server...')

    await ftpUtils.renameBulkFtpPackage(
      submitter ? submitter.meta.ftpUserName : null,
      datedFolder,
      processedFilename,
      ftpDirPath,
    )
  } catch (err) {
    logger.error('Error with updating FTP', err)
  }
}
