const { taggerImportMonitorLogger: logger } = require('../utils/loggers')

const chokidar = require('chokidar')
const { execSync } = require('child_process')
const config = require('config')
const async = require('async')
const taggersImportProcess = require('./taggersImportProcess')

const ftpTagger = config.get('ftp_tagger')
const { errorDevEmail } = require('../email')

const parentRootPath = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}`
const rootPath = `${parentRootPath}/Done`
const ignoreNewFolder = `${parentRootPath}/New`
const errorFolder = `${parentRootPath}/Error`

const cmd = `mkdir -p ${rootPath} ${ignoreNewFolder} ${errorFolder}`
execSync(cmd)

// chokidar monitors tagged folder
const watcher = chokidar.watch(`${rootPath}**/*.tar.gz`, {
  persistent: true,
  usePolling: true,
  alwaysStat: true,
  ignored: ignoreNewFolder,
  awaitWriteFinish: {
    stabilityThreshold: 120000,
    pollInterval: 2000,
  },
})

const queue = async.queue(async taskPath => {
  const profiler = logger.startTimer()
  logger.info(`Initialising job for file: ${taskPath}`)
  await taggersImportProcess.runProcess(taskPath)
  profiler.done({ message: `Job complete for file: ${taskPath}` })
  return true
})

queue.drain(() => {
  logger.info('All files processed.')
})

queue.error((err, task) => {
  logger.error(`Error in task ${task}: ${err}`)
})

process
  .on('uncaughtException', (err, origin) => {
    logger.error('Uncaught Exception thrown: ', err)
    logger.error('Exception thrown at: ', origin)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
  })

watcher
  .on('add', path => {
    logger.info(`New file ${path} has been detected`)
    queue.push(path)
  })
  .on('change', path => logger.debug(`File ${path} has been changed`))
  .on('unlink', path => logger.debug(`File ${path} has been removed`))
  .on('error', error => {
    logger.error(`Watcher error: ${error}`)
    errorDevEmail('the FTP from taggers utility', `Watcher error: ${error}`)
  })
  .on('ready', () =>
    logger.info(
      `Initial scan complete. Ready for changes under folder ${rootPath}`,
    ),
  )
  .on('raw', (event, path, details) => {
    logger.debug('Raw event info:', event, path, details)
  })

watcher.on('change', (path, stats) => {
  if (stats) logger.debug(`File ${path} changed size to ${stats.size}`)
})
