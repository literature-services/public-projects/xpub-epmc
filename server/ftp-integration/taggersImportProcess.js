const { taggerImportMonitorLogger: logger } = require('../utils/loggers')

const path = require('path')
const fs = require('fs')
const tar = require('../utils/unTar.js')
const files = require('../utils/files.js')
const ftpUtils = require('../utils/ebi-ftp.js')
const uuidv4 = require('uuid/v4')
const config = require('config')
const {
  FileManager,
  ManuscriptManager,
  IdentityManager,
} = require('../xpub-model/')
const { transaction } = require('objection')
const BaseModel = require('@pubsweet/base-model')

const knex = BaseModel.knex()
const ftpTagger = config.get('ftp_tagger')
const { processedTaggerEmail, bulkUploaderEmail } = require('../email')

const parentRootPath = `${process.env.HOME}/${config.get('ftp_directory')}/${
  ftpTagger.username
}`

const errorFolder = `${parentRootPath}/Error`

module.exports.runProcess = async filePath => {
  logger.info(`Processing tagged XML package: ${filePath}`)
  const fileName = path.basename(filePath)
  const trx = await transaction.start(knex)
  const fileTypes = ['PMC', 'IMGsmall', 'IMGview', 'IMGprint', 'supplement_tag']
  let suffixedFilePath
  try {
    logger.info(`${fileName}: Extracting package`)
    if (fileName.includes(' ')) {
      const errMsg = `File name contains spaces: ${path.basename(filePath)}`
      logger.error()
      throw new Error(errMsg)
    }

    const user = await IdentityManager.findByFtpUsername(
      ftpTagger.username,
      trx,
    )
    const { userId } = user
    const tmpPath = await tar.createTempDir()
    let extractedFilePath
    try {
      extractedFilePath = await tar.untar(filePath, tmpPath)
    } catch (untarError) {
      logger.error(`Tagger Import: untar failed: ${filePath}`)
      logger.error(untarError)
      fs.unlink(filePath, unlinkerr => {
        if (unlinkerr) {
          logger.error(`Tagger Import: delete file failed: ${filePath}`)
          logger.error(unlinkerr)
        } else {
          logger.error(`Tagger Import: ${filePath} deleted`)
        }
      })
      // clean up tmp folder
      await files.tidyUp(tmpPath)
      throw untarError
    }

    suffixedFilePath = await files.renameFileSuffix(filePath)
    const manifestFilename = await files.getManifestFilename(extractedFilePath)
    const [manId, filesData] = await files.getManifestFileData(
      extractedFilePath,
      manifestFilename,
    )

    logger.info(`${fileName}: Getting manuscript and file info`)
    let version = ''
    let title = ''
    try {
      const man = await ManuscriptManager.findById(manId, userId, trx, false)
      ;({ version } = man)
      ;({ title } = man.meta)
    } catch (err) {
      throw new Error(`Submission ${manId} is no longer available for tagging`)
    }
    const filesArr = await files.checkFiles(filesData, version, tmpPath, userId)

    // delete current existing 'tagging' files
    logger.info(`${fileName}: Updating files in databases`)
    await fileTypes.reduce(async (promise, type) => {
      await promise
      return FileManager.deleteByManIdAndType(manId, type, userId, trx)
    }, Promise.resolve())

    let xmlFile = null
    // upload to file database
    await filesArr.reduce(async (promise, file) => {
      await promise
      const uuid = uuidv4()
      file.dbFileName = `${uuid}${file.extension}`
      await files.fileUpload(
        manId,
        version,
        file.dbFileName,
        file.filename,
        file.url,
        file.mimeType,
      )
      // create rows in 'file' table
      file.url = `/download/${file.dbFileName}`
      delete file.extension
      delete file.dbFileName
      if (file.type === 'PMC') {
        xmlFile = file.url
      }
      return FileManager.save(file, userId, trx)
    }, Promise.resolve())
    if (xmlFile) {
      await ManuscriptManager.update(
        { id: manId, pdfDepositState: 'VALIDATING_XML' },
        userId,
        trx,
      )
    } else {
      throw new Error(`${filePath}: No XML file!`)
    }
    await trx.commit()

    logger.info(`${fileName}: Sending success email and tidying.`)
    // send email to taggers
    processedTaggerEmail(ftpTagger.email, manId, title, fileName)
    // move file to dated folder
    const datedFilePath = await files.renameFile(suffixedFilePath)

    await updateFTP(datedFilePath)
    // clean up tmp folder
    await files.tidyUp(tmpPath)
    return true
  } catch (err) {
    if (trx) await trx.rollback()
    logger.error('Error with taggersImport process', err.message)
    bulkUploaderEmail(ftpTagger.email, err.message, fileName)
    const datedFilePath = await files.moveErroneousFile(
      `${filePath}.start`,
      errorFolder,
    )
    // update FTP - rename files accordingly
    await updateFTP(datedFilePath)
    return true
  }
}

const updateFTP = async datedFilePath => {
  try {
    const [datedFolder, processedFilename] = await files.getDirAndFilename(
      datedFilePath,
    )
    logger.info(`${processedFilename}: Updating FTP server.`)
    let folderName = 'Done'
    if (!datedFolder && datedFilePath.split('/').slice(-2)[0] === 'Error')
      folderName = 'Error'

    await ftpUtils.renameTaggerFtpPackage(
      ftpTagger.username,
      folderName,
      datedFolder,
      processedFilename,
    )
  } catch (err) {
    logger.error(`${datedFilePath}: Error with updating FTP`)
  }
}
