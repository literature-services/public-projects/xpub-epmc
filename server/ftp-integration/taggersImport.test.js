const taggersImportProcess = require('./taggersImportProcess')

// eslint-disable-next-line jest/no-disabled-tests
test.skip('taggersImport: End to End test', () =>
  expect(
    taggersImportProcess.runProcess(
      `/home/ftpdatadir/beta_plus_tagger/Done/EMS82936.tar.gz`,
    ),
  ).resolves.toBe(true))
