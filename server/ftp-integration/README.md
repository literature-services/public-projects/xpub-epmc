# FTP integration

## Directory monitoring utility
The monitoring utility server/ftp-integration/bulkUpload.js monitors a local folder for changes. 
The folder path is configured with an `ftp_root` configuration parameter, currently defined in the 
development/production.js as ftpdata under the user's home:
 
`ftp_directory: 'ftpdata'`

This location should be mapped to the FTP path in the corresponding container. 
The extract below is taken from docker-compose.yml:

```
volumes: 
    - "~/ftpdata/:/home/ftpusers/"
    - "~/passwd:/etc/pure-ftpd/passwd"
```

The utility runs on server startup as a pm2 job or manually by ```node server/ftp-integration/bulkUpload.js ```. 

## FTP container
The configuration for the the FTP container can be found in the docker-compose.yml. 
Basically, it maps the ftp location to a local folder in the hosting maching, like showing above. 

## FTP user creation
To create an FTP user we first need to open a bash in the ftp container:

`docker exec -it xpubepmc_ftpd_server_1 -- /bin/bash`

and then create the user (e.g. 'ieee'):

`pure-pw useradd ieee -u ftpuser -d /home/ftpusers/ieee -m`

We will be asked for the password (twice).
This will create user `ieee` under the default ftp user group `ftpuser` with a pertinent home directory in the ftp container. 
The container configuration maps this to a location on the application container so that it will be made available in the application.
The `-m` flag commits the changes.

More information, along with more commands for managing ftp users, can be found here: https://github.com/stilliard/docker-pure-ftpd and https://download.pureftpd.org/pure-ftpd/doc/README.Virtual-Users 

## Application user creation
The uploaded manuscripts should belong to PubSweet users. 
Therefore, we need to create them. This can be accomplished by 
using the application's user management web form.  
The newly created user will have an email for username. 
This should be associated with the ftp user created above.
This is accomplished by adding a special element, `ftpUserName`, to the database column `identity.meta`. 
For example if some row has already the value `{"publisher": "NPG"}`, it should become `{"publisher": "NPG", "ftpUserName": "npg"}`.
 
 ### FTP user for tagging
Part of the application is the construction of a tar package for every manuscript to be send 
to the taggers. We need to create an ftp account, like above, so that the packages will be made 
available to that location for the tagger to download from.

`pure-pw useradd taggers -m -u ftpuser -d /home/ftpusers/taggers`

The `ftp_tagger: 'taggers'` value (currently in `development.js`) should match this FTP username.

This is an one-of thing as we only have one tagger.
