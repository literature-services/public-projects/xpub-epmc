const logger = require('@pubsweet/logger')
const _ = require('lodash')
const axios = require('axios')

const MAX_IDS = 200

/**
 * Call PMC ID Converter api. Split the input IDs into chunks of 200.
 * Exceptions will propagate to the caller
 */
const callPmcIdConv = async (idType, ids) => {
  const convertedChunks = await Promise.all(
    _.chunk(ids.split(','), MAX_IDS) // array of ids
      .map(async id_chunk => {
        const url = `https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=europe_pmc_plus&email=plusdev-shared@ebi.ac.uk&format=json&ids=${id_chunk.join(
          ',',
        )}&idtype=${idType}&versions=no`
        logger.info('Getting ID matches')
        const response = await axios.get(url)
        return response.data.records
      }),
  )
  return convertedChunks.flat()
}

module.exports = { callPmcIdConv }
