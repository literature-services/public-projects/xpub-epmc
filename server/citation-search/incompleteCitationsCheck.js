const logger = require('@pubsweet/logger')
const config = require('config')
const fetch = require('node-fetch')
const moment = require('moment')
const Manuscript = require('../xpub-model/entities/manuscript/data-access')
const ManuscriptManager = require('../xpub-model/entities/manuscript/')
const { getAdminUser } = require('../utils/user')
const { token } = require('../utils/authentication')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const { getCitation } = require('./getCitation')
const {
  isCitationPartialReady,
  isCitationReady,
} = require('../xpub-model/entities/util')

const { baseUrl } = config.get('pubsweet-server')

if (!process.env.ENABLE_CRONJOB_INCOMPLETECITATIONSCHECK) {
  logger.info(
    'ENABLE_CRONJOB_INCOMPLETECITATIONSCHECK not defined. incompleteCitationsCheck cronjob exits.',
  )
  process.exit(0)
}

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError(
      'check-incomplete-citations',
      `Uncaught Exception thrown: ${err}`,
    )
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError(
      'check-incomplete-citations',
      `Unhandled Rejection: ${reason}`,
    )
    process.exit(1)
  })

let user = null
let headers = null
;(async () => {
  await checkJobStatus('check-incomplete-citations', async () => {
    const beforeUpdate = Date.now()
    logger.info(`Running incomplete citations check...`)
    user = await getAdminUser()
    const bearer = token.create(user)
    headers = new fetch.Headers({ Authorization: `Bearer ${bearer}` })
    await pubmedCheck()
    await crossRefCheck()
    logger.info(
      `Incomplete citations check was finished in ${Date.now() -
        beforeUpdate} ms`,
    )
  })
  await Manuscript.knex().destroy()
  process.exit(0)
})()

async function pubmedCheck() {
  const manuscripts = await Manuscript.query()
    .whereIn('status', ['xml-complete', 'published-ahead-of-print'])
    .whereJsonSupersetOf('meta,article_ids', [{ pubIdType: 'pmid' }])
    .whereNull('deleted')

  const pmids = manuscripts
    .map(
      manuscript =>
        manuscript['meta,articleIds'].filter(id => id.pubIdType === 'pmid')[0]
          .id,
    )
    .join(',')

  await checkPubmed(manuscripts, pmids)
}

async function checkPubmed(manuscripts, pmids) {
  const response = await fetch(
    `${baseUrl}/eutils/esummary?db=pubmed&id=${pmids}`,
    { headers },
  )
  const results = await response.json()
  return selectResult(results, manuscripts)
}

async function selectResult(results, manuscripts) {
  if (results.result && results.result.uids) {
    await results.result.uids.reduce(async (promise, thisUid) => {
      await promise
      const result = results.result[thisUid]
      const citationData = await getCitation(result, thisUid)

      const manuscript = manuscripts.find(m =>
        m['meta,articleIds'].find(
          aid => aid.id === thisUid && aid.pubIdType === 'pmid',
        ),
      )

      if (manuscript) {
        const citationDataMerged = {
          id: manuscript.id,
          meta: {
            articleIds:
              citationData.meta.articleIds || manuscript['meta,articleIds'],
            volume: citationData.meta.volume || manuscript['meta,volume'],
            issue: citationData.meta.issue || manuscript['meta,issue'],
            location: citationData.meta.location || {
              fpage:
                manuscript['meta,location'] &&
                manuscript['meta,location'].fpage,
              lpage:
                manuscript['meta,location'] &&
                manuscript['meta,location'].lpage,
              elocationId:
                manuscript['meta,location'] &&
                manuscript['meta,location'].elocationId,
            },
            citerefUrl:
              citationData.meta.citerefUrl || manuscript['meta,citerefUrl'],
            fulltextUrl:
              citationData.meta.fulltextUrl || manuscript['meta,fulltextUrl'],
            publicationDates:
              citationData.meta.publicationDates ||
              manuscript['meta,publicationDates'],
          },
        }
        const partialReady = isCitationPartialReady(citationDataMerged.meta)
        const fullReady = isCitationReady(citationDataMerged.meta)

        if (
          (manuscript.status === 'xml-complete' && partialReady) ||
          (manuscript.status === 'published-ahead-of-print' && fullReady)
        ) {
          citationDataMerged.status = 'repo-ready'
          citationDataMerged.pdfDepositState = 'ADDING_CITATION'
          if (manuscript.status === 'published-ahead-of-print') {
            citationDataMerged.aheadOfPrint = false
            // Merge incomming articles IDs with existing Ids.
            manuscript['meta,articleIds'].forEach(aidExisting => {
              if (
                !citationDataMerged.meta.articleIds.some(
                  aidIncomming =>
                    aidIncomming.pubIdType === aidExisting.pubIdType,
                )
              ) {
                citationDataMerged.meta.articleIds.push(aidExisting)
              }
            })
          }
          try {
            await ManuscriptManager.update(citationDataMerged, user.id)
            logger.info(
              `Updated incomplete citation for manuscript ${manuscript.id}`,
            )
          } catch (err) {
            logger.error(`${manuscript.id} unable to update citation`)
            logger.error(err)
            await ManuscriptManager.update(
              {
                id: manuscript.id,
                formState: `Problem with citation: ${err}`,
              },
              user.id,
            )
          }
        }
      }
    }, Promise.resolve())
  } else {
    logger.info('No citation results from PubMed')
  }
}

async function crossRefCheck() {
  const manuscripts = await Manuscript.query()
    .whereIn('status', ['xml-complete', 'published-ahead-of-print'])
    .whereJsonSupersetOf('meta,article_ids', [{ pubIdType: 'doi' }])
    .whereNull('deleted')

  const dois = manuscripts
    .map(
      manuscript =>
        manuscript['meta,articleIds'].filter(id => id.pubIdType === 'doi')[0]
          .id,
    )
    .map(doi => `doi:${doi}`)
    .join(',')

  if (dois) {
    await checkCrossRef(manuscripts, dois)
  } else {
    logger.info('No DOI(s) to check')
  }
}

// Calling crossref api for citation info
// TODO Can the rows=1000 be removed?
async function checkCrossRef(manuscripts, dois) {
  if (!dois) return false
  const response = await fetch(
    `${baseUrl}/crossref?filter=${dois}&select=DOI,URL,volume,page,issue,published-online,published-print,license&rows=1000`,
    { headers },
  )
  const results = await response.json()
  const { items } = results.message
  if (items.length > 0) {
    return updateCitationByCrossref(items, manuscripts)
  }
  logger.info('No citation results from CrossRef')
  return true
}

// Update manuscript citation info from crossref results
async function updateCitationByCrossref(items, manuscripts) {
  await items.reduce(async (promise, item) => {
    await promise
    const { DOI, URL, page } = item
    let { volume, issue } = item
    if (volume === 0 || volume === '0') {
      volume = null
    }
    if (issue === 0 || issue === '0') {
      issue = null
    }
    const manuscript = manuscripts.find(m =>
      m['meta,articleIds'].find(
        aid =>
          aid.pubIdType === 'doi' && aid.id.toLowerCase() === DOI.toLowerCase(),
      ),
    )
    const pages = page && page.split('-')

    let location = {
      fpage: pages ? pages[0] : null,
      lpage: pages ? pages.pop() : null,
      elocationId: page ? null : DOI.split('/').pop(),
    }
    if (!(location.fpage || location.lpage || location.elocationId)) {
      location = manuscript['meta,location']
    }
    const dates = {
      epub: item['published-online'],
      ppub: item['published-print'],
    }
    const publicationDates = Object.keys(dates).reduce((arr, curr) => {
      if (dates[curr]) {
        const [year, month, day] = dates[curr]['date-parts'][0]
        const date = {
          type: curr,
          jatsDate: {},
          date: `${moment.utc(
            `${year} ${month || 1} ${day || 1}`,
            'YYYY MM DD',
          )}`,
        }
        if (year) date.jatsDate.year = `${year}`
        if (month) date.jatsDate.month = `${month}`
        if (day) date.jatsDate.day = `${day}`
        arr.push(date)
      }
      return arr
    }, [])
    const meta = {
      articleIds: manuscript['meta,articleIds'],
      volume: volume || manuscript['meta,volume'],
      issue: issue || manuscript['meta,issue'],
      location,
      citerefUrl: URL || manuscript['meta,citerefUrl'],
      fulltextUrl: item.license ? URL : null,
      publicationDates: publicationDates || manuscript['meta,publicationDates'],
    }
    const partialReady = isCitationPartialReady(meta)
    const fullReady = isCitationReady(meta)
    if (manuscript) {
      if (
        (manuscript.status === 'xml-complete' && partialReady) ||
        (manuscript.status === 'published-ahead-of-print' && fullReady)
      )
        try {
          await ManuscriptManager.update(
            {
              id: manuscript.id,
              status: 'repo-ready',
              pdfDepositState: 'ADDING_CITATION',
              meta,
            },
            user.id,
          )
          logger.info(
            `Updated citation info from crossref for manuscript ${manuscript.id}`,
          )
        } catch (err) {
          logger.error(`${manuscript.id} unable to update citation`)
          logger.error(err)
          await ManuscriptManager.update(
            { id: manuscript.id, formState: `Problem with citation: ${err}` },
            user.id,
          )
        }
    }
  }, Promise.resolve())
}
