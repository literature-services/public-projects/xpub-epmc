const logger = require('@pubsweet/logger')
const Note = require('../xpub-model/entities/note')
const BaseModel = require('@pubsweet/base-model')
const getUser = require('../utils/user.js')
const { checkJobStatus, uncaughtError } = require('../job-runner')
const config = require('config')

const knex = BaseModel.knex()
const { transaction } = require('objection')
const httpUtil = require('../utils/http.js')

const { thorApiUrl: thorUrl } = config
const thorApiUrl = `${thorUrl}/api/orcid/findOrcidUpdates/20`

if (!process.env.ENABLE_CRONJOB_ORCIDUPDATE) {
  logger.info(
    'ENABLE_CRONJOB_ORCIDUPDATE not defined. orcid-update cronjob exits.',
  )
  process.exit(0)
}

;(async () => {
  await checkJobStatus('orcid-update', async () => {
    logger.info('Running ORCID updates.')
    const adminUser = await getUser.getAdminUser()
    updateOrcidWorks(adminUser.id).then(() =>
      logger.info(`Orcid verification is done`),
    )
  })
  teardownDBConnectionPool()
  process.exit(0)
})()

process
  .on('uncaughtException', async (err, origin) => {
    logger.error(`Uncaught Exception thrown: ${err}`)
    logger.error('Exception thrown at: ', origin)
    await uncaughtError('orcid-update', `Uncaught Exception thrown: ${err}`)
    teardownDBConnectionPool()
    process.exit(1)
  })
  .on('unhandledRejection', async (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    await uncaughtError('orcid-update', `Unhandled Rejection: ${reason}`)
    teardownDBConnectionPool()
    process.exit(1)
  })

async function teardownDBConnectionPool() {
  logger.info('orcid-update: closing connection to the database...')
  Note.knex && (await Note.knex().destroy())
}

function timeDiff(beforeUpdate) {
  return Math.floor((Date.now() - beforeUpdate) / 60000)
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

async function updateOrcidWorks(userId) {
  try {
    const beforeUpdate = Date.now()
    const orcidResJson = await getThorOrcidUpdates()
    const resultSet = JSON.parse(orcidResJson.text)

    let trx = null
    let commit = false
    if (!trx) {
      commit = true
      trx = await transaction.start(knex)
    }

    if (resultSet && resultSet.length > 0) {
      resultSet.forEach(async res => {
        const { orcId, works: orcidWorks } = res
        const noteDBResultSet = await Note.getOrcidNotes(
          orcId,
          'orcidClaim',
          trx,
        )
        if (noteDBResultSet) {
          const { noteId, manArticleIds, manuscriptId } = noteDBResultSet
          const doiDB = manArticleIds.find(
            el => el.pubIdType.toLowerCase() === 'doi',
          )

          if (orcidWorks) {
            const doiArr = orcidWorks.reduce((doiArr, obj) => {
              const { workExternalIdentifiers } = obj
              const doiObj = workExternalIdentifiers
                ? workExternalIdentifiers.filter(
                    id => id.workExternalIdentifierType.toLowerCase() === 'doi',
                  )
                : {}
              if (Object.keys(doiObj).length > 0) doiArr.push(doiObj)
              return doiArr
            }, [])

            // If any DOIs appear in the list retrieved from the DB AND do NOT appear in the profile works, delete that note
            const doiArray = doiArr.map(el => el[0].workExternalIdentifierId)
            const found = doiArray.find(doi => doi === doiDB.id)

            if (!found) {
              logger.info(
                `Deleting ORCID link note to ${manuscriptId} for orcId: ${orcId}`,
              )
              await Note.delete(noteId, userId, trx)
              if (commit) {
                await trx.commit()
              }
            }
          } else {
            // delete note retrieved from DB for profile without `works` section
            logger.info(
              `Deleting ORCID link note to ${manuscriptId} for orcId: ${orcId}`,
            )
            await Note.delete(noteId, userId, trx)
            if (commit) {
              await trx.commit()
            }
          }
        }
      })
    }
    await sleep(3000)
    logger.info(`Note table was updated in: ${timeDiff(beforeUpdate)} minutes`)
    logger.info('Orcid verification for Notes is completed.')
  } catch (e) {
    logger.error(e)
    throw e
  }
}

function getThorOrcidUpdates() {
  return new Promise((resolve, reject) => {
    httpUtil.get(thorApiUrl).end((err, res) => {
      if (err) {
        logger.error(err)
        reject(err)
      }
      resolve(res)
    })
  })
}
