# Overview

# Development notes
## Tasks
* partial citation and complete citation checking functions. 3 places to change.
  - [ ] There is a checkCitation function defined in ManuscriptManager (server/xpub-model/entities/manuscript/index.js). It will use the */idconv* api. This will be checked for all state transition.
    - Current implementation: Uses Manuscript db object as input. Has 3 checks: hasPmId, hasPprId, hasCitation.
      ```
      const idCheck = originalMan['meta,articleIds'] || [] // The original manuscript
      // hasPmid means 3 conditions satisfied at the same time:
      /// 1. has a valid pmid, 2. location has a valid value (any of fpage, lpage, elocationId), 3. either volume or issue is present.
      const hasPmid =
        idCheck.some(aid => aid.pubIdType === 'pmid') &&
        Object.values(originalMan['meta,location']).some(v => !!v) &&
        (originalMan['meta,volume'] || originalMan['meta,issue'])
      // hasPprId means 2 conditions satisfied at the same time:
      // 1. has a valid pprid 2. has a valid publicationDates, this could either be of a epub or a ppub type.
      const hasPprid =–
        idCheck.some(aid => aid.pubIdType === 'pprid') &&
        originalMan['meta,publicationDates']
      // hasCitation means
      // 1. has a valid volume
      // 2. any of fpage, lpage, elocationId
      // 3. either of epub or ppub
      // 4. citerefUrl
      const hasCitation =
        originalMan['meta,volume'] &&
        Object.values(originalMan['meta,location']).some(v => !!v) &&
        originalMan['meta,publicationDates'] &&
        originalMan['meta,citerefUrl']
      const citationReady = hasPmid || hasPprid || hasCitation
      ```
  - [ ] The citation completeness check is also in incompleteCitationsCheck.js (the automatic citation route). If the check passes, mss will try to set the article to *repo-ready* status.
    There are two checks implemented. One for pubmed search (using pmid) result, one for crossref search (using doi) result.
    - Uses the meta object.
    - pubmed search result completeness check, 2 conditions (and):
    ```
    1. any valid location value, fpage, lpage, elocationId. 
    2. volume or issue.
    ```
    - crossref search result completeness check:
    ```
    1. volume
    2. volume does not contains "ahead" (case insensitive)
    3. volume is not 0.
    It will assume the crossref search result will return pages information, and fpage/lpage could be derived. If pages info is not retured, then it will get the elocationId from DOI.
    publicationDates are also extracted from the response but is not used as a citation completeness condition.
    ```
  - [ ] In MetaEdit.jsx, when suplying the *change* callback function to CitationEdit, a simplyfied version of hasPmId, hasPprId, hasCitation has been implemented. If has || hasPprId || hasCitation, then the manuscript status will be set to setStatus('repo-ready').
    - Uses meta object.
    - Similar to the checks in Manuscript manager, has hasPmid, hasPprid, hasCitation checks. Either checks passes will be considered citation complete.
    - hasPmid: has a valid pmid and a valid location value (any of fpage, lpage, elocationId)
    - hasPprid: has a valid pprid and any valid publication date (ppub or epub)
    - hasCitation: has any publication dates (epub or ppub)

* embargo calculation needs to be changed to use epub.
  - [x] embargo.js
  This actually simplyfied the implementation.
* state transision handling
  The key statuses involved are *xml-complete* and *repo-ready*. *xml-complete* is the last stage of xml review, also called *needs citation*. If an article's xml is complete and citation is complete, it can be moved to repo-ready, which means waiting for the xml to be finilized/converted/sent to ebi repo.
  In most cases, trying to set an article to *xml-complete* or *repo-ready* is no difference. ManuscriptManger will check the citation completeness and switch to the appopriate status automatically.
  There are two methods in ManuscriptManger to handle state transision: review and update.
    - review route: through reviewManuscript mutation -> ManuscriptManager.review. Like operations in the ReviewFooter go through this route. Like in `repo-triage` status, click `retry repository submission`, or ___, ___, will send the article to *xml-complete*. 
    - update route: setStatus() -> updateManuscript mutation -> ManascriptManager.update route. like after an article is approved (Check ReviewFooter.jsx), it can be sent to *xml-complete*.
    In both ManuscriptManager.review/update, 
      - When transitting to *repo-ready* but mss finds citation is not complete, the article will be set to *xml-complete* status.
      - When transitting to *xml-complete* and mss finds citation is complete, the article will be set to *repo-ready*.
      - [x] update the review and update method of manuscript manager, when partial citation is matched, set the aheadOfPrint flag to true and continue the process as if the citation is complete. If the citation is complete, set the aheadOfPrint flag to false.
  - *published*, *published-ahead-of-print*
    - [x] update the update and review method, if the target status is published and the flag ahead of print is set, set the new status to "published-ahead-of-print"

* pdfDepositState
  - validating_xml
    This is used in the xml review stage.
  - converting_xml
    Where the convertion to nxml/html is carried out.
    - After *validating_xml*
    - After *adding_citation*
  - adding_citation
    this state is used for calling addCitation.xsl, add article citatin information and reference citation to the xml.
    - when incompleteCitationsJob succeeds, article will be set to *repo-ready + adding_citation*.
    - when processing manuscript ebi repo callback for complete or withdrawn.
    - when converting_xml finishes and the new status is *repo-ready* and the current status is not. If the current status is already *repo-ready*, do not go to *adding_citation* (used when recieving ebi repo callback with fulltext id).
    - updateManuscript mutation, when the new status is *repo-ready*
    - reviewManuscript mutation, when the new status is *repo-ready*

* xsl convertion
  - [x] set the *ahead-of-print* flag when calling the addCitations.xsl.
  - [x] update addCitations.xsl
    - [x] include <ahead-of-print> tag under <article-meta> only if the aheadOfPrint flag is true. Since <article-meta> will be regenerated each time, so there is no need to remove the existing <ahead-of-print> tag.

* a new status *published_ahead_of_print
  - [x] modify states.json. 
* A new state for ahead of print published, to identify whether the artile is in ahead of print or not.
  - [ ] modify database add column
    - [x] beta
    - [ ] prod
  - [x] create a knex migration script
  - [x] modify knex/object model to add the property.
  - [x] modify graphql model
  TODO do we need to let the user the have the ability to manually edit the ahead of print status? probably not.


## automatic citation (by incomplete citation check job)
incomplete citation check job retrieves citation information for articles with a valid pmid or doi.
For pmid, get citations from pubmed. For doi, from crossref.
If the jurnal is registered with pubmed, then pmid is needed.
when citation is complete, the article will be sent to *repo-ready* status automatically.

  - [x] check citation for ahead of print articles. This means for articles in "published-ahead-of-print" in addition to *xml-complete* status, incompleteCitationsCheck job will still run the check. 
  - [ ] If citations become complete or partial complete, set it to 'repo-ready' state. The ahead-of-print flag will be set in the ManuscriptManager.update.


## manual citation
If an article is approved, it will be put into /xml-complete/ state. If the citation is complete, mss will send to 
repo-ready status automatically. We can rely on this routes, only check the ahead of print condiction when arriving
at xml-complete status or repo-ready status.
The user can also manually supply the missing citation data, and then push the manuscript to *repo-ready* status manualy.
For articles in *published-ahead-of-print* status, if citation data is manually provided, it need to be moved to *xml-complete* or *repo-ready* status, to be included in the next round of xml conversion. TODO how about the pdfDepositState?

in MetaEdit.jsx, there is also a route for setStats('repo-ready'). Here after the citation is changed, if citation is complete it will go to *repo-ready* status automatically. This is not a good design, as state transision logic is implemented in the frontend.
  - [ ] modify MetaEdit, change the logic of hasCitation, to allow partial citation.
    - [ ] for hasPmid
    - [ ] for doi
    - [ ] check the processing for this case, if article is already in ahead of print, should we bring it back to *repo-ready*? Last time we discussed, we don't allow the case for bring it back to *repo-ready* if the citation becomes complete. But how about other citation changes? If it is still partial complete but the citatin is wrong?
  - [ ] refactor CitationEdit component to use graphql mutation
    - [ ] create a new graphql mutation citationEdit
    - [ ] modify CitationEdit componet to use the new mutation

## ui
  * Display ahead of print flag on the following places
    - [ ] manuscript page
    - [ ] activity page
    - [ ] list view

## xml-complete queue (need citation queue)
In admin dashboard, the needs citation queue collects all articles with status *xml-complete*.
It is further divided to 4 subqueues: PMID matching needed,DOI needed,Matched to DOIs,Matched to PMIDs.
*incomplate citation jobs* will only run the check for the later 2 subqueues, that's articles with either PMID or DOI.

# Other
* TODO check whether the processing of pubmed_status in needCitation queue is correct? Whether we need to consider the start/end date of pubmed subscription?
* TODO the citation completeness check criterion and its implementation needs to be thoroughly checked.
