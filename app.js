const logger = require('@pubsweet/logger')
const startServer = require('pubsweet-server')

startServer().catch(err => {
  logger.error('FATAL ERROR, SHUTTING DOWN:', err)
  process.exit(1)
})

process
  .on('uncaughtException', (err, origin) => {
    logger.error('Uncaught Exception thrown: ', err)
    logger.error('Exception thrown at: ', origin)
    process.exit(1)
  })
  .on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection: ', reason)
    logger.error('Rejected at promise: ', promise)
    process.exit(1)
  })

