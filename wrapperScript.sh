#!/bin/bash

echo "Running pre_init.sh script"
sh ./pre_init.sh

echo "Running pubsweet server in the background"
# cmd="pm2 start pm2.pubsweet.config.js"
cmd="node ./node_modules/pubsweet/src/startup/start.js"

$cmd &

echo "Wait forever for server to respond, check every 20 seconds"
while true
do
   RUNNING=$(curl --silent --connect-timeout 20 "http://localhost:$APP_SERVER_PORT" | grep "/assets/")
   if [ -n "$RUNNING" ] ; then
       echo "xPub is running"
       #echo "Creating the Users"
       #node scripts/adduser.js rakeshnambiar rakeshnbr@ebi.ac.uk Password_01 false
       echo "Running post_init.sh script"
       sh ./post_init.sh
       echo "Finished running post_init.sh"
       break
   fi
   echo "Waiting for xPub..."
   sleep 20
done

echo "Finished init."

tail -f /dev/null
